﻿using System;
using System.Threading.Tasks;

namespace UAU
{
	public interface ILoginManager
	{
		void ShowWelcomeNavigationPage();
		void ShowMainPage(DetailPageType pageType = DetailPageType.Feed);
		void ShowListPage(WishlistData wishlistData);
		void ShowLoginPage();
		Task Logout();
		void ShowLoadingPage();
	}
}

