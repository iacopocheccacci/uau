﻿using System;

namespace CustomRenderer
{
	public enum ListItemType
	{
		CaroselloItem = 0,
		BirthdayItem,
		SmallItem,
		LargeItem,
		RequestItem,
		CreateListItem
	}

	public interface IListItem
	{
		ListItemType ItemType	{ get; set; }
	}
}

