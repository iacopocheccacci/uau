﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace CustomRenderer
{
    public class DealViewCell : ViewCell
    {
        public static readonly BindableProperty NameProperty =
        BindableProperty.Create("Name", typeof(string), typeof(ItemViewCell), "");

        public string Name
        {
            get { return (string)GetValue(NameProperty); }
            set { SetValue(NameProperty, value); }
        }

        public static readonly BindableProperty ImageProperty =
        BindableProperty.Create("Image", typeof(string), typeof(ItemViewCell), "");

        public string Image
        {
            get { return (string)GetValue(ImageProperty); }
            set { SetValue(ImageProperty, value); }
        }

        public static readonly BindableProperty ShopImageProperty =
        BindableProperty.Create("ShopImage", typeof(string), typeof(ItemViewCell), "");

        public string ShopImage
        {
            get { return (string)GetValue(ShopImageProperty); }
            set { SetValue(ShopImageProperty, value); }
        }

        public static readonly BindableProperty PriceProperty =
        BindableProperty.Create("Name", typeof(string), typeof(ItemViewCell), "");

        public string Price
        {
            get { return (string)GetValue(PriceProperty); }
            set { SetValue(PriceProperty, value); }
        }
    }
}
