﻿using System;
using Xamarin.Forms;

namespace CustomRenderer
{
	public class ListViewCell : ViewCell
	{
		public static readonly BindableProperty NameProperty = 
			BindableProperty.Create("Name", typeof(string), typeof(ListViewCell), "");

		public string Name
		{
			get { return (string)GetValue (NameProperty); }
			set { SetValue (NameProperty, value); }
		}

		public static readonly BindableProperty WishesProperty = 
			BindableProperty.Create("Wishes", typeof(string), typeof(ListViewCell), "");

		public string Wishes
		{
			get { return (string)GetValue (WishesProperty); }
			set { SetValue (WishesProperty, value); }
		}

		public static readonly BindableProperty LockProperty =
			BindableProperty.Create("Lock", typeof(bool), typeof(ListViewCell), true);

		public bool Lock
		{
			get { return (bool)GetValue (LockProperty); }
			set { SetValue (LockProperty, value); }
		}

		public static readonly BindableProperty ExpireInLabelProperty =
			BindableProperty.Create("ExpireInLabel", typeof(string), typeof(ListViewCell), "");

		public string ExpireInLabel
		{
			get { return (string)GetValue (ExpireInLabelProperty); }
			set { SetValue (ExpireInLabelProperty, value); }
		}

		public static readonly BindableProperty DaysNumberProperty =
			BindableProperty.Create("DaysNumber", typeof(string), typeof(ListViewCell), "");

		public string DaysNumber
		{
			get { return (string)GetValue (DaysNumberProperty); }
			set { SetValue (DaysNumberProperty, value); }
		}

		public static readonly BindableProperty DaysLabelProperty =
			BindableProperty.Create("DaysLabel", typeof(string), typeof(ListViewCell), "");

		public string DaysLabel
		{
			get { return (string)GetValue (DaysLabelProperty); }
			set { SetValue (DaysLabelProperty, value); }
		}
	}
}

