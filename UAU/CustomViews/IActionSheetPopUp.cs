﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CustomRenderer
{
    public interface IActionSheetPopUp
    {
        void WhatsappClick();
        void MailClick();
    }
}
