﻿using System;
using Xamarin.Forms;

namespace CustomRenderer
{
	public class PickAGiftViewCell : ViewCell
	{
		public static readonly BindableProperty NameProperty = 
			BindableProperty.Create("Name", typeof(string), typeof(PickAGiftViewCell), "");

		public string Name
		{
			get { return (string)GetValue (NameProperty); }
			set { SetValue (NameProperty, value); }
		}

		public static readonly BindableProperty DetailsProperty = 
			BindableProperty.Create("Details", typeof(string), typeof(PickAGiftViewCell), "");

		public string Details
		{
			get { return (string)GetValue (DetailsProperty); }
			set { SetValue (DetailsProperty, value); }
		}

		public static readonly BindableProperty ImageFileNameProperty =
			BindableProperty.Create("ImageFileName", typeof(string), typeof(PickAGiftViewCell), "");

		public string ImageFileName
		{
			get { return (string)GetValue (ImageFileNameProperty); }
			set { SetValue (ImageFileNameProperty, value); }
		}
	}
}

