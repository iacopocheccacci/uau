﻿using System;
using System.Collections.Generic;
using System.Text;
using UAU;
using Xamarin.Forms;

namespace CustomRenderer
{
    public class CustomPage : ContentPage
    {
        public static readonly BindableProperty PageTypeProperty =
            BindableProperty.Create("PageType", typeof(string), typeof(CustomPage), "");

        public string PageType
        {
            get { return (string)GetValue(PageTypeProperty); }
            set { SetValue(PageTypeProperty, value); }
        }

        public static readonly BindableProperty ButtonProperty =
            BindableProperty.Create("Button", typeof(string), typeof(CustomPage), "");

        public string Button
        {
            get { return (string)GetValue(ButtonProperty); }
            set { SetValue(ButtonProperty, value); }
        }

		public static readonly BindableProperty BackButtonProperty =
			BindableProperty.Create("BackButton", typeof(string), typeof(CustomPage), "");

		public string BackButton
		{
			get { return (string)GetValue(BackButtonProperty); }
			set { SetValue(BackButtonProperty, value); }
		}

        public static readonly BindableProperty ItemSelectedProperty = BindableProperty.Create(nameof(ItemSelected), typeof(MyModel), typeof(CustomPage));
//       public static readonly BindableProperty ItemSelectedProperty =
//       BindableProperty.Create<CustomPage, MyModel>(p => p.ItemSelected, null);

        public MyModel ItemSelected
        {
            get { return (MyModel)GetValue(ItemSelectedProperty); }
            set { SetValue(ItemSelectedProperty, value); } // it would rise ElementPropertyChanged event
        }


        public CustomPage()
        {
            this.BindingContext = new CustomViewModel();
            this.SetBinding(CustomPage.TitleProperty, "ItemSelected");
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            string temp = this.Title;
            Title = String.Empty;
            Title = temp;
        }

        public virtual void OnCancelClick()
        {

        }

        public virtual void OnButtonClick()
        {

        }
    }
}
