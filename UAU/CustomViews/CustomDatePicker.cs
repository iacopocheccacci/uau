﻿using System;
using Xamarin.Forms;

namespace CustomRenderer
{
	public class CustomDatePicker : DatePicker
	{
		public const string NullableDatePropertyName = "NullableDate";
        public static readonly BindableProperty NullableDateProperty = BindableProperty.Create(nameof(NullableDate), typeof(DateTime?), typeof(CustomDatePicker), null, BindingMode.TwoWay, null, NullableDateChanged);
//		public static readonly BindableProperty NullableDateProperty = BindableProperty.Create<CustomDatePicker, DateTime?>(i => i.NullableDate, null, BindingMode.TwoWay, null, NullableDateChanged);

        public DateTime? NullableDate
		{
			get
			{
				return (DateTime?)this.GetValue(NullableDateProperty);
			}
			set
			{
				this.SetValue(NullableDateProperty, value);
			}
		}

		public const string NullTextPropertyName = "NullText";
        public static readonly BindableProperty NullTextProperty = BindableProperty.Create(nameof(NullText), typeof(string), typeof(CustomDatePicker), default(string), BindingMode.TwoWay);
//		public static readonly BindableProperty NullTextProperty = BindableProperty.Create<CustomDatePicker, string>(i => i.NullText, default(string), BindingMode.TwoWay);

        public string NullText
		{
			get
			{
				return (string)this.GetValue(NullTextProperty);
			}
			set
			{
				this.SetValue(NullTextProperty, value);
			}
		}

		public const string DisplayBorderPropertyName = "DisplayBorder";
        public static readonly BindableProperty DisplayBorderProperty = BindableProperty.Create(nameof(DisplayBorder), typeof(bool), typeof(CustomDatePicker), default(bool), BindingMode.TwoWay);
//		public static readonly BindableProperty DisplayBorderProperty = BindableProperty.Create<CustomDatePicker, bool>(i => i.DisplayBorder, default(bool), BindingMode.TwoWay);

        public bool DisplayBorder
		{
			get
			{
				return (bool)this.GetValue(DisplayBorderProperty);
			}
			set
			{
				this.SetValue(DisplayBorderProperty, value);
			}
		}
			
		public CustomDatePicker()
		{
			this.DateSelected += CustomDatePicker_DateSelected;

			this.Format = "dd/MM/yyyy";
		}

		void CustomDatePicker_DateSelected(object sender, DateChangedEventArgs e)
		{
			this.NullableDate = new DateTime(
				e.NewDate.Year, 
				e.NewDate.Month, 
				e.NewDate.Day, 
				this.NullableDate.HasValue ? this.NullableDate.Value.Hour : 0,
				this.NullableDate.HasValue ? this.NullableDate.Value.Minute : 0,
				this.NullableDate.HasValue ? this.NullableDate.Value.Second : 0);
		}

		private static void NullableDateChanged(BindableObject obj, object oldValue, object newValue)
		{
			var customDatePicker = obj as CustomDatePicker;

			if (customDatePicker != null)
			{
				if (((DateTime?)newValue).HasValue)
				{
					customDatePicker.Date = ((DateTime?)newValue).Value;
				}
			}
		}
	}
}

