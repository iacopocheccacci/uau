﻿using Xamarin.Forms;
using System.Collections.Generic;

namespace CustomRenderer
{
	public struct FBUserData
	{
		public string id				{ get; set; }
		public string first_name		{ get; set; }
		public string last_name			{ get; set; }
		public string birthday			{ get; set; }
		public string email				{ get; set; }
		public string gender			{ get; set; }
		public Friends friends			{ get; set; }
	}

	public struct Friends
	{
		public List<FriendData> data	{ get; set; }
	}

	public class AddFriendsRequest
	{
		public string owner_id			{ get; set; }
		public List<FriendData> friends	{ get; set; }
		public string cliqueName		{ get; set; }

		public AddFriendsRequest()
		{
			owner_id = string.Empty;
			friends = null;
			cliqueName = "contacts";  // default value means that this is the main friends circle
		}
	}

	public class FriendData
	{
		public string id				{ get; set; }
		public string name				{ get; set; }
	}

	public class FacebookLoginButton : Button
	{
	}
}

