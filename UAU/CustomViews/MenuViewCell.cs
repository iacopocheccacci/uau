﻿using System;
using Xamarin.Forms;

namespace CustomRenderer
{
	public class MenuViewCell : ViewCell
	{
		public static readonly BindableProperty NameProperty =
			BindableProperty.Create("Name", typeof(string), typeof(ItemViewCell), "");

		public string Name
		{
			get { return (string)GetValue(NameProperty); }
			set { SetValue(NameProperty, value); }
		}

		public static readonly BindableProperty ImageProperty =
			BindableProperty.Create("Image", typeof(string), typeof(ItemViewCell), "");

		public string Image
		{
			get { return (string)GetValue(ImageProperty); }
			set { SetValue(ImageProperty, value); }
		}

        public static readonly BindableProperty IndexProperty =
            BindableProperty.Create("Index", typeof(int), typeof(ItemViewCell), 0);

        public int Index
        {
            get { return (int)GetValue(IndexProperty); }
            set { SetValue(IndexProperty, value); }
        }

        //	public static readonly BindableProperty
    }
}

