﻿using System;
using Xamarin.Forms;

namespace CustomRenderer
{
	public class MyItemViewCell : ViewCell
	{
		public static readonly BindableProperty NameProperty = 
			BindableProperty.Create("Name", typeof(string), typeof(ItemViewCell), "");

		public string Name
		{
			get { return (string)GetValue (NameProperty); }
			set { SetValue (NameProperty, value); }
		}

		public static readonly BindableProperty DetailsProperty = 
			BindableProperty.Create("Details", typeof(string), typeof(ItemViewCell), "");

		public string Details
		{
			get { return (string)GetValue (DetailsProperty); }
			set { SetValue (DetailsProperty, value); }
		}

		public static readonly BindableProperty ImageProperty =
			BindableProperty.Create("Image", typeof(string), typeof(ItemViewCell), "");

		public string Image
		{
			get { return (string)GetValue (ImageProperty); }
			set { SetValue (ImageProperty, value); }
		}

		public static readonly BindableProperty IsNewProperty =
			BindableProperty.Create("IsNew", typeof(bool), typeof(ItemViewCell), true);

		public bool IsNew
		{
			get { return (bool)GetValue (IsNewProperty); }
			set { SetValue (IsNewProperty, value); }
		}
	}
}


