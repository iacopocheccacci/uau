﻿using System;
using Xamarin.Forms;

namespace CustomRenderer
{
	public class FriendsViewCell : ViewCell
	{
		public static readonly BindableProperty NameProperty = 
			BindableProperty.Create("Name", typeof(string), typeof(FriendsViewCell), "");

		public string Name
		{
			get { return (string)GetValue (NameProperty); }
			set { SetValue (NameProperty, value); }
		}

		public static readonly BindableProperty DetailsProperty = 
			BindableProperty.Create("Birthday", typeof(string), typeof(FriendsViewCell), "");

		public string Details
		{
			get { return (string)GetValue (DetailsProperty); }
			set { SetValue (DetailsProperty, value); }
		}

		public static readonly BindableProperty ImageFileNameProperty =
			BindableProperty.Create("ImageFileName", typeof(string), typeof(FriendsViewCell), "");

		public string ImageFileName
		{
			get { return (string)GetValue (ImageFileNameProperty); }
			set { SetValue (ImageFileNameProperty, value); }
		}

		public static readonly BindableProperty SwitchProperty =
			BindableProperty.Create("Switch", typeof(bool), typeof(FriendsViewCell), true);

		public bool Switch
		{
			get { return (bool)GetValue (SwitchProperty); }
			set { SetValue (SwitchProperty, value); }
		}
	}
}