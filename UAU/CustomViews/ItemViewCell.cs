﻿using System;
using Xamarin.Forms;

namespace CustomRenderer
{
	public enum ReservationType
	{
		Reservable = 0,
		ReservedByMe,
		ReservedByOther,
		ReservedByMeCollect,
		MyCollect,
		ActiveCollect
	};

	public class ItemViewCell : ViewCell
	{
		public static readonly BindableProperty NameProperty = 
			BindableProperty.Create("Name", typeof(string), typeof(ItemViewCell), "");

		public string Name
		{
			get { return (string)GetValue (NameProperty); }
			set { SetValue (NameProperty, value); }
		}

		public static readonly BindableProperty DetailsProperty = 
			BindableProperty.Create("Details", typeof(string), typeof(ItemViewCell), "");

		public string Details
		{
			get { return (string)GetValue (DetailsProperty); }
			set { SetValue (DetailsProperty, value); }
		}

		public static readonly BindableProperty ImageProperty =
			BindableProperty.Create("Image", typeof(string), typeof(ItemViewCell), "");

		public string Image
		{
			get { return (string)GetValue (ImageProperty); }
			set { SetValue (ImageProperty, value); }
		}

		public static readonly BindableProperty IsNewProperty =
			BindableProperty.Create("IsNew", typeof(bool), typeof(ItemViewCell), true);

		public bool IsNew
		{
			get { return (bool)GetValue (IsNewProperty); }
			set { SetValue (IsNewProperty, value); }
		}

		public static readonly BindableProperty ReservationProperty =
			BindableProperty.Create("Reservation", typeof(ReservationType), typeof(ItemViewCell), ReservationType.Reservable);

		public ReservationType Reservation
		{
			get { return (ReservationType)GetValue (ReservationProperty); }
			set { SetValue (ReservationProperty, value); }
		}

		public static readonly BindableProperty DaysNumberProperty =
			BindableProperty.Create("DaysNumber", typeof(string), typeof(ListViewCell), "");

		public string DaysNumber
		{
			get { return (string)GetValue (DaysNumberProperty); }
			set { SetValue (DaysNumberProperty, value); }
		}

		public static readonly BindableProperty DaysLabelProperty =
			BindableProperty.Create("DaysLabel", typeof(string), typeof(ListViewCell), "");

		public string DaysLabel
		{
			get { return (string)GetValue (DaysLabelProperty); }
			set { SetValue (DaysLabelProperty, value); }
		}

        public static readonly BindableProperty PhotoProperty =
            BindableProperty.Create("Photo", typeof(string), typeof(ItemViewCell), "");

        public string Photo
        {
            get { return (string)GetValue(PhotoProperty); }
            set { SetValue(PhotoProperty, value); }
        }

    }
}

