﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Xamarin.Forms;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Text;
using System.Net.Http;

namespace UAU
{
	public class Registration
	{
		public bool Success { get; set; }
	}

	public class FBRegistrationData
	{
		public string birthday = "10/01/1982";
		public string email = "iacopocheccacci@gmail.com";
		public string firstName = "Iacopo";
		public string lastName = "Prova";
		public string password = "password";
	}

	public partial class FacebookLoginPage : ContentPage
	{
		public FacebookLoginPage ()
		{
			InitializeComponent ();
		}

		void OnForgotPasswordTapped(object sender, EventArgs args)
		{
			Debug.WriteLine ("Tapped");
		}

		async void OnFacebookSignInClicked(object sender, EventArgs args)
		{
			FBRegistrationData item = new FBRegistrationData ();

			string response = await FBRegistration (item);

			Debug.WriteLine (response);

//			IEnumerable<Registration> result = await GetResult ("testoProva");	
//
//			if (result == null)
//			{
//				await DisplayAlert ("Ops!", "Unable to retrieve the data", "Cancel", "OK");
//			}
//			else
//			{
//				await DisplayAlert ("OK", result.ToString(), "Cancel", "OK");
//			}
		}

//		public async Task<List<Registration>> GetResult(string SearchString)
//		{
//			const string Url = "http://52.31.185.204/index.php/auth/custom_registration/";
//		
//			try
//			{
//				var client = new HttpClient ();
//				HttpContent content;
//
//				HttpResponseMessage response = await client.PostAsync(Url, content);
////				var json = await client.GetStringAsync (string.Format (Url, SearchString));
//
//
//				return JsonConvert.DeserializeObject<List<Registration>>(json.ToString());
//			}
//			catch (System.Exception e)
//			{
//				return null;
//			}
//		}

		public async Task<string> FBRegistration (FBRegistrationData item)
		{
			var client = new HttpClient ();
			var uri = new Uri (string.Format("http://52.31.185.204/index.php/auth/custom_registration/"));

			var json = JsonConvert.SerializeObject(item);
		
			var content = new StringContent(json, Encoding.UTF8, "application/x-www-form-urlencoded");

			HttpResponseMessage response = null;
			response = await client.PostAsync (uri, content);

			if (response.IsSuccessStatusCode)
			{
//				Debug.WriteLine (response.ToString());
			}
				
			return response.ToString ();
		}
	}
}

