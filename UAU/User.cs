﻿using System;
using CustomRenderer;

namespace UAU
{
	public class User
	{
		public string uau_id			{ get; set; }
		public string fb_uid			{ get; set; }
		public string firstName			{ get; set; }
		public string lastName			{ get; set; }
		public string sex				{ get; set; }
		public string birthday			{ get; set; }
		public string token				{ get; set; } 
		public string email				{ get; set; }
		public string password			{ get; set; }
		public string photo				{ get; set; }
		public string city				{ get; set; }
		public string country			{ get; set; }
		public Friends friends			{ get; set; }
		public string first_access		{ get; set; }
        public string active_lists      { get; set; }

		public User()
		{
			uau_id = string.Empty;
			fb_uid = string.Empty;
			firstName = string.Empty;
			lastName = string.Empty;
			sex = string.Empty;
			birthday = string.Empty;
			token = string.Empty;
			email = string.Empty;
			password = string.Empty;
			photo = string.Empty;
			city = string.Empty;
			country = string.Empty;
			first_access = string.Empty;
            active_lists = String.Empty;
		}

		public User(string fb_uid, 
					string name, 
					string surname, 
					string birthday, 
					string email,
					string gender, 
					string token,
					Friends friends)
		{
			this.uau_id = string.Empty;
			this.fb_uid = fb_uid;
			this.firstName = name;
			this.lastName = surname;
			this.birthday = birthday;
			this.email = email;
			this.password = string.Empty;
			this.sex = gender;
			this.token = token;
			this.photo = "https://graph.facebook.com/" + fb_uid + "/picture?type=large";
			city = string.Empty;
			country = string.Empty;
			first_access = string.Empty;
			this.friends = friends;
		}
	}

	public class UserEdit
	{
		public string uau_id			{ get; set; }
		public string firstName			{ get; set; }
		public string lastName			{ get; set; }
		public string sex				{ get; set; }
		public string birthday			{ get; set; }
		public string email				{ get; set; }
		public byte[] photo				{ get; set; }
		public string city				{ get; set; }
		public string country			{ get; set; }

		public UserEdit()
		{
			uau_id = string.Empty;
			firstName = string.Empty;
			lastName = string.Empty;
			sex = string.Empty;
			birthday = string.Empty;
			email = string.Empty;
			city = string.Empty;
			country = string.Empty;
			photo = null;
		}

		public UserEdit(string uau_id,
						string name, 
			            string surname, 
			            string birthday, 
			            string email,
			            string gender,
		                string city,
		                string country,
			            byte[] photo)
		{
			this.uau_id = uau_id;
			this.firstName = name;
			this.lastName = surname;
			this.birthday = birthday;
			this.email = email;
			this.sex = gender;
			this.photo = photo;
			this.city = city;
			this.country = country;
		}
	}
}

