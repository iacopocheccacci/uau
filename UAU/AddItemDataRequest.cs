﻿using System;

namespace UAU
{
	public class AddItemDataRequest
	{
		public string wishlist_id	{ get; set; }
		public string title			{ get; set; }
		public string description	{ get; set; }
		public string info			{ get; set; }
		public byte[] photo			{ get; set; }
		public string uau_id		{ get; set; }
		public string date_created	{ get; set; }

		public AddItemDataRequest ()
		{
			wishlist_id = string.Empty;
			title = string.Empty;
			description = string.Empty;
			info = string.Empty;
			photo = null;
			uau_id = string.Empty;
			date_created = string.Empty;
		}

		public AddItemDataRequest(string wishlist_id, string title, string description, string info, byte[] photo, string uau_id, string date_created)
		{
			this.wishlist_id = wishlist_id;
			this.title = title;
			this.description = description;
			this.info = info;
			this.photo = photo;
			this.uau_id = uau_id;
			this.date_created = date_created;
		}
	}

	public class AddItemStringDataRequest
	{
		public string wishlist_id	{ get; set; }
		public string title			{ get; set; }
		public string description	{ get; set; }
		public string info			{ get; set; }
		public string photo			{ get; set; }
		public string uau_id		{ get; set; }

		public AddItemStringDataRequest ()
		{
			wishlist_id = string.Empty;
			title = string.Empty;
			description = string.Empty;
			info = string.Empty;
			photo = string.Empty;
			uau_id = string.Empty;
		}

		public AddItemStringDataRequest(string wishlist_id, string title, string description, string info, string photo, string uau_id)
		{
			this.wishlist_id = wishlist_id;
			this.title = title;
			this.description = description;
			this.info = info;
			this.photo = photo;
			this.uau_id = uau_id;
		}
	}
}

