﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UAU
{
    public class CustomViewModel : ObservableObject
    {
        private MyModel _itemSelected;
        public MyModel ItemSelected { get { return _itemSelected; } set { SetProperty(ref _itemSelected, value); } }

        public CustomViewModel()
        {
            ItemSelected = new MyModel();
        }
    }
}
