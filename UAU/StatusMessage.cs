﻿using System;
using System.Collections.Generic;

namespace UAU
{
	public class StatusMessage
	{
		public string list_created			 	{ get; set; }
		public string wishList_admin_created	{ get; set; }
		public string list_err					{ get; set; }
		public string list_id					{ get; set; }
	}

	public class AddAdminStatusMessage
	{
		public List<Admin> s					{ get; set; }
	}

	public class Admin
	{
		public string id						{ get; set; }
		public string s							{ get; set; }
	}
}

