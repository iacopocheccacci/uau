﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using CustomRenderer;

namespace UAU
{
	// -------------------------------------------//
	// Data needed to user login
	// - email: user email
	// - password: user password
	//
	// Used in:
	// - LoginPage
	// - EnterPasswordPage
	// - LoadingPage
	// -------------------------------------------//
	public struct LoginRequestData
	{
		public string email			{ get; set; }
		public string password		{ get; set; }
	}

	// -------------------------------------------//
	// Data needed to request new user password
	// - email: user email
	//
	// Used in:
	// - LoginPage
	// -------------------------------------------//
	public struct ForgotPasswordRequestData
	{
		public string email			{ get; set; }
	}

	// -------------------------------------------//
	// Data needed to request app friends list
	// - user_id: current user id
	//
	// Used in:
	// - AddFriendsPage
	// -------------------------------------------//
	public struct UserFriendsRequestData
	{
		public string user_id 		{ get; set; }

		public UserFriendsRequestData(string uau_id)
		{
			user_id = uau_id;
		}
	}

	// -------------------------------------------//
	// Data used to contain friends data to 
	// display in a list view
	// - FullName: friend full name
	// - Id: friend app id
	// - Picture: friend profile photo
	// - IsEnabled: selection flag in the list view
	// - Details: details of friend wishlists
	//
	// Used in:
	// - AddFriendsPage
	// - PickAGiftPage
	// -------------------------------------------//
	public class FriendContactData
	{
		public string FullName 		{ get; set; }
		public string Id			{ get; set; }
		public string Picture		{ get; set; }
		public bool IsEnabled		{ get; set; }
		public string Birthday		{ get; set; }
		public string Details		{ get; set; }
        public string Email         { get; set; }
    }

	// -------------------------------------------//
	// Data needed to request a password change
	// - email: current user email
	// - password: new password
	// - confirm_password: new password
	//
	// Used in:
	// - ChangePasswordForm
	// -------------------------------------------//
	public struct ChangePasswordRequestData
	{
		public string email				{ get; set; }
		public string password			{ get; set; }
		public string confirm_password	{ get; set; }
	}

	// -------------------------------------------//
	// Data needed to request a user id
	// - email: requested user email
	//
	// Used in:
	// - StaticHttpClient
	// -------------------------------------------//
	public struct UserIdRequestData
	{
		public string email			{ get; set; }
	}

	// -------------------------------------------//
	// Data used to contain contacts data to 
	// display in a list view
	// - UserId: contact user id
	// - DisplayName: contact full name
	// - IsEnabled: selection flag in the list view
	// - Email: contact email
	//
	// Used in:
	// - ConnectWithFriendsPage
	// -------------------------------------------//
	public class ContactData
	{
		public string UserId 			{ get; set; }
		public string DisplayName 		{ get; set; }
		public bool IsEnabled 			{ get; set; }
		public string Email 			{ get; set; }
	}

	// -------------------------------------------//
	// Data used to contain feed data to 
	// display in a list view
	// - EventTitle: event name
	// - ImageName: name of the feed image to display
	// - EventText: feed verbose text
	// - EventTime: feed generation DateTime
	//
	// Used in:
	// - FeedPage
	// -------------------------------------------//
	public class FeedLayoutData : IListItem
	{
		internal string EventTitle;

		public string ImageName 									{ get; set; }
		public string FeedText 										{ get; set; }
		public string FeedTime 										{ get; set; }
		public string FriendId										{ get; set; }
		public ItemDataFeed ItemFeed								{ get; set; }
//		public ObservableCollection<FriendItems> FriendsItems		{ get; set; }
		public ListItemType ItemType 								{ get; set; }
		public string FeedColor										{ get; set; } 
		public string TextColor										{ get; set; }
		public string ImageURL										{ get; set; }
		public bool GoToListBtn										{ get; set; }
		public bool GoToWishBtn										{ get; set; }

		public ItemDataFeed ItemFeed1								{ get; set; }
		public ItemDataFeed ItemFeed2								{ get; set; }
		public ItemDataFeed ItemFeed3								{ get; set; }
		public string FriendId1										{ get; set; }
		public string FriendId2										{ get; set; }
		public string FriendId3										{ get; set; }
		public string UserPhoto1									{ get; set; }
		public string UserPhoto2									{ get; set; }
		public string UserPhoto3									{ get; set; }
		public string UserFirstName1								{ get; set; }
		public string UserFirstName2								{ get; set; }
		public string UserFirstName3								{ get; set; }
		public string UserLastName1									{ get; set; }
		public string UserLastName2									{ get; set; }
		public string UserLastName3									{ get; set; }
		public bool Box1Visibility									{ get; set; }
		public bool Box2Visibility									{ get; set; }
		public bool Box3Visibility									{ get; set; }
		public string ImageURL1										{ get; set; }
		public string ImageURL2										{ get; set; }
		public string ImageURL3										{ get; set; }
	}

	public struct ItemDataFeed
	{
		public string WishlistId									{ get; set; }
		public string ItemId										{ get; set; }
		public string UserPhoto										{ get; set; }
		public string UserId										{ get; set; }
		public string FirstName										{ get; set; }
		public string LastName										{ get; set; }
	}

	// -------------------------------------------//
	// Data used to contain circle data to 
	// display in a list view
	// - Name: circle name
	// - CircleId: circle id
	//
	// Used in:
	// - LoadCirclePage
	// -------------------------------------------//
	public class CircleData
	{
		public string Name 			{ get; set; }
		public string CircleId		{ get; set; }
	}


	// -------------------------------------------//
	// Data needed to request circles list
	// - user_id: current user id
	//
	// Used in:
	// - LoadCirclePage
	// -------------------------------------------//
	public struct UserCirclesRequestData
	{
		public string user_id 		{ get; set; }

		public UserCirclesRequestData(string uau_id)
		{
			user_id = uau_id;
		}
	}


	// -------------------------------------------//
	// Data needed to request product data from
	// web
	// - url: url from where to retrieve data
	//
	// Used in:
	// - AddWishPage
	// -------------------------------------------//
	public struct GetProductRequestData
	{
		public string url 		{ get; set; }

		public GetProductRequestData(string url)
		{
			this.url = url;

			if (url.Contains("amazon."))
			{
				this.url += "&is_mobile=true";
			}
		}
	}


	// -------------------------------------------//
	// Data needed to add an admin to a wishlist
	// - wishListId: wishlist id
	// - friends: list of new admins
	// - is_editing: true if is in editing mode
	//
	// Used in:
	// - AddListAdminPage
	// -------------------------------------------//
	public class AddAdminToListRequest
	{
		public string wishListId 			{ get; set; }
		public List<FriendData> friends		{ get; set; }
		public bool is_editing				{ get; set; }

		public AddAdminToListRequest()
		{
			wishListId = string.Empty;
			friends = null;
			is_editing = true;
		}

		public AddAdminToListRequest(string wishListId, List<FriendData> friends, bool is_editing = true)
		{
			this.wishListId = wishListId;
			this.friends = friends;
			this.is_editing = is_editing;
		}
	}


	// -------------------------------------------//
	// Data needed to get wishlist group number
	// - wishlist_id: wishlist id
	//
	// Used in:
	// - ListPage
	// -------------------------------------------//
	public class GetWishlistGroupNumberRequest
	{
		public string wishlist_id		{ get; set; }

		public GetWishlistGroupNumberRequest(string wishlist_id)
		{
			this.wishlist_id = wishlist_id;
		}
	}


	// -------------------------------------------//
	// Data needed to import items from a list to
	// another list
	// - fromList: original wishlist id
	// - toList: destination wishlist id
	//
	// Used in:
	// - AddWishPage
	// -------------------------------------------//
	public class ImportListRequest
	{
		public string fromList		{ get; set; }
		public string toList		{ get; set; }

		public ImportListRequest(string fromList, string toList)
		{
			this.fromList = fromList;
			this.toList = toList;
		}
	}

	// -------------------------------------------//
	// Data needed to get all users in a circle
	// - owner_id: id of the owner of the circle
	// - clique_id: id of the circle
	//
	// Used in:
	// - LoadCirclePage
	// -------------------------------------------//
	public class GetCliqueFriendsRequest
	{
		public string owner_id		{ get; set; }
		public string clique_id		{ get; set; }

		public GetCliqueFriendsRequest(string owner_id, string clique_id)
		{
			this.owner_id = owner_id;
			this.clique_id = clique_id;
		}
	}

	// -------------------------------------------//
	// Data needed to get all lists of a friend 
	// that are visible to user
	// - friend_id: id of the friend
	// - user_id: id of the user
	//
	// Used in:
	// - PickAGiftPage
	// -------------------------------------------//
	public class GetSharedListsRequest
	{
		public string friend_id		{ get; set; }
		public string user_id		{ get; set; }

		public GetSharedListsRequest(string friend_id, string user_id)
		{
			this.friend_id = friend_id;
			this.user_id = user_id;
		}
	}


	// -------------------------------------------//
	// Data needed to check if an item is reserved 
	// - item_id: id of the item
	// - uau_id: id of the user
	//
	// Used in:
	// - FriendListDetailPage
	// -------------------------------------------//
	public class CheckReservationRequest
	{
		public string item_id		{ get; set; }
		public string uau_id		{ get; set; }

		public CheckReservationRequest(string uau_id, string item_id)
		{
			this.item_id = item_id;
			this.uau_id = uau_id;
		}
	}


	// -------------------------------------------//
	// Data needed to reserve an item 
	// - list_id: id of the wishlist
	// - uau_id: id of the user
	// - item_id: id of the item
	// - expiration_date: reservation expiration date
	// - receiver_id: id of friend that make wishlist
	// - type: reservation type (standard or collection)
	// - partecipants: number of partecipants in collection case
	//
	// Used in:
	// - FriendListDetailPage
	// -------------------------------------------//
	public class ReservationRequest
	{
		public string list_id							{ get; set; }
		public string uau_id							{ get; set; }
		public string item_id							{ get; set; }
		public string expiration_date					{ get; set; }
		public string receiver_id						{ get; set; }
		public string type								{ get; set; }
		public string partecipants						{ get; set; }
		public string part_amount						{ get; set; }
		public List<UserReservationData> kitty_friends	{ get; set; }

		public ReservationRequest(string list_id, string uau_id, string item_id, string expiration_date, string receiver_id, string type, string partecipants, string part_amount)
		{
			this.list_id = list_id;
			this.uau_id = uau_id;
			this.item_id = item_id;
			this.expiration_date = expiration_date;
			this.receiver_id = receiver_id;
			this.type = type;
			this.partecipants = partecipants;
			this.part_amount = part_amount;
			this.kitty_friends = new List<UserReservationData>();
		}
	}

	public class UserReservationData
	{
		public string uau_id					{ get; set; }
	}


	// -------------------------------------------//
	// Data needed to get deal from amazon 
	// - lang: language
	// - keyword: keyword to search
	//
	// Used in:
	// - WishDetailPage
	// -------------------------------------------//
	public class AmazonMarketPlaceProductsRequest
	{
		public string lang		{ get; set; }
		public string keyword	{ get; set; }

		public AmazonMarketPlaceProductsRequest(string lang, string keyword)
		{
			this.lang = lang;
			this.keyword = keyword;
		}
	}


	// -------------------------------------------//
	// Data needed to delete a friend from friend 
	// list
	// - uau_id: friend id
	// - owner_id: current user id
	//
	// Used in:
	// - PickAGift
	// -------------------------------------------//
	public class DeleteUserFromFriendsRequest
	{
		public string uau_id		 { get; set; }
		public string owner_id		 { get; set; }

		public DeleteUserFromFriendsRequest (string uau_id, string owner_id)
		{
			this.uau_id = uau_id;
			this.owner_id = owner_id;
		}
	}


	// -------------------------------------------//
	// Data needed to delete a reservation 
	// - uau_id: current user id
	// - item_id; id of the item to unreserve
	//
	// Used in:
	// - WishDetailPage
	// -------------------------------------------//
	public class DeleteReservationRequest
	{
		public string uau_id		 { get; set; }
		public string item_id		 { get; set; }

		public DeleteReservationRequest (string uau_id, string item_id)
		{
			this.uau_id = uau_id;
			this.item_id = item_id;
		}
	}


	// -------------------------------------------//
	// Data needed to edit a collection 
	// - uau_id: current user id
	// - item_id; id of the item
	// - expiration_date: expiration date of the reservation
	// - partecipants: number of partecipants to the collection
	// - part_amount: amount of partecipants part
	//
	// Used in:
	// - KittyAdminPage
	// -------------------------------------------//
	public class EditCollectionRequest
	{
		public string uau_id		 	{ get; set; }
		public string item_id		 	{ get; set; }
		public string expiration_date	{ get; set; }
		public string partecipants		{ get; set; }
		public string part_amount		{ get; set; }

		public EditCollectionRequest (string uau_id, string item_id, string expiration_date, string partecipants, string part_amount)
		{
			this.uau_id = uau_id;
			this.item_id = item_id;
			this.expiration_date = expiration_date;
			this.partecipants = partecipants;
			this.part_amount = part_amount;
		}
	}


	// -------------------------------------------//
	// Data needed to get partecipants to a collect 
	// - item_id; id of the item
	//
	// Used in:
	// - KittyAdminPage
	// -------------------------------------------//
	public class getUsersCollectionInfoRequest
	{
		public string item_id		 	{ get; set; }

		public getUsersCollectionInfoRequest (string item_id)
		{
			this.item_id = item_id;
		}
	}


	// -------------------------------------------//
	// Data needed to get friends number 
	// - FriendsNr: number of friends
	//
	// Used in:
	// - ProfilePage
	// -------------------------------------------//
	public class FriendsNumber
	{
		public string FriendsNr		 	{ get; set; }

		public FriendsNumber (string friendsNr)
		{
			this.FriendsNr = friendsNr;
		}
	}


	// -------------------------------------------//
	// Data needed to remove user from clique
	// - uau_id: friend id
	// - clique_id: circle id
	//
	// Used in:
	// - CircleFriendsPage
	// -------------------------------------------//
	public class DeleteUserFromCiqueRequest
	{
		public string uau_id		 	{ get; set; }
		public string clique_id			{ get; set; }

		public DeleteUserFromCiqueRequest (string uau_id, string clique_id)
		{
			this.uau_id = uau_id;
			this.clique_id = clique_id;
		}
	}


	// -------------------------------------------//
	// Data needed to add user to clique
	// - clique_id: circle id
	// - cliqueName: name of the circle
	// - uau_id: friend id
	// - owner_id: id of the circle owner
	//
	// Used in:
	// - CircleFriendsPage
	// -------------------------------------------//
	public class AddUserToCiqueRequest
	{
		public string clique_id			{ get; set; }
		public string cliqueName		{ get; set; }
		public string uau_id		 	{ get; set; }
		public string owner_id			{ get; set; }

		public AddUserToCiqueRequest (string clique_id, string cliqueName, string uau_id, string owner_id)
		{
			this.clique_id = clique_id;
			this.cliqueName = cliqueName;
			this.uau_id = uau_id;
			this.owner_id = owner_id;
		}
	}


	// -------------------------------------------//
	// Data needed to delete a clique
	// - clique_id: circle id
	//
	// Used in:
	// - MyCirclePage
	// -------------------------------------------//
	public class DeleteCiqueRequest
	{
		public string clique_id			{ get; set; }

		public DeleteCiqueRequest (string clique_id)
		{
			this.clique_id = clique_id;
		}
	}

    // -------------------------------------------//
    // Data need to get the friends added to a wishlist
    // - wishlist_id = wishlist_id;
    // - owner_id = owner_id;
    //
    // Used in:
    // - AddFriendsPage
    // -------------------------------------------//
    public class GetWishListFriendsRequest
    {
        public string wishlist_id      { get; set; }
        public string owner_id         { get; set; }

        public GetWishListFriendsRequest (string wishlist_id, string owner_id)
        {
            this.wishlist_id = wishlist_id;
            this.owner_id = owner_id;
        }
    }


	// -------------------------------------------//
	// Data need to remove friend from a wishlist
	// - uau_id: id of the user to remove from list
	// - wishlist_id: id of the wishlist from whom remove friend
	//
	// Used in:
	// - ShowFriendsInListPage
	// -------------------------------------------//
	public class DeleteUserFromWishlistRequest
	{
		public string uau_id         { get; set; }
		public string wishlist_id      { get; set; }

		public DeleteUserFromWishlistRequest (string uau_id, string wishlist_id)
		{
			this.uau_id = uau_id;
			this.wishlist_id = wishlist_id;
		}
	}


	// -------------------------------------------//
	// Data need to get user by id
	// - uau_id: id of the user
	//
	// Used in:
	// - FeedPage
	// -------------------------------------------//
	public class GetUserByIdRequest
	{
		public string uau_id         { get; set; }

		public GetUserByIdRequest (string uau_id)
		{
			this.uau_id = uau_id;
		}
	}


	// -------------------------------------------//
	// Data need to get wishlist item details
	// - list_id: id of the wishlist
	// - item_id: id of the item
	//
	// Used in:
	// - FeedPage
	// -------------------------------------------//
	public class GetWishlistItemDetailRequest
	{
		public string list_id         { get; set; }
		public string item_id		  { get; set; }

		public GetWishlistItemDetailRequest (string list_id, string item_id)
		{
			this.list_id = list_id;
			this.item_id = item_id;
		}
	}


	// -------------------------------------------//
	// Data need to get wishlist details
	// - wishlist_id: id of the wishlist
	//
	// Used in:
	// - FeedPage
	// -------------------------------------------//
	public class GetWishlistInfoRequest
	{
		public string wishlist_id         { get; set; }

		public GetWishlistInfoRequest (string wishlist_id)
		{
			this.wishlist_id = wishlist_id;
		}
	}


	// -------------------------------------------//
	// Data need to delete item from list
	// - item_id: id of the item to delete
	// - wishlist_id: id of the item wishlist
	// - uau_id: id of the user that deleted the item
	//
	// Used in:
	// - ListPage
	// -------------------------------------------//
	public class DeleteItemRequest
	{
		public string item_id			{ get; set; }
		public string wishlist_id       { get; set; }
		public string uau_id			{ get; set; }

		public DeleteItemRequest (string item_id, string wishlist_id, string uau_id)
		{
			this.item_id = item_id;
			this.wishlist_id = wishlist_id;
			this.uau_id = uau_id;
		}
	}

	// -------------------------------------------//
	// Data need to request incoming birthdays
	// - uau_id: id of the current user
	//
	// Used in:
	// - FeedPage
	// -------------------------------------------//
	public class GetFriendsBirthdayRequest
	{
		public string uau_id			{ get; set; }

		public GetFriendsBirthdayRequest (string uau_id)
		{
			this.uau_id = uau_id;
		}
	}
}

