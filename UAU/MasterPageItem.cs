﻿using System;

namespace UAU
{
	public class MasterPageItem
	{
		public string Title { get; set; }

		public string IconSource { get; set; }

		public Type TargetType { get; set; }

        public int Index { get; set; }
	}
}

