﻿using System;
using CustomRenderer;
using Xamarin.Forms;

namespace UAU
{
	public class NewsItemTemplateSelector : DataTemplateSelector
	{
		public DataTemplate CreateListTemplate		{ get; set; }
		public DataTemplate BirthdayTemplate		{ get; set; }
		public DataTemplate CaroselloTemplate		{ get; set; }
		public DataTemplate SmallTemplate			{ get; set; }
		public DataTemplate LargeTemplate			{ get; set; }
		public DataTemplate RequestTemplate			{ get; set; }

		public override DataTemplate SelectTemplate (object item, BindableObject container)
		{
			// must have IListItems
			var li = (IListItem)item;
			DataTemplate template;

			switch(li.ItemType)
			{
				case ListItemType.CreateListItem:
					template = CreateListTemplate;
					break;

				case ListItemType.BirthdayItem:
					template = BirthdayTemplate;
					break;

				case ListItemType.CaroselloItem:
					template = CaroselloTemplate;
					break;

				case ListItemType.SmallItem:
					template = SmallTemplate;
					break;

				case ListItemType.LargeItem:
					template = LargeTemplate;
					break;

				case ListItemType.RequestItem:
					template = RequestTemplate;
					break;

				default:
					template = SmallTemplate;
					break;
			}

			return template;
		}
	}
}

