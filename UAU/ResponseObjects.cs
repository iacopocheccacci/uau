﻿using System;
using System.Collections.Generic;
using CustomRenderer;

namespace UAU
{
	// -------------------------------------------//
	// Response received after a customRegistration
	// request
	// - email: echo for user email
	// - pwd: echo for user password (encrypted)
	// - lastName: echo for user last name
	// - firstName: echo for user first name
	// - birthday: echo for user birthday
	// - sex: echo for user gender
	// - country: echo for user country
	// - city: echo for user city
	// - email_err: (“”, “Email already registered”, “Invalid email”)
	// - birthday_err: (“Invalid birthday date, Only over 14 years old users can sign up”, “Invalid birthday”)
	// - upload_err: ("Something went wrong during the image upload. Allowed only JPEG, JPG AND PNG images less then 2 MB.”,
	//				  "Invalid image format. Allowed only jpeg, jpg, png image", 
	//				  "Invalid image size. Allowed only image less then 2 MB”)
	// - pwd_err: “Password required”
	// - name_err: “Invalid firstname”
	// - last_err: “Invalid lastname”
	// - country_err: “Invalid country”
	// - city_err: “Invalid city”
	// 
	// Used in:
	// - CustomRegistrationPage
	// -------------------------------------------//
	public class CustomRegistrationResponse : ResponseObject
	{
		public string email			{ get; set; }
		public string pwd			{ get; set; }
		public string lastName		{ get; set; }
		public string firstName		{ get; set; }
		public string birthday		{ get; set; }
		public string sex			{ get; set; }
		public string country		{ get; set; }
		public string city			{ get; set; }

		// error logs
		public string email_err		{ get; set; }
		public string birthday_err	{ get; set; }
		public string upload_err	{ get; set; }
		public string pwd_err		{ get; set; }
		public string name_err		{ get; set; }
		public string last_err		{ get; set; }
		public string country_err	{ get; set; }
		public string city_err		{ get; set; }

		public CustomRegistrationResponse() : base()
		{
			email_err = string.Empty;
			birthday_err = string.Empty;
			upload_err = string.Empty;
			pwd_err = string.Empty;
			name_err = string.Empty;
			last_err = string.Empty;
			country_err = string.Empty;
			city_err = string.Empty;
		}

		public string GetErrorMessage()
		{
			string response = string.Empty;

			if (email_err != string.Empty)
			{
				response = email_err;
			}
			else if (birthday_err != string.Empty)
			{
				response = birthday_err;
			}
			else if (upload_err != string.Empty)
			{
				response = upload_err;
			}
			else if (pwd_err != string.Empty)
			{
				response = pwd_err;
			}
			else if (name_err != string.Empty)
			{
				response = name_err;
			}
			else if (last_err != string.Empty)
			{
				response = last_err;
			}
			else if (country_err != string.Empty)
			{
				response = country_err;
			}
			else if (city_err != string.Empty)
			{
				response = city_err;
			}

			return response;
		}
	}

	// -------------------------------------------//
	// Response received after a fbRegistration
	// request
	// - user: echo for all user data
	// - error: “invalid request” ( no email and no token)
	// - email_err: “email already registered"
	// - birthday_err: “Invalid birthday date, Only over 14 years old users can sign up”
	//
	// Used in:
	// - FacebookLoginButtonRendererIos
	// -------------------------------------------//
	public class FBLoginResponse : ResponseObject
	{
		public User user			{ get; set; }

		// error logs
		public string error			{ get; set; }
		public string email_err		{ get; set; }
		public string birthday_err	{ get; set; }

		public FBLoginResponse() : base()
		{
			user = null;
			error = string.Empty;
			email_err = string.Empty;
			birthday_err = string.Empty;
		}

		public string GetErrorMessage()
		{
			string response = string.Empty;

			if (error != string.Empty)
			{
				response = error;
			}
			else if (email_err != string.Empty)
			{
				response = email_err;
			}
			else if (birthday_err != string.Empty)
			{
				response = birthday_err;
			}

			return response;
		}
	}


	// -------------------------------------------//
	// Response received after a login request
	// - user: echo for all user data
	// - email: echo for user email
	// - pwd: echo for user password
	// - active_err: “Your account it’s not active yet, please check the activation email out”
	// - email_err: (“”, “Invalid email”)
	// - pwd_err: (“”, “Invalid password”, “Password required”)
	//
	// Used in:
	// - LoginPage
	// - LoadingPage
	// - EnterPasswordForm
	// -------------------------------------------//
	public class CustomLoginResponse : ResponseObject
	{
		public User user			{ get; set; }
		public string email			{ get; set; }
		public string pwd			{ get; set; }

		// error logs
		public string active_err	{ get; set; }
		public string email_err		{ get; set; }
		public string pwd_err		{ get; set; }

		public CustomLoginResponse() : base()
		{
			user = null;
			email = string.Empty;
			pwd = string.Empty;
			active_err = string.Empty;
			email_err = string.Empty;
			pwd_err = string.Empty;
		}

		public string GetErrorMessage()
		{
			string response = string.Empty;

			if (active_err != string.Empty)
			{
				response = active_err;
			}
			else if (email_err != string.Empty)
			{
				response = email_err;
			}
			else if (pwd_err != string.Empty)
			{
				response = pwd_err;
			}

			return response;
		}
	}

	// -------------------------------------------//
	// Response received after a forgotPassword
	// request
	// - email_err: (“”, “Email doesn’t exists”, “Invalid email”)
	//
	// Used in:
	// - LoginPage
	// -------------------------------------------//
	public class ForgotPasswordResponse : ResponseObject
	{
		public string email_err		{ get; set; }

		public ForgotPasswordResponse() : base()
		{
			email_err = string.Empty;
		}

		public string GetErrorMessage()
		{
			string response = string.Empty;

			if (email_err != string.Empty)
			{
				response = email_err;
			}

			return response;
		}
	}


	// -------------------------------------------//
	// Response received after a changePassword
	// request
	// - pwd_err: “Password mismatch”
	//
	// Used in:
	// - ChangePasswordForm
	// -------------------------------------------//
	public class ChangePasswordResponse : ResponseObject
	{
		public string pwd_err		{ get; set; }

		public ChangePasswordResponse() : base()
		{
			pwd_err = string.Empty;
		}

		public string GetErrorMessage()
		{
			string response = string.Empty;

			if (pwd_err != string.Empty)
			{
				response = pwd_err;
			}

			return response;
		}
	}


	// -------------------------------------------//
	// Response received after a getUserByEmail
	// request
	// - uau_id: requested user id
	//
	// Used in:
	// - StaticHttpClient
	// -------------------------------------------//
	public class UserIdResponse : ResponseObject
	{
		public string uau_id		{ get; set; }

		public UserIdResponse() : base()
		{
			uau_id = string.Empty;
		}
	}

	// -------------------------------------------//
	// Response received after a createWishlist
	// request
	// - status_msg: status info message
	// - list_name_err: “Please insert a valid list name”
	// - expiration_date_err: “Please insert an event date”
	// - listName: echo for created list name
	// - is_permanent: echo for created list duration
	// - expiration_date: echo for created list expiration date
	//
	// Used in:
	// - CreateListPage
	// -------------------------------------------//
	public class CreateWishListResponse : ResponseObject
	{
		public StatusMessage status_msg		{ get; set; }
		public string list_name_err			{ get; set; }
		public string expiration_date_err	{ get; set; }

		public string listName				{ get; set; }
		public string is_permanent			{ get; set; }
		public string expiration_date		{ get; set; }

		public CreateWishListResponse() : base()
		{
			status_msg = null;
			list_name_err = string.Empty;
			expiration_date_err = string.Empty;
			listName = string.Empty;
			is_permanent = string.Empty;
			expiration_date = string.Empty;
		}

		public string GetErrorMessage()
		{
			string response = string.Empty;

			if (status_msg != null && status_msg.list_err != string.Empty)
			{
				response = status_msg.list_err;
			}
			else if (list_name_err != string.Empty)
			{
				response = list_name_err;
			}
			else if (expiration_date_err != string.Empty)
			{
				response = expiration_date_err;
			}

			return response;
		}
	}

	// -------------------------------------------//
	// Response received after a editWishlist
	// request
	// - status_msg: “Something went wrong because of a technical problem, please try again”
	// - list_err: “The wish list you’re trying to edit doesn’t exists”
	// - list_name_err: “Please insert a valid list name”
	// - expiration_date_err: “Please insert an event date”
	// - listName: echo for edited list name
	// - is_permanent: echo for edited list duration
	// - expiration_date: echo for edited list expiration date
	//
	// Used in:
	// - CreateListPage
	// -------------------------------------------//
	public class EditWishListResponse : ResponseObject
	{
		public string status_msg			{ get; set; }
		public string list_err				{ get; set; }
		public string list_name_err			{ get; set; }
		public string expiration_date_err	{ get; set; }
		public string listName				{ get; set; }
		public string is_permanent			{ get; set; }
		public string expiration_date		{ get; set; }

		public EditWishListResponse() : base()
		{
			status_msg = string.Empty;
			list_err = string.Empty;
			list_name_err = string.Empty;
			expiration_date_err = string.Empty;
			listName = string.Empty;
			is_permanent = string.Empty;
			expiration_date = string.Empty;
		}

		public string GetErrorMessage()
		{
			string response = string.Empty;

			if (status_msg != string.Empty)
			{
				response = status_msg;
			}
			else if (list_err != string.Empty)
			{
				response = list_err;
			}
			else if (list_name_err != string.Empty)
			{
				response = list_name_err;
			}
			else if (expiration_date_err != string.Empty)
			{
				response = expiration_date_err;
			}

			return response;
		}
	}

	// -------------------------------------------//
	// Response received after a addFriendsToList
	// request
	// 
	//
	// Used in:
	// 
	// -------------------------------------------//
	public class AddFriendsToListResponse : ResponseObject
	{
		// TODO
	}
		

	// -------------------------------------------//
	// Response received after a removeWishlist
	// request
	// - delete_err: (“something went wrong, please try again”; “The wishlist doesn’t exists”, “No wishlist id in POST”)
	//
	// Used in:
	// MyListPage
	// -------------------------------------------//
	public class RemoveWishlistResponse : ResponseObject
	{
		public string delete_err	{ get; set; }

		public RemoveWishlistResponse() : base()
		{
			delete_err = string.Empty;
		}

		public string GetErrorMessage()
		{
			string response = string.Empty;

			if (delete_err != string.Empty)
			{
				response = delete_err;
			}

			return response;
		}
	}

	// -------------------------------------------//
	// Response received after a getCliques
	// request
	// 
	//
	// Used in:
	// 
	// -------------------------------------------//
	public class UserCliquesResponse : ResponseObject
	{
		// TODO
	}


	// -------------------------------------------//
	// Response received after a getUserFriends
	// request
	// - friends: app friends for the current user
	//
	// Used in:
	// - AddFriendsPage
	// -------------------------------------------//
	public struct FriendList
	{
		public List<User> friends	{ get; set; }
	}

	// -------------------------------------------//
	// Response received after a getUserCliques
	// request
	// - circles: cliques for the current user
	//
	// Used in:
	// - LoadCirclePage
	// -------------------------------------------//
	public struct CirclesList
	{
		public List<Circle> cliques	{ get; set; }
	}

	// -------------------------------------------//
	// Response received after a addFriendsToList
	// request
	// - status_msg: echo for added friends id
	//
	// Used in:
	// - AddFriendsPage
	// -------------------------------------------//
	public class AddFriendsListResponse : ResponseObject
	{
		public AddFriendsResult status_msg	{ get; set; }

		public AddFriendsListResponse() : base()
		{
			status_msg = null;
			s = string.Empty;
		}

		public string GetErrorMessage()
		{
			string response = string.Empty;

			if (status_msg.err != string.Empty)
			{
				response = status_msg.err;
			}

			return response;
		}
	}

	public class AddFriendsResult
	{
		public List<AddedFriends> s			{ get; set; }
		public string err					{ get; set; }
	}

	public class AddedFriends
	{
		public string id		{ get; set; }
		public string s			{ get; set; }
	}


	// -------------------------------------------//
	// Response received after a addItemToWishlist
	// request
	// - title_err: (“Missing title”, “Invalid title, please check it out”)
	// - info_err: “Invalid info, please chek it out”
	// - desc_err: “Invalid description, max 100 chars, please check it out”
	// - upload_err: ("Something went wrong during the image upload. Allowed only JPEG, JPG AND PNG images less then 2 MB.”,
	//				  "Invalid image format. Allowed only jpeg, jpg, png image", 
	//				  "Invalid image size. Allowed only image less then 2 MB”)
	//
	// Used in:
	// - AddWishPage
	// -------------------------------------------//
	public class AddItemResponse : ResponseObject
	{
		public string title_err		{ get; set; }
		public string info_err		{ get; set; }
		public string desc_err		{ get; set; }
		public string upload_err	{ get; set; }

		public AddItemResponse() : base()
		{
			title_err = string.Empty;
			info_err = string.Empty;
			desc_err = string.Empty;
			upload_err = string.Empty;
		}

		public string GetErrorMessage()
		{
			string response = string.Empty;

			if (title_err != string.Empty)
			{
				response = title_err;
			}
			else if (info_err != string.Empty)
			{
				response = info_err;
			}
			else if (desc_err != string.Empty)
			{
				response = desc_err;
			}
			else if (upload_err != string.Empty)
			{
				response = upload_err;
			}

			return response;
		}
	}


	// -------------------------------------------//
	// Response received after a GetProductData
	// request
	// - err: “missing url”
	// - title: product name
	// - description: product description
	// - imgUrl: product image Url
	//
	// Used in:
	// - AddWishPage
	// -------------------------------------------//
	public class GetProductDataResponse : ResponseObject
	{
		public string err			{ get; set; }
		public string title			{ get; set; }
		public string description	{ get; set; }
		public string imageUrl		{ get; set; }

		public GetProductDataResponse() : base()
		{
			err = string.Empty;
			title = string.Empty;
			description = string.Empty;
			imageUrl = string.Empty;
		}

		public string GetErrorMessage()
		{
			string response = string.Empty;

			if (err != string.Empty)
			{
				response = err;
			}

			return response;
		}
	}


	// -------------------------------------------//
	// Response received after a getWishListItems
	// request
	// - wishlist_id: current item wishlist id
	// - item_id: item id
	// - title: product name
	// - description: product description
	// - photo: product image Url
	//
	// Used in:
	// - ListPage
	// - FriendListDetailPage
	// -------------------------------------------//
	public class ItemData
	{
		public string wishlist_id				{ get; set; }
		public string item_id					{ get; set; }
		public string title						{ get; set; }
		public string description				{ get; set; }
		public string photo						{ get; set; }
		public ReservationType reservation		{ get; set; }
		public string expiration_date			{ get; set; }
		public string partecipants				{ get; set; }
		public string part_amount				{ get; set; }
        public string profilePhoto              { get; set; }
        public string daysLeft                  { get; set; }
        public string daysLabel              	{ get; set; }
		public string row_id					{ get; set; }
		public string added_by					{ get; set; }
		public string date_created				{ get; set; }
		public string is_deleted				{ get; set; }
		public bool is_new						{ get; set; }


        public ItemData ()
		{
			wishlist_id = string.Empty;
			item_id = string.Empty;
			title = string.Empty;
			description = string.Empty;
			photo = string.Empty;
			reservation = ReservationType.Reservable;
			expiration_date = string.Empty;
			partecipants = string.Empty;
			part_amount = string.Empty;
            profilePhoto = string.Empty;
            daysLeft = string.Empty;
            daysLabel = string.Empty;
			row_id = string.Empty;
			added_by = string.Empty;
			date_created = string.Empty;
			is_deleted = string.Empty;
        }

		public ItemData(string wishlist_id, string item_id, string title, string description, string info, string photo)
		{
			this.wishlist_id = wishlist_id;
			this.item_id = item_id;
			this.title = title;
			this.description = description;
			this.photo = photo;
		}
	}


	// -------------------------------------------//
	// Response received after a getWishListGroupNumbers
	// request
	// - group_numbers: friends count in the 
	//	 current wishlist
	//
	// Used in:
	// - ListPage
	// -------------------------------------------//
	public class GroupNumberResponse : ResponseObject
	{
		public string group_numbers	{ get; set; }

		public GroupNumberResponse () : base()
		{
			group_numbers = string.Empty;
		}

		public GroupNumberResponse(string group_numbers) : base()
		{
			this.group_numbers = group_numbers;
		}
	}


	// -------------------------------------------//
	// Response received after a DeleteItem
	// request
	// - delete_err:  (“something went wrong, please try again”; “The item doesn’t exists”, “No item id in POST”)
	//
	// Used in:
	// - ListPage
	// -------------------------------------------//
	public class DeleteItemResponse : ResponseObject
	{
		public string delete_err	{ get; set; }

		public DeleteItemResponse () : base()
		{
			delete_err = string.Empty;
		}

		public string GetErrorMessage()
		{
			string response = string.Empty;

			if (delete_err != string.Empty)
			{
				response = delete_err;
			}

			return response;
		}
	}


	// -------------------------------------------//
	// Response received after an AddAdminToList
	// request
	// - status_msg: (user_UAU_ID_added,“List doesn’t exists” //in caso di is_editing = true)
	//
	// Used in:
	// - AddListAdminPage
	// -------------------------------------------//
	public class AddAdminToListResponse : ResponseObject
	{
		public AddAdminStatusMessage status_msg	{ get; set; }

		public AddAdminToListResponse () : base()
		{
			status_msg = null;
		}
	}


	// -------------------------------------------//
	// Response received after an ImportFromList
	// request
	// - status_msg: contains fields "id" and "s"
	//
	// Used in:
	// - AddListAdminPage
	// -------------------------------------------//
	public class ImportFromListResponse : ResponseObject
	{
		public ImportFromListStatusMessage status_msg	{ get; set; }

		public ImportFromListResponse () : base()
		{
			status_msg = null;
		}
	}

	public class ImportFromListStatusMessage
	{
		public List<ImportedList> s				{ get; set; }
	}

	public class ImportedList
	{
		public string id						{ get; set; }
		public string s							{ get; set; }
	}


	// -------------------------------------------//
	// Response received after an EditList request
	// - status_msg:  “Something went wrong because of a technical problem, please try    again”
	// - list_err: “The wish list you’re trying to edit doesn’t exists”
	// - list_name_err: “Please insert a valid list name”
	// - expiration_date_err: “Please insert an event date”
	// - listName: echo of list name
	// - is_permanent: echo of is_permanent flag
	// - expiration_date: echo of expiration date
	//
	// Used in:
	// - ListSettingPage
	// - AddFriendsPage
	// -------------------------------------------//
	public class EditListResponse : ResponseObject
	{
		public string status_msg			{ get; set; }
		public string list_err				{ get; set; }
		public string list_name_err			{ get; set; }
		public string expiration_date_err	{ get; set; }
		public string listName 				{ get; set; }
		public string is_permanent			{ get; set; }
		public string expiration_date		{ get; set; }

		public EditListResponse () : base()
		{
			status_msg = string.Empty;
			list_err = string.Empty;
			list_name_err = string.Empty;
			expiration_date_err = string.Empty;
			listName = string.Empty;
			is_permanent = string.Empty;
			expiration_date = string.Empty;
		}

		public string GetErrorMessage()
		{
			string response = string.Empty;

			if (status_msg != string.Empty)
			{
				response = status_msg;
			}
			else if (list_err != string.Empty)
			{
				response = list_err;
			}
			else if (list_name_err != string.Empty)
			{
				response = list_name_err;
			}
			else if (expiration_date_err != string.Empty)
			{
				response = string.Empty;
			}

			return response;
		}
	}

	// -------------------------------------------//
	// Response received after a getCliqueFriends
	// request
	// - clique_friends
	//
	// Used in:
	// - LoadCirclePage
	// -------------------------------------------//
	public class GetCliqueFriendsResponse
	{
		public List<User> clique_friends		{ get; set; }

		public GetCliqueFriendsResponse () : base()
		{
			clique_friends = null;
		}
	}


	// -------------------------------------------//
	// Response received after a getSharedLists
	// request
	// - shared_lists: user visible friend lists
	// - err: “Missing friend_id or user_id”
	//
	// Used in:
	// - PickAGiftPage
	// -------------------------------------------//
	public class GetSharedListsResponse : ResponseObject
	{
		public List<WishlistData> shared_lists		{ get; set; }
		public string err							{ get; set; }

		public GetSharedListsResponse () : base()
		{
			shared_lists = null;
			err = string.Empty;
		}

		public string GetErrorMessage()
		{
			string response = string.Empty;

			if (err != string.Empty)
			{
				response = err;
			}

			return response;
		}
	}


	// -------------------------------------------//
	// Response received after a checkReservation
	// request
	// - reservation_msg: (‘Reserved by admin’, ‘reserved by other’ , ’Not reserved yet’)
	// - expiration_date: expiration date of the reservation
	// - reservation_type: (standard or collection)
	// - collection_joined: true if user partecipate to the collect, false if not
	//
	// Used in:
	// - FriendListDetailPage
	// -------------------------------------------//
	public class CheckReservationResponse : ResponseObject
	{
		public string reservation_msg		{ get; set; }
		public string expiration_date		{ get; set; }
		public string reservation_type		{ get; set; }
		public string collection_joined		{ get; set; }
		public string partecipants			{ get; set; }
		public string part_amount			{ get; set; }

		public CheckReservationResponse () : base()
		{
			reservation_msg = string.Empty;
			expiration_date = string.Empty;
			reservation_type = string.Empty;
			collection_joined = string.Empty;
			partecipants = string.Empty;
			part_amount = string.Empty;
		}

		public ReservationType GetReservationType()
		{
			ReservationType response = ReservationType.Reservable;

			switch (reservation_msg)
			{
			case "Reserved by me":
				if (reservation_type == "standard")
				{
					response = ReservationType.ReservedByMe;
				}
				else if (reservation_type == "collection")
				{
					response = ReservationType.MyCollect;
				}
				break;

			case "Reserved by other":
				if (reservation_type == "standard")
				{
					response = ReservationType.ReservedByOther;
				}
				else if (reservation_type == "collection")
				{
					if (collection_joined == "true")
					{
						response = ReservationType.ReservedByMeCollect;
					}
					else
					{
						response = ReservationType.ActiveCollect;
					}
				}
				break;
			}

			return response;
		}
	}

	// -------------------------------------------//
	// Response received after a reservation
	// request
	// - reservation_msg: (‘Item already reserved’, ‘Reserved’, ‘Something went wrong’)
	//
	// Used in:
	// - FriendListDetailPage
	// -------------------------------------------//
	public class ReservationResponse : ResponseObject
	{
		public string reservation_msg		{ get; set; }

		public ReservationResponse () : base()
		{
			reservation_msg = string.Empty;
		}

		public string GetErrorMessage()
		{
			string response = string.Empty;

			if (reservation_msg != string.Empty)
			{
				response = reservation_msg;
			}

			return response;
		}
	}


	// -------------------------------------------//
	// Response received after an 
	//	amazonMarketPlaceProducts request
	// - products: list of suggested products
	// - err: “Invalid request”, “Invalid request, please check if you are registered on (es.  http://webservices.amazon.co.uk)"
	//
	// Used in:
	// - WishDetailPage
	// -------------------------------------------//
	public class AmazonMarketPlaceProductsResponse : ResponseObject
	{
		public List<ShopItemData> products		{ get; set; }
		public string err						{ get; set; }

		public AmazonMarketPlaceProductsResponse () : base()
		{
			products = null;
			err = string.Empty;
		}

		public string GetErrorMessage()
		{
			string response = string.Empty;

			if (err != string.Empty)
			{
				response = err;
			}

			return response;
		}
	}

	public class ShopItemData
	{
		public string DetailPageURL						{ get; set; }
		public ShopImage LargeImage						{ get; set; }
		public ShopItemAttributes ItemAttributes		{ get; set; }
		public ShopOfferSummary OfferSummary			{ get; set; }

		public ShopItemData()
		{
			DetailPageURL = string.Empty;
			LargeImage = null;
			ItemAttributes = null;
			OfferSummary = null;
		}
	}

	public class ShopImage
	{
		public string URL								{ get; set; }
		public string Height							{ get; set; }
		public string Width								{ get; set; } 
	}

	public class ShopItemAttributes
	{
		public string Title								{ get; set; }
	}

	public class ShopOfferSummary
	{
		public ShopPrice LowestNewPrice					{ get; set; }
	}

	public class ShopPrice
	{
		public string FormattedPrice					{ get; set; }
		public string Amount							{ get; set; }
		public string CurrencyCode						{ get; set; }
	}

	// -------------------------------------------//
	// Response received after deleteUserFromFriends
	// request
	// - err: “missing uau_id or owner_id”
	//
	// Used in:
	// - PickAGift
	// -------------------------------------------//
	public class DeleteUserFromFriendsResponse : ResponseObject
	{
		public string err { get; set; }

		public DeleteUserFromFriendsResponse () : base()
		{
			err = string.Empty;
		}

		public string GetErrorMessage ()
		{
			string response = string.Empty;

			if (err != string.Empty) 
			{
				response = err;
			}

			return response;
		}
	}


	// -------------------------------------------//
	// Response received after deleteReservation
	// request
	// - reservation_msg: (‘Reservation deleted’, ‘Something went wrong’, ‘The item is not reserved’, “You can not delete the collection because you are not the admin”)
	//
	// Used in:
	// - WishDetailPage
	// -------------------------------------------//
	public class DeleteReservationResponse : ResponseObject
	{
		public string reservation_msg	 { get; set; }

		public DeleteReservationResponse () : base()
		{
			reservation_msg = string.Empty;
		}

		public string GetErrorMessage ()
		{
			string response = string.Empty;

			if (reservation_msg != string.Empty) 
			{
				response = reservation_msg;
			}

			return response;
		}
	}

    public class GetAllUsersResponse : ResponseObject
    {
        public User[] users;

        public GetAllUsersResponse()
        {
            users = null;
        }
    }


	// -------------------------------------------//
	// Response received after removeUserFromCollection
	// request
	// - err: (’Missing uau_id or item_id’, ’The reservation is not a collection or user is an admin’, ’No reservation for uau_id : $post[‘uau_id’] and item_id : $post[‘item_id’]’)
	//
	// Used in:
	// - WishDetailPage
	// -------------------------------------------//
	public class RemoveUserFromCollectionResponse : ResponseObject
	{
		public string err { get; set; }

		public RemoveUserFromCollectionResponse	 () : base()
		{
			err = string.Empty;
		}

		public string GetErrorMessage ()
		{
			string response = string.Empty;

			if (err != string.Empty) 
			{
				response = err;
			}

			return response;
		}
	}


	// -------------------------------------------//
	// Response received after editCollection
	// request
	// - reservation_msg: (’Reservation updated’, ‘Something went wrong’)
	// - err: (‘Missing uau_id or item_id’, ’Invalid expiration_date format’, ’Invalid number of partecipants, allowed up to 10’, ’Missing fields to update’)
	//
	// Used in:
	// - WishDetailPage
	// -------------------------------------------//
	public class EditCollectionResponse : ResponseObject
	{
		public string reservation_msg		{ get; set; }
		public string err 					{ get; set; }

		public EditCollectionResponse	 () : base()
		{
			err = string.Empty;
		}

		public string GetErrorMessage ()
		{
			string response = string.Empty;

			if (err != string.Empty) 
			{
				response = err;
			}

			return response;
		}
	}


	// -------------------------------------------//
	// Response received after getUsersCollectionInfo
	// request
	// - users_info: user data array
	//
	// Used in:
	// - KittyAdminPage
	// -------------------------------------------//
	public class getUsersCollectionInfoResponse : ResponseObject
	{
		public List<User> users_info		{ get; set; }
	}


	// -------------------------------------------//
	// Response received after updateProfile
	// request
	// - lastName: user last name
	// - firstName: user first name
	// - birthday: user birthday
	// - sex: user gender
	// - country: user country
	// - city: user city
	// - profile_type: user profile type
	// - photo: user photo
	// - uau:id_err: “Missing uau_id”
	// - birthday_err: (“Invalid birthday date, Only over 14 years old users can sign up”, “Invalid birthday”)
	// - upload_err: ("Something went wrong during the image upload. Allowed only JPEG, JPG AND PNG images less then 2 MB.”, 
	// 				  "Invalid image format. Allowed only jpeg, jpg, png image", "Invalid image size. Allowed only image less then 2 MB”)
	// - name_err: “Invalid firstname”
	// - last_err: “Invalid lastname”
	// - country_err: “Invalid country”
	// - city_err: “Invalid city”
	//
	// Used in:
	// - EditProfilePage
	// -------------------------------------------//
	public class UpdateProfileResponse : ResponseObject
	{
		public string lastName			{ get; set; }
		public string firstName			{ get; set; }
		public string birthday			{ get; set; }
		public string sex				{ get; set; }
		public string country			{ get; set; }
		public string city				{ get; set; }
		public string profile_type		{ get; set; }
		public string photo				{ get; set; }

		public string uau_id_err		{ get; set; }
		public string birthday_err		{ get; set; }
		public string upload_err 		{ get; set; }
		public string name_err			{ get; set; }
		public string last_err			{ get; set; }
		public string country_err		{ get; set; }
		public string city_err			{ get; set; }

		public UpdateProfileResponse () : base()
		{
			lastName = string.Empty;
			firstName = string.Empty;
			birthday = string.Empty;
			sex = string.Empty;
			country = string.Empty;
			city = string.Empty;
			profile_type = string.Empty;
			photo = string.Empty;

			uau_id_err = string.Empty;
			birthday_err = string.Empty;
			upload_err = string.Empty;
			name_err = string.Empty;
			last_err = string.Empty;
			country_err = string.Empty;
			city_err = string.Empty;
		}

		public string GetErrorMessage ()
		{
			string response = string.Empty;

			if (uau_id_err != string.Empty)
			{
				response = uau_id_err;
			}
			else if (birthday_err != string.Empty) 
			{
				response = birthday_err;
			}
			else if (upload_err != string.Empty)
			{
				response = upload_err;
			}
			else if (name_err != string.Empty)
			{
				response = name_err;
			}
			else if (last_err != string.Empty)
			{
				response = last_err;
			}
			else if (country_err != string.Empty)
			{
				response = country_err;
			}
			else if (city_err != string.Empty)
			{
				response = city_err;
			}

			return response;
		}
	}


	// -------------------------------------------//
	// Response received after deleteUserFromClique
	// request
	// - err: “missing uau_id or clique_id”
	//
	// Used in:
	// - CircleFriendsPage
	// -------------------------------------------//
	public class DeleteUserFromCliqueResponse : ResponseObject
	{
		public string err		{ get; set; }

		public DeleteUserFromCliqueResponse () : base()
		{
			err = string.Empty;
		}

		public string GetErrorMessage ()
		{
			string response = string.Empty;

			if (err != string.Empty)
			{
				response = err;
			}

			return response;
		}
	}


	// -------------------------------------------//
	// Response received after addUserToClique
	// request
	// - err: (‘Missing clique_id or cliqueName or uau_id or owner_id’, ‘No clique with the given clique_id’, ‘The clique is not owned by the given owner_id’)
	//
	// Used in:
	// - CircleFriendsPage
	// -------------------------------------------//
	public class AddUserToCliqueResponse : ResponseObject
	{
		public string err		{ get; set; }

		public AddUserToCliqueResponse () : base()
		{
			err = string.Empty;
		}

		public string GetErrorMessage ()
		{
			string response = string.Empty;

			if (err != string.Empty)
			{
				response = err;
			}

			return response;
		}
	}


	// -------------------------------------------//
	// Response received after deleteClique
	// request
	// - err: “missing clique_id”
	//
	// Used in:
	// - MyCirclePage
	// -------------------------------------------//
	public class DeleteCliqueResponse : ResponseObject
	{
		public string err		{ get; set; }

		public DeleteCliqueResponse () : base()
		{
			err = string.Empty;
		}

		public string GetErrorMessage ()
		{
			string response = string.Empty;

			if (err != string.Empty)
			{
				response = err;
			}

			return response;
		}
	}

    // -------------------------------------------//
    // Response received after get the friends 
	// added to a wishlist
    // - wishlist_friends: list of friends in wishlist
    //
    // Used in:
    // - AddFriendsPage
    // -------------------------------------------//
    public class GetWishListFriendsResponse
    {
        public List<User> wishlist_friends { get; set; }

		public GetWishListFriendsResponse() : base()
        {
            wishlist_friends = new List<User>();
        }
    }


	// -------------------------------------------//
	// Response received after delete friend from
	// a wishlist
	//
	// Used in:
	// - ShowFriendsInListPage
	// -------------------------------------------//
	public class DeleteUserFromWishlistResponse : ResponseObject
	{
		// ...
	}


	// -------------------------------------------//
	// Response received after getUserById request 
	// - user: user data
	//
	// Used in:
	// - FeedPage
	// -------------------------------------------//
	public class GetUserByIdResponse : ResponseObject
	{
		public User user			{ get; set; }
		
		public GetUserByIdResponse() : base()
		{
			user = new User();
		}
	}


	// -------------------------------------------//
	// Response received after getWishlistItemDetails
	// request 
	// - item_details: item details
	// - err: “Missing list_id or item_id”
	//
	// Used in:
	// - FeedPage
	// -------------------------------------------//
	public class GetWishlistItemDetailsResponse : ResponseObject
	{
		public ItemData item	{ get; set; }
		public string err				{ get; set; }

		public GetWishlistItemDetailsResponse() : base()
		{
			item = new ItemData();
			err = string.Empty;
		}

		public string GetErrorMessage()
		{
			string response = string.Empty;

			if (err != string.Empty)
			{
				response = err;
			}

			return response;
		}
	}


	// -------------------------------------------//
	// Response received after getWishlistInfo
	// request 
	// - wishlist: wishlist data
	//
	// Used in:
	// - FeedPage
	// -------------------------------------------//
	public class GetWishlistInfoResponse : ResponseObject
	{
		public WishlistData wishlist	{ get; set; }
		public string err				{ get; set; }

		public GetWishlistInfoResponse() : base()
		{
			wishlist = new WishlistData();
			err = string.Empty;
		}

		public string GetErrorMessage()
		{
			string response = string.Empty;

			if (err != string.Empty)
			{
				response = err;
			}

			return response;
		}
	}


	// -------------------------------------------//
	// Response received after getFriendsBirthday
	// request 
	// - friends: list of friends
	// - friends_err: "Missing uau_id”
	//
	// Used in:
	// - FeedPage
	// -------------------------------------------//
	public class GetFrindsBirthdayResponse : ResponseObject
	{
		public List<User> friends		{ get; set; }
		public string friends_err		{ get; set; }

		public GetFrindsBirthdayResponse() : base()
		{
			friends = new List<User>();
			friends_err = string.Empty;
		}

		public string GetErrorMessage()
		{
			string response = string.Empty;

			if (friends_err != string.Empty)
			{
				response = friends_err;
			}

			return response;
		}
	}
}

