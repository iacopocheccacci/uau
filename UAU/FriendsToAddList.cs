﻿using System;
using System.Collections.Generic;
using CustomRenderer;

namespace UAU
{
	public class FriendsToAddList
	{
		public string listName				{ get; set; }
		public string user_id				{ get; set; }
		public List<FriendData> friends		{ get; set; }
		public bool is_editing				{ get; set; }

		public FriendsToAddList()
		{
			listName = string.Empty;
			user_id = string.Empty;
			friends = null;
			is_editing = false;
		}

		public FriendsToAddList(string listName, string user_id, List<FriendData> friends, bool is_editing)
		{
			this.listName = listName;
			this.user_id = user_id;
			this.friends = friends;
			this.is_editing = is_editing;
		}
	}
}

