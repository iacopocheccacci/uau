using System;

namespace UAU
{
	public class Constants
	{
		// Server URL
		public const string serverUrl = "http://52.31.185.204";

		// Alert messages
		public const string wellDoneMessage = "Ben fatto!";
		public const string customLoginSucceededMessage = "Una email è stata inviata alla tua casella di posta. Fai click sul link per confermare la registrazione";	
		public const string warningMessage = "Attenzione!";
		public const string okMessage = "OK";
		public const string yesMessage = "Si";
		public const string noMessage = "No";
		public const string emailSentMessage = "Email inviata";
		public const string emptyEntryFieldMessage = "Alcuni campo obbligatori non sono stati riempiti";
		public const string shortPasswordEntryMessage = "La password deve essere lunga almeno 6 caratteri-";
		public const string missingListNameTitle = "Nome lista mancante";
		public const string missingListNameMessage = "Dai un nome alla tua lista per continuare";
		public const string connectionFailedMessage = "Connessione assente";
		public const string wishlistCreationSuccededTitle = "Lista dei desideri creata!";
		public const string addFirstItemMessage = "Vuoi inserire il tuo primo desiderio nella lista?";
		public const string deleteWishlistTitle = "Cancellare la lista?";
		public const string deleteWishlistMessage = "Cancellando la lista non potrai più recuperarla";
		public const string deleteItemTitle = "Cancellare questo oggetto?";
		public const string deleteItemMessage = "Cancellando l'oggetto non potrai più recuperarlo";
		public const string circleSavedTitle = "Gruppo creato";
		public const string circleSavedMessage = "Trovi il nuovo gruppo nella sezione Profilo";
		public const string circleEditMessage = "Il tuo gruppo è stato aggiornato";
		public const string circleDeletedMessage = "Vuoi cancellare questo gruppo?";
		public const string friendGroupCreationErrorMessage = "Errore durante la creazione del gruppo";
		public const string addAdminToListErrorMessage = "Errore durante l'aggiunta di un amministratore";
		public const string itemsImportedMessage = "Oggetto correttamente importato nella lista dei desideri";
		public const string itemsImportFailedMessage = "Errore durante l'importazione della lista";
		public const string linkNotFoundTitle = "Ops!";
		public const string linkNotFoundMessage = "Non siamo riusciti a trovare l'oggetto automaticamente. Per favore riprova";
		public const string linkFoundTitle = "Trovato!";
		public const string linkFoundMessage = "Abbiamo trovato un oggetto all'indirizzo da te inserito";
		public const string dataLoadFailedMessage = "Errore nel caricamento dati";
		public const string itemAddedMessage = "Oggetto aggiunto alla lista deisideri";
		public const string facebookLoginErrorMessage = "Accesso con Facebook fallito";
		public const string deleteFriendMessage = "Vuoi rimuovere la tua amicizia?";
        public const string selectFriendsMessage = "Selezionare almeno un amico da aggiungere";
        public const string itemExistMessage = "Questo oggetto è già presente nella tua lista";
		public const string inviteFriendsQuestionTitle = "Vuoi invitare i tuoi amici?";
		public const string inviteFriendsQuestionMessage = "Invita i tuoi amici a partecipare alla colletta";
		public const string maybeLaterMessage = "Forse dopo";
        public const string friendAddedMessage = "Amici aggiunti";
		public const string profileUpdatedeMessage = "Profilo aggiornato";
		public const string deleteUserFromCliqueTitle = "Rimuovi amico?";
		public const string deleteUserFromCliqueMessage = "Sei sicuro di voler rimuovere l'amico dal gruppo?";
		public const string userRemovedFromCircleMessage = "L'amico è stato rimosso dal gruppo";
		public const string deleteCliqueTitle = "Rimuovi gruppo?";
		public const string deleteCliqueMessage = "Sei sicuro di voler rimuovere l'amico dal gruppo?";
		public const string RemovedCircleMessage = "Gruppo eliminato";
        public const string InvalidLinkMessage = "L'indirizzo inserito non è valido, riprova";


		// Properties
		public const string rememberMeProperty = "RememberMe";
		public const string userEmailProperty = "UserEmail";
		public const string isLoggedInProperty = "IsLoggedIn";
		public const string userIdProperty = "UserId";
		public const string nameProperty = "Name";
		public const string surnameProperty = "Surname";
		public const string birthdayProperty = "Birthday";
		public const string firstAccessProperty = "FirstAccess";
		public const string userPasswordProperty = "UserPassword";

		// Colors
		public const string grigioScuro = "#D6D3D8";
		public const string paleGreyHex = "#FAF9FB";
		public const string myBlackHex = "#312E33";
		public const string verde2Hex = "#14BE6C";
		public const string greyblueHex = "#8B9DC3";
		public const string denimBlueHex = "#3B5998";
		public const string lightBlue = "#f6f7f9";

		// Feed types
		public const string kittyInvite = "kitty_invite";
		public const string kittyUserSubscription = "kitty_user_subscription";
		public const string removeReservedItem = "removed_reserved_item";
		public const string userFriendshipInvite = "user_friendship_invite";
		public const string userWishlistAddInvite = "user_wishlist_add_invite";
		public const string userSubscribedKittyDeleted = "user_subscribed_kitty_deleted";
		public const string fbUserJoined = "fb_user_joined";
		public const string friendWishlistCreated = "friend_wishlist_created";
		public const string friendWishlistItemAdded = "friend_wishlist_item_added";

		// Feed button type
		public const string goToList = "go_to_list";
		public const string wishDetail = "wish_detail";
		public const string goToProfile = "go_to_profile";
		public const string kittyAdmin = "kitty_admin";

		// Feed category type
		public const string spotlight = "spotlight";
		public const string notSpotloght = "not_spotlight";

		// Labels
		public const string hoursLabel = "ore";
		public const string minutesLabel = "minuti";
		public const string secondsLabel = "secondi";
		public const string cancelLabel = "Annulla";
		public const string nextLabel = "Avanti";
		public const string editLabel = "Modifica";
		public const string addLabel = "Aggiungi";
		public const string doneLabel = "Fatto";
		public const string unselectLabel = "Deseleziona tutti";
		public const string selectLabel = "Seleziona tutti";
		public const string expiredLabel = "scaduta";
		public const string dayLabel = "giorno";
		public const string daysLabel = "giorni";
		public const string expireInLabel = "Scade tra";
		public const string settingsLabel = "Impostazioni";
		public const string noExpirationLabel = "Nessuna scadenza";

		public const string availableItemLabel = "Disponibile";
		public const string reservedByMeItemLabel = "Prenotato da te";
		public const string reservedByMeCollectLabel = "Partecipi alla colletta";
		public const string activeCollectLabel = "Colletta disponibile";
		public const string reservedByOtherLabel = "PRENOTATO";
		public const string myCollectLabel = "Colletta creata da te";
		public const string myReservationExpirationLabel = "La tua prenotazione scade tra ";
		public const string byOtherReservationExpirationLabel = "Prenotato. La prenotazione scade tra ";
		public const string myCollectExpirationLabel = "La tua colletta scade tra ";
		public const string genericReservationExpirationLabel = "La prenotazione scade tra ";
	}
}

