﻿using System;
using System.Diagnostics;
using CustomRenderer;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;

namespace UAU
{
	public partial class LoginPage : CustomPage
	{
		ILoginManager ilm;

		public LoginPage (ILoginManager ilm)
		{
			this.ilm = ilm;

			InitializeComponent ();
			SetNameIfRememberMeSwitchIsEnabled ();

			App.PostSuccessFacebookAction = async response =>
			{
				if (response != null)
				{
					if (response.IsSucceeded())
					{
						App.CurrentApp.Properties[Constants.isLoggedInProperty] = true;
						await App.CurrentApp.SavePropertiesAsync();

						await StaticHttpClient.GetInstance.SaveUserDataFromFB(response.user);

						if (StaticHttpClient.GetInstance.CheckIfFirstAccessFlagIsOn(true))
						{
							var popup = new ChangePasswordPopup(ilm);
							await PopupNavigation.PushAsync(popup);
						}
						else if (StaticHttpClient.GetInstance.currentUser.password == string.Empty ||
								 StaticHttpClient.GetInstance.currentUser.password == null)
						{
							var popup = new EnterPasswordPopup(ilm);
							await PopupNavigation.PushAsync(popup);
						}
						else
						{
							StaticHttpClient.GetInstance.CheckIfFirstAccessFlagIsOn(true);
							ilm.ShowLoadingPage();
						}
					}
					else
					{
						await DisplayAlert(Constants.warningMessage, response.GetErrorMessage(), Constants.okMessage);
					}
				}
				else
				{
					await DisplayAlert(Constants.warningMessage, Constants.facebookLoginErrorMessage, Constants.okMessage);
				}
			};
		}
			
		async void OnSignUpClicked(object sender, EventArgs args)
		{
			await Navigation.PushModalAsync (new NavigationPage(new CustomRegistrationPage (EmailEntry, PasswordEntry)));
		}

		async void OnForgotPasswordTapped(object sender, EventArgs args)
		{
			Debug.WriteLine ("Forgot Password Tapped");

			ForgotPasswordRequestData forgotPasswordData = new ForgotPasswordRequestData ();
			forgotPasswordData.email = EmailEntry.Text;

			lv.ActivateLoading ();
			string response = await StaticHttpClient.GetInstance.RequestFromApp(RequestType.forgotPassword, forgotPasswordData);

			if (response != string.Empty)
			{
				ForgotPasswordResponse responseObject;
				JsonUtilities<ForgotPasswordResponse>.DeserializeToObject (response, out responseObject);

				if (responseObject != null)
				{
					if (responseObject.IsSucceeded ())
					{
						await DisplayAlert (Constants.okMessage, Constants.emailSentMessage, Constants.okMessage);
					}
					else
					{
						await DisplayAlert (Constants.warningMessage, responseObject.GetErrorMessage (), Constants.okMessage);
					}
				}
				else
				{
					await DisplayAlert (Constants.warningMessage, Constants.connectionFailedMessage, Constants.okMessage);
				}
			}
			else
			{
				await DisplayAlert (Constants.warningMessage, Constants.connectionFailedMessage, Constants.okMessage);
			}

			lv.DeactivateLoading ();
		}

		async void OnSignInClicked(object sender, EventArgs args)
		{
			LoginRequestData loginData = new LoginRequestData ();
			loginData.email = EmailEntry.Text;
			loginData.password = PasswordEntry.Text;

			lv.ActivateLoading ();
			string response = await StaticHttpClient.GetInstance.RequestFromApp (RequestType.login, loginData);

			if (response != string.Empty)
			{
				CustomLoginResponse responseObject;
				JsonUtilities<CustomLoginResponse>.DeserializeToObject (response, out responseObject);

				if (responseObject != null)
				{
					if (responseObject.IsSucceeded())
					{
						await StaticHttpClient.GetInstance.SaveUserData (responseObject.user);
						await StaticHttpClient.GetInstance.SavePassword (PasswordEntry.Text);

						if (StaticHttpClient.GetInstance.CheckIfFirstAccessFlagIsOn ())
						{
							ilm.ShowWelcomeNavigationPage ();
						}
						else
						{
							ilm.ShowMainPage ();
						}
					}
					else
					{
						await DisplayAlert(Constants.warningMessage, responseObject.GetErrorMessage (), Constants.okMessage);
						lv.DeactivateLoading ();
					}	
				}
				else
				{
					await DisplayAlert (Constants.warningMessage, Constants.connectionFailedMessage, Constants.okMessage);
					lv.DeactivateLoading ();
				}
			}
			else
			{
				await DisplayAlert (Constants.warningMessage, Constants.connectionFailedMessage, Constants.okMessage);
				lv.DeactivateLoading ();
			}
		}

		private void SetNameIfRememberMeSwitchIsEnabled()
		{
			RememberMe.IsToggled = App.CurrentApp.Properties.ContainsKey(Constants.rememberMeProperty) ? (bool)App.CurrentApp.Properties [Constants.rememberMeProperty] : false;

			if (RememberMe.IsToggled)
			{
				EmailEntry.Text = App.CurrentApp.Properties.ContainsKey (Constants.userEmailProperty) ? (string)App.CurrentApp.Properties [Constants.userEmailProperty] : string.Empty;
				PasswordEntry.Text = App.CurrentApp.Properties.ContainsKey(Constants.userPasswordProperty) ? (string)App.CurrentApp.Properties[Constants.userPasswordProperty] : string.Empty;
			}
		}

		async private void OnRememberMeSwitchToggled(object sender, EventArgs args)
		{
			App.CurrentApp.Properties [Constants.rememberMeProperty] = RememberMe.IsToggled;
			await App.CurrentApp.SavePropertiesAsync();
		}
    }
}

