﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CustomRenderer;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;

namespace UAU
{
	public partial class ChangePasswordPopup : PopupPage
	{
		ILoginManager ilm;

		public ChangePasswordPopup (ILoginManager ilm)
		{
			InitializeComponent ();

			this.ilm = ilm;
		}

		async public void OnConfirmClicked(object sender, EventArgs args)
		{
			if (PasswordEntry.Text != string.Empty || ConfirmPasswordEntry.Text != string.Empty)
			{
				ChangePasswordRequestData changePasswordData = new ChangePasswordRequestData ();
				changePasswordData.email = StaticHttpClient.GetInstance.currentUser.email;
				changePasswordData.password = (PasswordEntry.Text != null) ? PasswordEntry.Text : string.Empty;
				changePasswordData.confirm_password = (ConfirmPasswordEntry.Text != null) ? ConfirmPasswordEntry.Text : string.Empty;

				if (ValidatePasswordEntry (changePasswordData.password) && ValidatePasswordEntry (changePasswordData.confirm_password))
				{
					string response = await StaticHttpClient.GetInstance.RequestFromApp (RequestType.changePassword, changePasswordData);

					if (response != string.Empty)
					{
						ChangePasswordResponse responseObject;
						JsonUtilities<ChangePasswordResponse>.DeserializeToObject (response, out responseObject);

						if (responseObject != null) 
						{
							if (responseObject.IsSucceeded ()) 
							{
								//				await SendFriendsList ();
								await StaticHttpClient.GetInstance.SavePassword (PasswordEntry.Text);
								ilm.ShowWelcomeNavigationPage ();
								await PopupNavigation.PopAsync ();
							}
							else 
							{
								await DisplayAlert (Constants.warningMessage, responseObject.GetErrorMessage (), Constants.okMessage);
							}
						} 
						else 
						{
							await DisplayAlert (Constants.warningMessage, Constants.connectionFailedMessage, Constants.okMessage);
						}
					}
					else
					{
						await DisplayAlert (Constants.warningMessage, Constants.connectionFailedMessage, Constants.okMessage);
					}
				}
				else
				{
					await DisplayAlert (Constants.warningMessage, Constants.shortPasswordEntryMessage, Constants.okMessage);
				}
			}
			else
			{
				await DisplayAlert (Constants.warningMessage, Constants.emptyEntryFieldMessage, Constants.okMessage);
			}
		}

		async private Task SendFriendsList()
		{
			var request = new AddFriendsRequest ();

			request.friends = new List<FriendData> ();
			request.friends = StaticHttpClient.GetInstance.currentUser.friends.data;
			request.owner_id = StaticHttpClient.GetInstance.currentUser.fb_uid;

			string response = await StaticHttpClient.GetInstance.RequestFromApp (RequestType.saveClique, request);

			if (response != string.Empty)
			{
				ResponseObject responseObject;
				JsonUtilities<ResponseObject>.DeserializeToObject (response, out responseObject);
			}
			else
			{
				await DisplayAlert (Constants.warningMessage, Constants.connectionFailedMessage, Constants.okMessage);
			}
		}

		private bool ValidatePasswordEntry(string password)
		{
			bool result = true;

			if (password.Length < 6)
			{
				result = false;
			}

			return result;
		}

		async private void OnCancelClicked(object sender, EventArgs args)
		{
			await PopupNavigation.PopAsync ();
		}
	}
}

