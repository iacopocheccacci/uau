﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using CustomRenderer;
using Xamarin.Forms;

namespace UAU
{
	public partial class CircleFriendsPage : CustomPage
	{
		private CircleData circle;
		ObservableCollection<FriendContactData> friendsData;
		GetCliqueFriendsResponse friendlist;

		public CircleFriendsPage (CircleData circle)
		{
			InitializeComponent ();
			Title = circle.Name;

			this.circle = circle;

#if __IOS__
			ToolbarItem button = new ToolbarItem();
			button.Text = Constants.addLabel;
			button.Order = ToolbarItemOrder.Primary;
			button.Priority = 1;
			button.Clicked += OnAddFriendsButtonClicked;
			this.ToolbarItems.Add(button);
#endif
		}

		async protected override void OnAppearing()
		{
			base.OnAppearing ();

			await PopulateList();
		}

		async private Task PopulateList()
		{
			friendsData = new ObservableCollection<FriendContactData> ();
			FriendsView.ItemsSource = friendsData;

			var cliqueData = new GetCliqueFriendsRequest (StaticHttpClient.GetInstance.currentUser.uau_id, circle.CircleId);
			lv.ActivateLoading ();
			string response = await StaticHttpClient.GetInstance.RequestFromApp (RequestType.getCliqueFriends, cliqueData);
			JsonUtilities<GetCliqueFriendsResponse>.DeserializeToObject (response, out friendlist);

			if (friendlist.clique_friends != null)
			{
				foreach (User friend in friendlist.clique_friends)
				{
					DateTime dt = DateTime.ParseExact (friend.birthday, "yyyy-MM-dd", CultureInfo.InvariantCulture);
					var birthday = string.Format("Compleanno: {0} {1}", dt.Day, dt.ToMonthName());

					friendsData.Add (new FriendContactData { FullName = friend.firstName + " " + friend.lastName,
						Id = friend.uau_id,
						Picture = friend.photo,
						Birthday = birthday
					});
				}
			}
			else
			{
				await DisplayAlert (Constants.warningMessage, Constants.dataLoadFailedMessage, Constants.okMessage);
			}

			// Disable selection
			FriendsView.ItemSelected += (sender, e) => {

				((ListView)sender).SelectedItem = null;
			};

			lv.DeactivateLoading ();
		}

		private void OnTextChanged(object sender, EventArgs args)
		{
#if __IOS__
			FriendsView.BeginRefresh ();
#endif 

			if (string.IsNullOrWhiteSpace (SearchBarField.Text)) 
			{
				FriendsView.ItemsSource = friendsData;
			}
			else 
			{
				FriendsView.ItemsSource = friendsData
					.Where (x => x.FullName.ToLower ()
					        .Contains (SearchBarField.Text.ToLower ()));
			}

#if __IOS__
			FriendsView.EndRefresh ();
#endif
		}

		async private void OnAddFriendsButtonClicked(object sender, EventArgs args)
		{
			await Navigation.PushAsync(new AddFriendsToCirclePage(circle, friendlist.clique_friends));
		}

		async private void OnDelete(object sender, EventArgs args)
		{
			// TODO: delete circle
			MenuItem friend = (MenuItem)sender;

			var answer = await DisplayAlert (Constants.deleteUserFromCliqueTitle, Constants.deleteUserFromCliqueMessage, Constants.noMessage, Constants.yesMessage);

			if (!answer)
			{
				await RemoveUserFromClique(friend);	
			}					                      
		}

		async private Task RemoveUserFromClique(MenuItem item)
		{
			var request = new DeleteUserFromCiqueRequest(item.CommandParameter.ToString(), circle.CircleId);

			string response = await StaticHttpClient.GetInstance.RequestFromApp (RequestType.deleteUserFromClique, request);
			DeleteUserFromCliqueResponse responseObject;
			JsonUtilities<DeleteUserFromCliqueResponse>.DeserializeToObject (response, out responseObject);

			if (responseObject != null)
			{
				if (responseObject.IsSucceeded())
				{
					FriendContactData friendItem = (from itm in friendsData
					                                where itm.Id == item.CommandParameter.ToString ()
					                                select itm).FirstOrDefault<FriendContactData> ();
					friendsData.Remove(friendItem);
					await DisplayAlert(Constants.wellDoneMessage, Constants.userRemovedFromCircleMessage, Constants.okMessage);
				}
				else
				{
					await this.DisplayAlert (Constants.warningMessage, responseObject.GetErrorMessage (), Constants.okMessage);
				}
			}
			else
			{
				await DisplayAlert (Constants.warningMessage, Constants.connectionFailedMessage, Constants.okMessage);
			}
		}

        async public override void OnButtonClick()
        {
            base.OnButtonClick();
            await Navigation.PushAsync(new AddFriendsToCirclePage(circle, friendlist.clique_friends));
        }
    }
}

