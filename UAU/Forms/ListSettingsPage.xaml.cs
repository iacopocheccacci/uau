﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using System.Globalization;
using CustomRenderer;
using System.Threading.Tasks;

namespace UAU
{
	public partial class ListSettingsPage : CustomPage
	{
		private WishlistData wishlist;
		private string listName;
		private bool isReserved;
		private string groupNumber;
		private bool isEvent;
		private DateTime eventDate;

		public ListSettingsPage (WishlistData wishlist, string groupNumber)
		{
			InitializeComponent ();

			this.wishlist = wishlist;
			this.groupNumber = groupNumber;

#if __IOS__
            ToolbarItem button = new ToolbarItem();
			button.Text = Constants.cancelLabel;
            button.Order = ToolbarItemOrder.Primary;
            button.Priority = 0;
            button.Clicked += OnCancelButtonClicked;
            this.ToolbarItems.Add(button);

            ToolbarItem button2 = new ToolbarItem();
			button2.Text = Constants.doneLabel;
            button2.Order = ToolbarItemOrder.Primary;
            button2.Priority = 1;
            button2.Clicked += OnDoneButtonClicked;
            this.ToolbarItems.Add(button2);
#endif

            SetFields();
		}

		async protected override void OnAppearing()
		{
			base.OnAppearing ();
			await SetFields ();
		}


		async private void OnAddAdminClicked(object sender, EventArgs args)
		{
			await Navigation.PushModalAsync (new NavigationPage (new AddListAdminPage (wishlist)));
		}

		async private void OnCancelButtonClicked(object sender, EventArgs args)
		{
			await Navigation.PopModalAsync ();
		}

		async private void OnDoneButtonClicked(object sender, EventArgs args)
		{
			await EditWishlistOnDB ();
		}

		async private Task EditWishlistOnDB()
		{
			lv.ActivateLoading ();

			var wishlistToEdit = new WishlistToAdd (wishlist.list_id, 
													StaticHttpClient.GetInstance.currentUser.uau_id,
							                        ListNameEntry.Text,
							                        (EventSwitch.IsToggled) ? "0" : "1",
													(VisibilitySwitch.IsToggled) ? "1" : "0",
							                        EventDatePicker.Date);
 
			string response = await StaticHttpClient.GetInstance.RequestFromApp (RequestType.editList, wishlistToEdit);
			EditListResponse responseObject;

			JsonUtilities<EditListResponse>.DeserializeToObject (response, out responseObject);

			if (responseObject != null)
			{
				if (responseObject.IsSucceeded ())
				{
					lv.DeactivateLoading();
					wishlist.listName = wishlistToEdit.listName;
					wishlist.is_permanent = wishlistToEdit.is_permanent;
					wishlist.visibility = wishlistToEdit.visibility;
					wishlist.expiration_date = string.Format("{0:yyyy/MM/dd}", EventDatePicker.Date);

					await SetFields ();
				}
				else
				{
					await this.DisplayAlert (Constants.warningMessage, responseObject.GetErrorMessage (), Constants.okMessage);
					lv.DeactivateLoading ();
				}
			}
			else
			{
				await DisplayAlert (Constants.warningMessage, Constants.dataLoadFailedMessage, Constants.okMessage);
				lv.DeactivateLoading ();
			}
		}

		async private void OnSelectFriendsClicked(object sender, EventArgs args)
		{
			var wishlistToModify = new WishlistToAdd (wishlist.list_id,
													  StaticHttpClient.GetInstance.currentUser.uau_id,
													  wishlist.listName,
													  wishlist.is_permanent,
													  (wishlist.visibility == "public") ? "0" : "1",
													  eventDate);
			
			await Navigation.PushAsync(new ShowFriendsInListPage(wishlistToModify));
		}

		private void OnVisibilitySwitchToggled(object sender, EventArgs args)
		{
			AddFriendsButton.IsVisible = VisibilitySwitch.IsToggled;
			wishlist.visibility = (VisibilitySwitch.IsToggled == true) ? "private" : "public";

			ReservedLabel.IsVisible = VisibilitySwitch.IsToggled;
			PublicLabel.IsVisible = !VisibilitySwitch.IsToggled;
		}

		async private Task SetFields()
		{
			// List name entry setup
			listName = wishlist.listName;
			ListNameEntry.BindingContext = listName;
			ListNameEntry.SetBinding (Entry.TextProperty, ".", BindingMode.TwoWay);

			// Visibility switch setup
			isReserved = (wishlist.visibility == "private") ? true : false;
			VisibilitySwitch.BindingContext = isReserved;
			VisibilitySwitch.SetBinding (Switch.IsToggledProperty, ".", BindingMode.TwoWay);

			// Event switch button setup
			isEvent = (wishlist.is_permanent == "1") ? false : true;
			EventSwitch.BindingContext = isEvent;
			EventSwitch.SetBinding (Switch.IsToggledProperty, ".", BindingMode.TwoWay);

			// Event date picker setup
			string expirationDate = (wishlist.expiration_date != null) ? wishlist.expiration_date : DateTime.Now.Date.ToString();
			eventDate = Convert.ToDateTime (expirationDate);
			EventDatePicker.Date = eventDate;

			if (isEvent)
			{
				EventDateLabel.IsVisible = true;
				EventDatePicker.IsVisible = true;
			}

			// Add friends button setup
			//AddFriendsButton.Text = "This list is visible to " + groupNumber + " friends.";
			await SetVisibilityLabel();
			if (isReserved)
			{
				AddFriendsButton.IsVisible = true;
			}
		}

		private void OnEventSwitchToggled(object sender, EventArgs args)
		{
			Switch toggle = (Switch)sender;

			if (toggle.IsToggled)
			{
				EventDateLabel.IsVisible = true;
				EventDatePicker.IsVisible = true;
				DisableEvent.IsVisible =false;
				ActiveEvent.IsVisible = true;
			}	
			else
			{
				EventDateLabel.IsVisible = false;
				EventDatePicker.IsVisible = false;
				DisableEvent.IsVisible = true;
				ActiveEvent.IsVisible = false;
			}	
		}

		async private Task SetVisibilityLabel()
		{
			var list = new GetWishlistGroupNumberRequest (wishlist.list_id);

			lv.ActivateLoading ();
			string response = await StaticHttpClient.GetInstance.RequestFromApp (RequestType.getWishListGroupNumber, list);
			GroupNumberResponse responseObject;
			JsonUtilities<GroupNumberResponse>.DeserializeToObject (response, out responseObject);

			groupNumber = responseObject.group_numbers;
			AddFriendsButton.Text = groupNumber + " amici invitati.";

			lv.DeactivateLoading ();
		}

        async public override void OnCancelClick()
        {
            base.OnCancelClick();
            await Navigation.PopModalAsync();
        }

        async public override void OnButtonClick()
        {
            base.OnButtonClick();
            await EditWishlistOnDB();
        }
    }
}

