﻿using System;
using System.Collections.Generic;
using CustomRenderer;
using Xamarin.Forms;

namespace UAU
{
	public partial class InviteFriendsPage : CustomPage
	{
		ILoginManager ilm;

		public InviteFriendsPage (ILoginManager ilm)
		{
			InitializeComponent ();

			this.ilm = ilm;
		}

		private void OnStartUsingAppClicked(object sender, EventArgs args)
		{
			ilm.ShowMainPage ();
		}

        private async void InviteMyContactsClicked(object sender, EventArgs args)
        {
            var action = await DisplayActionSheet("Scegli come invitare", "Annulla", null, "WhatsApp", "Email");

			if (action == "Email")
            {
                DependencyService.Get<IActionSheetPopUp>().MailClick();
            }
			if (action == "WhatsApp")
            {
                DependencyService.Get<IActionSheetPopUp>().WhatsappClick();
            }
        }
    }
}

