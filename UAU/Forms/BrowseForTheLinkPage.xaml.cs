﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using System.Diagnostics;
using CustomRenderer;

namespace UAU
{
	public partial class BrowseForTheLinkPage : CustomPage
	{
		private AddWishPage parentPage;
		private string CurrentUrl = "https://www.google.it/#q=";
		private Entry urlEntry;

		public BrowseForTheLinkPage (AddWishPage parentPage, Entry urlEntry)
		{
			InitializeComponent ();

			this.parentPage = parentPage;
			this.urlEntry = urlEntry;

			SetPageToBrowse();

#if __IOS__
            ToolbarItem button = new ToolbarItem();
			button.Text = Constants.cancelLabel;
            button.Order = ToolbarItemOrder.Primary;
            button.Priority = 0;
            button.Clicked += OnCancelButtonClicked;
            this.ToolbarItems.Add(button);

#endif

            Browser.Navigated += OnNavigatedHandler;

//			Browser.Navigating += (object sender, WebNavigatingEventArgs e) => 
//			{
//				if (Uri.IsWellFormedUriString(e.Url, UriKind.Absolute) && !e.Url.StartsWith("file", true, System.Globalization.CultureInfo.CurrentCulture))
//				{
//					CurrentUrl = e.Url;
//				}
//			};
		}

		async private void OnCancelButtonClicked(object sender, EventArgs args)
		{
			await Navigation.PopModalAsync();
		}

		private void OnDoneButtonClicked(object sender, EventArgs args)
		{
			// TODO: togliere?!?!?
		}

		private void OnBackClicked(object sender, EventArgs e)
		{
			if (Browser.CanGoBack) 
			{
				Browser.GoBack ();
			}
		}

		private void OnForwardClicked(object sender, EventArgs e)
		{
			if (Browser.CanGoForward) 
			{
				Browser.GoForward ();
			}
		}

		void OnSetLinkClicked(object sender, EventArgs e)
		{
			urlEntry.Text = CurrentUrl;
			parentPage.CheckEntry(false);
		}

		private void OnNavigatedHandler (object sender, WebNavigatedEventArgs args)
		{
			CurrentUrl = args.Url;
			Console.WriteLine ("Navigated to: " + args.Url);
			SetLinkButton.IsEnabled = true;
		}

		private void webOnNavigating(object sender, WebNavigatingEventArgs e)
		{
			//SetLinkButton.IsEnabled = false;
		}

		//private void webOnEndNavigating(object sender, WebNavigatedEventArgs e)
		//{
		//	SetLinkButton.IsEnabled = true;
		//}

        async public override void OnCancelClick()
        {
            base.OnCancelClick();
            await Navigation.PopModalAsync();
        }

        public override void OnButtonClick()
        {
            base.OnButtonClick();
        }

		private void SetPageToBrowse()
		{
			if (urlEntry.Text != null)
			{
				Browser.Source = CurrentUrl + System.Uri.EscapeDataString(urlEntry.Text);
			}
			else
			{
				Browser.Source = CurrentUrl;
			}
		}
    }
}

