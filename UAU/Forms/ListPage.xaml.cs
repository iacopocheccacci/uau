﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Diagnostics;
using System.Threading.Tasks;
using CustomRenderer;

namespace UAU
{
	public partial class ListPage : CustomPage
	{
		ObservableCollection<ItemData> List;
		WishlistData wishlist;
		string groupNumber;
		bool pageLoaded;
        bool newList = false;

		public ListPage (WishlistData listData, bool newList = false)
		{
			InitializeComponent ();
			wishlist = listData;
			groupNumber = "0";
			pageLoaded = true;
            this.newList = newList;
            if (newList)
            {
                panel.IsVisible = true;
            }


#if __IOS__
            ToolbarItem button = new ToolbarItem();
			button.Text = Constants.settingsLabel;
            button.Order = ToolbarItemOrder.Primary;
            button.Priority = 1;
            button.Clicked += OnSettingsButtonClicked;
            this.ToolbarItems.Add(button);
#endif
        }

        async protected override void OnAppearing()
		{
			base.OnAppearing ();
            Title = null;

			await FillItemsList ();
			await FillWishlistData ();
            if (newList)
            {
                panel.IsVisible = true;
                await AnimatePanel();
            }
        }

		async private Task SetPageData()
		{
			this.Title = wishlist.listName;
			await SetVisibilityLabel ();

			if (wishlist.is_permanent == "0")
			{
				DateTime dt = DateTime.ParseExact (wishlist.expiration_date, "yyyy-MM-dd", CultureInfo.InvariantCulture);
				EventLabel.Text = string.Format("{0}/{1}/{2}", dt.Day, dt.Month, dt.Year);
			}
		}

		async private Task FillWishlistData()
		{
			StartLoading ();

			string response = await StaticHttpClient.GetInstance.RequestFromApp (RequestType.getAllUserLists, StaticHttpClient.GetInstance.currentUser);
			WishlistData[] wishListsList;

			if (response != null)
			{
				JsonUtilities<WishlistData[]>.DeserializeToObject (response, out wishListsList);

				if (wishListsList != null)
				{
					if (wishListsList.Length > 0)
					{
						foreach (WishlistData wishlistData in wishListsList)
						{
							if (wishlistData.is_deleted != null && wishlistData.is_deleted != "1")
							{
								if (wishlistData.listName != null && wishlistData.listName == wishlist.listName)
								{
									wishlist = wishlistData;
									await SetPageData ();
									StopLoading ();
									return;
								}
							}
						}
					}
				}
				else
				{
					await DisplayAlert (Constants.warningMessage, Constants.dataLoadFailedMessage, Constants.okMessage);
				}
			}
			else
			{
				await DisplayAlert (Constants.warningMessage, Constants.dataLoadFailedMessage, Constants.okMessage);
			}

			StopLoading ();
		}

		async private Task FillItemsList()
		{
			List = new ObservableCollection<ItemData> ();
			ItemView.ItemsSource = List;

			StartLoading ();
			string response = await StaticHttpClient.GetInstance.RequestFromApp (RequestType.getWishListItems, wishlist);
			ItemData[] ItemsList;
			JsonUtilities<ItemData[]>.DeserializeToObject (response, out ItemsList);

			if (ItemsList != null)
			{
				if (ItemsList.Length > 0)
				{
					foreach (ItemData item in ItemsList)
					{
						item.photo = Constants.serverUrl + item.photo;
						item.is_new = CheckIfItemIsNew(item.date_created);

						List.Add (item);
					}

					if (wishlist.items_number == "1")
					{
						ItemsNumberLabel.Text = ItemsList.Length.ToString () + " oggetto";
					}
					else
					{
						ItemsNumberLabel.Text = ItemsList.Length.ToString () + " oggetti";
					}
				}
			}
			else
			{
				await DisplayAlert (Constants.warningMessage, Constants.dataLoadFailedMessage, Constants.okMessage);
			}

			// Disable selection
			ItemView.ItemSelected += (sender, e) => {
				((ListView)sender).SelectedItem = null;
			};

			StopLoading ();
		}

		async private void OnSettingsButtonClicked(object sender, EventArgs args)
		{
			if (pageLoaded)
			{
				await Navigation.PushModalAsync (new NavigationPage (new ListSettingsPage (wishlist, groupNumber)));
			}
		}

		async private void OnAddWishClicked(object sender, EventArgs args)
		{
			await Navigation.PushModalAsync (new NavigationPage (new AddWishPage (wishlist)));
		}

		private void OnTextChanged(object sender, EventArgs args)
		{
#if __IOS__
			ItemView.BeginRefresh ();
#endif

			if (string.IsNullOrWhiteSpace (SearchBarField.Text)) 
			{
				ItemView.ItemsSource = List;
			}
			else 
			{
				ItemView.ItemsSource = List
					.Where (x => x.title.ToLower ()
						.Contains (SearchBarField.Text.ToLower ()));
			}

#if __IOS__
			ItemView.EndRefresh ();
#endif
		}

		async private Task SetVisibilityLabel()
		{
			if (wishlist.visibility == "private")
			{
				var list = new GetWishlistGroupNumberRequest (wishlist.list_id);

				StartLoading ();
				string response = await StaticHttpClient.GetInstance.RequestFromApp (RequestType.getWishListGroupNumber, list);
				GroupNumberResponse responseObject;
				JsonUtilities<GroupNumberResponse>.DeserializeToObject (response, out responseObject);

				groupNumber = responseObject.group_numbers;
				VisibilityLabel.Text = "Visibile a " + groupNumber + " amici";	

				StopLoading ();
			}
			else
			{
				VisibilityLabel.Text = "Visibile a tutti";
			}
		}

		async private void OnDelete(object sender, EventArgs args)
		{
			MenuItem item = (MenuItem)sender;
			Debug.WriteLine ("Delete Button pressed on " + item.CommandParameter.ToString());

			var answer = await DisplayAlert (Constants.deleteItemTitle, Constants.deleteItemMessage, Constants.noMessage, Constants.yesMessage);

			if (!answer)
			{
				await DeleteItem (item);
			}
		}

		async private Task DeleteItem(MenuItem item)
		{
			var itemData = new DeleteItemRequest (item.CommandParameter.ToString (), wishlist.list_id, StaticHttpClient.GetInstance.currentUser.uau_id);

			StartLoading ();
			string response = await StaticHttpClient.GetInstance.RequestFromApp (RequestType.deleteItem, itemData);
			DeleteItemResponse responseObject;

			JsonUtilities<DeleteItemResponse>.DeserializeToObject (response, out responseObject);

			if (responseObject != null)
			{
				if (responseObject.IsSucceeded ())
				{
					ItemData list = (from itm in List
						where itm.item_id == item.CommandParameter.ToString ()
						select itm).FirstOrDefault<ItemData> ();

					List.Remove (list);
				}
				else
				{
					await this.DisplayAlert (Constants.warningMessage, responseObject.GetErrorMessage (), Constants.okMessage);
				}
			}
			else
			{
				await DisplayAlert (Constants.warningMessage, Constants.connectionFailedMessage, Constants.okMessage);
			}

            await FillItemsList();
			StopLoading ();
		}

		private void StartLoading()
		{
			lv.ActivateLoading ();
			pageLoaded = false;
		}

		private void StopLoading()
		{
			lv.DeactivateLoading ();
			pageLoaded = true;
		}

        public async override void OnButtonClick()
        {
            base.OnButtonClick();
            if (pageLoaded)
            {
                await Navigation.PushModalAsync(new NavigationPage(new ListSettingsPage(wishlist, groupNumber)));
            }
        }

		async private Task AnimatePanel()
        {
            await Task.Delay(5000);
            var rect = new Rectangle(0,-60,_layout.Width,60);
            await _layout.LayoutTo(rect, 1000);
            newList = false;
            panel.IsVisible = false;
        }

		private bool CheckIfItemIsNew(string itemDateString)
		{
			bool isNew = false;
			DateTime itemDateTime;

			if (DateTime.TryParseExact(itemDateString, "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out itemDateTime))
			{
				if ((DateTime.Now - itemDateTime).Days < 15)
				{
					isNew = true;
				}
			}

			return isNew;
		}

		async private void OnItemSelected(object sender, SelectedItemChangedEventArgs e)
		{
			var item = e.SelectedItem as ItemData;
			if (item != null)
			{
				await Navigation.PushAsync(new MyWishDetailPage(wishlist, item));
			}
		}
    }
}

