﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Threading.Tasks;
using CustomRenderer;
using Xamarin.Forms;

namespace UAU
{
	public partial class ProfilePage : CustomPage
	{
		private float percentageValue = 0;
		private ILoginManager ilm;

		public ProfilePage (ILoginManager ilm)
		{
			InitializeComponent ();

			this.ilm = ilm;

#if __IOS__
			ToolbarItem button = new ToolbarItem();
			button.Text = Constants.editLabel;
			button.Order = ToolbarItemOrder.Primary;
			button.Priority = 1;
			button.Clicked += OnEditButtonClicked;
			this.ToolbarItems.Add(button);
#endif
		}

		async protected override void OnAppearing()
		{
			base.OnAppearing ();
			await FillProfileData ();
			UpdatePercentage();
		}

		private void OnLogoutButtonClicked(object sender, EventArgs args)
		{
			ilm.Logout();
		}

		async private void OnEditButtonClicked(object sender, EventArgs args)
		{
			await Navigation.PushAsync(new EditProfilePage());	
		}

		async private Task FillProfileData()
		{
			var user = StaticHttpClient.GetInstance.currentUser;
			if (user != null)
			{
				if (user.photo != string.Empty)
				{
					string tmp = string.Empty;
					if (!user.photo.Contains ("https://") && !user.photo.Contains ("http://"))
					{
						tmp = Constants.serverUrl;
					}

					tmp += user.photo;

					UserPhoto.Source = tmp;
				}

				if (user.firstName != string.Empty || user.lastName != string.Empty)
				{
					UserName.Text = user.firstName + " " + user.lastName;
				}

				if (user.sex != null)
				{
					switch(user.sex)
					{
						case "m":
							SexLabel.Text = "maschio";
							break;

						case "f":
							SexLabel.Text = "femmina";
							break;

						default:
							SexLabel.Text = "altro";
							break;
					}
				}

				if (user.birthday != null)
				{
//					DateTime dt = DateTime.ParseExact (user.birthday, "yyyy-MM-dd", CultureInfo.InvariantCulture);

					DateTime dt;
					DateTime.TryParseExact(user.birthday, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out dt);
					BirthdayLabel.Text = string.Format("{0} {1}", dt.Day, dt.ToMonthName());
				}

				if (user.city != null)
				{
					CityLabel.Text = user.city;
				}
			}

			int friendsCount = await GetFriendsNumber();
			string friendsNumberString = friendsCount + " Amici";
			FriendsNumberCell.BindingContext = friendsNumberString;
			FriendsNumberCell.SetBinding (Label.TextProperty, ".", BindingMode.TwoWay);

			int circlesCount = await GetCirclesNumber();
			string circlesNumberString = circlesCount + " Gruppi";
			CirclesNumberCell.BindingContext = circlesNumberString;
			CirclesNumberCell.SetBinding (Label.TextProperty, ".", BindingMode.TwoWay);
		}

		async private Task<int> GetFriendsNumber()
		{
			int count = 0;
			var user = new UserFriendsRequestData (StaticHttpClient.GetInstance.currentUser.uau_id);
			string response = await StaticHttpClient.GetInstance.RequestFromApp (RequestType.getUserFriends, user);
			FriendList friendlist;
			JsonUtilities<FriendList>.DeserializeToObject (response, out friendlist);

			if (friendlist.friends != null)
			{
				count = friendlist.friends.Count;
			}

			return count;
		}

		async private Task<int> GetCirclesNumber()
		{
			int count = 0;
			var user = new UserCirclesRequestData (StaticHttpClient.GetInstance.currentUser.uau_id);
			string response = await StaticHttpClient.GetInstance.RequestFromApp (RequestType.getUserCliques, user);
			CirclesList circleslist;
			JsonUtilities<CirclesList>.DeserializeToObject (response, out circleslist);

			if (circleslist.cliques != null)
			{
				count = circleslist.cliques.Count;
			}

			return count;
		}

		async private void OnFriendsClicked(object sender, EventArgs args)
		{
			await Navigation.PushAsync(new FriendsPage());
		}

		async private void OnCirclesClicked(object sender, EventArgs args)
		{
			await Navigation.PushAsync(new MyCirclesPage());
		}

		private void UpdatePercentage()
		{
			int value = 0;
			User user = StaticHttpClient.GetInstance.currentUser;

			if (user.firstName != null && user.firstName != string.Empty) {
				value += 13;
			}

			if (user.lastName != null && user.lastName != string.Empty)
			{
				value += 13;
			}

			if (user.email != null && user.email != string.Empty)
			{
				value += 13;
			}

			if (user.password != null && user.password != string.Empty)
			{
				value += 13;
			}

			if (user.birthday != null && user.birthday != string.Empty)
			{
				value += 12;
			}

			if (user.sex != null && user.sex != string.Empty)
			{
				value += 12;
			}

			if (user.city != null && user.city != string.Empty)
			{
				value += 12;
			}

			if (user.country != null && user.country != string.Empty)
			{
				value += 12;
			}

			percentageValue = value;

			PercentageLabel.Text = percentageValue.ToString() + "% completato";
			PercentageProgressBar.Progress = (percentageValue / 100);
		}

        async public override void OnButtonClick()
        {
            base.OnButtonClick();
            await Navigation.PushAsync(new EditProfilePage());
        }
    }
}

