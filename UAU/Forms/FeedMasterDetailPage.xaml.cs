﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace UAU
{
	public enum DetailPageType
	{
		Feed,
		Profile,
		MyLists,
		PickAGift,
		Settings
	}

	public partial class FeedMasterDetailPage : MasterDetailPage
	{
		ILoginManager ilm;

		public FeedMasterDetailPage (ILoginManager ilm, DetailPageType pageType = DetailPageType.Feed, WishlistData wishlistData = null)
		{
			InitializeComponent ();

			this.ilm = ilm;
			masterPage.ListView.ItemSelected += OnItemSelected;

			if (pageType == DetailPageType.MyLists)
			{
				GoToListPage (wishlistData);
			}
			else
			{
				SetFirstPage (pageType);
			}
		}

		void OnItemSelected(object sender, SelectedItemChangedEventArgs e)
		{
			var item = e.SelectedItem as MasterPageItem;
			if (item != null)
			{
				// TODO: temporary. Do it better
				if (item.TargetType.FullName == "UAU.MyListPage")
				{
					object[] args = { ilm, null };
					Detail = new NavigationPage ((Page)Activator.CreateInstance (item.TargetType, 0, null, args, null));
				}
				else
				{
					object[] args = { ilm };
					Detail = new NavigationPage ((Page)Activator.CreateInstance (item.TargetType, 0, null, args, null));
				}

				masterPage.ListView.SelectedItem = null;
				IsPresented = false;
			}
		}

		private void SetFirstPage(DetailPageType pageType)
		{
			switch (pageType)
			{
			case DetailPageType.MyLists:
				Detail = new NavigationPage (new MyListPage(ilm));
				break;

			case DetailPageType.PickAGift:
				Detail = new NavigationPage (new PickAGift (ilm));
				break;

			case DetailPageType.Profile:
				Detail = new NavigationPage (new ProfilePage (ilm));
				break;

			default:
				Detail = new NavigationPage (new FeedPage(ilm));
				break;
			}
		}

		private void GoToListPage(WishlistData wishlistData)
		{
			Detail = new NavigationPage (new MyListPage(ilm, wishlistData));
		}
	}
}

