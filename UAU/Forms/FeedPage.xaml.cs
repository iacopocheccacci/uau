﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using System.Collections.ObjectModel;
using CustomRenderer;
using System.Globalization;
using System.Threading.Tasks;
using System.Diagnostics;

namespace UAU
{
	public class FriendItems
	{
		public string ImageItem		{ get; set; }
	}

	public partial class FeedPage : CustomPage
	{
		ILoginManager ilm;
        bool firstTime = true;
		ObservableCollection<FeedLayoutData> feedList				{ get; set; }

		public FeedPage (ILoginManager ilm)
		{
			this.ilm = ilm;
			InitializeComponent ();
			InitPage();
			PopulateFeedList ();
		}

		public FeedPage ()
		{
			InitializeComponent ();
			InitPage();
			PopulateFeedList ();
		}

		private void InitPage()
		{
			feedList = new ObservableCollection<FeedLayoutData> ();
			FeedListView.ItemsSource = feedList;

			FeedListView.Refreshing += OnRefresh;
			FeedListView.IsPullToRefreshEnabled = true;
		}

		async private Task PopulateFeedList()
		{
			feedList.Clear();

			// always show create new list button
			feedList.Add (new FeedLayoutData { ItemType = ListItemType.CreateListItem });
			//

			await ShowIncomingBirthdays();

			var user = new FeedRequest (StaticHttpClient.GetInstance.currentUser.uau_id);
			string response = await StaticHttpClient.GetInstance.RequestFromApp (RequestType.getUserFeed, user);

			if (response != null)
			{
				FeedResponse[] responseObject;
				JsonUtilities<FeedResponse[]>.DeserializeToObject (response, out responseObject);

				if (responseObject != null && responseObject.Length > 0)
				{
					foreach (FeedResponse feed in responseObject)
					{
						var feedLayoutData = await GenerateFeedLayout(feed);

						if (feedLayoutData != null)
						{
							feedList.Add (feedLayoutData);
						}
					}
				}
			}

			// Disable selection
			FeedListView.ItemSelected += (sender, e) => {
				((ListView)sender).SelectedItem = null;
			};
		}

		async private void OnRefresh(object sender, EventArgs args)
		{
			Console.WriteLine ("refresh");

			await PopulateFeedList ();
			FeedListView.EndRefresh ();

		}

        async public Task OpenPage(string path)
        {
            if (firstTime)
            {
                firstTime = false;

				// TODO: capire qual'è il problema e togliere delay
                await System.Threading.Tasks.Task.Delay(1000);
				//

                await Navigation.PushAsync(new AddWishFromGallery(path));
            }
        }

		private void OnGoToMyListsClicked(object sender, EventArgs args)
		{
			if (ilm != null)
			{
				ilm.ShowListPage(null);
			}
		}

		async private void OnGoToListClicked(object sender, EventArgs args)
		{
			Button item = (Button)sender;

			if (item != null)
			{
				ItemDataFeed idf = (ItemDataFeed)item.CommandParameter;
				var wishlistData = await GetWishlistDataFromId(idf.WishlistId);
				var userData = await GetFriendContactDataFromUserId(idf.UserId);

				await Navigation.PushAsync(new FriendListDetailPage(wishlistData, idf.UserPhoto, userData.FullName));
			}
		}

		async private void OnGoToListsClicked(object sender, EventArgs args)
		{
			Button item = (Button)sender;

			if (item != null)
			{
				FriendContactData friend = await GetFriendContactDataFromUserId(item.CommandParameter.ToString());

				await Navigation.PushAsync(new FriendListsPage(friend));
			}
		}

		async private Task<FriendContactData> GetFriendContactDataFromUserId(string id)
		{
			FriendContactData friend = new FriendContactData(); 
			friend.Id = id; 

			var user = new GetUserByIdRequest (friend.Id);
			string response = await StaticHttpClient.GetInstance.RequestFromApp (RequestType.getUserById, user);
			if (response != null && response != string.Empty)
			{
				GetUserByIdResponse responseObject;
				JsonUtilities<GetUserByIdResponse>.DeserializeToObject (response, out responseObject);
				if (responseObject != null && responseObject.IsSucceeded())
				{
					friend.FullName = responseObject.user.firstName + " " + responseObject.user.lastName;
					friend.Birthday = FormatBirthdayDate(responseObject.user.birthday);
					friend.Email = responseObject.user.email;
					friend.IsEnabled = true;
					friend.Picture = responseObject.user.photo;
				}
			}

			return friend;
		}

		private string FormatBirthdayDate(string bd)
		{
			string birthday;

			DateTime dt;
			DateTime.TryParseExact(bd, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out dt);
			birthday = string.Format("{0}/{1}/{2}", dt.Day, dt.Month, dt.Year);

			return birthday;
		}

		async private void OnWishDetailClicked(object sender, EventArgs args)
		{
			Button item = (Button)sender;
			ItemDataFeed idf = (ItemDataFeed)item.CommandParameter;
			WishlistData wishlist = new WishlistData();
			ItemData itemData = new ItemData();

			if (idf.WishlistId != string.Empty && idf.ItemId != string.Empty)
			{
				wishlist = await GetWishlistDataFromId(idf.WishlistId);
				itemData = await GetItemDataFromId(idf.WishlistId, idf.ItemId);

				if (itemData != null)
				{
					FriendContactData friend = await GetFriendContactDataFromUserId(itemData.added_by);

					await Navigation.PushAsync(new WishDetailPage(wishlist, itemData, itemData.profilePhoto, friend.FullName));
				}
			}
		}

		async private Task<WishlistData> GetWishlistDataFromId(string list_id)
		{
			WishlistData listData = new WishlistData();
			var list = new GetWishlistInfoRequest (list_id);
			string response = await StaticHttpClient.GetInstance.RequestFromApp (RequestType.getWishlistInfo, list);
			if (response != null && response != string.Empty)
			{
				GetWishlistInfoResponse responseObject;
				JsonUtilities<GetWishlistInfoResponse>.DeserializeToObject (response, out responseObject);
				if (responseObject != null && responseObject.IsSucceeded())
				{
					listData = responseObject.wishlist;
				}
				else
				{
					Debug.WriteLine(responseObject.GetErrorMessage());
				}
			}

			return listData;
		}

		async private Task<ItemData> GetItemDataFromId(string list_id, string item_id)
		{
			ItemData itemData = null;
			var item = new GetWishlistItemDetailRequest (list_id, item_id);
			string response = await StaticHttpClient.GetInstance.RequestFromApp (RequestType.getItemInfo, item);
			if (response != null && response != string.Empty)
			{
				GetWishlistItemDetailsResponse responseObject;
				JsonUtilities<GetWishlistItemDetailsResponse>.DeserializeToObject (response, out responseObject);
				if (responseObject != null && responseObject.IsSucceeded())
				{
					if (responseObject.item.is_deleted != "1")
					{
						itemData = new ItemData();
						itemData = responseObject.item;
						itemData.reservation = await CheckReservationStatus(itemData);

						FriendContactData friend = await GetFriendContactDataFromUserId(itemData.added_by);
						itemData.profilePhoto = GetImageUrl(friend.Picture);

						itemData.photo = GetImageUrl(itemData.photo);
					}
				}
				else
				{
					Debug.WriteLine(responseObject.GetErrorMessage());
				}
			}

			return itemData;
		}

		async private Task<FeedLayoutData> GenerateFeedLayout(FeedResponse item)
		{
			FeedLayoutData layout = null;

			switch (item.GetFeedType())
			{
				case FeedType.kitty_invite:
				case FeedType.kitty_user_subscription:
				case FeedType.friend_wishlist_item_added:
					layout = await MakeLargeItem(item);
					break;

				case FeedType.user_friendship_invite:
					layout = MakeRequestItem(item);
					break;

				case FeedType.user_subscribed_kitty_deleted:
				case FeedType.removed_reserved_item:
				case FeedType.user_wishlist_add_invite:
				case FeedType.fb_user_joined:
					layout = MakeSmallItem(item);
					break;

				case FeedType.friend_wishlist_created:
					layout = await MakeCarouselItem(item);
					break;
			}

			return layout;
		}

		async private Task<FeedLayoutData> MakeLargeItem(FeedResponse item)
		{
			FeedLayoutData result = null;
			bool listButton = item.feed_btn.Contains(Constants.goToList);
			bool wishButton = item.feed_btn.Contains(Constants.wishDetail);
			var idf = new ItemDataFeed();
			idf.WishlistId = item.feed_data.wishlist_id;
			idf.ItemId = item.feed_data.item_id;
			idf.UserPhoto = GetImageUrl(item.feed_data.user_photo);
			idf.UserId = item.feed_data.friend_id;

			ItemData itemData = await GetItemDataFromId(item.feed_data.wishlist_id, item.feed_data.item_id);
			string photoURL = string.Empty;

			if (itemData != null && itemData.is_deleted == "0" && itemData.added_by != "0")
			{
				photoURL = GetImageUrl(itemData.photo);

				result = new FeedLayoutData { ItemType = ListItemType.LargeItem,
											  ImageName = GetImageUrl(item.feed_data.user_photo), 
											  FeedText = item.feed_text, 
											  FeedTime = GetPassedTime(GetFormattedDateTime(item.feed_date)),
											  FriendId = item.feed_data.friend_id,
											  ItemFeed = idf,
											  FeedColor = (item.GetCategory() == FeedCategoryType.spotlight) ? Constants.lightBlue : "White",
											  TextColor = "Black",
											  ImageURL = (photoURL == string.Empty) ? "giftImgBlue.png" : photoURL,
											  GoToListBtn = listButton,
											  GoToWishBtn = wishButton
				};
			}

			return result;
		}

		private FeedLayoutData MakeRequestItem(FeedResponse item)
		{
			FeedLayoutData result = null;

			if (item != null)
			{
				result = new FeedLayoutData { ItemType = ListItemType.RequestItem,
											  ImageName = GetImageUrl(item.feed_data.user_photo), 
											  FeedText = item.feed_text, 
											  FeedTime = GetPassedTime(GetFormattedDateTime(item.feed_date)),
											  FriendId = item.feed_data.friend_id,
											  FeedColor = (item.GetCategory() == FeedCategoryType.spotlight) ? Constants.lightBlue : "White",
											  TextColor = "Black" 
				};
			}

			return result;
		}

		private FeedLayoutData MakeSmallItem(FeedResponse item)
		{
			FeedLayoutData result = null;

			if (item != null)
			{
				var idf = new ItemDataFeed();
				idf.WishlistId = item.feed_data.wishlist_id;
				idf.ItemId = item.feed_data.item_id;
				idf.UserPhoto = GetImageUrl(item.feed_data.user_photo);
				idf.UserId = item.feed_data.friend_id;

				result = new FeedLayoutData { ItemType = ListItemType.SmallItem,
											ImageName = GetImageUrl(item.feed_data.user_photo), 
											FeedText = item.feed_text, 
											FeedTime = GetPassedTime(GetFormattedDateTime(item.feed_date)),
											FriendId = item.feed_data.friend_id,
											ItemFeed = idf,
											FeedColor = (item.GetCategory() == FeedCategoryType.spotlight) ? Constants.lightBlue : "White",
											TextColor = "Black" 
				};
			}

			return result;
		}

		async private Task<FeedLayoutData> MakeCarouselItem(FeedResponse item)
		{
			FeedLayoutData result = null;

			var idf1 = new ItemDataFeed();
			var idf2 = new ItemDataFeed();
			var idf3 = new ItemDataFeed();

			var wishlist = await GetWishlistDataFromId(item.feed_data.wishlist_id);

			if (wishlist.is_deleted == "0")
			{
				string response = await StaticHttpClient.GetInstance.RequestFromApp(RequestType.getWishListItems, wishlist);
				ItemData[] ItemsList;
				JsonUtilities<ItemData[]>.DeserializeToObject(response, out ItemsList);

				string photoURL1 = string.Empty;
				string photoURL2 = string.Empty;
				string photoURL3 = string.Empty;

				if (ItemsList != null)
				{                
					if (ItemsList.Length > 0 && ItemsList[0] != null)
					{
						photoURL1 = GetImageUrl(ItemsList[0].photo);
						idf1.WishlistId = item.feed_data.wishlist_id;
						idf1.ItemId = ItemsList[0].item_id;
						idf1.UserPhoto = GetImageUrl(item.feed_data.user_photo);
					}

					if (ItemsList.Length > 1 && ItemsList[1] != null)
					{
						photoURL2 = GetImageUrl(ItemsList[1].photo);
						idf2.WishlistId = item.feed_data.wishlist_id;
						idf2.ItemId = ItemsList[1].item_id;
						idf2.UserPhoto = GetImageUrl(item.feed_data.user_photo);
					}

					if (ItemsList.Length > 2 && ItemsList[2] != null)
					{
						photoURL3 = GetImageUrl(ItemsList[2].photo);
						idf3.WishlistId = item.feed_data.wishlist_id;
						idf3.ItemId = ItemsList[2].item_id;
						idf3.UserPhoto = GetImageUrl(item.feed_data.user_photo);
					}

					result = new FeedLayoutData { ItemType = ListItemType.CaroselloItem,
												  ImageName = GetImageUrl(item.feed_data.user_photo), 
												  FeedText = item.feed_text,
												  FriendId = item.feed_data.friend_id,
												  FeedTime = GetPassedTime(GetFormattedDateTime(item.feed_date)),
												  ItemFeed1 = idf1,
												  ItemFeed2 = idf2,
												  ItemFeed3 = idf3,
												  ImageURL1 = photoURL1,
												  ImageURL2 = photoURL2,
												  ImageURL3 = photoURL3,
												  Box1Visibility = (photoURL1 != string.Empty) ? true : false,
												  Box2Visibility = (photoURL2 != string.Empty) ? true : false,
												  Box3Visibility = (photoURL3 != string.Empty) ? true : false };
				}
			}

			return result;
		}

		private DateTime GetFormattedDateTime(string dateTimeString)
		{
			DateTime dt = DateTime.Now;
			DateTime.TryParseExact(dateTimeString, "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out dt);

			return dt;
		}

		private string GetPassedTime(DateTime feedDT)
		{
			string timePassed = string.Empty;

			var timeDt = DateTime.Now - feedDT;

			if (timeDt.Days > 0)
			{
				timePassed = timeDt.Days + " " + Constants.daysLabel;
			}
			else if (timeDt.Hours > 0)
			{
				timePassed = timeDt.Hours + " " + Constants.hoursLabel;
			}
			else if (timeDt.Minutes > 0)
			{
				timePassed = timeDt.Minutes + " " + Constants.minutesLabel;
			}
			else
			{
				timePassed = timeDt.Seconds + " " + Constants.secondsLabel; 
			}

			return timePassed;
		}

		private string GetImageUrl(string url)
		{
			if (url != null && url != string.Empty)
			{
				if (!url.Contains("http://") && !url.Contains("https://"))
				{
					url = Constants.serverUrl + url;
				}
			}

			return url;
		}

		async private Task<ReservationType> CheckReservationStatus(ItemData item)
		{
			ReservationType resType = ReservationType.Reservable;

			var reservationRequestData = new CheckReservationRequest(StaticHttpClient.GetInstance.currentUser.uau_id, item.item_id);
			string response = await StaticHttpClient.GetInstance.RequestFromApp(RequestType.checkReservation, reservationRequestData);
			CheckReservationResponse responseObject;
			JsonUtilities<CheckReservationResponse>.DeserializeToObject(response, out responseObject);

			if (responseObject != null)
			{
				if (!responseObject.IsSucceeded())
				{
					resType = responseObject.GetReservationType();

					item.expiration_date = responseObject.expiration_date;
					item.partecipants = responseObject.partecipants;
					item.part_amount = responseObject.part_amount;
				}
			}
			else
			{
				await DisplayAlert(Constants.warningMessage, Constants.connectionFailedMessage, Constants.okMessage);
			}

			return resType;
		}

		async private Task ShowIncomingBirthdays()
		{
			var birthdaysRequestData = new GetFriendsBirthdayRequest(StaticHttpClient.GetInstance.currentUser.uau_id);
			string response = await StaticHttpClient.GetInstance.RequestFromApp(RequestType.getFriendsBirthday, birthdaysRequestData);
			GetFrindsBirthdayResponse responseObject;
			JsonUtilities<GetFrindsBirthdayResponse>.DeserializeToObject(response, out responseObject);

			if (responseObject != null && responseObject.IsSucceeded())
			{
				var idf1 = new ItemDataFeed();
				var idf2 = new ItemDataFeed();
				var idf3 = new ItemDataFeed();

				bool element1Visibility = false;
				bool element2Visibility = false;
				bool element3Visibility = false;

				string photoURL1 = string.Empty;
				string photoURL2 = string.Empty;
				string photoURL3 = string.Empty;

				if (responseObject.friends != null)
				{
					if (responseObject.friends.Count > 0 && responseObject.friends[0] != null)
					{
						idf1.UserPhoto = GetImageUrl(responseObject.friends[0].photo);
						idf1.FirstName = responseObject.friends[0].firstName;
						idf1.LastName = responseObject.friends[0].lastName;
						idf1.UserId = responseObject.friends[0].uau_id;
						photoURL1 = await GetItemImageFromUser(responseObject.friends[0]);

						element1Visibility = true;
					}

					if (responseObject.friends.Count > 1 && responseObject.friends[1] != null)
					{
						idf2.UserPhoto = GetImageUrl(responseObject.friends[1].photo);
						idf2.FirstName = responseObject.friends[1].firstName;
						idf2.LastName = responseObject.friends[1].lastName;
						idf2.UserId = responseObject.friends[1].uau_id;
						photoURL2 = await GetItemImageFromUser(responseObject.friends[1]);

						element2Visibility = true;
					}

					if (responseObject.friends.Count > 2 && responseObject.friends[2] != null)
					{
						idf3.UserPhoto = GetImageUrl(responseObject.friends[2].photo);
						idf3.FirstName = responseObject.friends[2].firstName;
						idf3.LastName = responseObject.friends[2].lastName;
						idf3.UserId = responseObject.friends[2].uau_id;
						photoURL3 = await GetItemImageFromUser(responseObject.friends[2]);

						element3Visibility = true;
					}

					feedList.Add (new FeedLayoutData { ItemType = ListItemType.BirthdayItem,
													   ImageName = "appIconHome.png",
													   FeedText = "Fai un regalo in tempo per i prossimi compleanni dei tuoi amici!",
													   UserPhoto1 = idf1.UserPhoto,
													   UserPhoto2 = idf2.UserPhoto,
													   UserPhoto3 = idf3.UserPhoto,
													   FriendId1 = idf1.UserId,
													   FriendId2 = idf2.UserId,
													   FriendId3 = idf3.UserId,
													   ItemFeed2 = idf2,
													   ItemFeed3 = idf3,
													   UserFirstName1 = idf1.FirstName,
													   UserFirstName2 = idf2.FirstName,
													   UserFirstName3 = idf3.FirstName,
													   UserLastName1 = idf1.LastName,
													   UserLastName2 = idf2.LastName,
													   UserLastName3 = idf3.LastName,
													   Box1Visibility = element1Visibility,
													   Box2Visibility = element2Visibility,
													   Box3Visibility = element3Visibility,
													   ImageURL1 = photoURL1,
													   ImageURL2 = photoURL2,
													   ImageURL3 = photoURL3
					});
					
				}
				else
				{
					await DisplayAlert(Constants.warningMessage, responseObject.GetErrorMessage(), Constants.okMessage);
				}


			}
		}

		async private Task<string> GetItemImageFromUser(User friend)
		{
			string image = "giftImgBlue.png";
			var list = await GetFirstWishlist(friend);

			if (list != null)
			{
				image = await GetFirstItemImage(list);
			}

			return image;
		}

		async private Task<WishlistData> GetFirstWishlist(User friend )
		{
			WishlistData wishlist = null;

			string response = await StaticHttpClient.GetInstance.RequestFromApp(RequestType.getAllUserLists, friend);
			WishlistData[] wishListsList;
			JsonUtilities<WishlistData[]>.DeserializeToObject(response, out wishListsList);

			if (wishListsList != null)
			{
				foreach (WishlistData list in wishListsList)
				{
					int itemsCount;

					if (int.TryParse(list.items_number, out itemsCount))
					{
						if (itemsCount > 0)
						{
							wishlist = list;
							break;
						}
					}
				}
			}

			return wishlist;
		}

		async private Task<string> GetFirstItemImage(WishlistData wishlist)
		{
			string image = string.Empty;

			if (wishlist.is_deleted == "0")
			{
				string response = await StaticHttpClient.GetInstance.RequestFromApp(RequestType.getWishListItems, wishlist);
				ItemData[] ItemsList;
				JsonUtilities<ItemData[]>.DeserializeToObject(response, out ItemsList);

				if (ItemsList != null)
				{
					foreach (ItemData item in ItemsList)
					{
						if (item.photo != null && item.photo != string.Empty)
						{
							image = GetImageUrl(item.photo);
							break;
						}
					}
				}
			}

			return image;
		}
    }
}

