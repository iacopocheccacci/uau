﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using System.Collections.ObjectModel;
using CustomRenderer;
using System.Threading.Tasks;

namespace UAU
{
	public partial class FriendListsPage : CustomPage
	{
		ObservableCollection<WishlistData> Lists;
		FriendContactData friend;

		public FriendListsPage (FriendContactData friend)
		{
			InitializeComponent ();

			this.friend = friend;
			PopulatePage ();
		}

		async private void PopulatePage()
		{
			Title = friend.FullName;
			SetPhoto ();
			FriendName.Text = friend.FullName;
			FriendBirthday.Text = friend.Birthday;

			await PopulateList ();
		}

		private void SetPhoto()
		{
			FriendPhoto.Source = GetPhotoString();
		}

		private string GetPhotoString()
		{
			string photoString = string.Empty;

			if (friend.Picture != string.Empty)
			{
				if (!friend.Picture.Contains ("https://"))
				{
					photoString = Constants.serverUrl;
				}

				photoString += friend.Picture;
			}
			else
			{
				photoString = "avatar.png";
			}

			return photoString;
		}

		async private Task PopulateList()
		{
			Lists = new ObservableCollection<WishlistData> ();
			WishlistsView.ItemsSource = Lists;

			var friendUser = new GetSharedListsRequest (friend.Id, StaticHttpClient.GetInstance.currentUser.uau_id);

			lv.ActivateLoading ();
			string response = await StaticHttpClient.GetInstance.RequestFromApp (RequestType.getSharedLists, friendUser);

			if (response != string.Empty)
			{
				GetSharedListsResponse lists;
				JsonUtilities<GetSharedListsResponse>.DeserializeToObject (response, out lists);

				if (lists.shared_lists != null)
				{
					foreach (WishlistData wishlist in lists.shared_lists)
					{
						if (wishlist.is_deleted != "1")
						{
							wishlist.setReservedIcon ();
							wishlist.setDaysLeft ();
	                        if(wishlist.items_number == null || wishlist.items_number == string.Empty)
	                        {
	                            wishlist.detailText ="0 desideri";
	                        }
	                        else
	                        {
	                            wishlist.detailText = wishlist.items_number + " desideri";
	                        }

	                        if (wishlist.daysLeft != "scaduta")
	                        {
	                            Lists.Add(wishlist);
	                        }
						}
					}
				}
				else
				{
					await DisplayAlert (Constants.warningMessage, Constants.dataLoadFailedMessage, Constants.okMessage);
				}
			}
			else
			{
				await DisplayAlert (Constants.warningMessage, Constants.dataLoadFailedMessage, Constants.okMessage);
			}

			lv.DeactivateLoading ();
		}


		async private void OnItemSelected(object sender, SelectedItemChangedEventArgs e)
		{
			var item = e.SelectedItem as WishlistData;
			if (item != null)
			{
				await GoToListPage (item);
			}

			WishlistsView.SelectedItem = null;
		}

		async private Task GoToListPage(WishlistData list)
		{
			await Navigation.PushAsync (new FriendListDetailPage (list, GetPhotoString(), friend.FullName));
		}
	}
}

