﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using System.Globalization;
using CustomRenderer;
using System.Threading.Tasks;

namespace UAU
{
	public partial class CreateListPage : CustomPage
	{
		ILoginManager ilm;

		public CreateListPage (ILoginManager ilm)
		{
			InitializeComponent ();

#if __IOS__
            ToolbarItem button = new ToolbarItem();
			button.Text = Constants.cancelLabel;
            button.Order = ToolbarItemOrder.Primary;
            button.Priority = 0;
            button.Clicked += OnCancelButtonClicked;
            this.ToolbarItems.Add(button);

            ToolbarItem button2 = new ToolbarItem();
			button2.Text = Constants.nextLabel;
            button2.Order = ToolbarItemOrder.Primary;
            button2.Priority = 1;
            button2.Clicked += OnNextButtonClicked;
            this.ToolbarItems.Add(button2);
#endif

            this.ilm = ilm;
		}

		async private void OnNextButtonClicked(object sender, EventArgs args)
		{
            await Create();
		}

		private void OnListNameChanged(object sender, EventArgs args)
		{
			
		}

		private void OnEventSwitchToggled(object sender, EventArgs args)
		{
			Switch toggle = (Switch)sender;

			if (toggle.IsToggled)
			{
				EventDateLabel.IsVisible = true;
				EventDatePicker.IsVisible = true;
				DisableEvent.IsVisible =false;
				ActiveEvent.IsVisible = true;
			}	
			else
			{
				EventDateLabel.IsVisible = false;
				EventDatePicker.IsVisible = false;
				DisableEvent.IsVisible = true;
				ActiveEvent.IsVisible = false;
			}
		}

		private void OnReserveSwitchToggled(object sender, EventArgs args)
		{
			Switch toggle = (Switch)sender;

			if (toggle.IsToggled)
			{
				ReservedLabel.IsVisible = true;
				PublicLabel.IsVisible = false;
			}
			else
			{
				ReservedLabel.IsVisible = false;
				PublicLabel.IsVisible = true;
			}
		}

		private void OnDateChanged(object sender, EventArgs args)
		{
			
		}

		async public Task SaveWishlistOnDB()
		{
			string isPermanent = EventSwitch.IsToggled ? "0" : "1";
			string isReserved = ReserveSwitch.IsToggled ? "1" : "0";

			var wishlist = new WishlistToAdd (string.Empty,
											  StaticHttpClient.GetInstance.currentUser.uau_id,
				           	                  ListNameEntry.Text,
											  isPermanent,
											  isReserved,
				                              EventDatePicker.Date);

			lv.ActivateLoading ();
			string response = await StaticHttpClient.GetInstance.RequestFromApp (RequestType.createWishlist, wishlist);
			CreateWishListResponse responseObject;

			JsonUtilities<CreateWishListResponse>.DeserializeToObject (response, out responseObject);

			if (responseObject != null)
			{					
				if (responseObject.IsSucceeded ())
				{
					var accepted = await this.DisplayAlert (Constants.wishlistCreationSuccededTitle, Constants.addFirstItemMessage, Constants.noMessage, Constants.yesMessage);

					if (!accepted)
					{
						var wishlistData = new WishlistData ();
						wishlistData.uau_id = wishlist.uau_id;
						wishlistData.expiration_date = wishlist.expiration_date;
						wishlistData.listName = wishlist.listName;
						wishlistData.is_permanent = wishlist.is_permanent;
						wishlistData.list_id = responseObject.status_msg.list_id;
						wishlistData.visibility = wishlist.visibility;

						ilm.ShowListPage (wishlistData);
					}
					else
					{
						ilm.ShowMainPage (DetailPageType.MyLists);
					}
				}
				else
				{
					await this.DisplayAlert (Constants.warningMessage, responseObject.GetErrorMessage (), Constants.okMessage);
					lv.DeactivateLoading ();
				}
			}
			else
			{
				await this.DisplayAlert (Constants.warningMessage, Constants.connectionFailedMessage, Constants.okMessage);
				lv.DeactivateLoading ();
			}
		}

		async private void OnCancelButtonClicked(object sender, EventArgs args)
		{
			await Navigation.PopModalAsync();
		}

        async public override void OnCancelClick()
        {
            base.OnCancelClick();
            await Navigation.PopModalAsync();
        }

        async public override void OnButtonClick()
        {
            base.OnButtonClick();
            await Create();
        }

        async private Task Create()
        {
            if (ListNameEntry.Text == string.Empty)
            {
                await DisplayAlert(Constants.missingListNameTitle, Constants.missingListNameMessage, Constants.okMessage);
            }
            else
            {
                if (ReserveSwitch.IsToggled)
                {
                    var wishlist = new WishlistToAdd(string.Empty,
                                                      StaticHttpClient.GetInstance.currentUser.uau_id,
                                                      ListNameEntry.Text,
                                                      (EventSwitch.IsToggled ? "0" : "1"),
                                                      (ReserveSwitch.IsToggled ? "1" : "0"),
                                                      EventDatePicker.Date);

                    await Navigation.PushAsync(new SelectFriendsPage(ilm, wishlist));
                }
                else
                {
                    await SaveWishlistOnDB();
                }
            }
        }
    }

	static class DateTimeExtensions
	{
		public static string ToMonthName(this DateTime dateTime)
		{
			return CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(dateTime.Month);
		}

		public static string ToShortMonthName(this DateTime dateTime)
		{
			return CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(dateTime.Month);
		}
	}
}

