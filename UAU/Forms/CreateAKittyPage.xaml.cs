﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using CustomRenderer;
using Xamarin.Forms;

namespace UAU
{
	public partial class CreateAKittyPage : CustomPage
	{
		WishlistData wishlist;
		ItemData item;
		bool hasExpiration = false;
		List<UserReservationData> friendsToInviteList;
		bool isDonePressed = false;

		public CreateAKittyPage (WishlistData wishlist, ItemData item, string userPhoto)
		{
			InitializeComponent ();

			this.wishlist = wishlist;
			this.item = item;
			friendsToInviteList = new List<UserReservationData>();

			FillItemDetails(userPhoto);
			InitEntryValues();
			hasExpiration = (wishlist.is_permanent == "0") ? true : false;
			InitPicker();
            
#if __IOS__
            ToolbarItem button = new ToolbarItem();
			button.Text = Constants.cancelLabel;
            button.Order = ToolbarItemOrder.Primary;
            button.Priority = 0;
            button.Clicked += OnCancelButtonClicked;
            this.ToolbarItems.Add(button);

            ToolbarItem button2 = new ToolbarItem();
			button2.Text = Constants.doneLabel;
            button2.Order = ToolbarItemOrder.Primary;
            button2.Priority = 1;
            button2.Clicked += OnDoneButtonClicked;
            this.ToolbarItems.Add(button2);
#endif
        }

		async protected override void OnAppearing()
		{
			base.OnAppearing();

			if (isDonePressed && friendsToInviteList.Count > 0)
			{
				isDonePressed = false;
				await SaveCollect();
			}
		}

		private void InitPicker()
		{
			ExpirationPicker.SelectedIndex = 1;

			if (wishlist.is_permanent == "0" && wishlist.expiration_date != string.Empty)
			{
				DateTime dt = GetDateFromExpirationString(wishlist.expiration_date);

				if ((dt - DateTime.Now).TotalDays > 0)
				{
					string dateString = string.Format("Scadenza ({0}/{1}/{2})", dt.Day, dt.Month, dt.Year);
					ExpirationPicker.Items.Insert(0, dateString);	
				}
			}
		}

        private void FillItemDetails(string userPhoto)
		{
			ItemPhoto.Source = item.photo;
			ItemName.Text = item.title;
			ItemDescription.Text = item.description;
			UserPhoto.Source = userPhoto;
		}

		private void InitEntryValues()
		{
			PartecipantsNumberEntry.Text = "3";
			PartAmountEntry.Text = "25";
		}

		async private void OnCancelButtonClicked(object sender, EventArgs args)
		{
			await Navigation.PopModalAsync();
		}

		async private void OnInviteFriendsClicked(object sender, EventArgs args)
		{
			await Navigation.PushModalAsync(new NavigationPage(new InviteFriendsToKittyPage(wishlist.uau_id, ref friendsToInviteList)));
		}

		async private void OnDoneButtonClicked(object sender, EventArgs args)
		{
			isDonePressed = true;
            await CreateKitty();
		}

		async private Task SaveCollect()
		{
			var expirationDT = DateTime.Now;
			expirationDT = expirationDT.AddDays (GetExpiration());

			var expirationDate = string.Format("{0:yyyy-MM-dd}", expirationDT);

			var reservationRequestData = new ReservationRequest (wishlist.list_id,
			                                                     StaticHttpClient.GetInstance.currentUser.uau_id,
			                                                     item.item_id,
			                                                     expirationDate,
			                                                     wishlist.uau_id,
			                                                     "collection",
			                                                     PartecipantsNumberEntry.Text,
			                                                     PartAmountEntry.Text);

			reservationRequestData.kitty_friends = friendsToInviteList;

			string response = await StaticHttpClient.GetInstance.RequestFromApp (RequestType.reservation, reservationRequestData);
			ReservationResponse responseObject;
			JsonUtilities<ReservationResponse>.DeserializeToObject (response, out responseObject);

			if (responseObject != null)
			{
				if (responseObject.IsSucceeded ())
				{
					item.reservation = ReservationType.MyCollect;
					item.expiration_date = expirationDate;
					item.partecipants = PartecipantsNumberEntry.Text;
					item.part_amount = PartAmountEntry.Text;
					item.daysLeft = GetExpiration().ToString();
					item.daysLabel = Constants.daysLabel;
					await DisplayAlert (Constants.wellDoneMessage, responseObject.GetErrorMessage (), Constants.okMessage);
					await Navigation.PopModalAsync();
				}
			}
			else
			{
				await DisplayAlert (Constants.warningMessage, Constants.connectionFailedMessage, Constants.okMessage);
			}
		}

        async public override void OnButtonClick()
        {
            base.OnButtonClick();
            await CreateKitty();
        }

        async public override void OnCancelClick()
        {
            base.OnCancelClick();
            await Navigation.PopModalAsync();
        }

        async private Task CreateKitty()
        {
			if (friendsToInviteList.Count == 0)
			{
	            var accepted = await DisplayAlert(Constants.inviteFriendsQuestionTitle, Constants.inviteFriendsQuestionMessage, Constants.okMessage, Constants.maybeLaterMessage);

	            if (accepted)
	            {
					await Navigation.PushModalAsync(new NavigationPage(new InviteFriendsToKittyPage(wishlist.uau_id, ref friendsToInviteList)));
	            }
	            else
	            {
	                await SaveCollect();
	            }
			}
			else
			{
				await SaveCollect();
			}
        }

        private int GetExpiration()
        {
			var selectedValue = ExpirationPicker.Items[ExpirationPicker.SelectedIndex] as string;
			int value = 0;

			if (hasExpiration && ExpirationPicker.SelectedIndex == 0)
			{
				DateTime dt = GetDateFromExpirationString(wishlist.expiration_date);

				value = (int)Math.Ceiling((dt - DateTime.Now).TotalDays);
			}
			else
			{
				string resultString = Regex.Match(selectedValue, @"\d+").Value;
				int.TryParse(resultString, out value);
			}

			return value;
        }

		private DateTime GetDateFromExpirationString(string expirationString)
		{
			DateTime dt;
			DateTime.TryParseExact(wishlist.expiration_date, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out dt);

			return dt;
		}
    }
}

