﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using CustomRenderer;
using System.Threading.Tasks;

namespace UAU
{
	public partial class MyListPage : CustomPage
	{
		ObservableCollection<WishlistData> Lists;
		ILoginManager ilm;

		public MyListPage (ILoginManager ilm, WishlistData wishlistData = null)
		{
			InitializeComponent ();

			this.ilm = ilm;

#if __IOS__
            ToolbarItem button = new ToolbarItem();
			button.Text = Constants.addLabel;
            button.Order = ToolbarItemOrder.Primary;
            button.Priority = 1;
            button.Clicked += OnAddListClicked;
            this.ToolbarItems.Add(button);
#endif

            if (wishlistData != null)
			{
				GoToListPage (wishlistData, true);
			}
		}

		async protected override void OnAppearing()
		{
			base.OnAppearing ();
			await PopulateList ();
		}

		async private Task PopulateList()
		{
			Lists = new ObservableCollection<WishlistData> ();
			WishlistsView.ItemsSource = Lists;

			lv.ActivateLoading ();
			string response = await StaticHttpClient.GetInstance.RequestFromApp (RequestType.getAllUserLists, StaticHttpClient.GetInstance.currentUser);
			WishlistData[] wishListsList;
			JsonUtilities<WishlistData[]>.DeserializeToObject (response, out wishListsList);

			if (wishListsList != null)
			{
				foreach (WishlistData wishlist in wishListsList)
				{
					if (wishlist.is_deleted != "1")
					{
						wishlist.setReservedIcon ();
						wishlist.setDaysLeft ();
                        if (wishlist.items_number == null)
                        {
                            wishlist.items_number = "0";
                        }
                        wishlist.detailText = wishlist.items_number + " desideri";
						Lists.Add (wishlist);
					}
				}
			}
			else
			{
				await DisplayAlert (Constants.warningMessage, Constants.dataLoadFailedMessage, Constants.okMessage);
			}

			lv.DeactivateLoading ();
		}

		async private void OnAddListClicked(object sender, EventArgs args)
		{
			await Navigation.PushModalAsync (new NavigationPage(new CreateListPage(ilm)));
		}

		async private void OnDelete(object sender, EventArgs args)
		{
			MenuItem item = (MenuItem)sender;
			Debug.WriteLine ("Delete Button pressed on " + item.CommandParameter.ToString());

			var answer = await DisplayAlert (Constants.deleteWishlistTitle, Constants.deleteWishlistMessage, Constants.noMessage, Constants.yesMessage);

			if (!answer)
			{
				await DeleteListItem (item);
			}
		}

		async private Task DeleteListItem(MenuItem item)
		{
			var wishlist = new WishlistData ();
			wishlist.list_id = item.CommandParameter.ToString ();

			lv.ActivateLoading ();
			string response = await StaticHttpClient.GetInstance.RequestFromApp (RequestType.removeWishlist, wishlist);
			RemoveWishlistResponse responseObject;

			JsonUtilities<RemoveWishlistResponse>.DeserializeToObject (response, out responseObject);

			if (responseObject != null)
			{
				if (responseObject.IsSucceeded ())
				{
					WishlistData list = (from itm in Lists
										 where itm.list_id == item.CommandParameter.ToString ()
										 select itm).FirstOrDefault<WishlistData> ();

					Lists.Remove (list);
				}
				else
				{
					await this.DisplayAlert (Constants.warningMessage, responseObject.GetErrorMessage (), Constants.okMessage);
				}
			}
			else
			{
				await DisplayAlert (Constants.warningMessage, Constants.connectionFailedMessage, Constants.okMessage);
			}

			lv.DeactivateLoading ();
		}

		async private void OnItemSelected(object sender, SelectedItemChangedEventArgs e)
		{
			var item = e.SelectedItem as WishlistData;
			if (item != null)
			{
				await GoToListPage (item, false);
			}

			WishlistsView.SelectedItem = null;
		}

		async private Task GoToListPage(WishlistData wishlistData, bool newList)
		{
			await Navigation.PushAsync (new ListPage (wishlistData, newList));
		}

        async public override void OnButtonClick()
        {
            base.OnButtonClick();
            await Navigation.PushModalAsync(new NavigationPage(new CreateListPage(ilm)));
        }
    }
}

