﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using CustomRenderer;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Xamarin.Forms;

namespace UAU
{
	public partial class EditProfilePage : CustomPage
	{
		private float percentageValue = 0;
		private byte[] ImageRaw = null;

		public EditProfilePage ()
		{
			InitializeComponent ();

			PopulateFields ();
			UpdatePercentage ();

			var tapGestureRecognizer = new TapGestureRecognizer();
			tapGestureRecognizer.Tapped += OnTapGestureRecognizerTapped;
			TouchablePhoto.GestureRecognizers.Add(tapGestureRecognizer);
		}

		private void PopulateFields()
		{
			User user = StaticHttpClient.GetInstance.currentUser;

			if (user.photo != string.Empty)
			{
				string tmp = string.Empty;
				if (!user.photo.Contains ("https://") && !user.photo.Contains ("http://"))
				{
					tmp = Constants.serverUrl;
				}

				tmp += user.photo;

				UserPhoto.Source = tmp;
			}

			if (user.firstName != null)
			{
				FirstNameEntry.Text = user.firstName;
			}

			if (user.lastName != null)
			{
				LastNameEntry.Text = user.lastName;
			}

			if (user.birthday != null)
			{
				DateTime dt = Convert.ToDateTime (user.birthday);
				BirthDayEntry.Date = dt;
			}

			if (user.email != null)
			{
				EmailEntry.Text = user.email;
			}

			if (user.sex != null)
			{
				GenderEntry.SelectedIndex = GetIndexFromText (user.sex);
			}

			if (user.city != null)
			{
				CityEntry.Text = user.city;
			}

			if (user.country != null)
			{
				CountryEntry.Text = user.country;
			}
		}

		private void OnTextChanged(object sender, EventArgs e)
		{

		}

		async private void OnConfirmClicked(object sender, EventArgs e)
		{
			UserEdit user = SetUserDataToSend();
			lv.ActivateLoading();
			string response = await StaticHttpClient.GetInstance.RequestFromApp (RequestType.updateProfile, user);
			UpdateProfileResponse responseObject;
			JsonUtilities<UpdateProfileResponse>.DeserializeToObject (response, out responseObject);

			if (responseObject != null)
			{
				if (responseObject.IsSucceeded())
				{
					SaveUserData(responseObject);
					await DisplayAlert(Constants.wellDoneMessage, Constants.profileUpdatedeMessage, Constants.okMessage);
					await Navigation.PopAsync();
				}
				else
				{
					await DisplayAlert(Constants.warningMessage, responseObject.GetErrorMessage(), Constants.okMessage);
				}
			}
			else
			{
				await DisplayAlert(Constants.warningMessage, Constants.dataLoadFailedMessage, Constants.okMessage);
			}

			lv.DeactivateLoading();
		}

		private UserEdit SetUserDataToSend()
		{
			UserEdit newUserData = new UserEdit(StaticHttpClient.GetInstance.currentUser.uau_id,
												FirstNameEntry.Text,
			                                    LastNameEntry.Text,
			                                    string.Format("{0:yyyy-MM-dd}", BirthDayEntry.Date),
			                                    EmailEntry.Text,
			                                    getGenderTypeFromIndex(GenderEntry.SelectedIndex).ToString(),
			                                    CityEntry.Text,
			                                    CountryEntry.Text,
			                                    ImageRaw);

			return newUserData;
		}

		private void SaveUserData(UpdateProfileResponse response)
		{
			var currentUser = StaticHttpClient.GetInstance.currentUser;

			if (response.firstName != null)
			{
				currentUser.firstName = response.firstName;
			}

			if (response.lastName != null)
			{
				currentUser.lastName = response.lastName;
			}

			if (response.birthday != null)
			{
				currentUser.birthday = response.birthday;
			}

			if (response.sex != null)
			{
				currentUser.sex = response.sex;
			}

			if (response.country != null)
			{
				currentUser.country = response.country;
			}

			if (response.city != null)
			{
				currentUser.city = response.city;
			}

			if (response.photo != null)
			{
				currentUser.photo = response.photo;
			}
		}

		private GenderEnum getGenderTypeFromIndex(int index)
		{
			GenderEnum result;

			switch (index)
			{
				case 0:
				result = GenderEnum.m;
				break;

				case 1:
				result = GenderEnum.f;
				break;

				case 2:
				default:
				result = GenderEnum.o;
				break;
			}

			return result;
		}

		private int GetIndexFromText(string gender)
		{
			if (gender == "m")
				return 0;

			if (gender == "f")
				return 1;

			return 2;
		}

		private void UpdatePercentage()
		{
			int value = 0;
			User user = StaticHttpClient.GetInstance.currentUser;

			if (user.firstName != null && user.firstName != string.Empty)
			{
				value += 13;
			}

			if (user.lastName != null && user.lastName != string.Empty)
			{
				value += 13;
			}

			if (user.email != null && user.email != string.Empty)
			{
				value += 13;
			}

			if (user.password != null && user.password != string.Empty)
			{
				value += 13;
			}

			if (user.birthday != null && user.birthday != string.Empty)
			{
				value += 12;
			}

			if (user.sex != null && user.sex != string.Empty)
			{
				value += 12;
			}

			if (user.city != null && user.city != string.Empty)
			{
				value += 12;
			}

			if (user.country != null && user.country != string.Empty)
			{
				value += 12;
			}

			percentageValue = value;

			PercentageLabel.Text = percentageValue.ToString() + "% completato";
			PercentageProgressBar.Progress = (percentageValue / 100);
		}

		async void OnTapGestureRecognizerTapped(object sender, EventArgs args)
		{
			await PickPictureAsync ();
		}

		public async Task<string> PickPictureAsync() 
		{
			MediaFile file = null;
			string imageName = string.Empty;

			try 
			{
//				lv.ActivateLoading();
				file = await CrossMedia.Current.PickPhotoAsync();

				if(file == null) 
				{ 
					return null; 
				}

				UserPhoto.Source = ImageSource.FromStream(file.GetStream);

				using (var memoryStream = new MemoryStream())
				{
					file.GetStream().CopyTo(memoryStream);
					file.Dispose();
					ImageRaw = memoryStream.ToArray();
				}
			}
			catch(Exception ex) 
			{
				Debug.WriteLine("\nIn PictureViewModel.PickPictureAsync() - Exception:\n{0}\n", ex);
//				lv.DeactivateLoading ();
				return null;
			}
			finally 
			{ 
//				lv.DeactivateLoading ();

				if(file != null) 
				{ 
					file.Dispose(); 
				} 
			}

//			lv.DeactivateLoading ();

			return imageName;
		}
	}
}

