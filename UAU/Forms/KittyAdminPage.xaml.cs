﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using CustomRenderer;
using Xamarin.Forms;

namespace UAU
{
	public partial class KittyAdminPage : CustomPage
	{
		WishlistData wishlist;
		ItemData item;
		bool hasExpiration = false;
		ObservableCollection<FriendContactData> partecipantsData;
		List<UserReservationData> friendsToInviteList;

		public KittyAdminPage (WishlistData wishlist, ItemData item, string userPhoto)
		{
			InitializeComponent ();

			this.wishlist = wishlist;
			this.item = item;
			this.friendsToInviteList = new List<UserReservationData>();
			this.hasExpiration = (wishlist.is_permanent == "0") ? true : false;

			FillItemDetails(userPhoto);
			InitEntryValues();
			InitExpirationLabel();
			InitPicker();
			PopulatePartecipantsList();

#if __IOS__
            ToolbarItem button = new ToolbarItem();
			button.Text = Constants.cancelLabel;
            button.Order = ToolbarItemOrder.Primary;
            button.Priority = 0;
            button.Clicked += OnCancelButtonClicked;
            this.ToolbarItems.Add(button);

            ToolbarItem button2 = new ToolbarItem();
			button2.Text = Constants.doneLabel;
            button2.Order = ToolbarItemOrder.Primary;
            button2.Priority = 1;
            button2.Clicked += OnDoneButtonClicked;
            this.ToolbarItems.Add(button2);
#endif
        }

        private void FillItemDetails(string userPhoto)
		{
			ItemPhoto.Source = item.photo;
			ItemName.Text = item.title;
			ItemDescription.Text = item.description;
			UserPhoto.Source = userPhoto;
		}

		private void InitEntryValues()
		{
			// TODO: get data from DB
			PartecipantsNumberEntry.Text = item.partecipants;
			PartAmountEntry.Text = item.part_amount;
		}

		private void InitExpirationLabel()
		{
			ExpirationLabel.Text = "La tua colletta scade tra " + item.daysLeft + " giorni";
		}

		private void InitPicker()
		{
			ExpirationPicker.SelectedIndex = 1;

			if (wishlist.is_permanent == "0" && wishlist.expiration_date != string.Empty)
			{
				DateTime dt = GetDateFromExpirationString(wishlist.expiration_date);

				if ((dt - DateTime.Now).TotalDays > 0)
				{
					string dateString = string.Format("Scadenza ({0}/{1}/{2})", dt.Day, dt.Month, dt.Year);
					ExpirationPicker.Items.Insert(0, dateString);	
				}
			}
		}

		async private void OnCancelButtonClicked(object sender, EventArgs args)
		{
			await Navigation.PopModalAsync();
		}

		async private void OnDoneButtonClicked(object sender, EventArgs args)
		{
			await Done();
		}

		async private void OnCancelTheKittyButtonClicked(object sender, EventArgs args)
		{
			var deleteReservationRequestData = new DeleteReservationRequest (StaticHttpClient.GetInstance.currentUser.uau_id,
			                                                             item.item_id);

			string response = await StaticHttpClient.GetInstance.RequestFromApp (RequestType.deleteReservation, deleteReservationRequestData);
			DeleteReservationResponse responseObject;
			JsonUtilities<DeleteReservationResponse>.DeserializeToObject (response, out responseObject);

			if (responseObject != null)
			{
				if (!responseObject.IsSucceeded ())
				{
					await DisplayAlert (Constants.wellDoneMessage, responseObject.GetErrorMessage (), Constants.okMessage);
				}
				else
				{
					item.reservation = ReservationType.Reservable;
					await Navigation.PopModalAsync();
				}
			}
			else
			{
				await DisplayAlert (Constants.warningMessage, Constants.connectionFailedMessage, Constants.okMessage);
			}
		}

        async public override void OnCancelClick()
        {
            base.OnCancelClick();
            await Navigation.PopModalAsync();
        }

		async private Task PopulatePartecipantsList()
		{
			partecipantsData = new ObservableCollection<FriendContactData> ();
			PartecipantsListView.ItemsSource = partecipantsData;

			var collectedItem = new getUsersCollectionInfoRequest (item.item_id);
			string response = await StaticHttpClient.GetInstance.RequestFromApp (RequestType.getUsersCollectionInfo, collectedItem);
			getUsersCollectionInfoResponse responseObject;
			JsonUtilities<getUsersCollectionInfoResponse>.DeserializeToObject (response, out responseObject);

			if (responseObject != null && responseObject.users_info != null)
			{
				PartecipantsNumeberLabel.Text = responseObject.users_info.Count.ToString() + " amici partecipano alla colletta";

				int iPartecipants;
				if (int.TryParse(item.partecipants, out iPartecipants))
				{
					int missingPartecipantsNumber = int.Parse(item.partecipants) - responseObject.users_info.Count;
					MissingPartecipantsNumberLabel.Text = "Servono altri " + missingPartecipantsNumber + " partecipanti per chiudere la colletta"; 

					foreach (User friend in responseObject.users_info)
					{
						int listNumber = await GetVisibleLists (friend.uau_id);

						partecipantsData.Add (new FriendContactData { FullName = friend.firstName + " " + friend.lastName,
							Id = friend.uau_id,
							Picture = friend.photo,
							Details = (listNumber > 0) ? "Ha " + listNumber + " liste" : "Nessuna lista"
						});
					}
				}
			}
			else
			{
				await DisplayAlert (Constants.warningMessage, Constants.dataLoadFailedMessage, Constants.okMessage);
			}
		}

		async private Task<int> GetVisibleLists(string friend_id)
		{
			var user = new GetSharedListsRequest (friend_id, StaticHttpClient.GetInstance.currentUser.uau_id);
			string response = await StaticHttpClient.GetInstance.RequestFromApp (RequestType.getSharedLists, user);
			GetSharedListsResponse lists;
			JsonUtilities<GetSharedListsResponse>.DeserializeToObject (response, out lists);

			return lists.shared_lists.Count;
		}

		async private void OnInviteFriendsClicked(object sender, EventArgs args)
		{
			await Navigation.PushModalAsync(new NavigationPage(new InviteFriendsToKittyPage(wishlist.uau_id, ref friendsToInviteList)));	
		}

		private int GetExpiration()
		{
			var selectedValue = ExpirationPicker.Items[ExpirationPicker.SelectedIndex] as string;
			int value = 0;

			if (hasExpiration && ExpirationPicker.SelectedIndex == 0)
			{
				DateTime dt = GetDateFromExpirationString(wishlist.expiration_date);

				value = (int)Math.Ceiling((dt - DateTime.Now).TotalDays);
			}
			else
			{
				string resultString = Regex.Match(selectedValue, @"\d+").Value;
				int.TryParse(resultString, out value);
			}

			return value;
		}

		private DateTime GetDateFromExpirationString(string expirationString)
		{
			DateTime dt;
			DateTime.TryParseExact(wishlist.expiration_date, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out dt);

			return dt;
		}

		public void SetDaysLeft(ItemData item)
		{
			DateTime today = DateTime.Now;
			DateTime expiration;
			if (DateTime.TryParseExact(item.expiration_date, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out expiration))
			{
				double differenceInDays = (expiration - today).TotalDays;
				double ceilingDifferenceInDays = Math.Ceiling(differenceInDays);

				if (ceilingDifferenceInDays < 0)
				{
					item.daysLeft = "0";
				}
				else if (ceilingDifferenceInDays > 1)
				{
					item.daysLeft = ceilingDifferenceInDays.ToString();
					item.daysLabel = Constants.daysLabel;
				}
				else
				{
					item.daysLeft = ceilingDifferenceInDays.ToString();
					item.daysLabel = Constants.dayLabel;
				}
			}
		}

        async public override void OnButtonClick()
        {
            base.OnButtonClick();
            await Done();
        }

        async private Task Done()
        {
			var expirationDT = DateTime.Now;
			int daysToAdd = GetExpiration();

			if (daysToAdd > 0)
			{
				expirationDT = expirationDT.AddDays (daysToAdd);
				item.expiration_date = string.Format("{0:yyyy-MM-dd}", expirationDT);
				SetDaysLeft(item);
			}

			var editCollectionRequestData = new EditCollectionRequest (StaticHttpClient.GetInstance.currentUser.uau_id,
			                                                           item.item_id,
			                                                           item.expiration_date,
			                                                           PartecipantsNumberEntry.Text,
			                                                           PartAmountEntry.Text);

			string response = await StaticHttpClient.GetInstance.RequestFromApp (RequestType.editCollection, editCollectionRequestData);
			EditCollectionResponse responseObject;
			JsonUtilities<EditCollectionResponse>.DeserializeToObject (response, out responseObject);

			if (responseObject != null)
			{
				if (!responseObject.IsSucceeded ())
				{
					await DisplayAlert (Constants.warningMessage, responseObject.GetErrorMessage (), Constants.okMessage);
				}
				else
				{
					await DisplayAlert (Constants.wellDoneMessage, responseObject.reservation_msg, Constants.okMessage);
					item.partecipants = PartecipantsNumberEntry.Text;
					item.part_amount = PartAmountEntry.Text;
					await Navigation.PopModalAsync();
				}
			}
			else
			{
				await DisplayAlert (Constants.warningMessage, Constants.connectionFailedMessage, Constants.okMessage);
			}
        }
    }
}

