﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CustomRenderer;
using Xamarin.Forms;

namespace UAU
{
	public partial class MyWishDetailPage : CustomPage
	{
		ItemData item;
		string userPhoto;
		string userName;

		public MyWishDetailPage (WishlistData wishlist, ItemData item)
		{
			InitializeComponent ();

			this.item = item;
			this.userPhoto = GetImageUrl(StaticHttpClient.GetInstance.currentUser.photo);
			this.userName = StaticHttpClient.GetInstance.currentUser.firstName + StaticHttpClient.GetInstance.currentUser.lastName;

			if (userPhoto != null)
			{
				FillItemDetails();
			}
		}

		private void FillItemDetails()
		{
			if (item != null)
			{
				ItemPhoto.Source = item.photo;
				if(ItemPhoto.Height < 200)
				{
					ItemPhoto.HeightRequest = 200;
				}

				ItemName.Text = item.title;
				ItemDescription.Text = item.description;
				UserPhoto.Source = userPhoto;
				UseraName.Text = userName;
			}
		}

		private string GetImageUrl(string url)
		{
			if (url != null && url != string.Empty)
			{
				if (!url.Contains("http://") && !url.Contains("https://"))
				{
					url = Constants.serverUrl + url;
				}
			}

			return url;
		}

		async private void OnDelete(object sender, EventArgs args)
		{
			await DeleteItem (item);
		}

		async private Task DeleteItem(ItemData itemData)
		{
			string response = await StaticHttpClient.GetInstance.RequestFromApp (RequestType.deleteItem, itemData);
			DeleteItemResponse responseObject;

			JsonUtilities<DeleteItemResponse>.DeserializeToObject (response, out responseObject);

			if (responseObject != null)
			{
				if (responseObject.IsSucceeded ())
				{
					await Navigation.PopAsync();
				}
				else
				{
					await this.DisplayAlert (Constants.warningMessage, responseObject.GetErrorMessage (), Constants.okMessage);
				}
			}
			else
			{
				await DisplayAlert (Constants.warningMessage, Constants.connectionFailedMessage, Constants.okMessage);
			}
		}
	}
}

