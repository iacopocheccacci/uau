﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using System.Threading.Tasks;
using System.Diagnostics;

namespace UAU
{
	public partial class LoadingPage : ContentPage
	{
		private ILoginManager ilm;

		public LoadingPage (ILoginManager ilm)
		{
			InitializeComponent ();
			this.ilm = ilm;
			AutoLogin ();
		}

		async Task AutoLogin()
		{
			LoginRequestData loginData = new LoginRequestData ();
			var properties = App.CurrentApp.Properties;

			loginData.email = properties.ContainsKey(Constants.userEmailProperty) ? (string)properties[Constants.userEmailProperty] : string.Empty;
			loginData.password = properties.ContainsKey(Constants.userPasswordProperty) ? (string)properties[Constants.userPasswordProperty] : string.Empty;

			string response = await StaticHttpClient.GetInstance.RequestFromApp (RequestType.login, loginData);

			if (response != string.Empty)
			{
				CustomLoginResponse responseObject;
				JsonUtilities<CustomLoginResponse>.DeserializeToObject (response, out responseObject);

				if (responseObject != null)
				{
					if (responseObject.IsSucceeded ()/* || responseObject.s == "false"*/)
					{
						await StaticHttpClient.GetInstance.SaveUserData (responseObject.user);

						if (StaticHttpClient.GetInstance.CheckIfFirstAccessFlagIsOn ())
						{
							ilm.ShowWelcomeNavigationPage ();
						}
						else
						{
							ilm.ShowMainPage ();
						}
					}
					else
					{
						await DisplayAlert (Constants.warningMessage, responseObject.GetErrorMessage (), Constants.okMessage);
						Debug.WriteLine ("Fail to login");
						ilm.ShowLoginPage ();
					}
				}
				else
				{
					await DisplayAlert (Constants.warningMessage, Constants.connectionFailedMessage, Constants.okMessage);
				}
			}
			else
			{
				await DisplayAlert (Constants.warningMessage, Constants.connectionFailedMessage, Constants.okMessage);
				ilm.ShowLoginPage ();
			}
		}
	}
}

