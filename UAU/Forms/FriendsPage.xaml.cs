﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using CustomRenderer;
using Xamarin.Forms;

namespace UAU
{
	public partial class FriendsPage : CustomPage
	{
		ObservableCollection<FriendContactData> friendsData;

		public FriendsPage ()
		{
			InitializeComponent ();

			#if __IOS__
			ToolbarItem button = new ToolbarItem();
			button.Text = Constants.addLabel;
			button.Order = ToolbarItemOrder.Primary;
			button.Priority = 1;
			button.Clicked += OnAddFriendsButtonClicked;
			this.ToolbarItems.Add(button);
			#endif
		}

		async protected override void OnAppearing()
		{
			base.OnAppearing ();

			await PopulateList();
		}

		async private Task PopulateList()
		{
			friendsData = new ObservableCollection<FriendContactData> ();
			FriendsView.ItemsSource = friendsData;

			var user = new UserFriendsRequestData (StaticHttpClient.GetInstance.currentUser.uau_id);
			lv.ActivateLoading ();
			string response = await StaticHttpClient.GetInstance.RequestFromApp (RequestType.getUserFriends, user);
			FriendList friendlist;
			JsonUtilities<FriendList>.DeserializeToObject (response, out friendlist);

			if (friendlist.friends != null)
			{
				foreach (User friend in friendlist.friends)
				{
					//DateTime dt = DateTime.ParseExact (friend.birthday, "yyyy-MM-dd", CultureInfo.InvariantCulture);
					//var birthday = string.Format("Birthday: {0} {1}", dt.Day, dt.ToMonthName());
					int listNumber = await GetVisibleLists (friend.uau_id);

					friendsData.Add (new FriendContactData { FullName = friend.firstName + " " + friend.lastName,
						Id = friend.uau_id,
						Picture = friend.photo,
						Details = (listNumber > 0) ? "Ha " + listNumber + " liste" : "Nessuna lista"
						//Birthday = birthday
					});
                    if (lv.IsRunning())
                    {
                        lv.DeactivateLoading();
                    }
                }
			}
			else
			{
				await DisplayAlert (Constants.warningMessage, Constants.dataLoadFailedMessage, Constants.okMessage);
                if (lv.IsRunning())
                {
                    lv.DeactivateLoading();
                }
            }

            if (lv.IsRunning())
            {
                lv.DeactivateLoading();
            }
        }

		async private void OnAddFriendsButtonClicked(object sender, EventArgs args)
		{
			// non posso metterla su una modale altrimenti non funziona lo share su FB (??)
			await Navigation.PushAsync(new AddFriendsToUserPage());
		}

		private void OnTextChanged(object sender, EventArgs args)
		{
			#if __IOS__
			FriendsView.BeginRefresh ();
			#endif 

			if (string.IsNullOrWhiteSpace (SearchBarField.Text)) 
			{
				FriendsView.ItemsSource = friendsData;
			}
			else 
			{
				FriendsView.ItemsSource = friendsData
					.Where (x => x.FullName.ToLower ()
					                              .Contains (SearchBarField.Text.ToLower ()));
			}

			#if __IOS__
			FriendsView.EndRefresh ();
			#endif
		}

		async private void OnDelete(object sender, EventArgs args)
		{
			MenuItem friend = (MenuItem)sender;
			Debug.WriteLine ("Delete Button pressed on " + friend.CommandParameter.ToString());

			var answer = await DisplayAlert (Constants.deleteWishlistTitle, Constants.deleteFriendMessage, Constants.noMessage, Constants.yesMessage);

			if (!answer)
			{
				await DeleteListItem (friend);
			}
		}

		async private void OnItemSelected(object sender, SelectedItemChangedEventArgs e)
		{
			var item = e.SelectedItem as FriendContactData;
			if (item != null)
			{
				await GoToFriendPage (item);
			}

			FriendsView.SelectedItem = null;
		}

		async private Task GoToFriendPage(FriendContactData friend)
		{
			await Navigation.PushAsync (new FriendListsPage (friend));
		}

		async private Task<int> GetVisibleLists(string friend_id)
		{
			var user = new GetSharedListsRequest (friend_id, StaticHttpClient.GetInstance.currentUser.uau_id);
			string response = await StaticHttpClient.GetInstance.RequestFromApp (RequestType.getSharedLists, user);
			GetSharedListsResponse lists;
			JsonUtilities<GetSharedListsResponse>.DeserializeToObject (response, out lists);

			return lists.shared_lists.Count;
		}

		async private Task DeleteListItem (MenuItem friend)
		{
			var request = new DeleteUserFromFriendsRequest (friend.CommandParameter.ToString(), StaticHttpClient.GetInstance.currentUser.uau_id);

			lv.ActivateLoading ();
			string response = await StaticHttpClient.GetInstance.RequestFromApp (RequestType.deleteUserFromFriends, request);
			DeleteUserFromFriendsResponse responseObject;

			JsonUtilities<DeleteUserFromFriendsResponse>.DeserializeToObject (response, out responseObject);

			if (responseObject != null)
			{
				if (responseObject.IsSucceeded ()) 
				{
					FriendContactData user = (from usr in friendsData
					                          where usr.Id == friend.CommandParameter.ToString ()
					                          select usr).FirstOrDefault<FriendContactData> ();

					friendsData.Remove (user);
				} 
				else 
				{
					await this.DisplayAlert (Constants.warningMessage, responseObject.GetErrorMessage (), Constants.okMessage);
				}
			}
			else
			{
				await DisplayAlert (Constants.warningMessage, Constants.connectionFailedMessage, Constants.okMessage);
			}

			lv.DeactivateLoading ();
		}

		async public override void OnButtonClick()
		{
			base.OnButtonClick();
			await Navigation.PushAsync(new AddFriendsToUserPage());
		}
	}
}

