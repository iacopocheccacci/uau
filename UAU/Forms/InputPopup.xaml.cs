﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using CustomRenderer;

namespace UAU
{
	public partial class InputPopup : PopupPage
	{
		PopupParentPageInterface ParentPage;

		public InputPopup (PopupParentPageInterface parent)
		{
			InitializeComponent ();

			ParentPage = parent;
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();
		}

		protected override void OnDisappearing()
		{
			base.OnDisappearing();
		}

		protected override bool OnBackButtonPressed()
		{
			// Prevent hide popup
			//return base.OnBackButtonPressed();
			return true; 
		}

		async private void OnCancelClicked(object sender, EventArgs args)
		{
			await PopupNavigation.PopAsync ();
		}

		async private void OnConfirmClicked(object sender, EventArgs args)
		{
			await PopupNavigation.PopAsync ();
			await ParentPage.SaveClique (NameEntry.Text);
		}
	}
}

