﻿using System;
using System.Collections.Generic;
using CustomRenderer;
using Xamarin.Forms;
using System.Diagnostics;

namespace UAU
{
	public partial class AddFriendsToUserPage : CustomPage
	{
		public AddFriendsToUserPage ()
		{
			InitializeComponent ();
        }

        async private void OnSearchByNameOrMailClicked(object sender, EventArgs args)
		{
            await Navigation.PushAsync(new SearchForFriends());

        }

		private async void GetAccessToMyContactsClicked(object sender, EventArgs args)
		{
            await Navigation.PushAsync(new ConnectWithFriendsPage(null, false));
        }

		private async void InviteMyContactsClicked(object sender, EventArgs args)
		{
            var action = await DisplayActionSheet("Choose how to invite", "Cancel", null, "What's app", "Mail");

            if (action == "Mail")
            {
                DependencyService.Get<IActionSheetPopUp>().MailClick();
            }
            if(action == "What's app")
            {
                DependencyService.Get<IActionSheetPopUp>().WhatsappClick();
            } 
        }
	}
}

