﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using CustomRenderer;
using Xamarin.Forms;

namespace UAU
{
	public partial class OffertsPage : CustomPage
	{
		ObservableCollection<ShopItemData> DealList;
		string searchName;

		public OffertsPage (string searchName)
		{
			InitializeComponent ();
			this.searchName = searchName;

#if __IOS__
			ToolbarItem button = new ToolbarItem();
			button.Text = Constants.cancelLabel;
			button.Order = ToolbarItemOrder.Primary;
			button.Priority = 0;
			button.Clicked += OnCancelButtonClicked;
			this.ToolbarItems.Add(button);
#endif

			FillDealList();
		}

		async private Task FillDealList ()
		{
			DealList = new ObservableCollection<ShopItemData> ();
			DealListView.ItemsSource = DealList;

			var requestData = new AmazonMarketPlaceProductsRequest ("it", searchName);

			lv.ActivateLoading ();
			string response = await StaticHttpClient.GetInstance.RequestFromApp (RequestType.amazonMarketPlaceProducts, requestData);
			AmazonMarketPlaceProductsResponse responseObject;
			JsonUtilities<AmazonMarketPlaceProductsResponse>.DeserializeToObject (response, out responseObject);

			if (responseObject != null && responseObject.IsSucceeded()) 
			{
				if (responseObject.products.Count > 0)
				{
					DealsNumberLabel.Text =  responseObject.products.Count + " offerte trovate";
					foreach (ShopItemData shopItem in responseObject.products)
					{
						FormatPrice(shopItem);
						DealList.Add (shopItem);
						lv.DeactivateLoading();
					}
				}
                else
                {
					DealsNumberLabel.Text = "Nessuna offerta trovata";
                    DealsFoundLabel.IsVisible = false;
                }
			}
			else 
			{
				await DisplayAlert (Constants.warningMessage, Constants.dataLoadFailedMessage, Constants.okMessage);
			}

			lv.DeactivateLoading();
		}

		private void FormatPrice(ShopItemData shopItem)
		{
			if (shopItem.OfferSummary.LowestNewPrice != null)
			{
				string currency = shopItem.OfferSummary.LowestNewPrice.CurrencyCode;

				var amount = float.Parse(shopItem.OfferSummary.LowestNewPrice.Amount);

				switch (currency)
				{
					case "EUR":
					currency = "€";
					break;

					case "USD":
					currency = "$";
					break;
				}


				amount /= 100;
				shopItem.OfferSummary.LowestNewPrice.FormattedPrice = currency + " " + amount;
			}
		}

		private void OnItemSelected(object sender, SelectedItemChangedEventArgs e)
		{
			var shopItem = e.SelectedItem as ShopItemData;
			if (shopItem != null)
			{
				Device.OpenUri(new Uri(shopItem.DetailPageURL));
			}
		}

		async private void OnCancelButtonClicked(object sender, EventArgs args)
		{
			await PopPage ();
		}

		async public override void OnCancelClick()
		{
			base.OnCancelClick();
			await PopPage();
		}

		async private Task  PopPage()
		{
			await Navigation.PopModalAsync();
		}
	}
}

