﻿using System;
using System.Collections.Generic;
using CustomRenderer;
using Xamarin.Forms;

namespace UAU
{
	public partial class GetMoreFriendsPage : CustomPage
	{
		ILoginManager ilm;

		public GetMoreFriendsPage (ILoginManager ilm)
		{
			InitializeComponent ();

			this.ilm = ilm;
		}

		async private void OnGoOnClicked(object sender, EventArgs args)
		{
			await Navigation.PushAsync (new InviteFriendsPage (ilm));
		}

		async private void OnFindFriendsClicked(object sender, EventArgs args)
		{
			await Navigation.PushAsync (new ConnectWithFriendsPage (ilm));
		}
	}
}

