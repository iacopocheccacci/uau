﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Diagnostics;
using System.Threading.Tasks;
using CustomRenderer;

namespace UAU
{
	public partial class PickAGift : CustomPage
	{
		ObservableCollection<FriendContactData> friendsData;

		public PickAGift (ILoginManager ilm)
		{
			InitializeComponent ();
			PopulateList();
        }

        protected override void OnAppearing()
		{
			base.OnAppearing ();

			// TODO: refresh list when friend added
		}

		async private Task PopulateList()
		{
			friendsData = new ObservableCollection<FriendContactData> ();
			FriendsView.ItemsSource = friendsData;

			var user = new UserFriendsRequestData (StaticHttpClient.GetInstance.currentUser.uau_id);
			lv.ActivateLoading ();
			string response = await StaticHttpClient.GetInstance.RequestFromApp (RequestType.getUserFriends, user);
			FriendList friendlist;
			JsonUtilities<FriendList>.DeserializeToObject (response, out friendlist);

			if (friendlist.friends != null)
			{
				foreach (User friend in friendlist.friends)
				{
					DateTime dt = DateTime.ParseExact (friend.birthday, "yyyy-MM-dd", CultureInfo.InvariantCulture);
					var birthday = string.Format("Compleanno: {0} {1}", dt.Day, dt.ToMonthName());
					int listNumber = await GetVisibleLists (friend.uau_id);

					friendsData.Add (new FriendContactData { FullName = friend.firstName + " " + friend.lastName,
															 Id = friend.uau_id,
															 Picture = friend.photo,
															 Details = (listNumber > 0) ? "Ha " + listNumber + " liste" : "Nessuna lista",
															 Birthday = birthday
					});
                    if (lv.IsRunning())
                    {
                        lv.DeactivateLoading();
                    }
                }
			}
			else
			{
				await DisplayAlert (Constants.warningMessage, Constants.dataLoadFailedMessage, Constants.okMessage);
                if (lv.IsRunning())
                {
                    lv.DeactivateLoading();
                }
            }
            if (lv.IsRunning())
            {
                lv.DeactivateLoading();
            }
		}

		private void OnTextChanged(object sender, EventArgs args)
		{
			#if __IOS__
			FriendsView.BeginRefresh ();
			#endif 

			if (string.IsNullOrWhiteSpace (SearchBarField.Text)) 
			{
				FriendsView.ItemsSource = friendsData;
			}
			else 
			{
				FriendsView.ItemsSource = friendsData
										  .Where (x => x.FullName.ToLower ()
										  .Contains (SearchBarField.Text.ToLower ()));
			}

			#if __IOS__
			FriendsView.EndRefresh ();
			#endif
		}

		async private void OnItemSelected(object sender, SelectedItemChangedEventArgs e)
		{
			var item = e.SelectedItem as FriendContactData;
			if (item != null)
			{
				await GoToFriendPage (item);
			}

			FriendsView.SelectedItem = null;
		}

		async private Task GoToFriendPage(FriendContactData friend)
		{
			await Navigation.PushAsync (new FriendListsPage (friend));
		}

		async private Task<int> GetVisibleLists(string friend_id)
		{
			var user = new GetSharedListsRequest (friend_id, StaticHttpClient.GetInstance.currentUser.uau_id);
			string response = await StaticHttpClient.GetInstance.RequestFromApp (RequestType.getSharedLists, user);
			GetSharedListsResponse lists;
			JsonUtilities<GetSharedListsResponse>.DeserializeToObject (response, out lists);

			return lists.shared_lists.Count;
		}
    }
}

