﻿using System;
using System.Collections.Generic;
using CustomRenderer;
using Xamarin.Forms;

namespace UAU
{
	public partial class MenuPage : ContentPage
	{
		public UnscrollableListView ListView { get { return listView; } }

		public MenuPage ()
		{
			InitializeComponent ();

            FileImageSource icon = new FileImageSource();
            icon.File = "menu.png";
            Icon = icon;


			var masterPageItems = new List<MasterPageItem> ();
            masterPageItems.Add(new MasterPageItem {
                Title = "Primo piano",
                IconSource = "homeIcon.png",
                TargetType = typeof(FeedPage),
                Index = 0
			});
			masterPageItems.Add (new MasterPageItem {
				Title = "Profilo",
				IconSource = "profileIcon.png",
				TargetType = typeof(ProfilePage),
                Index = 1
			});
			masterPageItems.Add (new MasterPageItem {
				Title = "Le mie liste",
				IconSource = "myListIcon.png",
				TargetType = typeof(MyListPage),
                Index = 2
			});
			masterPageItems.Add (new MasterPageItem {
				Title = "Fai un regalo",
				IconSource = "pickGiftIcon.png",
				TargetType = typeof(PickAGift),
                Index = 3
			});
						
			listView.ItemsSource = masterPageItems;
		}

        protected override void OnAppearing()
        {
            base.OnAppearing();     
        }
    }
}

