﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using CustomRenderer;
using Xamarin.Forms;

namespace UAU
{
	public partial class AddFriendsToCirclePage : CustomPage
	{
		CircleData circle;
		List<User> friends;
		ObservableCollection<FriendContactData> friendsData;

		public AddFriendsToCirclePage (CircleData circle, List<User> friends)
		{
			InitializeComponent ();

			this.circle = circle;
			this.friends = friends;

			PopulateList();

#if __IOS__
			ToolbarItem button = new ToolbarItem();
			button.Text = Constants.addLabel;
			button.Order = ToolbarItemOrder.Primary;
			button.Priority = 1;
			button.Clicked += OnAddButtonClicked;
			this.ToolbarItems.Add(button);
#endif
		}

		async private Task PopulateList()
		{
			friendsData = new ObservableCollection<FriendContactData> ();
			FriendsView.ItemsSource = friendsData;

			var user = new UserFriendsRequestData (StaticHttpClient.GetInstance.currentUser.uau_id);
			lv.ActivateLoading ();
			string response = await StaticHttpClient.GetInstance.RequestFromApp (RequestType.getUserFriends, user);
			FriendList friendlist;
			JsonUtilities<FriendList>.DeserializeToObject (response, out friendlist);

			if (friendlist.friends != null)
			{
				foreach (User friend in friendlist.friends)
				{
					if (!IsInCircle(friend.uau_id) && friend.uau_id != StaticHttpClient.GetInstance.currentUser.uau_id)
					{
						DateTime dt = DateTime.ParseExact (friend.birthday, "yyyy-MM-dd", CultureInfo.InvariantCulture);
						var birthday = string.Format("Compleanno: {0} {1}", dt.Day, dt.ToMonthName());

						friendsData.Add (new FriendContactData { FullName = friend.firstName + " " + friend.lastName,
																 Id = friend.uau_id,
																 Picture = friend.photo,
																 IsEnabled = true,
																 Birthday = birthday
						});
					}
				}
			}
			else
			{
				await DisplayAlert (Constants.warningMessage, Constants.dataLoadFailedMessage, Constants.okMessage);
			}

			// Disable selection
			FriendsView.ItemSelected += (sender, e) => {

				((ListView)sender).SelectedItem = null;
			};

			lv.DeactivateLoading ();
		}

		private void OnTextChanged(object sender, EventArgs args)
		{
#if __IOS__
			FriendsView.BeginRefresh ();
#endif 

			if (string.IsNullOrWhiteSpace (SearchBarField.Text)) 
			{
				FriendsView.ItemsSource = friendsData;
			}
			else 
			{
				FriendsView.ItemsSource = friendsData
					.Where (x => x.FullName.ToLower ()
					        .Contains (SearchBarField.Text.ToLower ()));
			}

#if __IOS__
			FriendsView.EndRefresh ();
#endif
		}

		private bool IsInCircle(String id)
		{
			var userFriend = friends.Find(item => item.uau_id == id);

			return (userFriend != null) ? true : false;
		}

		private void OnUnselectAllClicked(object sender, EventArgs args)
		{
			bool selected;

			if (SelectAllButton.Text == Constants.unselectLabel)
			{
				selected = false;
				SelectAllButton.Text = Constants.selectLabel;
			}
			else
			{
				selected = true;
				SelectAllButton.Text = Constants.unselectLabel;
			}

			for (int i = 0; i < friendsData.Count; i++)
			{
				FriendContactData tmp = friendsData [i];
				tmp.IsEnabled = selected;
				friendsData [i] = tmp;
			}
		}

		private void OnAddButtonClicked(object sender, EventArgs args)
		{
            AddFriends();
		}

		private List<FriendData> GetSelectedFriends()
		{
			var friendsDataList = new List<FriendData> ();

			foreach (FriendContactData f in FriendsView.ItemsSource)
			{
				if (f.IsEnabled)
				{
					FriendData data = new FriendData ();
					data.id = f.Id;
					data.name = f.FullName;

					friendsDataList.Add (data);
				}
			}

			return friendsDataList;
		}

        async private void AddFriends()
        {
            var friendsDataList = GetSelectedFriends();

            var request = new AddFriendsRequest();
            request.cliqueName = circle.Name;
            request.friends = friendsDataList;
            request.owner_id = StaticHttpClient.GetInstance.currentUser.uau_id;

            lv.ActivateLoading();
            string response = await StaticHttpClient.GetInstance.RequestFromApp(RequestType.saveClique, request);

            if (response != string.Empty)
            {
                ResponseObject responseObject;
                JsonUtilities<ResponseObject>.DeserializeToObject(response, out responseObject);

                if (responseObject != null)
                {
                    if (responseObject.IsSucceeded())
                    {
						await DisplayAlert (Constants.circleSavedTitle, Constants.circleEditMessage, Constants.okMessage);
                    }
                    else
                    {
                        await DisplayAlert(Constants.warningMessage, Constants.connectionFailedMessage, Constants.okMessage);
                    }
                }
                else
                {
                    await DisplayAlert(Constants.warningMessage, Constants.dataLoadFailedMessage, Constants.okMessage);
                }
            }
            else
            {
                await DisplayAlert(Constants.warningMessage, Constants.connectionFailedMessage, Constants.okMessage);
            }

            lv.DeactivateLoading();

            await Navigation.PopAsync();
        }

        public override void OnButtonClick()
        {
            base.OnButtonClick();
            AddFriends();
        }
    }
}

