﻿using System;
using Xamarin.Forms;
using System.Collections.ObjectModel;
using System.Linq;
using Rg.Plugins.Popup.Services;
using System.Diagnostics;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Globalization;
using CustomRenderer;

namespace UAU
{		
	public partial class AddFriendsPage : CustomPage, PopupParentPageInterface
	{
		ObservableCollection<FriendContactData> friendsData;
		List<FriendContactData> existingFriends;
		ILoginManager ilm = null;
		WishlistToAdd wishlist;
		bool isEditing;
        List<string> friendsId;

		public AddFriendsPage (ILoginManager ilm, WishlistToAdd wishlist, bool isEditing = false)
		{
			InitializeComponent ();
			
#if __IOS__
            ToolbarItem button = new ToolbarItem();
			button.Text = Constants.nextLabel;
            button.Order = ToolbarItemOrder.Primary;
            button.Priority = 1;
            button.Clicked += OnNextButtonClicked;
            this.ToolbarItems.Add(button);
#endif

            if (ilm != null)
			{
				this.ilm = ilm;
			}

			this.wishlist = wishlist;
			this.isEditing = isEditing;  
        }

        async protected override void OnAppearing()
        {
            base.OnAppearing();

            await PopulateList();
        }

		async private Task PopulateList()
		{
			await GetWishlistFriends();

			friendsData = new ObservableCollection<FriendContactData> ();
			existingFriends = new List<FriendContactData>();
			FriendsView.ItemsSource = friendsData;

			var user = new UserFriendsRequestData (StaticHttpClient.GetInstance.currentUser.uau_id);
			lv.ActivateLoading ();
			string response = await StaticHttpClient.GetInstance.RequestFromApp (RequestType.getUserFriends, user);
			FriendList friendlist;
			JsonUtilities<FriendList>.DeserializeToObject (response, out friendlist);

			if (friendlist.friends != null)
			{
				foreach (User friend in friendlist.friends)
				{
					DateTime dt = DateTime.ParseExact (friend.birthday, "yyyy-MM-dd", CultureInfo.InvariantCulture);
					var birthday = string.Format("Compleanno: {0} {1}", dt.Day, dt.ToMonthName());

                    if (!friendsId.Contains(friend.uau_id))
	                {                    
						friendsData.Add (new FriendContactData { FullName = friend.firstName + " " + friend.lastName,
							Id = friend.uau_id,
							Picture = friend.photo,
							IsEnabled = false,
							Birthday = birthday
						});
	                    
						if (lv.IsRunning())
	                    {
	                        lv.DeactivateLoading();
	                    }
					}
					else
					{
						existingFriends.Add (new FriendContactData { FullName = friend.firstName + " " + friend.lastName,
							Id = friend.uau_id,
							Picture = friend.photo,
							IsEnabled = false,
							Birthday = birthday
						});

						if (lv.IsRunning())
						{
							lv.DeactivateLoading();
						}
					}
                }
			}
			else
			{
				await DisplayAlert (Constants.warningMessage, Constants.dataLoadFailedMessage, Constants.okMessage);
                if (lv.IsRunning())
                {
                    lv.DeactivateLoading();
                }
            }

			// Disable selection
			FriendsView.ItemSelected += (sender, e) => {
				((ListView)sender).SelectedItem = null;
			};

            if (lv.IsRunning())
            {
                lv.DeactivateLoading();
            }
        }

		private void OnTextChanged(object sender, EventArgs args)
		{
#if __IOS__
			FriendsView.BeginRefresh ();
#endif 

			if (string.IsNullOrWhiteSpace (SearchBarField.Text)) 
			{
				FriendsView.ItemsSource = friendsData;
			}
			else 
			{
				FriendsView.ItemsSource = friendsData
										  .Where (x => x.FullName.ToLower ()
										  .Contains (SearchBarField.Text.ToLower ()));
			}

			#if __IOS__
			FriendsView.EndRefresh ();
			#endif
		}

		async private void OnNextButtonClicked(object sender, EventArgs args)
		{
			await Next();
		}

		async private Task<bool> SaveWishlistOnDB()
		{
			bool result = false;
			lv.ActivateLoading ();
			string response = await StaticHttpClient.GetInstance.RequestFromApp (RequestType.createWishlist, wishlist);
			CreateWishListResponse responseObject;

			JsonUtilities<CreateWishListResponse>.DeserializeToObject (response, out responseObject);

			if (responseObject != null)
			{
				if (responseObject.IsSucceeded ())
				{
					wishlist.list_id = responseObject.status_msg.list_id;
					result = true;
				}
				else
				{
					await this.DisplayAlert (Constants.warningMessage, responseObject.GetErrorMessage (), Constants.okMessage);
					lv.DeactivateLoading ();
				}
			}
			else
			{
				await DisplayAlert (Constants.warningMessage, Constants.dataLoadFailedMessage, Constants.okMessage);
				lv.DeactivateLoading ();
			}

			return result;
		}

		async private Task<bool> EditWishlistOnDB()
		{
			bool result = false;
			lv.ActivateLoading ();
			wishlist.reserved = "true";

			string response = await StaticHttpClient.GetInstance.RequestFromApp (RequestType.editList, wishlist);
			EditListResponse responseObject;

			JsonUtilities<EditListResponse>.DeserializeToObject (response, out responseObject);

			if (responseObject != null)
			{
				if (responseObject.IsSucceeded ())
				{
					result = true;
				}
				else
				{
					await this.DisplayAlert (Constants.warningMessage, responseObject.GetErrorMessage (), Constants.okMessage);
					lv.DeactivateLoading ();
				}
			}
			else
			{
				await DisplayAlert (Constants.warningMessage, Constants.dataLoadFailedMessage, Constants.okMessage);
				lv.DeactivateLoading ();
			}

			return result;
		}

		async private Task<bool> SaveFriendGroup()
		{
			bool result = false;
			var friendsToAdd = new FriendsToAddList (wishlist.listName, 
													 StaticHttpClient.GetInstance.currentUser.uau_id, 
													 GetSelectedFriends(true),
													 isEditing);
			
			lv.ActivateLoading ();
			string response = await StaticHttpClient.GetInstance.RequestFromApp (RequestType.addFriendsToList, friendsToAdd);
			AddFriendsListResponse responseObject;

			JsonUtilities<AddFriendsListResponse>.DeserializeToObject (response, out responseObject);

			if (responseObject != null)
			{
				if (responseObject.IsSucceeded ())
				{
					result = true;
				}
				else
				{
					await this.DisplayAlert (Constants.warningMessage, Constants.friendGroupCreationErrorMessage, Constants.okMessage);
					lv.DeactivateLoading ();
				}
			}
			else
			{
				await DisplayAlert (Constants.warningMessage, Constants.selectFriendsMessage, Constants.okMessage);
				lv.DeactivateLoading ();
			}

		
			return result;
		}

		private void OnUnselectAllClicked(object sender, EventArgs args)
		{
			bool selected;

			if (SelectAllButton.Text == Constants.unselectLabel)
			{
				selected = false;
				SelectAllButton.Text = Constants.selectLabel;
			}
			else
			{
				selected = true;
				SelectAllButton.Text = Constants.unselectLabel;
			}

			for (int i = 0; i < friendsData.Count; i++)
			{
				FriendContactData tmp = friendsData [i];
				tmp.IsEnabled = selected;
				friendsData [i] = tmp;
			}
		}

		async public Task SaveClique(string name)
		{
			Debug.WriteLine ("Save clique " + name);

			var friends = GetSelectedFriends();
			await SendNewCircleRequest (friends, name);
		}

		private List<FriendData> GetSelectedFriends(bool addExisting = false)
		{
			var friends = new List<FriendData> ();

			if (addExisting)
			{
				foreach (FriendContactData existingFriend in existingFriends)
				{
					FriendData data = new FriendData ();
					data.id = existingFriend.Id;
					data.name = existingFriend.FullName;

					friends.Add (data);
				}
			}

			foreach (FriendContactData friend in FriendsView.ItemsSource)
			{
				if (friend.IsEnabled)
				{
					FriendData data = new FriendData ();
					data.id = friend.Id;
					data.name = friend.FullName;

					friends.Add (data);
				}
			}

			return friends;
		}

		async private Task SendNewCircleRequest(List<FriendData> friends, string circleName)
		{
			var request = new AddFriendsRequest ();
			request.cliqueName = circleName;
			request.friends = friends;
			request.owner_id = StaticHttpClient.GetInstance.currentUser.uau_id;

			lv.ActivateLoading ();
			string response = await StaticHttpClient.GetInstance.RequestFromApp (RequestType.saveClique, request);

			if (response != string.Empty)
			{
				ResponseObject responseObject;
				JsonUtilities<ResponseObject>.DeserializeToObject (response, out responseObject);

				if (responseObject != null)
				{
					if (responseObject.IsSucceeded ())
					{
						await DisplayAlert (Constants.circleSavedTitle, Constants.circleSavedMessage, Constants.okMessage);
					}
					else
					{
						await DisplayAlert (Constants.warningMessage, Constants.connectionFailedMessage, Constants.okMessage);
					}
				}
				else
				{
					await DisplayAlert (Constants.warningMessage, Constants.dataLoadFailedMessage, Constants.okMessage);
				}
			}
			else
			{
				await DisplayAlert (Constants.warningMessage, Constants.connectionFailedMessage, Constants.okMessage);
			}

            if (lv.IsRunning())
            {
                lv.DeactivateLoading();
            }
        }

        async public override void OnButtonClick()
        {
            base.OnButtonClick();
            await Next();
        }

		async Task Next()
        {
            bool res = true;

            if (!isEditing)
            {
                // Save list only if not in edit mode
                res = await SaveWishlistOnDB();
            }
            else
            {
                // Edit mode
                res = await EditWishlistOnDB();
            }

            if (res)
            {
                if (await SaveFriendGroup())
                {
                    if (isEditing)
                    {
                        await Navigation.PopAsync();
                    }
                    else
                    {
                        var accepted = await this.DisplayAlert(Constants.wishlistCreationSuccededTitle, Constants.addFirstItemMessage, Constants.noMessage, Constants.yesMessage);
                        lv.DeactivateLoading();

                        if (!accepted)
                        {
                            var wishlistData = new WishlistData();
                            wishlistData.listName = wishlist.listName;
							wishlistData.list_id = wishlist.list_id;
							wishlistData.uau_id = wishlist.uau_id;
							wishlistData.expiration_date = wishlist.expiration_date;
							wishlistData.is_permanent = wishlist.is_permanent;
							wishlistData.visibility = wishlist.visibility;

                            if (ilm != null)
                            {
                                ilm.ShowListPage(wishlistData);
                            }

                        }
                        else
                        {
                            if (ilm != null)
                            {
                                ilm.ShowMainPage(DetailPageType.MyLists);
                            }
                        }
                    }
                }
            }
        }

        async private Task GetWishlistFriends()
        {
            var user = new GetWishListFriendsRequest(wishlist.list_id,StaticHttpClient.GetInstance.currentUser.uau_id);
            lv.ActivateLoading();
            string response = await StaticHttpClient.GetInstance.RequestFromApp(RequestType.getWishListFriends, user);
            GetWishListFriendsResponse friendlist;
            JsonUtilities<GetWishListFriendsResponse>.DeserializeToObject(response, out friendlist);
            friendsId = new List<string>();

			if (friendlist != null && friendlist.wishlist_friends != null)
            {
                foreach (User friend in friendlist.wishlist_friends)
                {
                    friendsId.Add(friend.uau_id);
                }
            }
        }
    }
}

