﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;
using CustomRenderer;

namespace UAU
{
	public partial class LoadCirclePage : CustomPage
	{
		ObservableCollection<CircleData> circlesData;
		ILoginManager ilm = null;
		WishlistToAdd wishlist;
        bool newList = true;

		public LoadCirclePage (ILoginManager ilm, WishlistToAdd wishlist, bool newList = true)
		{
			InitializeComponent ();
			PopulateList ();
            this.newList = newList;

#if __IOS__
			ToolbarItem button = new ToolbarItem();
			button.Text = Constants.cancelLabel;
			button.Order = ToolbarItemOrder.Primary;
			button.Priority = 0;
			button.Clicked += OnCancelButtonClicked;
			this.ToolbarItems.Add(button);

            ToolbarItem button1 = new ToolbarItem();
			button1.Text = Constants.doneLabel;
			button1.Order = ToolbarItemOrder.Primary;
			button1.Priority = 1;
			button1.Clicked += OnDoneButtonClicked;
			this.ToolbarItems.Add(button1);
#endif

            if (ilm != null)
			{
				this.ilm = ilm;
			}

			this.wishlist = wishlist;
		}

		async private Task PopulateList()
		{
			circlesData = new ObservableCollection<CircleData> ();
			CircleView.ItemsSource = circlesData;

			var user = new UserCirclesRequestData (StaticHttpClient.GetInstance.currentUser.uau_id);
			lv.ActivateLoading ();
			string response = await StaticHttpClient.GetInstance.RequestFromApp (RequestType.getUserCliques, user);
			CirclesList circleslist;
			JsonUtilities<CirclesList>.DeserializeToObject (response, out circleslist);

			if (circleslist.cliques != null)
			{
				foreach (Circle circle in circleslist.cliques)
				{
					if (circle.name != "contacts")		// Hide main circle
					{
						circlesData.Add (new CircleData { Name = circle.name, 
														  CircleId = circle.clique_id});
					}
				}
			}
			else
			{
				await DisplayAlert (Constants.warningMessage, Constants.dataLoadFailedMessage, Constants.okMessage);
			}

			lv.DeactivateLoading ();
		}

		private void OnDoneButtonClicked(object sender, EventArgs args)
		{
            Done();
		}

		async private Task<bool> SaveWishlistOnDB()
		{
			bool result = false;
			lv.ActivateLoading ();
			string response = await StaticHttpClient.GetInstance.RequestFromApp (RequestType.createWishlist, wishlist);
			CreateWishListResponse responseObject;

			JsonUtilities<CreateWishListResponse>.DeserializeToObject (response, out responseObject);

			if (responseObject != null)
			{
				if (responseObject.IsSucceeded ())
				{
					result = true;
				}
				else
				{
					await this.DisplayAlert (Constants.warningMessage, responseObject.GetErrorMessage (), Constants.okMessage);
					lv.DeactivateLoading ();
				}
			}
			else
			{
				await DisplayAlert (Constants.warningMessage, Constants.dataLoadFailedMessage, Constants.okMessage);
				lv.DeactivateLoading ();
			}

			return result;
		}

		async private Task<bool> SaveFriendGroup(CircleData circle)
		{
			bool res = false;

			var cliqueData = new GetCliqueFriendsRequest (StaticHttpClient.GetInstance.currentUser.uau_id, circle.CircleId);
			lv.ActivateLoading ();
			string response = await StaticHttpClient.GetInstance.RequestFromApp (RequestType.getCliqueFriends, cliqueData);
			GetCliqueFriendsResponse friendlist;
			JsonUtilities<GetCliqueFriendsResponse>.DeserializeToObject (response, out friendlist);

			if (friendlist.clique_friends != null)
			{
				var friendsToAdd = new FriendsToAddList (wishlist.listName, 
					StaticHttpClient.GetInstance.currentUser.uau_id, 
					GetFriendsInCircle(friendlist.clique_friends),
					false);

				response = await StaticHttpClient.GetInstance.RequestFromApp (RequestType.addFriendsToList, friendsToAdd);
				AddFriendsListResponse responseObject;

				JsonUtilities<AddFriendsListResponse>.DeserializeToObject (response, out responseObject);

				if (responseObject != null)
				{
					if (responseObject.IsSucceeded ())
					{
						res = true;
					}
					else
					{
						await this.DisplayAlert (Constants.warningMessage, responseObject.GetErrorMessage(), Constants.okMessage);
						lv.DeactivateLoading ();
					}
				}
				else
				{
					await DisplayAlert (Constants.warningMessage, Constants.dataLoadFailedMessage, Constants.okMessage);
					lv.DeactivateLoading ();
				}

			}
			else
			{
				await DisplayAlert (Constants.warningMessage, Constants.dataLoadFailedMessage, Constants.okMessage);
				lv.DeactivateLoading ();
			}

			lv.DeactivateLoading ();

			return res;
		}

		private List<FriendData> GetFriendsInCircle(List<User> userList)
		{
			var friends = new List<FriendData> ();

			foreach (User friend in userList)
			{
				if (friend != null)
				{
					FriendData data = new FriendData ();
					data.id = friend.uau_id;
					data.name = friend.firstName + " " + friend.lastName;

					friends.Add (data);
				}
			}

			return friends;
		}
			
		async private void OnCancelButtonClicked(object sender, EventArgs args)
		{
			await Navigation.PopModalAsync ();
		}

		async public override void OnCancelClick()
		{
			base.OnCancelClick();
			await Navigation.PopModalAsync();
		}

        public override void OnButtonClick()
        {
            base.OnButtonClick();
            Done();
        }

        async private void Done()
        {
            bool res = true;

            if (CircleView.SelectedItem != null)
            {
                var circle = (CircleData)CircleView.SelectedItem;

                if (circle != null)
                {
                    if (newList)
                    {
                        res = await SaveWishlistOnDB();

                        if (res)
                        {
                            if (await SaveFriendGroup(circle))
                            {
                                var accepted = await this.DisplayAlert(Constants.wishlistCreationSuccededTitle, Constants.addFirstItemMessage, Constants.noMessage, Constants.yesMessage);
                                lv.DeactivateLoading();

                                if (!accepted)
                                {
                                    var wishlistData = new WishlistData();
                                    wishlistData.listName = wishlist.listName;

                                    if (ilm != null)
                                    {
                                        ilm.ShowListPage(wishlistData);
                                    }

                                }
                                else
                                {
                                    if (ilm != null)
                                    {
                                        ilm.ShowMainPage(DetailPageType.MyLists);
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        await SaveFriendGroup(circle);
                        await Navigation.PopModalAsync();
                    }
                }
            }
        }
    }
}

