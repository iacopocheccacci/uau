﻿using CustomRenderer;
using Plugin.Media.Abstractions;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace UAU
{
	public partial class AddWishFromGallery : CustomPage
	{
        private byte[] ImageRaw;
        private PopupPage page;

		public AddWishFromGallery (string path)
		{
			InitializeComponent ();
            FileStream file = new FileStream(path,FileMode.OpenOrCreate);
            if (file != null)
            {
                using (BinaryReader br = new BinaryReader(file))
                {
                    ImageRaw = br.ReadBytes((int)file.Length);
                }
                PhotoImage.Source = ImageSource.FromStream(() => new MemoryStream(ImageRaw));
            }
        }

        async private Task OpenPopupPage(PopupPage page)
        {
            await PopupNavigation.PushAsync(page);
        }

        async public override void OnButtonClick()
        {
            base.OnButtonClick();
            ItemData item = new ItemData(null, null, TitleEntry.Text, NotesEntry.Text, null, null);
            page = new AddToWishListPopUp(this, item, ImageRaw);
            await OpenPopupPage(page);

        }

        async public override void OnCancelClick()
        {
            base.OnCancelClick();
            await Navigation.PopAsync();
        }
    }
}
