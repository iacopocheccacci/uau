﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using CustomRenderer;
using Xamarin.Forms;

namespace UAU
{
	public partial class MyCirclesPage : CustomPage
	{
		ObservableCollection<CircleData> circlesData;

		public MyCirclesPage ()
		{
			InitializeComponent ();

#if __IOS__
			ToolbarItem button = new ToolbarItem();
			button.Text = Constants.addLabel;
			button.Order = ToolbarItemOrder.Primary;
			button.Priority = 1;
			button.Clicked += OnAddCircleButtonClicked;
			this.ToolbarItems.Add(button);
#endif
		}

		async protected override void OnAppearing()
		{
			base.OnAppearing ();

			await PopulateList();
		}

		async private Task PopulateList()
		{
			circlesData = new ObservableCollection<CircleData> ();
			CirclesView.ItemsSource = circlesData;

			var user = new UserCirclesRequestData (StaticHttpClient.GetInstance.currentUser.uau_id);
			lv.ActivateLoading ();
			string response = await StaticHttpClient.GetInstance.RequestFromApp (RequestType.getUserCliques, user);
			CirclesList circleslist;
			JsonUtilities<CirclesList>.DeserializeToObject (response, out circleslist);

			if (circleslist.cliques != null)
			{
				foreach (Circle circle in circleslist.cliques)
				{
					if (circle.name != "contacts")		// Hide main circle
					{
						circlesData.Add (new CircleData { Name = circle.name, 
														  CircleId = circle.clique_id});
					}
				}
			}
			else
			{
				await DisplayAlert (Constants.warningMessage, Constants.dataLoadFailedMessage, Constants.okMessage);
			}

			lv.DeactivateLoading ();
		}

		private void OnTextChanged(object sender, EventArgs args)
		{
			#if __IOS__
			CirclesView.BeginRefresh ();
			#endif 

			if (string.IsNullOrWhiteSpace (SearchBarField.Text)) 
			{
				CirclesView.ItemsSource = circlesData;
			}
			else 
			{
				CirclesView.ItemsSource = circlesData
					.Where (x => x.Name.ToLower ()
					        .Contains (SearchBarField.Text.ToLower ()));
			}

			#if __IOS__
			CirclesView.EndRefresh ();
			#endif
		}

		async private void OnItemSelected(object sender, SelectedItemChangedEventArgs e)
		{
			CirclesView.SelectedItem = null;

			var item = e.SelectedItem as CircleData;
			if (item != null)
			{
				await Navigation.PushAsync(new CircleFriendsPage(item));
			}
		}

		async private void OnAddCircleButtonClicked(object sender, EventArgs args)
		{
			await Navigation.PushAsync(new SaveCliquePage());
		}

		async private void OnDelete(object sender, EventArgs args)
		{
			MenuItem item = (MenuItem)sender;

			var answer = await DisplayAlert (Constants.deleteCliqueTitle, Constants.deleteCliqueMessage, Constants.noMessage, Constants.yesMessage);

			if (!answer)
			{
				await RemoveCircle(item);
			}
		}

		async private Task RemoveCircle(MenuItem circleItem)
		{
			var request = new DeleteCiqueRequest(circleItem.CommandParameter.ToString());

			lv.ActivateLoading ();
			string response = await StaticHttpClient.GetInstance.RequestFromApp (RequestType.deleteClique, request);
			DeleteCliqueResponse responseObject;
			JsonUtilities<DeleteCliqueResponse>.DeserializeToObject (response, out responseObject);

			if (responseObject != null)
			{
				if (responseObject.IsSucceeded())
				{
					await DisplayAlert(Constants.wellDoneMessage, Constants.RemovedCircleMessage, Constants.okMessage);
				}
				else
				{
					await this.DisplayAlert (Constants.warningMessage, responseObject.GetErrorMessage (), Constants.okMessage);
				}
			}
			else
			{
				await DisplayAlert (Constants.warningMessage, Constants.connectionFailedMessage, Constants.okMessage);
			}

			lv.DeactivateLoading();
		}

        async public override void OnButtonClick()
        {
            base.OnButtonClick();
            await Navigation.PushAsync(new SaveCliquePage());
        }
    }
}

