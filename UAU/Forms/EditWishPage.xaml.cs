﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using System.Threading.Tasks;
using Plugin.Media;
using System.IO;
using System.Diagnostics;
using Plugin.Media.Abstractions;
using CustomRenderer;

namespace UAU
{
	public partial class EditWishPage : CustomPage
	{
		private byte[] ImageRaw;
		private string wishlistId;
		private string imgUrl;

		public EditWishPage (string wishlistId, string title, string description, string imgUrl)
		{
			InitializeComponent ();

			this.wishlistId = wishlistId;
			ImageRaw = null;
			TitleEntry.Text = title;
			NotesEntry.Text = description;

			if (imgUrl != string.Empty)
			{
				this.imgUrl = imgUrl;
				PhotoImage.Source = imgUrl;
			}

			var tapGestureRecognizer = new TapGestureRecognizer();
			tapGestureRecognizer.Tapped += OnTapGestureRecognizerTapped;
			PhotoImage.GestureRecognizers.Add(tapGestureRecognizer);

#if __IOS__
            ToolbarItem button = new ToolbarItem();
			button.Text = Constants.cancelLabel;
            button.Order = ToolbarItemOrder.Primary;
            button.Priority = 0;
            button.Clicked += OnCancelButtonClicked;
            this.ToolbarItems.Add(button);

            ToolbarItem button2 = new ToolbarItem();
			button2.Text = Constants.doneLabel;
            button2.Order = ToolbarItemOrder.Primary;
            button2.Priority = 1;
            button2.Clicked += OnDoneButtonClicked;
            this.ToolbarItems.Add(button2);
#endif
        }

        async private void OnCancelButtonClicked(object sender, EventArgs args)
		{
			await Navigation.PopModalAsync ();
		}

		async private void OnDoneButtonClicked(object sender, EventArgs args)
		{
            await AddItem();
		}

		async void OnTapGestureRecognizerTapped(object sender, EventArgs args)
		{
			await PickPictureAsync ();
		}

		public async Task<string> PickPictureAsync() 
		{
			MediaFile file = null;
			string imageName = string.Empty;

			try 
			{
				lv.ActivateLoading();
				file = await CrossMedia.Current.PickPhotoAsync();

				if(file == null) 
				{ 
					return null; 
				}
					
				PhotoImage.Source = ImageSource.FromStream(file.GetStream);

				using (var memoryStream = new MemoryStream())
				{
					file.GetStream().CopyTo(memoryStream);
					file.Dispose();
					ImageRaw = memoryStream.ToArray();
				}
			}
			catch(Exception ex) 
			{
				Debug.WriteLine("\nIn PictureViewModel.PickPictureAsync() - Exception:\n{0}\n", ex);
				lv.DeactivateLoading ();
				return null;
			}
			finally 
			{ 
				lv.DeactivateLoading ();

				if(file != null) 
				{ 
					file.Dispose(); 
				} 
			}

			lv.DeactivateLoading ();

			return imageName;
		}

        async private Task PopPage()
        {
            await Navigation.PopModalAsync();
        }
        
		async protected override void OnAppearing()
        {
            base.OnAppearing();
            await AnimatePanel();
        }

        async private Task AnimatePanel()
        {
            await Task.Delay(5000);
            var rect = new Rectangle(0, -60, layout.Width, layout.Height);
            await layout.LayoutTo(rect, 1000);
            panel2.IsVisible = false;
        }

        async public override void OnCancelClick()
        {
            base.OnCancelClick();
            await Navigation.PopModalAsync();
        }

        async public override void OnButtonClick()
        {
            base.OnButtonClick();
            await AddItem();
        }

        async private Task AddItem()
        {
            string response;
            lv.ActivateLoading();

            if (ImageRaw != null)
            {
				string now = string.Format("{0:yyyy-MM-dd}", DateTime.Now);
                var item = new AddItemDataRequest(wishlistId,
                                                   TitleEntry.Text,
                                                   NotesEntry.Text,
                                                   "",
                                                   ImageRaw,
				                                   StaticHttpClient.GetInstance.currentUser.uau_id,
				                                   now);

                response = await StaticHttpClient.GetInstance.RequestFromApp(RequestType.addItemToWishList, item);
            }
            else
            {
                var item = new AddItemStringDataRequest(wishlistId,
                                                            TitleEntry.Text,
                                                         NotesEntry.Text,
                                                         "",
                                                         imgUrl,
				                                         StaticHttpClient.GetInstance.currentUser.uau_id);

                response = await StaticHttpClient.GetInstance.RequestFromApp(RequestType.addItemToWishList, item);
            }



            AddItemResponse responseObject;

            JsonUtilities<AddItemResponse>.DeserializeToObject(response, out responseObject);

            if (responseObject != null)
            {
                if (responseObject.IsSucceeded())
                {
                    await DisplayAlert(Constants.wellDoneMessage, Constants.itemAddedMessage, Constants.okMessage);
                    await PopPage();
                }
                else
                {
                    await this.DisplayAlert(Constants.warningMessage, responseObject.GetErrorMessage(), Constants.okMessage);
                }
            }
            else
            {
                await DisplayAlert(Constants.warningMessage, Constants.dataLoadFailedMessage, Constants.okMessage);
            }

            lv.DeactivateLoading();
        }
    }
}

