﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CustomRenderer;
using Xamarin.Forms;

namespace UAU
{
	public partial class SelectFriendsPage : CustomPage
	{
		ILoginManager ilm;
		WishlistToAdd wishlist;

		public SelectFriendsPage (ILoginManager ilm, WishlistToAdd wishlist)
		{
			InitializeComponent ();

			this.ilm = ilm;
			this.wishlist = wishlist;
		}

		async private void OnAddFriendsClicked(object sender, EventArgs args)
		{
			await Navigation.PushAsync (new AddFriendsPage (ilm, wishlist));
		}

		async private void OnLoadCircleClicked(object sender, EventArgs args)
		{
			await Navigation.PushAsync (new LoadCirclePage (ilm, wishlist));
		}

		async private void OnSkipClicked(object sender, EventArgs args)
		{
			await SaveWishlistOnDB();
		}

		async private Task SaveWishlistOnDB()
		{
//			lv.ActivateLoading ();
			string response = await StaticHttpClient.GetInstance.RequestFromApp (RequestType.createWishlist, wishlist);
			CreateWishListResponse responseObject;

			JsonUtilities<CreateWishListResponse>.DeserializeToObject (response, out responseObject);

			if (responseObject != null)
			{
				if (responseObject.IsSucceeded ())
				{
					var accepted = await this.DisplayAlert(Constants.wishlistCreationSuccededTitle, Constants.addFirstItemMessage, Constants.noMessage, Constants.yesMessage);
//					lv.DeactivateLoading();

					if (!accepted)
					{
						var wishlistData = new WishlistData();
						wishlistData.listName = wishlist.listName;

						if (ilm != null)
						{
							ilm.ShowListPage(wishlistData);
						}

					}
					else
					{
						if (ilm != null)
						{
							ilm.ShowMainPage(DetailPageType.MyLists);
						}
					}
				}
				else
				{
					await this.DisplayAlert (Constants.warningMessage, responseObject.GetErrorMessage (), Constants.okMessage);
//					lv.DeactivateLoading ();
				}
			}
			else
			{
				await DisplayAlert (Constants.warningMessage, Constants.dataLoadFailedMessage, Constants.okMessage);
//				lv.DeactivateLoading ();
			}
		}
	}
}

