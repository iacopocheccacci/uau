﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using System.Collections.ObjectModel;
using System.Linq;
using CustomRenderer;
using System.Threading.Tasks;

namespace UAU
{
	public partial class AddListAdminPage : CustomPage
	{
		ObservableCollection<FriendContactData> friendsData;
		private WishlistData wishlist;

		public AddListAdminPage (WishlistData wishlist)
		{
			InitializeComponent ();
			PopulateList ();

#if __IOS__
            ToolbarItem button = new ToolbarItem();
			button.Text = Constants.cancelLabel;
            button.Order = ToolbarItemOrder.Primary;
            button.Priority = 0;
            button.Clicked += OnCancelButtonClicked;
            this.ToolbarItems.Add(button);

            ToolbarItem button2 = new ToolbarItem();
			button2.Text = Constants.doneLabel;
            button2.Order = ToolbarItemOrder.Primary;
            button2.Priority = 1;
            button2.Clicked += OnDoneButtonClicked;
            this.ToolbarItems.Add(button2);
#endif

            this.wishlist = wishlist;
		}

		async private Task PopulateList()
		{
			friendsData = new ObservableCollection<FriendContactData> ();
			FriendsView.ItemsSource = friendsData;

			var user = new UserFriendsRequestData (StaticHttpClient.GetInstance.currentUser.uau_id);

			lv.ActivateLoading ();
			string response = await StaticHttpClient.GetInstance.RequestFromApp (RequestType.getUserFriends, user);
			FriendList friendlist;
			JsonUtilities<FriendList>.DeserializeToObject (response, out friendlist);

			if (friendlist.friends != null)
			{
				foreach (User friend in friendlist.friends)
				{
					friendsData.Add (new FriendContactData { FullName = friend.firstName + " " + friend.lastName,
						Id = friend.uau_id,
						Picture = friend.photo,
						IsEnabled = false
					});
				}
			}
			else
			{
				await DisplayAlert (Constants.warningMessage, Constants.dataLoadFailedMessage, Constants.okMessage);
			}

			// Disable selection
			FriendsView.ItemSelected += (sender, e) => {
				((ListView)sender).SelectedItem = null;
			};

			lv.DeactivateLoading ();
		}

		private void OnTextChanged(object sender, EventArgs args)
		{
			#if __IOS__
			FriendsView.BeginRefresh ();
			#endif 

			if (string.IsNullOrWhiteSpace (SearchBarField.Text)) 
			{
				FriendsView.ItemsSource = friendsData;
			}
			else 
			{
				FriendsView.ItemsSource = friendsData
					.Where (x => x.FullName.ToLower ()
						.Contains (SearchBarField.Text.ToLower ()));
			}

			#if __IOS__
			FriendsView.EndRefresh ();
			#endif
		}

		async private void OnCancelButtonClicked(object sender, EventArgs args)
		{
			await Navigation.PopModalAsync();
		}

		async private void OnDoneButtonClicked(object sender, EventArgs args)
		{
			var adminToAdd = new AddAdminToListRequest (wishlist.list_id,
												 	    GetSelectedFriends(),
												 	    true);

			lv.ActivateLoading ();
			string response = await StaticHttpClient.GetInstance.RequestFromApp (RequestType.addAdminToList, adminToAdd);
			AddAdminToListResponse responseObject;

			JsonUtilities<AddAdminToListResponse>.DeserializeToObject (response, out responseObject);

			if (responseObject != null)
			{
				if (responseObject.IsSucceeded ())
				{
					await Navigation.PopModalAsync();
				}
				else
				{
					await this.DisplayAlert (Constants.warningMessage, Constants.addAdminToListErrorMessage, Constants.okMessage);
				}
			}
			else
			{
				await DisplayAlert (Constants.warningMessage, Constants.dataLoadFailedMessage, Constants.okMessage);
			}

			lv.DeactivateLoading ();
		}

		private List<FriendData> GetSelectedFriends()
		{
			var friends = new List<FriendData> ();

			foreach (FriendContactData friend in FriendsView.ItemsSource)
			{
				if (friend.IsEnabled)
				{
					FriendData data = new FriendData ();
					data.id = friend.Id;
					data.name = friend.FullName;

					friends.Add (data);
				}
			}

			return friends;
		}

        async public override void OnButtonClick()
        {
            base.OnButtonClick();
            var adminToAdd = new AddAdminToListRequest(wishlist.list_id,
                                             GetSelectedFriends(),
                                             true);

            lv.ActivateLoading();
            string response = await StaticHttpClient.GetInstance.RequestFromApp(RequestType.addAdminToList, adminToAdd);
            AddAdminToListResponse responseObject;

            JsonUtilities<AddAdminToListResponse>.DeserializeToObject(response, out responseObject);

            if (responseObject != null)
            {
                if (responseObject.IsSucceeded())
                {
                    await Navigation.PopModalAsync();
                }
                else
                {
                    await this.DisplayAlert(Constants.warningMessage, Constants.addAdminToListErrorMessage, Constants.okMessage);
                }
            }
            else
            {
                await DisplayAlert(Constants.warningMessage, Constants.dataLoadFailedMessage, Constants.okMessage);
            }

            lv.DeactivateLoading();
        }

        async public override void OnCancelClick()
        {
            base.OnCancelClick();
            await Navigation.PopModalAsync();
        }
    }
}

