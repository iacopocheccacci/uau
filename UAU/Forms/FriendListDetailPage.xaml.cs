﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using System.Collections.ObjectModel;
using CustomRenderer;
using System.Threading.Tasks;
using System.Globalization;

namespace UAU
{
    public partial class FriendListDetailPage : CustomPage
    {
        ObservableCollection<ItemData> ItemList;
        WishlistData wishlist;
        string photo;
		string userName;

        public FriendListDetailPage(WishlistData wishlist, string photo, string userName)
        {
            InitializeComponent();
            this.wishlist = wishlist;
            this.photo = photo;
			this.userName = userName;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            FillWishlistData();
            FillItemsList();
        }

        private void FillWishlistData()
        {
            this.Title = wishlist.listName;
            SetExpirationLabel();
        }

        async private void FillItemsList()
        {
            ItemList = new ObservableCollection<ItemData>();
            ItemListView.ItemsSource = ItemList;

            lv.ActivateLoading();
            string response = await StaticHttpClient.GetInstance.RequestFromApp(RequestType.getWishListItems, wishlist);
            ItemData[] ItemsList;
            JsonUtilities<ItemData[]>.DeserializeToObject(response, out ItemsList);

            if (ItemsList != null)
            {
                if (ItemsList.Length > 0)
                {
                    foreach (ItemData item in ItemsList)
                    {
						item.photo = Constants.serverUrl + item.photo;
                        item.reservation = await CheckReservationStatus(item);
                        item.profilePhoto = photo;
						item.is_new = CheckIfItemIsNew(item.date_created);

						// sort list in order to have available items first
						switch (item.reservation)
						{
							case ReservationType.Reservable:
							case ReservationType.ActiveCollect:
								ItemList.Insert(0, item);
								break;

							default:
								ItemList.Insert(ItemList.Count, item);
								break;
						}

                        if (lv.IsRunning())
                        {
                            lv.DeactivateLoading();
                        }
                    }
                }
            }
            else
            {
                if (lv.IsRunning())
                {
                    lv.DeactivateLoading();
                }
                await DisplayAlert(Constants.warningMessage, Constants.dataLoadFailedMessage, Constants.okMessage);
            }

            SetItemsNumberLabel(ItemsList.Length);

            // Disable selection
            ItemListView.ItemSelected += (sender, e) =>
            {
                ((ListView)sender).SelectedItem = null;
            };

            if (lv.IsRunning())
            {
                lv.DeactivateLoading();
            }
        }

        async private Task<ReservationType> CheckReservationStatus(ItemData item)
        {
            ReservationType resType = ReservationType.Reservable;

            var reservationRequestData = new CheckReservationRequest(StaticHttpClient.GetInstance.currentUser.uau_id, item.item_id);
            string response = await StaticHttpClient.GetInstance.RequestFromApp(RequestType.checkReservation, reservationRequestData);
            CheckReservationResponse responseObject;
            JsonUtilities<CheckReservationResponse>.DeserializeToObject(response, out responseObject);

            if (responseObject != null)
            {
                if (!responseObject.IsSucceeded())
                {
                    resType = responseObject.GetReservationType();

                    item.expiration_date = responseObject.expiration_date;
                    item.partecipants = responseObject.partecipants;
                    item.part_amount = responseObject.part_amount;
                    SetDaysLeft(item);
                }
            }
            else
            {
                await DisplayAlert(Constants.warningMessage, Constants.connectionFailedMessage, Constants.okMessage);
            }

            return resType;
        }

        private void SetExpirationLabel()
        {
            if (wishlist.is_permanent == "0")
            {
                wishlist.setDaysLeft();

				DateTime dt = DateTime.ParseExact ( wishlist.expiration_date, "yyyy-MM-dd", CultureInfo.InvariantCulture);
				var dateString = string.Format("{0} {1} {2}", dt.Day, dt.ToMonthName(), dt.Year);

				ListExpiration.Text = dateString;
            }
            else
            {
				ListExpiration.Text = Constants.noExpirationLabel;
            }
        }

        private void SetItemsNumberLabel(int itemsNumber)
        {
            ItemsNumber.Text = itemsNumber.ToString();

            if (itemsNumber == 1)
            {
                ItemsNumber.Text += " desiderio";
            }
            else
            {
                ItemsNumber.Text += " desideri";
            }
        }

        async private void OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var item = e.SelectedItem as ItemData;
            if (item != null)
            {
				await Navigation.PushAsync(new WishDetailPage(wishlist, item, photo, userName));
            }
        }

        private void OnRefresh(object sender, EventArgs args)
        {
            Console.WriteLine("refresh");

            FillItemsList();

#if __IOS__
            ItemListView.EndRefresh();
#endif
        }

        public void SetDaysLeft(ItemData item)
        {
            DateTime today = DateTime.Now;
            DateTime expiration;
            if (DateTime.TryParseExact(item.expiration_date, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out expiration))
            {
                double differenceInDays = (expiration - today).TotalDays;
                double ceilingDifferenceInDays = Math.Ceiling(differenceInDays);

                if (ceilingDifferenceInDays < 0)
                {
                    item.daysLeft = "0";
                }
                else if (ceilingDifferenceInDays > 1)
                {
                    item.	daysLeft = ceilingDifferenceInDays.ToString();
					item.daysLabel = Constants.daysLabel;
                }
                else
                {
                    item.daysLeft = ceilingDifferenceInDays.ToString();
					item.daysLabel = Constants.dayLabel;
                }
            }
        }

		private bool CheckIfItemIsNew(string itemDateString)
		{
			bool isNew = false;
			DateTime itemDateTime;

			if (DateTime.TryParseExact(itemDateString, "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out itemDateTime))
			{
				if ((DateTime.Now - itemDateTime).Days < 15)
				{
					isNew = true;
				}
			}

			return isNew;
		}
    }
}

