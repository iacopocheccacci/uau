﻿using System;
using System.Collections.Generic;
using CustomRenderer;
using Xamarin.Forms;

namespace UAU
{
	public partial class WelcomeToAppPage : CustomPage
	{
		ILoginManager ilm;

		public WelcomeToAppPage (ILoginManager ilm)
		{
			InitializeComponent ();
			NavigationPage.SetHasNavigationBar (this, false);

			this.ilm = ilm;
		}

		async public void OnStartClicked(object sender, EventArgs args)
		{
			await Navigation.PushAsync (new GetMoreFriendsPage (ilm));
		}
	}
}

