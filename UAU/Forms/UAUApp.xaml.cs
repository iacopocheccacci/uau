﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace UAU
{
	public partial class App : Application, ILoginManager
	{
		static ILoginManager loginManager;

		public static App CurrentApp;
		public static Action<FBLoginResponse> PostSuccessFacebookAction { get; set; }

		public App ()
		{
			//InitializeComponent ();
			BuildStyle();
			CurrentApp = this;

			var isLoggedIn = Properties.ContainsKey (Constants.isLoggedInProperty) ? (bool)Properties [Constants.isLoggedInProperty] : false;

			if (isLoggedIn)
			{
				StaticHttpClient.GetInstance.FillCurrentUserData ();
				ShowLoadingPage();
			}
			else
			{
				ShowLoginPage();
			}
		}

		protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}

		public void ShowWelcomeNavigationPage ()
		{
			MainPage = new NavigationPage (new WelcomeToAppPage (this));
		}

		public void ShowMainPage(DetailPageType pageType = DetailPageType.Feed)
		{
			MainPage = new FeedMasterDetailPage (this, pageType);
		}

		public void ShowListPage(WishlistData wishlistData)
		{
			MainPage = new FeedMasterDetailPage (this, DetailPageType.MyLists, wishlistData);
		}

		public void ShowLoginPage()
		{
			MainPage = new NavigationPage (new LoginPage (this));
		}

		async public Task Logout()
		{
			Properties [Constants.isLoggedInProperty] = false;
			await SavePropertiesAsync();

			MainPage = new NavigationPage (new LoginPage(this));
		}

		public void ShowLoadingPage()
		{
			MainPage = new LoadingPage (this);
		}

		private void BuildStyle()
		{
			var entryStyle = new Style (typeof(Entry)) {
				Setters = {
					new Setter { Property = Entry.BackgroundColorProperty, Value = Color.White },
					new Setter { Property = Entry.HorizontalOptionsProperty, Value = LayoutOptions.FillAndExpand },
					new Setter { Property = Entry.TextColorProperty, Value = Constants.myBlackHex },
					new Setter { Property = Entry.HeightRequestProperty, Value = 44 },
					new Setter { Property = Entry.FontFamilyProperty, Value = "Lato-Regular" }
				}
			};

			var buttonStyle = new Style (typeof(Button)) {
				Setters = {
					new Setter { Property = Button.BorderRadiusProperty, Value = 1 },
					new Setter { Property = Button.HorizontalOptionsProperty, Value = LayoutOptions.FillAndExpand }, 
					new Setter { Property = Button.HeightRequestProperty, Value = 44 },
					new Setter { Property = Button.FontSizeProperty, Value = 16 },
					new Setter { Property = Button.TextColorProperty, Value = Color.White },
					new Setter { Property = Button.BackgroundColorProperty, Value = "#3B5998" },
					new Setter { Property = Button.FontFamilyProperty, Value = "Lato-Bold" }
				}
			};

			var labelStyle = new Style (typeof(Label)) {
				Setters = {
					new Setter { Property = Label.TextColorProperty, Value = Constants.myBlackHex },
					new Setter { Property = Label.FontFamilyProperty, Value = "Lato-Regular" }
				}
			};

			var navigationStyle = new Style (typeof(NavigationPage)) {
				Setters = {
					new Setter { Property = NavigationPage.BarBackgroundColorProperty, Value = Constants.myBlackHex }
				}
			};

			// Not working! Need custom renderer setting
			var contentPageStyle = new Style (typeof(ContentPage)) {
				Setters = {
					new Setter { Property = ContentPage.BackgroundColorProperty, Value = Constants.denimBlueHex }
				}
			};

			Resources = new ResourceDictionary ();
			Resources.Add (entryStyle);
			Resources.Add (buttonStyle);
			Resources.Add (labelStyle);
			Resources.Add (navigationStyle);
			Resources.Add (contentPageStyle);
		}
	}
}

