﻿using CustomRenderer;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace UAU
{
    public partial class SearchForFriends : CustomPage
    {
        bool isSearch = false;
        int clickTimes = 1;
        GetAllUsersResponse allUsers;
        List<FriendContactData> searchList;
        ObservableCollection<FriendContactData> tmpList;
        ObservableCollection<FriendContactData> UserList;
        FriendList friendsList;
        int userForClick = 2;

        public SearchForFriends()
        {
            InitializeComponent();
            tmpList = new ObservableCollection<FriendContactData>();
            searchList = new List<FriendContactData>();
            GetUsersFromServer();

#if __IOS__
            ToolbarItem button = new ToolbarItem();
			button.Text = Constants.addLabel;
            button.Order = ToolbarItemOrder.Primary;
            button.Priority = 1;
            button.Clicked += OnAddButtonClicked;
            this.ToolbarItems.Add(button);
#endif
        }

        public void OnTextChanged(object sender, EventArgs args)
        {
#if __IOS__
			UserListView.BeginRefresh ();
#endif

            if (string.IsNullOrWhiteSpace(SearchBarField.Text))
            {
                isSearch = false;
                clickTimes = 1;
                tmpList.Clear();
                ShowMore(null, false);
                UserListView.ItemsSource = tmpList;
            }
            else
            {
                isSearch = true;
                searchList.Clear();
                tmpList.Clear();
                UpdateList(SearchBarField.Text.ToLower());
                UserListView.ItemsSource = tmpList;
            }

#if __IOS__
			UserListView.EndRefresh ();
#endif
        }

        private async Task GetUsersFromServer()
        {
            lv.ActivateLoading();
            allUsers = new GetAllUsersResponse();
            string response = await StaticHttpClient.GetInstance.RequestFromApp(RequestType.getAllUsers, StaticHttpClient.GetInstance.currentUser);
            JsonUtilities<GetAllUsersResponse>.DeserializeToObject(response, out allUsers);

            if (allUsers.users != null)
            {
				await GetUserFriends ();
            }
            else
            {
                await DisplayAlert(Constants.warningMessage, Constants.dataLoadFailedMessage, Constants.okMessage);
            }
        }

        private void PopulateList()
        {
            string currentUser = StaticHttpClient.GetInstance.currentUser.uau_id;
            UserList = new ObservableCollection<FriendContactData>();
            if (allUsers.users != null)
            {
                foreach (User user in allUsers.users)
                {
                    if (!IsFriend(user.uau_id) && user.uau_id != currentUser)
                    {
						FriendContactData contact = new FriendContactData();

						if (user.active_lists == "0")
						{
							contact.Details = "Nessuna lista";
						}
						else
						{
							contact.Details = "Ha " + user.active_lists + " liste";
						}

						contact.FullName = user.firstName + " " + user.lastName;
						contact.IsEnabled = false;
						contact.Picture = user.photo;
						contact.Id = user.uau_id;
						contact.Email = user.email;

                        UserList.Add(contact);
                    }
                    if (lv.IsRunning())
                    {
                        lv.DeactivateLoading();
                    }

                    //TODO can't click on next if is loading
                }
            }
            ShowMore(null, false);
            UserListView.ItemsSource = tmpList;
            if (lv.IsRunning())
            {
                lv.DeactivateLoading();
            }
        }

        private async Task GetUserFriends()
        {
            var user = new UserFriendsRequestData(StaticHttpClient.GetInstance.currentUser.uau_id);
            string response = await StaticHttpClient.GetInstance.RequestFromApp(RequestType.getUserFriends, user);
            
            JsonUtilities<FriendList>.DeserializeToObject(response, out friendsList);

            PopulateList();
        }

        private bool IsFriend(String id)
        {
            var userFriend = friendsList.friends.Find(item => item.uau_id == id);
            
			return (userFriend != null) ? true : false;
        }

        private List<FriendData> GetSelectedFriends()
        {
            List<FriendData> friendsToAdd = new List<FriendData>();
            for (int i = 0; i < UserList.Count; i++)
            {
                if (UserList[i].IsEnabled)
                {
                    FriendData friend = new FriendData();
                    friend.id = UserList[i].Id;
                    friendsToAdd.Add(friend);
                }
            }
            return friendsToAdd;
        }

        async private void OnAddButtonClicked(object sender, EventArgs args)
        {
			await AddFriends();    
		}

        async public override void OnButtonClick()
        {
            base.OnButtonClick();
			await AddFriends();
        }

		async private Task AddFriends() 
		{
			var request = new AddFriendsRequest();
			request.friends = GetSelectedFriends();
			if (request.friends.Count == 0)
			{
				await DisplayAlert(Constants.warningMessage, Constants.selectFriendsMessage, Constants.okMessage);
			}
			else
			{
				request.owner_id = StaticHttpClient.GetInstance.currentUser.uau_id;

				string response = await StaticHttpClient.GetInstance.RequestFromApp(RequestType.saveClique, request);

				if (response != string.Empty)
				{
					ResponseObject responseObject;
					JsonUtilities<ResponseObject>.DeserializeToObject(response, out responseObject);

					if (responseObject != null)
					{
						if (responseObject.IsSucceeded())
						{
							await DisplayAlert(Constants.wellDoneMessage, Constants.friendAddedMessage, Constants.okMessage);
							UserListView.ItemsSource = new ObservableCollection<FriendContactData>();
							SearchBarField.Text = String.Empty;
							await GetUsersFromServer();

							await Navigation.PopAsync();
						}
						else
						{
							await DisplayAlert(Constants.warningMessage, Constants.connectionFailedMessage, Constants.okMessage);
						}
					}
					else
					{
						await DisplayAlert(Constants.warningMessage, Constants.dataLoadFailedMessage, Constants.okMessage);
					}
				}
				else
				{
					await DisplayAlert(Constants.warningMessage, Constants.connectionFailedMessage, Constants.okMessage);
				}

			}

		}

        private void OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            UserListView.SelectedItem = null;
        }

        public void OnButtonClicked(object sender, EventArgs args)
        {
            if (isSearch)
            {
                ShowMore(searchList);  
            }
            else
            {
                ShowMore(null, false);
            }
        }

        private void ShowMore(List<FriendContactData> list, bool isSearch = true)
        {
            if (isSearch)
            {
                int maxLenght;
                if (list.Count <= userForClick * clickTimes)
                {
                    maxLenght = list.Count;
                    showButton.IsVisible = false;
                }
                else
                {
                    maxLenght = userForClick * clickTimes;
                    showButton.IsVisible = true  ;
                }
                
				for (int i = (userForClick * clickTimes) - userForClick; i < maxLenght; i++)
                {
                    tmpList.Add(list[i]);
                    UserListView.ItemsSource = tmpList;
                }
            }
            else
            {
                int maxLenght;
                if (UserList.Count <= userForClick * clickTimes)
                {
                    maxLenght = UserList.Count;
                    showButton.IsVisible = false;
                }
                else
                {
                    maxLenght = userForClick * clickTimes;
                    showButton.IsVisible = true;
                }
                
				for (int i = (userForClick * (clickTimes)) - userForClick; i < maxLenght; i++)
                {
                    tmpList.Add(UserList[i]);
                    UserListView.ItemsSource = tmpList;
                }
            }

            clickTimes++;
        }

        private void UpdateList(string text)
        {
            searchList = UserList.Where(x => x.FullName.ToLower()
                       .Contains(text) || x.Email.ToLower().Contains(text)).ToList<FriendContactData>();
            clickTimes = 1;
            ShowMore(searchList);
        }
    }
}
