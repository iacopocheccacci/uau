﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

using Xamarin.Forms;
using Xamarin.Contacts;
using System.Linq;
using System.Threading.Tasks;
using System.Diagnostics;
using CustomRenderer;

namespace UAU
{
	public partial class ConnectWithFriendsPage : CustomPage
	{
        bool firstAccess;
        bool isContactListLoaded = false;
		ILoginManager ilm = null;

		public ConnectWithFriendsPage(ILoginManager ilm, bool firstAccess = true)
        {
            InitializeComponent();
            this.firstAccess = firstAccess;

			if (ilm != null)
			{
				this.ilm = ilm;
			}

            PopulateContactList();

#if __IOS__
            ToolbarItem button = new ToolbarItem();
			button.Text = Constants.nextLabel;
            button.Order = ToolbarItemOrder.Primary;
            button.Priority = 1;
            button.Clicked += OnNextButtonClicked;
            this.ToolbarItems.Add(button);
#endif
        }

		async private Task PopulateContactList()
		{
			int counter = 0;
			ObservableCollection<ContactData> contactList = new ObservableCollection<ContactData> ();

			ContactListView.ItemsSource = contactList;

#if __IOS__
			var book = new Xamarin.Contacts.AddressBook ();
#elif __ANDROID__
			var book = new Xamarin.Contacts.AddressBook(Forms.Context.ApplicationContext);
#endif

			if (!await book.RequestPermission())
			{
				Debug.WriteLine("Permission denied by user or manifest");
				return;
			}


			if (!book.Any ())
			{
				Debug.WriteLine("No contacts found");
			}
			else
			{
				lv.ActivateLoading ();
				List<string> friendIdList = await GetUserFriendIDs ();

				foreach (Contact contact in book /*.OrderBy (c => c.LastName)*/) // OrderBy doesn't work on Android
				{
					if (contact.Emails.Any ())
					{
						foreach (Email email in contact.Emails)
						{
							Debug.WriteLine(email.Address);
							string id = await StaticHttpClient.GetInstance.GetUserIdFromEmail (email.Address);

							if (id != null && id != StaticHttpClient.GetInstance.currentUser.uau_id 
								&& !friendIdList.Contains(id))
							{
								counter++;
								contactList.Add (new ContactData { UserId = id, 
																   DisplayName = contact.DisplayName, 
																   IsEnabled = true, 
																   Email = email.Address });
							}
						}
					}
				}

				// Disable selection
				ContactListView.ItemSelected += (sender, e) => {
					((ListView)sender).SelectedItem = null;
				};

				lv.DeactivateLoading ();
				isContactListLoaded = true;
			}

			if (contactList.Count > 0)
			{
				TitleLabel.IsVisible = true;
				SecondLabel.IsVisible = true;
				CountLabel.IsVisible = true;
				CountLabel.Text = "Abbiamo trovato " + counter.ToString () + " amici che usano SpyGift";
			}
			else
			{
				CountLabel.IsVisible = true;
				CountLabel.Text = "Non abbiamo trovato nessun amico su SpyGift";
			}
		}

		async private void OnNextButtonClicked(object sender, EventArgs args)
		{
			if (isContactListLoaded)
			{
				await AddFriends ();

				if (firstAccess && ilm != null)
                {
                    await Navigation.PushAsync(new InviteFriendsPage(ilm));
                }
                else
                {
                    await Navigation.PopAsync();
                }
			}
		}
			
		async private Task AddFriends()
		{
			var friends = new List<FriendData> ();

			foreach (ContactData contact in ContactListView.ItemsSource)
			{
				if (contact.IsEnabled)
				{
					Debug.WriteLine (contact.Email);

					FriendData data = new FriendData ();
					data.id = contact.UserId;

					friends.Add (data);
				}
			}

			if (friends.Count > 0)
			{
				var request = new AddFriendsRequest ();

				request.friends = friends;
				request.owner_id = StaticHttpClient.GetInstance.currentUser.uau_id;

				lv.ActivateLoading ();
				string response = await StaticHttpClient.GetInstance.RequestFromApp (RequestType.saveClique, request);

				if (response != string.Empty)
				{
					ResponseObject responseObject;
					JsonUtilities<ResponseObject>.DeserializeToObject (response, out responseObject);
				}
				else
				{
					await DisplayAlert (Constants.warningMessage, Constants.connectionFailedMessage, Constants.okMessage);
					lv.DeactivateLoading ();
				}
			}
		}

		async private Task<List<string>> GetUserFriendIDs()
		{
			FriendList friendList = new FriendList ();
			var user = new UserFriendsRequestData (StaticHttpClient.GetInstance.currentUser.uau_id);
			string response = await StaticHttpClient.GetInstance.RequestFromApp (RequestType.getUserFriends, user);
			JsonUtilities<FriendList>.DeserializeToObject (response, out friendList);

			List<string> friendIdList = new List<string> ();

			foreach (User friendData in friendList.friends)
			{
				friendIdList.Add (friendData.uau_id);
			}

			return friendIdList;
		}

        async public override void OnButtonClick()
        {
            base.OnButtonClick();
            if (isContactListLoaded)
            {
                await AddFriends();

				if (firstAccess && ilm != null)
                {
                    await Navigation.PushAsync(new InviteFriendsPage(ilm));
                }
                else
                {
                    await Navigation.PopAsync();
                }
            }
        }
    }
}

