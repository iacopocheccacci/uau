﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using CustomRenderer;
using Xamarin.Forms;

namespace UAU
{
	public partial class ShowFriendsInListPage : CustomPage
	{
		ObservableCollection<FriendContactData> friendsData;
		WishlistToAdd wishlist;
		List<string> friendsId;

		public ShowFriendsInListPage (WishlistToAdd wishlist)
		{
			InitializeComponent ();

#if __IOS__
			ToolbarItem button = new ToolbarItem();
			button.Text = Constants.addLabel;
			button.Order = ToolbarItemOrder.Primary;
			button.Priority = 1;
			button.Clicked += OnAddFriendsButtonClicked;
			this.ToolbarItems.Add(button);
#endif

			this.wishlist = wishlist;
		}

		async protected override void OnAppearing()
		{
			base.OnAppearing ();
			await PopulateList ();
		}

		async private Task PopulateList()
		{
			await GetWishlistFriends();

			friendsData = new ObservableCollection<FriendContactData> ();
			FriendsView.ItemsSource = friendsData;

			var user = new UserFriendsRequestData (StaticHttpClient.GetInstance.currentUser.uau_id);
			lv.ActivateLoading ();
			string response = await StaticHttpClient.GetInstance.RequestFromApp (RequestType.getUserFriends, user);
			FriendList friendlist;
			JsonUtilities<FriendList>.DeserializeToObject (response, out friendlist);

			if (friendlist.friends != null)
			{
				foreach (User friend in friendlist.friends)
				{
					if (friendsId.Contains(friend.uau_id))
					{
						DateTime dt = DateTime.ParseExact (friend.birthday, "yyyy-MM-dd", CultureInfo.InvariantCulture);
						var birthday = string.Format("Compleanno: {0} {1}", dt.Day, dt.ToMonthName());

						friendsData.Add (new FriendContactData { FullName = friend.firstName + " " + friend.lastName,
							Id = friend.uau_id,
							Picture = friend.photo,
							Birthday = birthday
						});
					}

					if (lv.IsRunning())
					{
						lv.DeactivateLoading();
					}
					//TODO  can't click on next if is loading
				}
			}
			else
			{
				await DisplayAlert (Constants.warningMessage, Constants.dataLoadFailedMessage, Constants.okMessage);
				if (lv.IsRunning())
				{
					lv.DeactivateLoading();
				}
			}

			// Disable selection
			FriendsView.ItemSelected += (sender, e) => {
				((ListView)sender).SelectedItem = null;
			};

			if (lv.IsRunning())
			{
				lv.DeactivateLoading();
			}
		}

		async private Task GetWishlistFriends()
		{
			var user = new GetWishListFriendsRequest(wishlist.list_id,StaticHttpClient.GetInstance.currentUser.uau_id);
			lv.ActivateLoading();
			string response = await StaticHttpClient.GetInstance.RequestFromApp(RequestType.getWishListFriends, user);
			GetWishListFriendsResponse friendlist;
			JsonUtilities<GetWishListFriendsResponse>.DeserializeToObject(response, out friendlist);
			friendsId = new List<string>();

			if (friendlist != null && friendlist.wishlist_friends != null)
			{
				foreach (User friend in friendlist.wishlist_friends)
				{
					friendsId.Add(friend.uau_id);
				}
			}
		}

		async private void OnDelete(object sender, EventArgs args)
		{
			MenuItem item = (MenuItem)sender;
			var user = new DeleteUserFromWishlistRequest(item.CommandParameter.ToString(), wishlist.list_id);
			lv.ActivateLoading();
			string response = await StaticHttpClient.GetInstance.RequestFromApp(RequestType.deleteUserFromWishlist, user);
		
			if (response != null)
			{
				DeleteUserFromFriendsResponse responseObject;
				JsonUtilities<DeleteUserFromFriendsResponse>.DeserializeToObject(response, out responseObject);

				if (responseObject != null && responseObject.IsSucceeded())
				{
					await PopulateList();
				}
				else
				{
					await DisplayAlert (Constants.warningMessage, Constants.dataLoadFailedMessage, Constants.okMessage);
				}
			}
		}

		private void OnTextChanged(object sender, EventArgs args)
		{
			#if __IOS__
			FriendsView.BeginRefresh ();
			#endif 

			if (string.IsNullOrWhiteSpace (SearchBarField.Text)) 
			{
				FriendsView.ItemsSource = friendsData;
			}
			else 
			{
				FriendsView.ItemsSource = friendsData
					.Where (x => x.FullName.ToLower ()
					                              .Contains (SearchBarField.Text.ToLower ()));
			}

			#if __IOS__
			FriendsView.EndRefresh ();
			#endif
		}

		async private void OnAddFriendsButtonClicked(object sender, EventArgs args)
		{
			await GoToAddFriends(wishlist);
		}

		async public override void OnButtonClick()
		{
			base.OnButtonClick();
			await GoToAddFriends(wishlist);
		}

		async private Task GoToAddFriends(WishlistToAdd wl)
		{
			await Navigation.PushAsync (new AddFriendsPage (null, wl, true));
		}
	}
}

