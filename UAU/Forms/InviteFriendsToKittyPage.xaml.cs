﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using CustomRenderer;
using Xamarin.Forms;

namespace UAU
{
	public partial class InviteFriendsToKittyPage : CustomPage
	{
		string ownerId;
		ObservableCollection<FriendContactData> friendsData;
		List<UserReservationData> friendsToInviteList;

		public InviteFriendsToKittyPage (string ownerId, ref List<UserReservationData> friendsList)
		{
			InitializeComponent ();

			this.ownerId = ownerId;
			friendsToInviteList = friendsList;
			PopulateList();

#if __IOS__
            ToolbarItem button = new ToolbarItem();
			button.Text = Constants.cancelLabel;
            button.Order = ToolbarItemOrder.Primary;
            button.Priority = 0;
            button.Clicked += OnCancelButtonClicked;
            this.ToolbarItems.Add(button);

            ToolbarItem button2 = new ToolbarItem();
			button2.Text = Constants.doneLabel;
            button2.Order = ToolbarItemOrder.Primary;
            button2.Priority = 1;
            button2.Clicked += OnDoneButtonClicked;
            this.ToolbarItems.Add(button2);
#endif
        }

        async private void PopulateList()
		{
			friendsData = new ObservableCollection<FriendContactData> ();
			FriendsView.ItemsSource = friendsData;

			var user = new UserFriendsRequestData (StaticHttpClient.GetInstance.currentUser.uau_id);
			lv.ActivateLoading ();
			string response = await StaticHttpClient.GetInstance.RequestFromApp (RequestType.getUserFriends, user);
			FriendList friendlist;
			JsonUtilities<FriendList>.DeserializeToObject (response, out friendlist);

			if (friendlist.friends != null)
			{
				foreach (User friend in friendlist.friends)
				{
					if (friend.uau_id != ownerId)
					{
						DateTime dt = DateTime.ParseExact (friend.birthday, "yyyy-MM-dd", CultureInfo.InvariantCulture);
						var birthday = string.Format("Birthday: {0} {1}", dt.Day, dt.ToMonthName());

						friendsData.Add (new FriendContactData { FullName = friend.firstName + " " + friend.lastName,
							Id = friend.uau_id,
							Picture = friend.photo,
							IsEnabled = false,
							Birthday = birthday
						});
					}
				}
			}
			else
			{
				await DisplayAlert (Constants.warningMessage, Constants.dataLoadFailedMessage, Constants.okMessage);
			}

			// Disable selection
			FriendsView.ItemSelected += (sender, e) => {
				((ListView)sender).SelectedItem = null;
			};

			lv.DeactivateLoading ();
		}

		async private void OnCancelButtonClicked(object sender, EventArgs args)
		{
			await Navigation.PopModalAsync();
		}

		async private void OnDoneButtonClicked(object sender, EventArgs args)
		{
			GetSelectedFriends();

			await Navigation.PopModalAsync();
		}

		private void OnTextChanged(object sender, EventArgs args)
		{
			#if __IOS__
			FriendsView.BeginRefresh ();
			#endif 

			if (string.IsNullOrWhiteSpace (SearchBarField.Text)) 
			{
				FriendsView.ItemsSource = friendsData;
			}
			else 
			{
				FriendsView.ItemsSource = friendsData
										  .Where (x => x.FullName.ToLower ().Contains (SearchBarField.Text.ToLower ()));
			}

			#if __IOS__
			FriendsView.EndRefresh ();
			#endif
		}

		private void GetSelectedFriends()
		{
			foreach (FriendContactData f in FriendsView.ItemsSource)
			{
				if (f.IsEnabled)
				{
					UserReservationData data = new UserReservationData ();
					data.uau_id = f.Id;

					friendsToInviteList.Add (data);
				}
			}
		}

        async public override void OnButtonClick()
        {
            base.OnButtonClick();
            GetSelectedFriends();

            await Navigation.PopModalAsync();
        }

        async public override void OnCancelClick()
        {
            base.OnCancelClick();
            await Navigation.PopModalAsync();
        }
    }
}

