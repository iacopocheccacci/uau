﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using CustomRenderer;
using System.Collections.ObjectModel;
using Rg.Plugins.Popup.Services;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Threading.Tasks;

namespace UAU
{
	public partial class WishDetailPage : CustomPage
	{
		WishlistData wishlist;
		ItemData item;
		string userPhoto;
		string userName;
		bool hasExpiration = false;

		public WishDetailPage (WishlistData wishlist, ItemData item, string userPhoto, string userName)
		{
			InitializeComponent ();
			InitElements();

			this.wishlist = wishlist;
			this.item = item;
			this.userPhoto = userPhoto;
			this.userName = userName;

			hasExpiration = (wishlist.is_permanent == "0") ? true : false;

            if (userPhoto != null)
            {
                FillItemDetails();
            }

			SetPriceAverage();

			InitPicker();
		}

		protected override void OnAppearing()
		{
			base.OnAppearing ();

			SetDaysLeft(item);
			ExpirationDateLabel.Text = Constants.genericReservationExpirationLabel + item.daysLeft + " " + Constants.daysLabel;
            SetReservationTypeFeedback();
        }

		private void InitPicker()
		{
			if (wishlist.is_permanent == "0" && wishlist.expiration_date != string.Empty)
			{
				DateTime dt = GetDateFromExpirationString(wishlist.expiration_date);

				if ((dt - DateTime.Now).TotalDays > 0)
				{
					string dateString = string.Format("Scadenza ({0}/{1}/{2})", dt.Day, dt.Month, dt.Year);
					ExpirationPicker.Items.Insert(0, dateString);	
				}
			}

			ExpirationPicker.SelectedIndex = -1;
			if (Device.OS == TargetPlatform.iOS)
			{
				ExpirationPicker.Unfocused += async delegate
				{
					if (ExpirationPicker.SelectedIndex != -1)
					{
						await SetReservation();
					}
				};
			}
			else
			{
				ExpirationPicker.SelectedIndexChanged += async delegate
				{
					if (ExpirationPicker.SelectedIndex != -1)
					{
						await SetReservation();
					}
				};
			}
		}

		private void InitElements()
		{
			ReservedPanel.IsVisible = false;
			ReserveButton.IsVisible = false;
			CancelReservationButton.IsVisible = false;
			CreateKittyButton.IsVisible = false;
			CreateAKittyLabel.IsVisible = false;
			CancelFromKittyButton.IsVisible = false;
			JoinTheKittyButton.IsVisible = false;
			JoinTheKittyLabel.IsVisible = false;
			KittyAdminButton.IsVisible = false;
			PartecipantLabel.IsVisible = false;
			PartecipantPriceLabel.IsVisible = false;
		}

		private void FillItemDetails()
		{
            if (item != null)
            {
                ItemPhoto.Source = item.photo;
                if(ItemPhoto.Height < 200)
                {
                    ItemPhoto.HeightRequest = 200;
                }
                
				ItemName.Text = item.title;
                ItemDescription.Text = item.description;
                UserPhoto.Source = userPhoto;
				UseraName.Text = userName;
            }
		}

		async private Task SetPriceAverage ()
		{
			string searchName;
			float priceTotal = 0;
			int itemCount = 0;

			TruncateSearcName(out searchName);

			var requestData = new AmazonMarketPlaceProductsRequest ("it", searchName);

			string response = await StaticHttpClient.GetInstance.RequestFromApp (RequestType.amazonMarketPlaceProducts, requestData);
			AmazonMarketPlaceProductsResponse responseObject;
			JsonUtilities<AmazonMarketPlaceProductsResponse>.DeserializeToObject (response, out responseObject);

			if (responseObject != null && responseObject.IsSucceeded()) 
			{
				if (responseObject.products.Count > 0)
				{
					foreach (ShopItemData shopItem in responseObject.products)
					{
						// only first 3 deals
						if (itemCount < 3)
						{
							var amount = float.Parse(shopItem.OfferSummary.LowestNewPrice.Amount) / 100;
							priceTotal += amount;
							itemCount++;
						}
					}

					priceTotal /= itemCount;
					priceTotal += (priceTotal / 10);

					PriceLabel.BindingContext = priceTotal.ToString("c2") + " €";
					PriceLabel.SetBinding (Label.TextProperty, ".", BindingMode.TwoWay);
				}
				else
				{
					PriceLabel.Text = "---";
				}
			}
		}

		async private void SetReservationTypeFeedback()
		{
			switch(item.reservation)
			{
				case ReservationType.Reservable:
					ReservedPanel.IsVisible = false;
					ReserveButton.IsVisible = true;
					CancelReservationButton.IsVisible = false;
					CreateKittyButton.IsVisible = true;
					CreateAKittyLabel.IsVisible = true;
					CancelFromKittyButton.IsVisible = false;
					JoinTheKittyButton.IsVisible = false;
					JoinTheKittyLabel.IsVisible = false;
					KittyAdminButton.IsVisible = false;
                    helpText.IsVisible = false;
					PartecipantLabel.IsVisible = false;
					PartecipantPriceLabel.IsVisible = false;
					break;

				case ReservationType.ReservedByMe:
					//ExpirationDateLabel.Text = item.
					ReservedPanel.IsVisible = true;
					ReserveButton.IsVisible = false;
					CancelReservationButton.IsVisible = true;
					CreateKittyButton.IsVisible = false;
					CreateAKittyLabel.IsVisible = false;
					CancelFromKittyButton.IsVisible = false;
					JoinTheKittyButton.IsVisible = false;
					JoinTheKittyLabel.IsVisible = false;
					KittyAdminButton.IsVisible = false;
                    helpText.IsVisible = true;
					PartecipantLabel.IsVisible = false;
					PartecipantPriceLabel.IsVisible = false;
                    break;

				case ReservationType.ActiveCollect:
					ReservedPanel.IsVisible = true;
					ReserveButton.IsVisible = false;
					CancelReservationButton.IsVisible = false;
					CreateKittyButton.IsVisible = false;
					CreateAKittyLabel.IsVisible = false;
					CancelFromKittyButton.IsVisible = false;
					JoinTheKittyButton.IsVisible = true;
					JoinTheKittyLabel.IsVisible = true;
					KittyAdminButton.IsVisible = false;
                    helpText.IsVisible = false;
					await SetCollectElements();
                    break;

				case ReservationType.MyCollect:
					ReservedPanel.IsVisible = true;
					ReserveButton.IsVisible = false;
					CancelReservationButton.IsVisible = false;
					CreateKittyButton.IsVisible = false;
					CreateAKittyLabel.IsVisible = false;
					CancelFromKittyButton.IsVisible = false;
					JoinTheKittyButton.IsVisible = false;
					JoinTheKittyLabel.IsVisible = false;
					KittyAdminButton.IsVisible = true;
                    helpText.IsVisible = false;
				 	await SetCollectElements();
                    break;

				case ReservationType.ReservedByMeCollect:
					ReservedPanel.IsVisible = true;
					ReserveButton.IsVisible = false;
					CancelReservationButton.IsVisible = false;
					CreateKittyButton.IsVisible = false;
					CreateAKittyLabel.IsVisible = false;
					CancelFromKittyButton.IsVisible = true;
					JoinTheKittyButton.IsVisible = false;
					JoinTheKittyLabel.IsVisible = false;
					KittyAdminButton.IsVisible = false;
                    helpText.IsVisible = false;
					await SetCollectElements();
                    break;

				case ReservationType.ReservedByOther:
					ReservedPanel.IsVisible = true;
					ReserveButton.IsVisible = false;
					CancelReservationButton.IsVisible = false;
					CreateKittyButton.IsVisible = false;
					CreateAKittyLabel.IsVisible = false;
					CancelFromKittyButton.IsVisible = false;
					JoinTheKittyButton.IsVisible = false;
					JoinTheKittyLabel.IsVisible = false;
					KittyAdminButton.IsVisible = false;
                    helpText.IsVisible = false;
					PartecipantLabel.IsVisible = false;
					PartecipantPriceLabel.IsVisible = false;
                    break;
			}
		}

		private void OnReserveButtonClicked(object sender, EventArgs args)
		{
            if (ExpirationPicker.IsFocused)
            {
                ExpirationPicker.Unfocus();
            }
            ExpirationPicker.Focus();
        }

		async private void OnCreateAKittyButtonClicked(object sender, EventArgs args)
		{
			await Navigation.PushModalAsync(new NavigationPage(new CreateAKittyPage(wishlist, item, userPhoto)));
		}

		async private void OnCancelReservationButtonClicked(object sender, EventArgs args)
		{
            ExpirationPicker.SelectedIndex = -1;

            var deleteReservationRequestData = new DeleteReservationRequest (StaticHttpClient.GetInstance.currentUser.uau_id,
			                                                     			 item.item_id);

			string response = await StaticHttpClient.GetInstance.RequestFromApp (RequestType.deleteReservation, deleteReservationRequestData);
			DeleteReservationResponse responseObject;
			JsonUtilities<DeleteReservationResponse>.DeserializeToObject (response, out responseObject);

			if (responseObject != null)
			{
				if (!responseObject.IsSucceeded ())
				{
					await DisplayAlert (Constants.warningMessage, responseObject.GetErrorMessage (), Constants.okMessage);
				}
				else
				{
					item.reservation = ReservationType.Reservable;
					SetReservationTypeFeedback();
				}
			}
			else
			{
				await DisplayAlert (Constants.warningMessage, Constants.connectionFailedMessage, Constants.okMessage);
			}
		}

		private async void OnJoinTheKittyButtonClicked(object sender, EventArgs args)
		{
			var reservationRequestData = new ReservationRequest (wishlist.list_id,
			                                                     StaticHttpClient.GetInstance.currentUser.uau_id,
			                                                     item.item_id,
			                                                     null,
			                                                     wishlist.uau_id,
			                                                     "collection",
			                                                     string.Empty,
			                                                     string.Empty);

			string response = await StaticHttpClient.GetInstance.RequestFromApp (RequestType.reservation, reservationRequestData);
			ReservationResponse responseObject;
			JsonUtilities<ReservationResponse>.DeserializeToObject (response, out responseObject);

			if (responseObject != null)
			{
				if (responseObject.IsSucceeded ())
				{
					item.reservation = ReservationType.MyCollect;
					await DisplayAlert (Constants.wellDoneMessage, responseObject.GetErrorMessage (), Constants.okMessage);
					await Navigation.PopAsync();
				}
				else
				{
					await DisplayAlert (Constants.warningMessage, responseObject.GetErrorMessage (), Constants.okMessage);
				}
			}
			else
			{
				await DisplayAlert (Constants.warningMessage, Constants.connectionFailedMessage, Constants.okMessage);
			}
		}

		async private void OnKittyAdminButtonClicked(object sender, EventArgs args)
		{
			await Navigation.PushModalAsync(new NavigationPage(new KittyAdminPage(wishlist, item, userPhoto)));
		}

		private void TruncateSearcName(out string itemName)
		{
			itemName = ItemName.Text;

			if (itemName.Length > 25)
			{
				itemName = itemName.Substring(0, 25);
			}
		}

        private async void OnIconButtonClicked(object sender, EventArgs e)
        {
            var page = new AddToWishListPopUp(item);
            await PopupNavigation.PushAsync(page);
        }

		private async void OnCancelFromKittyButtonClicked(object sender, EventArgs args)
		{
			var deleteReservationRequestData = new DeleteReservationRequest (StaticHttpClient.GetInstance.currentUser.uau_id,
			                                                                 item.item_id);

			string response = await StaticHttpClient.GetInstance.RequestFromApp (RequestType.removeUserFromCollection, deleteReservationRequestData);
			RemoveUserFromCollectionResponse responseObject;
			JsonUtilities<RemoveUserFromCollectionResponse>.DeserializeToObject (response, out responseObject);

			if (responseObject != null)
			{
				if (!responseObject.IsSucceeded ())
				{
					await DisplayAlert (Constants.warningMessage, responseObject.GetErrorMessage (), Constants.okMessage);
				}
				else
				{
					item.reservation = ReservationType.ActiveCollect;
					SetReservationTypeFeedback();
				}
			}
			else
			{
				await DisplayAlert (Constants.warningMessage, Constants.connectionFailedMessage, Constants.okMessage);
			}
		}

		async private Task SetReservation()
        {
			ExpirationDateLabel.Text = Constants.genericReservationExpirationLabel + GetExpiration() + " " + Constants.daysLabel;
            var expirationDT = DateTime.Now;
            expirationDT = expirationDT.AddDays(GetExpiration());      // reservation expiration date is after 15 days

            var expirationDate = string.Format("{0:yyyy-MM-dd}", expirationDT);

            var reservationRequestData = new ReservationRequest(wishlist.list_id,
                                                                 StaticHttpClient.GetInstance.currentUser.uau_id,
                                                                 item.item_id,
                                                                 expirationDate,
                                                                 wishlist.uau_id,
                                                                 "standard",
                                                                 string.Empty,
                                                                 string.Empty);

            string response = await StaticHttpClient.GetInstance.RequestFromApp(RequestType.reservation, reservationRequestData);
            ReservationResponse responseObject;
            JsonUtilities<ReservationResponse>.DeserializeToObject(response, out responseObject);

            if (responseObject != null)
            {
                if (!responseObject.IsSucceeded())
                {
                    await DisplayAlert(Constants.wellDoneMessage, responseObject.GetErrorMessage(), Constants.okMessage);
                }
                else
                {
                    item.reservation = ReservationType.ReservedByMe;
                    SetReservationTypeFeedback();
                }
            }
            else
            {
                await DisplayAlert(Constants.warningMessage, Constants.connectionFailedMessage, Constants.okMessage);
            }
        }

        private int GetExpiration()
        {
			var selectedValue = ExpirationPicker.Items[ExpirationPicker.SelectedIndex] as string;
			int value = 0;

			if (hasExpiration && ExpirationPicker.SelectedIndex == 0)
			{
				DateTime dt = GetDateFromExpirationString(wishlist.expiration_date);

				value = (int)Math.Ceiling((dt - DateTime.Now).TotalDays);
			}
			else
			{
				string resultString = Regex.Match(selectedValue, @"\d+").Value;
				int.TryParse(resultString, out value);
			}

			return value;
        }

		async private void OnSeeOffersClicked(object sender, EventArgs args)
		{	
			string name;
			TruncateSearcName(out name);
			await  Navigation.PushModalAsync(new NavigationPage(new OffertsPage(name)));
		}

		private DateTime GetDateFromExpirationString(string expirationString)
		{
			DateTime dt;
			DateTime.TryParseExact(wishlist.expiration_date, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out dt);

			return dt;
		}

		async private Task SetCollectElements()
		{
			int partecipantsNumber = await FindKittyPartecipants();

			if (partecipantsNumber == int.Parse(item.partecipants, NumberStyles.Integer))
			{
				JoinTheKittyButton.IsVisible = false;
				JoinTheKittyLabel.IsVisible = false;
			}

			if (item.reservation == ReservationType.ActiveCollect)
			{
				PartecipantLabel.Text = "Alla colletta partecipano " + partecipantsNumber + " amici su " + item.partecipants + ".";
			}
			else
			{
				PartecipantLabel.Text = "Partecipate alla colletta in " + partecipantsNumber + " su " + item.partecipants + ".";
			}

			PartecipantPriceLabel.Text = "La quota a partecipante è " + item.part_amount + " €.";

			PartecipantLabel.IsVisible = true;
			PartecipantPriceLabel.IsVisible = true;
		}

		async private Task<int> FindKittyPartecipants()
		{
			int count = 0;

			var collectedItem = new getUsersCollectionInfoRequest (item.item_id);
			string response = await StaticHttpClient.GetInstance.RequestFromApp (RequestType.getUsersCollectionInfo, collectedItem);
			getUsersCollectionInfoResponse responseObject;
			JsonUtilities<getUsersCollectionInfoResponse>.DeserializeToObject (response, out responseObject);

			if (responseObject != null && responseObject.users_info != null)
			{
				count = responseObject.users_info.Count;
			}
			else
			{
				await DisplayAlert (Constants.warningMessage, Constants.dataLoadFailedMessage, Constants.okMessage);
			}

			return count;
		}

		public void SetDaysLeft(ItemData item)
		{
			DateTime today = DateTime.Now;
			DateTime expiration;
			if (DateTime.TryParseExact(item.expiration_date, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out expiration))
			{
				double differenceInDays = (expiration - today).TotalDays;
				double ceilingDifferenceInDays = Math.Ceiling(differenceInDays);

				if (ceilingDifferenceInDays < 0)
				{
					item.daysLeft = "0";
				}
				else if (ceilingDifferenceInDays > 1)
				{
					item.	daysLeft = ceilingDifferenceInDays.ToString();
					item.daysLabel = Constants.daysLabel;
				}
				else
				{
					item.daysLeft = ceilingDifferenceInDays.ToString();
					item.daysLabel = Constants.dayLabel;
				}
			}
		}
	}
}

