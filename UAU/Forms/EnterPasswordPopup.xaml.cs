﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;

namespace UAU
{
	public partial class EnterPasswordPopup : PopupPage
	{
		ILoginManager ilm;

		public EnterPasswordPopup (ILoginManager ilm)
		{
			InitializeComponent ();

			this.ilm = ilm;
		}

		async private void OnCancelClicked(object sender, EventArgs args)
		{
			await PopupNavigation.PopAsync ();
		}

		async private void OnConfirmClicked(object sender, EventArgs args)
		{
			if (PasswordEntry.Text != string.Empty)
			{
				LoginRequestData loginData = new LoginRequestData ();
				loginData.email = StaticHttpClient.GetInstance.currentUser.email;
				loginData.password = PasswordEntry.Text;

				string response = await StaticHttpClient.GetInstance.RequestFromApp (RequestType.login, loginData);

				if (response != string.Empty)
				{
					CustomLoginResponse responseObject;
					JsonUtilities<CustomLoginResponse>.DeserializeToObject (response, out responseObject);

					if (responseObject != null)
					{
						if (responseObject.IsSucceeded ())
						{
							await StaticHttpClient.GetInstance.SaveUserData (responseObject.user);
							await StaticHttpClient.GetInstance.SavePassword (PasswordEntry.Text);

							if (StaticHttpClient.GetInstance.CheckIfFirstAccessFlagIsOn ())
							{
								await PopupNavigation.PopAsync ();
								ilm.ShowWelcomeNavigationPage ();
							}
							else
							{
								await PopupNavigation.PopAsync ();
								ilm.ShowMainPage ();
							}
						}
						else
						{
							await DisplayAlert (Constants.warningMessage, responseObject.GetErrorMessage (), Constants.okMessage);
							Debug.WriteLine ("Fail to login");
						}
					}
					else
					{
						await DisplayAlert (Constants.warningMessage, Constants.connectionFailedMessage, Constants.okMessage);
					}
				}
				else
				{
					await DisplayAlert (Constants.warningMessage, Constants.connectionFailedMessage, Constants.okMessage);
				}
			}
			else
			{
				await DisplayAlert (Constants.warningMessage, Constants.emptyEntryFieldMessage, Constants.okMessage);
			}
		}
	}
}

