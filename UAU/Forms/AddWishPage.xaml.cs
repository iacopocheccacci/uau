﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using System.Diagnostics;
using Plugin.Media;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Plugin.Media.Abstractions;
using System.IO;
using HtmlAgilityPack;
using CustomRenderer;

namespace UAU
{
	public partial class AddWishPage : CustomPage
	{
		private WishlistData Wishlist;
		private byte[] ImageRaw;
		ObservableCollection<WishlistData> Lists;
		WishlistData SelectedItem;

		private ObservableCollection<MediaFile> _images;
		public  ObservableCollection<MediaFile> Images 
		{
			get { return _images ?? (_images = new ObservableCollection< MediaFile >()); }
			set {
				if(_images != value) 
				{
					_images = value;
					OnPropertyChanged();
				}
			}
		}

		public AddWishPage (WishlistData wishlist)
		{
			InitializeComponent ();
			PopulateList ();

			Wishlist = wishlist;
			ImageRaw = null;
			SelectedItem = null;

#if __IOS__
            ToolbarItem button = new ToolbarItem();
			button.Text = Constants.cancelLabel;
            button.Order = ToolbarItemOrder.Primary;
            button.Priority = 0;
            button.Clicked += OnCancelButtonClicked;
            this.ToolbarItems.Add(button);

            ToolbarItem button2 = new ToolbarItem();
			button2.Text = Constants.doneLabel;
            button2.Order = ToolbarItemOrder.Primary;
            button2.Priority = 1;
            button2.Clicked += OnDoneButtonClicked;
            this.ToolbarItems.Add(button2);
#endif

            var tapGestureRecognizer = new TapGestureRecognizer();
			tapGestureRecognizer.Tapped += OnTapGestureRecognizerTapped;
			PhotoImage.GestureRecognizers.Add(tapGestureRecognizer);
		}

		async private void OnCancelButtonClicked(object sender, EventArgs args)
		{
            await PopPage();
		}

		async private void OnDoneButtonClicked(object sender, EventArgs e)
		{
            await ImportItem();
		}

		async public void CheckEntry(bool isWebLink)
		{
			if (!isWebLink)
			{
				await Navigation.PopModalAsync();
			}

			var link = new GetProductRequestData (LinkEntry.Text);

			lv.ActivateLoading ();
			string response = await StaticHttpClient.GetInstance.RequestFromApp (RequestType.getProductData, link);
			GetProductDataResponse responseObject;

			JsonUtilities<GetProductDataResponse>.DeserializeToObject (response, out responseObject);

			if (responseObject != null)
			{
				if (responseObject.IsSucceeded ())
				{
					Debug.WriteLine (responseObject.title + " " + responseObject.description + " " + responseObject.imageUrl);
					await Navigation.PushModalAsync (new NavigationPage (new EditWishPage (Wishlist.list_id, responseObject.title, responseObject.description, responseObject.imageUrl)));
				}
				else
				{
                    string error = responseObject.GetErrorMessage();
                    if (error != null && error != String.Empty)
                    {
                        await this.DisplayAlert(Constants.warningMessage, error, Constants.okMessage);
                    }
                    else
                    {
                        await this.DisplayAlert(Constants.warningMessage, Constants.InvalidLinkMessage, Constants.okMessage);
                    }
				}
			}
			else
			{
				await this.DisplayAlert (Constants.linkNotFoundTitle, Constants.linkNotFoundMessage, Constants.okMessage);
			}

			lv.DeactivateLoading ();
		}

		async private void OnSearchClicked(object sender, EventArgs e)
		{
			if (LinkEntry.Text.Contains("http://") ||
			    LinkEntry.Text.Contains("https://") ||
			    LinkEntry.Text.Contains("www."))
			{
				CheckEntry(true);
			}
			else
			{
				await Navigation.PushModalAsync(new NavigationPage (new BrowseForTheLinkPage(this, LinkEntry)));
			}
		}

		private void OnSegmentPressed(object sender, EventArgs e)
		{
			if (SegControl.SelectedValue.ToString () == "Usa un link")
			{
				UseLinkPage.IsVisible = true;
				ImportPage.IsVisible = false;
				AddManuallyPage.IsVisible = false;
			}
			else if (SegControl.SelectedValue.ToString () == "Importa")
			{
				UseLinkPage.IsVisible = false;
				ImportPage.IsVisible = true;
				AddManuallyPage.IsVisible = false;
			}
			else if (SegControl.SelectedValue.ToString () == "Manuale")
			{
				UseLinkPage.IsVisible = false;
				ImportPage.IsVisible = false;
				AddManuallyPage.IsVisible = true;
			}
		}

		async private Task SaveItem()
		{
			string now = string.Format("{0:yyyy-MM-dd}", DateTime.Now);
			var item = new AddItemDataRequest (Wishlist.list_id,
									 TitleEntry.Text,
									 NotesEntry.Text,
									 "",
									 ImageRaw,
			                         StaticHttpClient.GetInstance.currentUser.uau_id,
			                         now);

			lv.ActivateLoading ();
			string response = await StaticHttpClient.GetInstance.RequestFromApp (RequestType.addItemToWishList, item);
			AddItemResponse responseObject;

			JsonUtilities<AddItemResponse>.DeserializeToObject (response, out responseObject);

			if (responseObject != null)
			{
				if (responseObject.IsSucceeded ())
				{
					await DisplayAlert (Constants.wellDoneMessage, Constants.itemAddedMessage, Constants.okMessage);
                    await PopPage();
                }
				else
				{
					await this.DisplayAlert (Constants.warningMessage, responseObject.GetErrorMessage (), Constants.okMessage);
				}
			}
			else
			{
				await DisplayAlert (Constants.warningMessage, Constants.dataLoadFailedMessage, Constants.okMessage);
			}

			lv.DeactivateLoading ();
        }

		async void OnTapGestureRecognizerTapped(object sender, EventArgs args)
		{
			await PickPictureAsync ();
		}

		public async Task<string> PickPictureAsync() 
		{
			MediaFile file = null;
			string imageName = string.Empty;

			try 
			{
				lv.ActivateLoading();
				file = await CrossMedia.Current.PickPhotoAsync();

				if(file == null) 
				{ 
					return null; 
				}

				PhotoImage.Source = ImageSource.FromStream(file.GetStream);

				using (var memoryStream = new MemoryStream())
				{
					file.GetStream().CopyTo(memoryStream);
					file.Dispose();
					ImageRaw = memoryStream.ToArray();
				}
			}
			catch(Exception ex) 
			{
				Debug.WriteLine("\nIn PictureViewModel.PickPictureAsync() - Exception:\n{0}\n", ex);
				lv.DeactivateLoading ();
				return null;
			}
			finally 
			{ 
				lv.DeactivateLoading ();

				if(file != null) 
				{ 
					file.Dispose(); 
				} 
			}

			lv.DeactivateLoading ();

			return imageName;
		}

		async private Task PopulateList()
		{
			Lists = new ObservableCollection<WishlistData> ();
			WishlistsView.ItemsSource = Lists;

			lv.ActivateLoading ();
			string response = await StaticHttpClient.GetInstance.RequestFromApp (RequestType.getAllUserLists, StaticHttpClient.GetInstance.currentUser);
			WishlistData[] wishListsList;
			JsonUtilities<WishlistData[]>.DeserializeToObject (response, out wishListsList);

			if (wishListsList != null)
			{
				foreach (WishlistData wishlist in wishListsList)
				{
					if (wishlist.list_id != Wishlist.list_id && wishlist.is_deleted != "1")
					{
                        wishlist.setReservedIcon();
                        wishlist.setDaysLeft();
                        if(wishlist.items_number == null)
                        {
                            wishlist.items_number = "0";
                        }
                        wishlist.detailText = wishlist.items_number + " desideri";
                        Lists.Add (wishlist);
					}
				}
			}
			else
			{
				await DisplayAlert (Constants.warningMessage, Constants.dataLoadFailedMessage, Constants.okMessage);
			}

			lv.DeactivateLoading ();
		}

		private void OnItemSelected(object sender, SelectedItemChangedEventArgs e)
		{
			var item = e.SelectedItem as WishlistData;
			if (item != null)
			{
				SelectedItem = item;
			}
		}

		async private Task ImportListItems()
		{
			if (Wishlist != null)
			{
				var importListRequest = new ImportListRequest (SelectedItem.list_id, Wishlist.list_id);

				lv.ActivateLoading ();
				string response = await StaticHttpClient.GetInstance.RequestFromApp (RequestType.importFromList, importListRequest);
				ImportFromListResponse responseObject;

				JsonUtilities<ImportFromListResponse>.DeserializeToObject (response, out responseObject);

				if (responseObject != null)
				{
					if (responseObject.IsSucceeded ())
					{
						await this.DisplayAlert (Constants.wellDoneMessage, Constants.itemsImportedMessage, Constants.okMessage);
                        await PopPage();
                    }
					else
					{
						await this.DisplayAlert (Constants.warningMessage, Constants.itemsImportFailedMessage, Constants.okMessage);
					}
				}
				else
				{
					await DisplayAlert (Constants.warningMessage, Constants.dataLoadFailedMessage, Constants.okMessage);	
				}

				lv.DeactivateLoading ();
            }
		}

        async public override void OnButtonClick()
        {
            base.OnButtonClick();
            await ImportItem();
        }

        async public override void OnCancelClick()
        {
            base.OnCancelClick();
            await PopPage();
        }

        async private Task  PopPage()
        {
            await Navigation.PopModalAsync();
        }

        async private Task ImportItem()
        {
            if (SegControl.SelectedValue != null)
            {
                if (SegControl.SelectedValue.ToString() == "Usa un link")
                {
                    await PopPage();
                }
                else if (SegControl.SelectedValue.ToString() == "Importa")
                {
                    await ImportListItems();
                }
                else if (SegControl.SelectedValue.ToString() == "Manuale")
                {
                    await SaveItem();
                }
            }
        }
    }
}

