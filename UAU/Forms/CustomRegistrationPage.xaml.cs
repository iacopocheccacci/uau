﻿using System;
using Xamarin.Forms;
using System.Diagnostics;
using System.Collections.Generic;
using CustomRenderer;

namespace UAU
{
	enum GenderEnum
	{
		m = 0,
		f,
		o
	};
		
	public partial class CustomRegistrationPage : CustomPage
	{
		private Entry emailEntry;
		private Entry passwordEntry;

		private float percentageValue = 0;
		private User user;

		public CustomRegistrationPage (Entry emailEntry, Entry passwordEntry)
		{
			user = new User ();

			InitializeComponent ();

			this.emailEntry = emailEntry;
			this.passwordEntry = passwordEntry;

			UpdatePercentage ();

#if __IOS__
            ToolbarItem button = new ToolbarItem();
			button.Text = Constants.cancelLabel;
            button.Order = ToolbarItemOrder.Primary;
            button.Priority = 0;
            button.Clicked += OnCancelClicked;
            this.ToolbarItems.Add(button);
#endif
        }

		async public void OnConfirmClicked(object sender, EventArgs args)
		{
			lv.ActivateLoading ();
			string response = await StaticHttpClient.GetInstance.RequestFromApp (RequestType.customRegistration, user);

			if (response != string.Empty)
			{
				CustomRegistrationResponse responseObject;
				JsonUtilities<CustomRegistrationResponse>.DeserializeToObject (response, out responseObject);

				if (responseObject != null)
				{
					if (responseObject.IsSucceeded ())
					{
						StaticHttpClient.GetInstance.currentUser = user;

						emailEntry.Text = user.email;
						passwordEntry.Text = user.password;

						await this.DisplayAlert (Constants.wellDoneMessage, Constants.customLoginSucceededMessage, Constants.okMessage);
						await Navigation.PopModalAsync ();
					}
					else
					{
						await this.DisplayAlert (Constants.warningMessage, responseObject.GetErrorMessage (), Constants.okMessage);
						lv.DeactivateLoading ();
					}
				}
				else
				{
					await DisplayAlert (Constants.warningMessage, Constants.connectionFailedMessage, Constants.okMessage);
					lv.DeactivateLoading ();
				}
			}
			else
			{
				await DisplayAlert (Constants.warningMessage, Constants.connectionFailedMessage, Constants.okMessage);
				lv.DeactivateLoading ();
			}
		}

		private GenderEnum getGenderTypeFromIndex(int index)
		{
			GenderEnum result;

			switch (index)
			{
			case 0:
				result = GenderEnum.m;
				break;

			case 1:
				result = GenderEnum.f;
				break;

			case 2:
			default:
				result = GenderEnum.o;
				break;
			}

			return result;
		}

		private void OnTextChanged(object sender, EventArgs args)
		{
			UpdateField (sender);
		}

		private void UpdateField(object sender)
		{
			if (sender == FirstNameEntry)
			{
				user.firstName = FirstNameEntry.Text;
			}
	
			if (sender == LastNameEntry)
				user.lastName = LastNameEntry.Text;
	
			if (sender == EmailEntry)
				user.email = EmailEntry.Text;

			if (sender == PasswordEntry)
				user.password = PasswordEntry.Text;

			if (sender == BirthDayEntry)
				user.birthday = string.Format("{0:dd/MM/yyyy}", BirthDayEntry.Date);

			if (sender == GenderEntry)
				user.sex = getGenderTypeFromIndex(GenderEntry.SelectedIndex).ToString();

			if (sender == CityEntry)
				user.city = CityEntry.Text;

			if (sender == CountryEntry)
				user.country = CountryEntry.Text;
		}

		private void UpdatePercentage()
		{
			int value = 0;

			if (user.firstName != null && user.firstName != string.Empty)
				value += 13;

			if (user.lastName != null && user.lastName != string.Empty)
				value += 13;

			if (user.email != null && user.email != string.Empty)
				value += 13;

			if (user.password != null && user.password != string.Empty)
				value += 13;

			if (user.birthday != null && user.birthday != string.Empty)
				value += 12;

			if (user.sex != null && user.sex != string.Empty)
				value += 12;

			if (user.city != null && user.city != string.Empty)
				value += 12;

			if (user.country != null && user.country != string.Empty)
				value += 12;

			percentageValue = value;

			PercentageLabel.Text = percentageValue.ToString() + "% completato";
			PercentageProgressBar.Progress = (percentageValue / 100);
		}
	
		async private void OnCancelClicked(object sender, EventArgs args)
		{
			await Navigation.PopModalAsync ();
		}

        async public override void OnCancelClick()
        {
            await Navigation.PopModalAsync();
        }
    }
}

