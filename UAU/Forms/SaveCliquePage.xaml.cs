﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using CustomRenderer;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;

namespace UAU
{
	public partial class SaveCliquePage : CustomPage, PopupParentPageInterface
	{
		ObservableCollection<FriendContactData> friendsData;

		public SaveCliquePage ()
		{
			InitializeComponent ();

			PopulateList ();

#if __IOS__
			ToolbarItem button = new ToolbarItem();
			button.Text = Constants.doneLabel;
			button.Order = ToolbarItemOrder.Primary;
			button.Priority = 1;
			button.Clicked += OnSaveGroupButtonClicked;
			this.ToolbarItems.Add(button);
#endif
		}

		async private Task PopulateList()
		{
			friendsData = new ObservableCollection<FriendContactData> ();
			FriendsView.ItemsSource = friendsData;

			var user = new UserFriendsRequestData (StaticHttpClient.GetInstance.currentUser.uau_id);
			lv.ActivateLoading ();
			string response = await StaticHttpClient.GetInstance.RequestFromApp (RequestType.getUserFriends, user);
			FriendList friendlist;
			JsonUtilities<FriendList>.DeserializeToObject (response, out friendlist);

			if (friendlist.friends != null)
			{
				foreach (User friend in friendlist.friends)
				{
					DateTime dt = DateTime.ParseExact (friend.birthday, "yyyy-MM-dd", CultureInfo.InvariantCulture);
					var birthday = string.Format("Compleanno: {0} {1}", dt.Day, dt.ToMonthName());

					friendsData.Add (new FriendContactData { FullName = friend.firstName + " " + friend.lastName,
						Id = friend.uau_id,
						Picture = friend.photo,
						IsEnabled = true,
						Birthday = birthday
					});
				}
			}
			else
			{
				await DisplayAlert (Constants.warningMessage, Constants.dataLoadFailedMessage, Constants.okMessage);
			}

			// Disable selection
			FriendsView.ItemSelected += (sender, e) => {
				((ListView)sender).SelectedItem = null;
			};

			lv.DeactivateLoading ();
		}

		async public Task SaveClique (string name)
		{
			Debug.WriteLine ("Save clique " + name);

			var friends = GetSelectedFriends();
			await SendNewCircleRequest (friends, name);
		}

		private List<FriendData> GetSelectedFriends()
		{
			var friends = new List<FriendData> ();

			foreach (FriendContactData friend in FriendsView.ItemsSource)
			{
				if (friend.IsEnabled)
				{
					FriendData data = new FriendData ();
					data.id = friend.Id;
					data.name = friend.FullName;

					friends.Add (data);
				}
			}

			return friends;
		}

		async private Task SendNewCircleRequest(List<FriendData> friends, string circleName)
		{
			var request = new AddFriendsRequest ();
			request.cliqueName = circleName;
			request.friends = friends;
			request.owner_id = StaticHttpClient.GetInstance.currentUser.uau_id;

			lv.ActivateLoading ();
			string response = await StaticHttpClient.GetInstance.RequestFromApp (RequestType.saveClique, request);

			if (response != string.Empty)
			{
				ResponseObject responseObject;
				JsonUtilities<ResponseObject>.DeserializeToObject (response, out responseObject);

				if (responseObject != null)
				{
					if (responseObject.IsSucceeded ())
					{
						await DisplayAlert (Constants.circleSavedTitle, Constants.circleSavedMessage, Constants.okMessage);
						await Navigation.PopAsync();
					}
					else
					{
						await DisplayAlert (Constants.warningMessage, Constants.connectionFailedMessage, Constants.okMessage);
					}
				}
				else
				{
					await DisplayAlert (Constants.warningMessage, Constants.dataLoadFailedMessage, Constants.okMessage);
				}
			}
			else
			{
				await DisplayAlert (Constants.warningMessage, Constants.connectionFailedMessage, Constants.okMessage);
			}

			lv.DeactivateLoading ();
		}

		async private void OnSaveGroupButtonClicked(object sender, EventArgs args)
		{
			var page = new InputPopup(this);
			await PopupNavigation.PushAsync(page);
		}

		private void OnTextChanged(object sender, EventArgs args)
		{
			#if __IOS__
			FriendsView.BeginRefresh ();
			#endif 

			if (string.IsNullOrWhiteSpace (SearchBarField.Text)) {
				FriendsView.ItemsSource = friendsData;
			} else {
				FriendsView.ItemsSource = friendsData
											.Where (x => x.FullName.ToLower ()
											.Contains (SearchBarField.Text.ToLower ()));
			}

#if __IOS__
			FriendsView.EndRefresh ();
			#endif
		}

		private void OnUnselectAllClicked(object sender, EventArgs args)
		{
			bool selected;

			if (SelectAllButton.Text == Constants.unselectLabel)
			{
				selected = false;
				SelectAllButton.Text = Constants.selectLabel;
			}
			else
			{
				selected = true;
				SelectAllButton.Text = Constants.unselectLabel;
			}

			for (int i = 0; i < friendsData.Count; i++)
			{
				FriendContactData tmp = friendsData [i];
				tmp.IsEnabled = selected;
				friendsData [i] = tmp;
			}
		}

        async public override void OnButtonClick()
        {
            base.OnButtonClick();
            var page = new InputPopup(this);
            await PopupNavigation.PushAsync(page);
        }
    }
}

