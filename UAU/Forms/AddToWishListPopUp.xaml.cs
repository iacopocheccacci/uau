﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;

using Xamarin.Forms;
using CustomRenderer;
using System.Collections.ObjectModel;
using System.Net;
using System.IO;

namespace UAU
{
	public partial class AddToWishListPopUp : PopupPage
	{
        ObservableCollection<WishlistData> Lists;
        List<string> nameList = new List<string>();
        ItemData itemData;
        byte[] imageByte;

        public AddToWishListPopUp (ItemData item)
		{
			InitializeComponent ();

            this.itemData = item;
            itemImage.Source = item.photo;
            DownloadImage(item.photo);

            PopulateList();
        }

        public AddToWishListPopUp(CustomPage parent, ItemData item, byte[] image)
        {
            InitializeComponent();

            this.itemData = item;
            itemImage.Source = ImageSource.FromStream(() => new MemoryStream(image));
            imageByte = image;

            PopulateList();
        }

		async private Task PopulateList()
        {
            Lists = new ObservableCollection<WishlistData>();
            WishlistsView.ItemsSource = Lists;

            string response = await StaticHttpClient.GetInstance.RequestFromApp(RequestType.getAllUserLists, StaticHttpClient.GetInstance.currentUser);
            WishlistData[] wishListsList;
            JsonUtilities<WishlistData[]>.DeserializeToObject(response, out wishListsList);

            if (wishListsList != null)
            {
                foreach (WishlistData wishlist in wishListsList)
                {
                    if (wishlist.is_deleted != "1")
                    {
                        wishlist.setReservedIcon();
                        wishlist.setDaysLeft();
                        wishlist.detailText = wishlist.items_number + " desideri";
                        Lists.Add(wishlist);
                    }
                }
            }
            else
            {
                await DisplayAlert(Constants.warningMessage, Constants.dataLoadFailedMessage, Constants.okMessage);
            }
        }

        async private void OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var wishlistData = e.SelectedItem as WishlistData;
            if (wishlistData != null)
            {
                await GetItemName(wishlistData);
            }
            WishlistsView.SelectedItem = null;
        }

        async private Task AddItemTolist(ItemData item, WishlistData wishlistData)
        {
			string now = string.Format("{0:yyyy-MM-dd}", DateTime.Now);
            var itemToAdd = new AddItemDataRequest(wishlistData.list_id,
                                   item.title,
                                   item.description,
                                   "",
                                   imageByte, 
			                       StaticHttpClient.GetInstance.currentUser.uau_id,
			                       now);
            await SaveItem(itemToAdd);
        }

        async private Task SaveItem(AddItemDataRequest itemToAdd)
        {
            string response = await StaticHttpClient.GetInstance.RequestFromApp(RequestType.addItemToWishList, itemToAdd);
            AddItemResponse responseObject;

            JsonUtilities<AddItemResponse>.DeserializeToObject(response, out responseObject);

            if (responseObject != null)
            {
                if (responseObject.IsSucceeded())
                {
					// TODO: cambiare perché il display alert non si vede su iOS
                    await PopupNavigation.PopAsync();
                    await DisplayAlert(Constants.wellDoneMessage, Constants.itemAddedMessage, Constants.okMessage);
                }
                else
                {
                    await this.DisplayAlert(Constants.warningMessage, responseObject.GetErrorMessage(), Constants.okMessage);
                }
            }
            else
            {
                await DisplayAlert(Constants.warningMessage, Constants.dataLoadFailedMessage, Constants.okMessage);
            }

            lv.DeactivateLoading();
        }

        private void DownloadImage(string imageUrl)
        {
            lv.ActivateLoading();
            var webClient = new WebClient();
            webClient.DownloadDataCompleted += (s, e) => {
                imageByte = e.Result; // get the downloaded data
                lv.DeactivateLoading();
            };
            var url = new Uri(imageUrl);
            webClient.DownloadDataAsync(url);
        }

        private async Task GetItemName(WishlistData wishlist)
        {
            lv.ActivateLoading();
            string response = await StaticHttpClient.GetInstance.RequestFromApp(RequestType.getWishListItems, wishlist);
            ItemData[] ItemsList;
            JsonUtilities<ItemData[]>.DeserializeToObject(response, out ItemsList);
        
            if (ItemsList != null)
            {                
                foreach (ItemData item in ItemsList)
                {
                    nameList.Add(item.title);
                }
                
				var result = nameList.Find(x => x == itemData.title);
                
				if (result == null)
                {
					await AddItemTolist(itemData, wishlist);
                }
                else
                {
                    lv.DeactivateLoading();
                    await DisplayAlert(Constants.warningMessage, Constants.itemExistMessage, Constants.okMessage);
                }
            }
            else
            {
                lv.DeactivateLoading();
                await DisplayAlert(Constants.warningMessage, Constants.dataLoadFailedMessage, Constants.okMessage);
            }
        }

        private void OnBackgroundClicked(object sender, EventArgs e)
        {
            
        }
    }
}
