﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using System.Threading.Tasks;

namespace UAU
{
	public partial class LoadingView : ContentView
	{
		public LoadingView ()
		{
			InitializeComponent ();
		}

		public void ActivateLoading()
		{
			IsEnabled = true;
			GrayLayer.IsVisible = true;
			LoadingIndicator.IsVisible = true;
			LoadingIndicator.IsRunning = true;
		}

		async public void DeactivateLoading()
		{
			await Task.Delay (1000);
			IsEnabled = false;
			GrayLayer.IsVisible = false;
			LoadingIndicator.IsVisible = false;
			LoadingIndicator.IsRunning = false;
		}

        public bool IsRunning()
        {
            return LoadingIndicator.IsRunning;
        }
	}
}

