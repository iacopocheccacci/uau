﻿using System;
using System.Collections.Generic;

namespace UAU
{
	public enum FeedType
	{
		none = 0,						// default value
		kitty_invite,  					// invite to join a kitty
		kitty_user_subscription,		// a friend join a kitty of yours
		removed_reserved_item,		    // reserved by you item is removed from list
		user_friendship_invite,			// friendship request received
		user_wishlist_add_invite,		// friend invite you to create a wishlist
		user_subscribed_kitty_deleted,	// joined by you kitty is removed
		fb_user_joined,					// facebook friend has joined SpyGift
		friend_wishlist_created,		// friend add a new wishlist
		friend_wishlist_item_added		// friend add a new item in one of his wishlists
	}

	public enum FeedButtonType
	{
		none = 0,			// default value
		go_to_list,			// redirect to specified wishlist page
		wish_detail,		// redirect to specified item page
		go_to_profile,		// redirect to specified friend lists page
		kitty_admin			// redirect to specified kitty admin page
	}

	public enum FeedCategoryType
	{
		spotlight = 0,
		not_spotlight
	}

	public class FeedRequest
	{
		public string uau_id		{ get; set; }

		public FeedRequest (string uau_id)
		{
			this.uau_id = uau_id;
		}
	}

	// -------------------------------------------//
	// Response received after get feed request
	// - id: feed id
	// - uau_id: id of receiver user
	// - feed_type: type of the feed
	// - feed_category: category of the feed
	// - feed_text: text of the feed
	// - feed_btn: array of buttons to display
	// - feed_data: data of the feed
	// - feed_date: date of the feed in format yyyy-MM-dd hh:mm:ss
	// - feed_read_status: 1 if read, 0 if not
	//
	// Used in:
	// - FeedPage
	// -------------------------------------------//
	public class FeedResponse
	{
		public string id				{ get; set; }
		public string uau_id			{ get; set; }
		public string feed_type			{ get; set; }
		public string feed_category		{ get; set; }
		public string feed_text			{ get; set; }
		public List<string> feed_btn	{ get; set; }
		public FeedData feed_data		{ get; set; }
		public string feed_date			{ get; set; }
		public string feed_read_status	{ get; set; }

		public FeedResponse(string id, string uau_id, string feed_type, string feed_category, 
		                    string feed_text, List<string> feed_btn, FeedData feed_data, string feed_date,
		                    string feed_read_status)
		{
			this.id = id;
			this.uau_id = uau_id;
			this.feed_type = feed_type;
			this.feed_category = feed_category;
			this.feed_text = feed_text;
			this.feed_btn = feed_btn;
			this.feed_data = feed_data;
			this.feed_date = feed_date;
			this.feed_read_status = feed_read_status;
		}

		public FeedType GetFeedType()
		{
			FeedType type = FeedType.none;

			switch (feed_type)
			{
				case Constants.kittyInvite:
					type = FeedType.kitty_invite;
					break;

				case Constants.kittyUserSubscription:
					type = FeedType.kitty_user_subscription;
					break;

				case Constants.removeReservedItem:
					type = FeedType.removed_reserved_item;
					break;

				case Constants.userFriendshipInvite:
					type = FeedType.user_friendship_invite;
					break;

				case Constants.userWishlistAddInvite:
					type = FeedType.user_wishlist_add_invite;
					break;

				case Constants.userSubscribedKittyDeleted:
					type = FeedType.user_subscribed_kitty_deleted;
					break;

				case Constants.fbUserJoined:
					type = FeedType.fb_user_joined;
					break;

				case Constants.friendWishlistCreated:
					type = FeedType.friend_wishlist_created;
					break;

				case Constants.friendWishlistItemAdded:
					type = FeedType.friend_wishlist_item_added;
					break;
			}

			return type;
		}

		public FeedButtonType GetButtonType(int index)
		{
			FeedButtonType type = FeedButtonType.none;

			if (index < feed_btn.Count)
			{
				switch (feed_btn[index])
				{
					case Constants.goToList:
						type = FeedButtonType.go_to_list;
						break;

					case Constants.wishDetail:
						type = FeedButtonType.wish_detail;
						break;

					case Constants.goToProfile:
						type = FeedButtonType.go_to_profile;
						break;

					case Constants.kittyAdmin:
						type = FeedButtonType.kitty_admin;
						break;
				}
			}

			return type;
		}

		public FeedCategoryType GetCategory()
		{
			FeedCategoryType type = FeedCategoryType.not_spotlight;

			switch (feed_category)
			{
				case Constants.spotlight:
					type = FeedCategoryType.spotlight;
					break;

				case Constants.notSpotloght:
					type = FeedCategoryType.not_spotlight;
					break;
			}

			return type;
		}
	}

	// -------------------------------------------//
	// Data of the feed
	// - friend_id: id of the user that genereted feed
	// - firstname: name of the user that generated feed
	// - lastname: lastname of the user that generated feed
	// - user_photo: picture if the user that generated feed
	// - wishlist_id: id of the wishlist (if needed)
	// - wishlist_name: name of the wishlist (if needed)
	//
	// Used in:
	// - FeedPage
	// -------------------------------------------//
	public class FeedData
	{
		public string friend_id			{ get; set; }
		public string firstname			{ get; set; }
		public string lastname			{ get; set; }
		public string user_photo		{ get; set; }
		public string item_id			{ get; set; }
		public string item_title		{ get; set; }
		public string wishlist_id		{ get; set; }
		public string wishlist_name		{ get; set; }

		public FeedData(string friend_id, string firstname, string lastname, string user_photo,
		                string item_id, string item_name, string wishlist_id, string wishlist_name)
		{
			this.friend_id = friend_id;
			this.firstname = firstname;
			this.lastname = lastname;
			this.user_photo = user_photo;
			this.item_id = item_id;
			this.item_title = item_name;
			this.wishlist_id = wishlist_id;
			this.wishlist_name = wishlist_name;
		}
	}
}

