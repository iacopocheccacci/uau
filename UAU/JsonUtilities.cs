﻿using System;
using Newtonsoft.Json;
using System.Diagnostics;

namespace UAU
{
	public class JsonUtilities<T>
	{
//		public enum ObjectType
//		{
//			ResponseObject = 0,
//			FBUserObject
//		}

		public static string SerializeObj(object value)
		{
			string result = "";

			try
			{
				result = JsonConvert.SerializeObject (value);
			}
			catch (JsonSerializationException e)
			{
				Debug.WriteLine (e.Message);
				Debug.WriteLine ("Serialization error on SerializeObj function.");
			}

			return result;
		}
			
		public static void DeserializeToObject(string value, out T obj)
		{
			obj = default(T);

			try
			{
				obj = JsonConvert.DeserializeObject <T>(value);
			}
			catch(JsonSerializationException e)
			{
				Debug.WriteLine (e.Message);
				Debug.WriteLine ("Deserialization error on DeserializeObj function.");
			}
			catch(JsonReaderException re)
			{
				Debug.WriteLine (re.Message);
				Debug.WriteLine ("Reading error on DeserializeObj function.");
			}
		}
	}
}

