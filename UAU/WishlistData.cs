﻿using System;
using System.Globalization;

namespace UAU
{
	public class WishlistData
	{
		public string list_id 				{ get; set; }
		public string uau_id 				{ get; set; }
		public string listName 				{ get; set; }
		public string visibility 			{ get; set; }
		public string is_permanent 			{ get; set; }
		public string expiration_date 		{ get; set; }
		public string date_created 			{ get; set; }
		public string group_id 				{ get; set; }
		public string is_deleted			{ get; set; }
		public string detailText			{ get; set; }
		public bool lockOn					{ get; set; }
		public string items_number			{ get; set; }
		public string expireInLabel			{ get; set; }
		public string daysLeft				{ get; set; }
		public string daysLabel				{ get; set; }

		public WishlistData()
		{
			list_id = string.Empty;
			uau_id = string.Empty;
			listName = string.Empty;
			visibility = string.Empty;
			is_permanent = string.Empty;
			expiration_date = string.Empty;
			date_created = string.Empty;
			group_id = string.Empty;
			is_deleted = string.Empty;
			detailText = string.Empty;
			lockOn = false;
			items_number = string.Empty;
			expireInLabel = string.Empty;
			daysLeft = string.Empty;
			daysLabel = string.Empty;
		}

		public void setReservedIcon()
		{
			if (visibility == "private")
			{
				lockOn = true;
			}
			else
			{
				lockOn = false;
			}
		}

		public void setDaysLeft()
		{
			if (is_permanent == "0")
			{
				DateTime today = DateTime.Now;
				DateTime expiration = DateTime.ParseExact (expiration_date, "yyyy-MM-dd", CultureInfo.InvariantCulture);

				TimeSpan ts = expiration - today;
				int differenceInDays = ts.Days;

				if (differenceInDays < 0)
				{
					daysLeft = Constants.expiredLabel;
				}
				else if (differenceInDays > 1)
				{
					expireInLabel = Constants.expireInLabel;
					daysLeft = differenceInDays.ToString ();
					daysLabel = Constants.daysLabel;
				}
				else
				{
					expireInLabel = Constants.expireInLabel;
					daysLeft = differenceInDays.ToString ();
					daysLabel = Constants.dayLabel;
				}
			}
		}
	}

	public class WishlistToAdd
	{
		public string list_id				{ get; set; }
		public string uau_id				{ get; set; }
		public string listName				{ get; set; }
		public string is_permanent			{ get; set; }
		public string reserved				{ get; set; }
		public string visibility			{ get; set; }
		public string expiration_date		{ get; set; }

		public WishlistToAdd(string list_id, string uau_id, string listName, string is_permanent, string reserved, DateTime expiration_date)
		{
			this.list_id = list_id;
			this.uau_id = uau_id;
			this.listName = listName;
			this.is_permanent = is_permanent;
			this.reserved = reserved;
			this.visibility = (reserved == "1") ? "private" : "public";

			if (is_permanent == "0")
			{
				this.expiration_date = string.Format("{0:yyyy-MM-dd}", expiration_date.Date);
			}
			else
			{
				this.expiration_date = string.Empty;
			}
		}

	}
}

