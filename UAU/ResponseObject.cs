﻿using System;

namespace UAU
{
	public class ResponseObject
	{
		public string s    { get; set; }

		public ResponseObject()
		{
			s = string.Empty;
		}

		public bool IsSucceeded()
		{
			return s.ToLower() == "true" ? true : false;
		}
	}
}

