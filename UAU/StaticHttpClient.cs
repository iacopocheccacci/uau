﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Text;

namespace UAU
{
	public enum RequestType
	{
		login = 0,
		customRegistration,
		fbRegistration,
		forgotPassword,
		changePassword,
		saveClique,
		getUserByEmail,
		getAllUserLists,
		createWishlist,
		removeWishlist,
		getUserFriends,
		getUserCliques,
		addFriendsToList,
		addItemToWishList,
		getWishListItems,
		getProductData,
		getWishListGroupNumber,
		deleteItem,
		addAdminToList,
		importFromList,
		editList,
		getCliqueFriends,
		getSharedLists,
		checkReservation,
		reservation,
		amazonMarketPlaceProducts,
		deleteUserFromFriends,
        getAllUsers,
		deleteReservation,
		removeUserFromCollection,
		editCollection,
		getUsersCollectionInfo,
		updateProfile,
		deleteUserFromClique,
		addUserToClique,
		deleteClique,
        getWishListFriends,
		deleteUserFromWishlist,
		getUserFeed,
		getUserById,
		getItemInfo,
		getWishlistInfo,
		getFriendsBirthday
    }

	public class StaticHttpClient : HttpClient
	{
		private static StaticHttpClient instance;

		private const string urlString = "http://52.31.185.204/";
		private const string contentType = "application/x-www-form-urlencoded";
		private const string loginFunctionName = "auth/login/";
		private const string customRegistrationFunctionName = "auth/custom_registration/";
		private const string fbRegistrationFunctionName = "auth/fb_register/";
		private const string forgotPasswordFunctionName = "auth/forgotPassword/";
		private const string changePasswordRequestFunctionName = "auth/changePasswordRequest/";
		private const string saveCliqueFunctionName = "user/saveClique/";  // also to store friends
		private const string getUserByEmailFunctionName = "user/getUserByEmail/";
		private const string getAllUserListsFunctionName = "user/getAllUserLists/";
		private const string createWishlistFunctionName = "lists/saveList/";
		private const string removeWishlistFunctionName = "lists/deleteWishList/";
		private const string getUserFriendsFunctionName = "lists/userFriends/";
		private const string getUserCliquesFunctionName = "lists/userCliques/";
		private const string addFriendsToListFunctionName = "lists/addFriendsToList/";
		private const string addItemToWishListFunctionName = "lists/addItemToList/";
		private const string getWishListItemsFunctionName = "user/getWishListItems/";
		private const string getProductDataFunctionName = "lists/getProductDataTest/";
		private const string getWishListGroupNumbersFunctionName = "user/getWishListGroupNumbers/";
		private const string deleteItemFunctionName = "lists/deleteItem/";
		private const string addAdminToListFunctionName = "lists/addAdminToList/";
		private const string importFromListFunctionName = "lists/importFromList/";
		private const string editListFunctionName = "lists/editList/";
		private const string getCliqueFriendsFunctionName = "user/getCliqueFriends/";
		private const string getSharedListsFunctionName = "lists/getSharedLists/";
		private const string checkReservationFunctionName = "lists/checkReservation/";
		private const string reservationFunctionName = "lists/reservation/";
		private const string amazonMarketPlaceProductsFunctionName = "user/amazonMarketPlaceProducts/";
		private const string deleteUserFromFriendsFunctionName = "user/deleteUserFromFriends/";
		private const string deleteReservationFunctionName = "lists/deleteReservation/";
        private const string getAllUsersFunctionName = "user/getAllUsers/";
		private const string removeUserFromCollectionFunctionName = "lists/removeUserFromCollection/";
		private const string editCollectionFunctionName = "lists/editCollection/";
		private const string getUsersCollectionInfoFunctionName = "lists/getUsersCollectionInfo/";
		private const string updateProfileFunctionName = "user/profileUpdate/";
		private const string deleteUserFromCliqueFunctionName = "user/deleteUserFromClique/";
		private const string addUserToCliqueFunctionName = "user/addUserToClique/";
		private const string deleteCliqueFunctionName = "user/deleteClique/";
        private const string getWishListFriendsFunctionName = "user/getWishListFriends/";
		private const string deleteUserFromWishlistFunctionName = "user/deleteUserFromWishlist/";
		private const string getUserFeedFunctionName = "user/getUserFeed/";
		private const string getUserByIdFunctionName = "user/getUserById/";
		private const string getiteminfoFunctionName = "lists/getiteminfo/";
		private const string getWishlistInfoFunctionName = "lists/getWishlistInfo/";
		private const string getFriendsBirthday = "user/getFriendsBirthday/";


        public User currentUser { get; set; }

		private StaticHttpClient () : base() 
		{
			currentUser = new User ();	
		}

		public static StaticHttpClient GetInstance
		{
			get
			{
				if (instance == null)
				{
					instance = new StaticHttpClient ();
				}

				return instance;
			}
		}

		async public Task<string> RequestFromApp(RequestType type, object value)
		{
			var json = JsonUtilities<RequestType>.SerializeObj(value);
			var content = new StringContent(json, Encoding.UTF8, contentType);
			string items = string.Empty;

			Debug.WriteLine (json.ToString ());

			HttpResponseMessage response = null;

			try
			{
				response = await instance.PostAsync (getUri(type), content);

				if (response.IsSuccessStatusCode)
				{
					items = response.Content.ReadAsStringAsync ().Result;
					Debug.WriteLine (items);
				}
				else
				{
					Debug.WriteLine ("Failure");
				}
			}
			catch (Exception e)
			{
				Debug.WriteLine (e.Message);
			}
				
			return items;
		}
			
		private Uri getUri(RequestType type)
		{
			Uri uri;
			var uriString = urlString;

			switch (type)
			{
				case RequestType.login:
					uriString += loginFunctionName;
					break;

				case RequestType.customRegistration:
					uriString += customRegistrationFunctionName;
					break;

				case RequestType.fbRegistration:
					uriString += fbRegistrationFunctionName;
					break;

				case RequestType.forgotPassword:
					uriString += forgotPasswordFunctionName;
					break;

				case RequestType.changePassword:
					uriString += changePasswordRequestFunctionName;
					break;

				case RequestType.saveClique:
					uriString += saveCliqueFunctionName;
					break;

				case RequestType.getUserByEmail:
					uriString += getUserByEmailFunctionName;
					break;

				case RequestType.getAllUserLists:
					uriString += getAllUserListsFunctionName + currentUser.uau_id;
					break;

				case RequestType.createWishlist:
					uriString += createWishlistFunctionName;
					break;

				case RequestType.removeWishlist:
					uriString += removeWishlistFunctionName;
					break;

				case RequestType.getUserFriends:
					uriString += getUserFriendsFunctionName;
					break;

				case RequestType.getUserCliques:
					uriString += getUserCliquesFunctionName;
					break;

				case RequestType.addFriendsToList:
					uriString += addFriendsToListFunctionName;
					break;

				case RequestType.addItemToWishList:
					uriString += addItemToWishListFunctionName;
					break;

				case RequestType.getWishListItems:
					uriString += getWishListItemsFunctionName;
					break;

				case RequestType.getProductData:
					uriString += getProductDataFunctionName;
					break;

				case RequestType.getWishListGroupNumber:
					uriString += getWishListGroupNumbersFunctionName;
					break;

				case RequestType.deleteItem:
					uriString += deleteItemFunctionName;
					break;

				case RequestType.addAdminToList:
					uriString += addAdminToListFunctionName;
					break;

				case RequestType.importFromList:
					uriString += importFromListFunctionName;
					break;

				case RequestType.editList:
					uriString += editListFunctionName;
					break;

				case RequestType.getCliqueFriends:
					uriString += getCliqueFriendsFunctionName;
					break;

				case RequestType.getSharedLists:
					uriString += getSharedListsFunctionName;
					break;

				case RequestType.checkReservation:
					uriString += checkReservationFunctionName;
					break;

				case RequestType.reservation:
					uriString += reservationFunctionName;
					break;

				case RequestType.amazonMarketPlaceProducts:
					uriString += amazonMarketPlaceProductsFunctionName;
					break;

				case RequestType.deleteUserFromFriends:
					uriString += deleteUserFromFriendsFunctionName;
					break;

	            case RequestType.getAllUsers:
	                uriString += getAllUsersFunctionName;
	                break;

				case RequestType.deleteReservation:
					uriString += deleteReservationFunctionName;
					break;

				case RequestType.removeUserFromCollection:
					uriString += removeUserFromCollectionFunctionName;
					break;

				case RequestType.editCollection:
					uriString += editCollectionFunctionName;
					break;

				case RequestType.getUsersCollectionInfo:
					uriString += getUsersCollectionInfoFunctionName;
					break;

				case RequestType.updateProfile:
					uriString += updateProfileFunctionName;
					break;

				case RequestType.deleteUserFromClique:
					uriString += deleteUserFromCliqueFunctionName;
					break;

				case RequestType.addUserToClique:
					uriString += addUserToCliqueFunctionName;
					break;

				case RequestType.deleteClique:
					uriString += deleteCliqueFunctionName;
					break;

                case RequestType.getWishListFriends:
                    uriString += getWishListFriendsFunctionName;
                    break;

				case RequestType.deleteUserFromWishlist:
					uriString += deleteUserFromWishlistFunctionName;
					break;

				case RequestType.getUserFeed:
					uriString += getUserFeedFunctionName;
					break;

				case RequestType.getUserById:
					uriString += getUserByIdFunctionName;
					break;

				case RequestType.getItemInfo:
					uriString += getiteminfoFunctionName;
					break;

				case RequestType.getWishlistInfo:
					uriString += getWishlistInfoFunctionName;
					break;

				case RequestType.getFriendsBirthday:
					uriString += getFriendsBirthday;
					break;
            }

			uri = new Uri (string.Format(uriString));

			return uri;
		}

		async public Task<string> GetUserIdFromEmail(string email)
		{
			string id = null;
			var emailData = new UserIdRequestData ();
			emailData.email = email;

			string response = await StaticHttpClient.GetInstance.RequestFromApp (RequestType.getUserByEmail, emailData);
			UserIdResponse responseObject;

			JsonUtilities<UserIdResponse>.DeserializeToObject (response, out responseObject);

			if (responseObject != null && responseObject.s != null && responseObject.IsSucceeded())
			{
				id = responseObject.uau_id;
			}

			return id;
		}

		public void FillCurrentUserData()
		{
			var user = StaticHttpClient.GetInstance.currentUser;
			var properties = App.CurrentApp.Properties;

			if (user != null)
			{
				user.password = properties.ContainsKey (Constants.userPasswordProperty) ? (string)properties[Constants.userPasswordProperty] : string.Empty;
				user.email = properties.ContainsKey (Constants.userEmailProperty) ? (string)properties [Constants.userEmailProperty] : string.Empty;
				user.uau_id = properties.ContainsKey (Constants.userIdProperty) ? (string)properties [Constants.userIdProperty] : string.Empty;
				user.firstName = properties.ContainsKey (Constants.nameProperty) ? (string)properties [Constants.nameProperty] : string.Empty;
				user.lastName = properties.ContainsKey (Constants.surnameProperty) ? (string)properties [Constants.surnameProperty] : string.Empty;
				user.birthday = properties.ContainsKey (Constants.birthdayProperty) ? (string)properties [Constants.birthdayProperty] : string.Empty;
			}
		}

		async public Task SaveUserData(User userData)
		{
			if (userData != null)
			{
				App.CurrentApp.Properties [Constants.isLoggedInProperty] = true;
				await App.CurrentApp.SavePropertiesAsync();

				App.CurrentApp.Properties [Constants.userEmailProperty] = userData.email;
				await App.CurrentApp.SavePropertiesAsync();

	//			App.CurrentApp.Properties ["UserPassword"] = userData.password;
				App.CurrentApp.Properties [Constants.userIdProperty] = userData.uau_id;
				await App.CurrentApp.SavePropertiesAsync();

				App.CurrentApp.Properties [Constants.nameProperty] = userData.firstName;
				await App.CurrentApp.SavePropertiesAsync();

				App.CurrentApp.Properties [Constants.surnameProperty] = userData.lastName;
				await App.CurrentApp.SavePropertiesAsync();

				App.CurrentApp.Properties [Constants.birthdayProperty] = userData.birthday;
				await App.CurrentApp.SavePropertiesAsync();

				App.CurrentApp.Properties [Constants.firstAccessProperty] = userData.first_access;
				await App.CurrentApp.SavePropertiesAsync();

				currentUser = userData;
			}
		}

		async public Task SavePassword(string password)
		{
			App.CurrentApp.Properties [Constants.userPasswordProperty] = password;
			await App.CurrentApp.SavePropertiesAsync();
		}

		async public Task SaveUserDataFromFB(User userData)
		{
			if (userData != null)
			{
				App.CurrentApp.Properties [Constants.isLoggedInProperty] = true;
				await App.CurrentApp.SavePropertiesAsync();

				App.CurrentApp.Properties [Constants.userIdProperty] = userData.uau_id;
				await App.CurrentApp.SavePropertiesAsync();

				App.CurrentApp.Properties [Constants.userEmailProperty] = userData.email;
				await App.CurrentApp.SavePropertiesAsync();

				App.CurrentApp.Properties [Constants.firstAccessProperty] = userData.first_access;
				await App.CurrentApp.SavePropertiesAsync();
			//	App.CurrentApp.Properties [Constants.userPasswordProperty] = "40778171d95db27ada13dc4042c5833053dc82a9644302378a60a0a0190a8c61";

				currentUser.uau_id = userData.uau_id;
			}
		}

		public bool CheckIfFirstAccessFlagIsOn(bool isFB = false)
		{
			var properties = App.CurrentApp.Properties;
			bool result = false;

			if (properties.ContainsKey (Constants.firstAccessProperty))
			{
				if (isFB && (string)properties [Constants.firstAccessProperty] != "0")
				{
					result = true;
				}
				else if ((string)properties [Constants.firstAccessProperty] != "0"
						&& (string)properties [Constants.firstAccessProperty] != "1")
				{
					result = true;
				}
			}

			return result;
		}
	}
}

