using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Forms;
using UAU.Droid;
using Xamarin.Forms.Platform.Android;
using Android.Content.Res;
using CustomRenderer;

[assembly: ExportRenderer(typeof(Xamarin.Forms.ProgressBar), typeof(CustomProgressBarRendererAndroid))]
namespace CustomRenderer
{
    class CustomProgressBarRendererAndroid : ProgressBarRenderer
    {

        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.ProgressBar> e)
        {
            base.OnElementChanged(e);

            if (Android.OS.Build.VERSION.SdkInt >= Android.OS.BuildVersionCodes.Lollipop)
            {
                //set bar color
                Control.ProgressTintList = ColorStateList.ValueOf(Android.Graphics.Color.Rgb(139, 157, 195));
            }
            //set bar height
            Control.ScaleY = 6;
        }
    }
}