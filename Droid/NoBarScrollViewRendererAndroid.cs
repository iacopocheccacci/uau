using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Forms.Platform.Android;
using CustomRenderer;
using Xamarin.Forms;
using System.ComponentModel;

[assembly: ExportRenderer(typeof(NoBarScrollView), typeof(NoBarScrollViewRendererAndroid))]
namespace CustomRenderer
{
    class NoBarScrollViewRendererAndroid : ScrollViewRenderer
    {
        protected override void OnElementChanged(VisualElementChangedEventArgs e)
        {
            base.OnElementChanged(e);

            e.NewElement.PropertyChanged += delegate
            {
                OnElementPropertyChanged();
            };
            
        }

        protected void OnElementPropertyChanged()
        {
            if (ChildCount > 0)
            {
                GetChildAt(0).HorizontalScrollBarEnabled = false;
                GetChildAt(0).VerticalScrollBarEnabled = false;
            }
        }
    }
}