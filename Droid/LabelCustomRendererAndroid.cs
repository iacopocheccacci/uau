using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using CustomRenderer;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Android.Graphics;

[assembly: ExportRenderer(typeof(Label), typeof(LabelCustomRendererAndroid))]
namespace CustomRenderer
{
    class LabelCustomRendererAndroid : LabelRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Label> e)
        {
            base.OnElementChanged(e);

            if(e != null)
            {
                if(e.NewElement.FontFamily == "Lato-Bold")
                {
                    Typeface Font = Typeface.CreateFromAsset(Forms.Context.Assets, "Lato-Bold.ttf");
                    Control.Typeface = Font;
                }

                if (e.NewElement.FontFamily == "Lato-Regular")
                {
                    Typeface Font = Typeface.CreateFromAsset(Forms.Context.Assets, "Lato-Regular.ttf");
                    Control.Typeface = Font;
                }
            }
        }
    }
}