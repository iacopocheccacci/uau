using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Forms;
using UAU.Droid;
using Xamarin.Forms.Platform.Android;
using Android.Graphics;
using Android.Support.V4.Util;
using Android.Content.Res;
using System.ComponentModel;
using System.Threading.Tasks;
using Android.Text;
using Android.Text.Style;
using Android.Graphics.Drawables;
using CustomRenderer;
using UAU;

[assembly: ExportRenderer(typeof(ContentPage), typeof(ContentPageRendererAndroid))]
namespace CustomRenderer
{

    class ContentPageRendererAndroid : PageRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Page> e)
        {
            this.SetBackgroundColor(Android.Graphics.Color.Rgb(250,249,251));

            var actionBar = ((Activity)this.Context).ActionBar;
            actionBar.SetCustomView(UAU.Droid.Resource.Layout.Actionbar);
            actionBar.SetDisplayShowCustomEnabled(true);
            actionBar.SetIcon(new ColorDrawable(Xamarin.Forms.Color.Transparent.ToAndroid()));

            var title = e.NewElement as ContentPage;
            App.Current.BindingContext = title;

            //                SetText(e);

        }

        private  void SetText(ElementChangedEventArgs<Page> e)
        {
          var actionBar = ((Activity)this.Context).ActionBar;
           var title = e.NewElement as ContentPage;
            var text = actionBar.CustomView.FindViewById<TextView>(UAU.Droid.Resource.Id.text1);
            if (title != null)
            {
                text.Text = title.Title;
            }
            text.SetTextColor(ColorStateList.ValueOf(Android.Graphics.Color.White));
            Typeface font = Typeface.CreateFromAsset(Forms.Context.Assets, "KaufmannStd.otf");  // font name specified here
            text.Typeface = font;
        }

        protected override void OnDetachedFromWindow()
        {
            
        }
    }
}