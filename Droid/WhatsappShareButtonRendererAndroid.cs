﻿using System;
using Xamarin.Forms;
using UAU;
using UAU.Droid;
using Xamarin.Forms.Platform.Android;
using Android.Content;
using Android.App;
using Android.Widget;
using CustomRenderer;

[assembly: ExportRenderer(typeof(WhatsappShareButton), typeof(WhatsappShareButtonRendererAndroid))]
namespace CustomRenderer
{
	public class WhatsappShareButtonRendererAndroid : ButtonRenderer
	{
		protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.Button> e)
		{
			base.OnElementChanged (e);

			if (Control != null)
			{
				var button = Control;

				button.Click += delegate {
					HandleWhatsappShareClicked ();
				};
			}
		}

		private void HandleWhatsappShareClicked()
		{
			Intent sendIntent = new Intent (Intent.ActionSend);
			var userId = App.CurrentApp.Properties.ContainsKey (Constants.userIdProperty) ? App.CurrentApp.Properties [Constants.userIdProperty] : string.Empty;

			string referralUrl = "Iscriviti a SpyGift! " + Constants.serverUrl + "/index.php/user/getReferralLink/" +
								 userId + "/analyticCode";

			try
			{
				sendIntent.PutExtra (Intent.ExtraText, referralUrl);
				sendIntent.SetType ("text/plain");
				sendIntent.SetPackage ("com.whatsapp");
				Context.StartActivity (sendIntent);
			}
			catch (Exception e)
			{
				AlertDialog.Builder alert = new AlertDialog.Builder (Context);
				alert.SetTitle (Constants.warningMessage);
				alert.SetMessage ("Whatsapp not detected on this device");
				alert.SetPositiveButton (Constants.okMessage, (sendAlert, args) => {
					Toast.MakeText (Context, "Confirm", ToastLength.Short).Show ();
				});

				Dialog dialog = alert.Create ();
				dialog.Show ();

				Console.WriteLine (e.Message);
			}
		}
	}
}

