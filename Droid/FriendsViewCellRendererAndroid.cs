﻿using System;
using System.IO;
using Xamarin.Forms;
using CustomRenderer;
using Android.Views;
using Android.Content;
using Android.App;
using UAU.Droid;
using Xamarin.Forms.Platform.Android;
using Android.Widget;
using Android.Graphics;
using Android.Graphics.Drawables;
using System.Threading.Tasks;
using System.Text;
using Java.Net;
using Java.IO;
using System.Net;
using static Android.Graphics.Bitmap;
using Java.Nio.Channels;
using Java.Nio;
using static Java.Nio.Channels.FileChannel;
using UAU;

[assembly: ExportRenderer(typeof(FriendsViewCell), typeof(FriendsViewCellRendererAndroid))]
namespace CustomRenderer
{
	public class FriendsViewCellRendererAndroid : ViewCellRenderer
	{
		protected override Android.Views.View GetCellCore (Xamarin.Forms.Cell item, Android.Views.View convertView, ViewGroup parent, Context context)
		{
			var x = (FriendsViewCell)item;

			var view = convertView;

			if (view == null) {
				// no view to re-use, create new
				view = (context as Activity).LayoutInflater.Inflate (UAU.Droid.Resource.Layout.FriendsViewCellAndroid, null);
			}

            Typeface latoRegular = Typeface.CreateFromAsset(Forms.Context.Assets, "Lato-Regular.ttf");
            Typeface latoBold = Typeface.CreateFromAsset(Forms.Context.Assets, "Lato-Bold.ttf");

            view.FindViewById<TextView> (UAU.Droid.Resource.Id.Text1).Text = x.Name;
            view.FindViewById<TextView>(UAU.Droid.Resource.Id.Text1).Typeface = latoBold;

            view.FindViewById<TextView> (UAU.Droid.Resource.Id.Text2).Text = x.Details;
            view.FindViewById<TextView>(UAU.Droid.Resource.Id.Text2).Typeface = latoRegular;

            view.FindViewById<Android.Widget.Switch>(UAU.Droid.Resource.Id.switch1).Checked = x.Switch;
            view.FindViewById<Android.Widget.Switch>(UAU.Droid.Resource.Id.switch1).CheckedChange += delegate
            {
                x.Switch = !x.Switch;
            };




            // If a new image is required, display it
            if (!String.IsNullOrWhiteSpace(x.ImageFileName))
            {
                string uri = String.Empty;

                if (!x.ImageFileName.Contains("https://"))
                {
					uri = Constants.serverUrl;
                }

                uri += x.ImageFileName;

				view.FindViewById<ImageView>(UAU.Droid.Resource.Id.imageCircle).SetImageBitmap(GetImageFormUrl(uri));
                view.FindViewById<ImageView>(UAU.Droid.Resource.Id.imageCircle).SetAdjustViewBounds(true);
                view.FindViewById<ImageView>(UAU.Droid.Resource.Id.imageCircle).SetScaleType(ImageView.ScaleType.FitCenter);
                view.FindViewById<ImageView>(UAU.Droid.Resource.Id.corner).SetScaleType(ImageView.ScaleType.FitCenter);
            }
            else
            {
                view.FindViewById<ImageView>(UAU.Droid.Resource.Id.imageCircle).SetImageResource(UAU.Droid.Resource.Drawable.profileIcon);
            }



			return view;
		}

        public Bitmap GetImageFormUrl(string url)
        {

            Bitmap imageBitmap = null;

            using (var webClient = new WebClient())
            {
                var imageBytes = webClient.DownloadData(url);
                if (imageBytes != null && imageBytes.Length > 0)
                {
                    imageBitmap = BitmapFactory.DecodeByteArray(imageBytes, 0, imageBytes.Length);
                    if (imageBitmap.Width != imageBitmap.Height)
                    {
                        if (imageBitmap.Width < imageBitmap.Height)
                        {
                            Bitmap squareBitmap = Bitmap.CreateBitmap(imageBitmap, 0, 0, imageBitmap.Width, imageBitmap.Width);
                            return squareBitmap;
                        }
                        else if (imageBitmap.Width > imageBitmap.Height)
                        {
                            Bitmap squareBitmap = Bitmap.CreateBitmap(imageBitmap, 0, 0, imageBitmap.Height, imageBitmap.Height);
                            return squareBitmap;
                        }
                    }
                }
            }
            return imageBitmap;
        }
    }
}

