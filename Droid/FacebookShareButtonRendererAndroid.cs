﻿using System;
using Xamarin.Forms;
using UAU;
using UAU.Droid;
using Xamarin.Forms.Platform.Android;
using Xamarin.Facebook.Share.Widget;
using Xamarin.Facebook.Share.Model;
using Android.App;
using Xamarin.Facebook;
using CustomRenderer;

[assembly: ExportRenderer(typeof(FacebookShareButton), typeof(FacebookShareButtonRendererAndroid))]
namespace CustomRenderer
{
	public class FacebookShareButtonRendererAndroid : ButtonRenderer
	{
		private static Activity _activity;

		protected override void OnElementChanged(ElementChangedEventArgs<Button> e)
		{
			base.OnElementChanged(e);
			_activity = this.Context as Activity;

			if (Control != null)
			{
				var button = Control;

				button.Click += delegate {
					HandleShareClicked ();
				};
			}
		}

		private void HandleShareClicked()
		{
			var userId = App.CurrentApp.Properties.ContainsKey (Constants.userIdProperty) ? App.CurrentApp.Properties [Constants.userIdProperty] : string.Empty;
			string referralUrl = Constants.serverUrl + "/index.php/user/getReferralLink/" +
								 userId +  "/analyticCode";


			ShareLinkContent content = new ShareLinkContent.Builder ()
										   .SetContentTitle ("Spy Gift")
										   .SetContentDescription ("Join Spy Gift!!!")
										   .Build ();

			//	TODO: content.
			ShareDialog shareDialog = new ShareDialog (_activity);
			shareDialog.Show (content);
		}
	}
}


