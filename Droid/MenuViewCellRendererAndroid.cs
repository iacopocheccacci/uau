using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using CustomRenderer;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Android.Graphics;
using Android.Content.Res;

[assembly: ExportRenderer(typeof(MenuViewCell), typeof(MenuViewCellRendererAndroid))]
namespace CustomRenderer
{
    class MenuViewCellRendererAndroid : ViewCellRenderer
    {
        public static List<Android.Views.View> menuList;
        protected override Android.Views.View GetCellCore(Xamarin.Forms.Cell item, Android.Views.View convertView, ViewGroup parent, Context context)
        {

            if(menuList == null)
            {
                menuList = new List<Android.Views.View>();
            }

            var x = (MenuViewCell)item;

            var view = convertView;
            if (view == null)
            {
                // no view to re-use, create new
                view = (context as Activity).LayoutInflater.Inflate(UAU.Droid.Resource.Layout.MenuViewCellAndroid, null);
            }

            Typeface latoLight = Typeface.CreateFromAsset(Forms.Context.Assets, "Lato-Light.ttf");
            view.FindViewById<TextView>(UAU.Droid.Resource.Id.menuText).Text = x.Name;
            view.FindViewById<TextView>(UAU.Droid.Resource.Id.menuText).Typeface = latoLight;

            menuList.Add(view);

            if(x.Index == 0)
            {
                view.FindViewById<TextView>(UAU.Droid.Resource.Id.menuText).SetTextColor(ColorStateList.ValueOf(Android.Graphics.Color.White));
                view.FindViewById<ImageView>(UAU.Droid.Resource.Id.menuIcon).ImageAlpha = 255;
                view.FindViewById<ImageView>(UAU.Droid.Resource.Id.menuIcon).Alpha = 1f;
                view.FindViewById<TextView>(UAU.Droid.Resource.Id.selectLabel).SetBackgroundColor(Android.Graphics.Color.Rgb(59, 89, 152));
            }

            switch (x.Image)
            {
                case ("homeIcon.png"):
                    view.FindViewById<ImageView>(UAU.Droid.Resource.Id.menuIcon).SetImageResource(UAU.Droid.Resource.Drawable.homeIcon);
                    break;
                case ("profileIcon.png"):
                    view.FindViewById<ImageView>(UAU.Droid.Resource.Id.menuIcon).SetImageResource(UAU.Droid.Resource.Drawable.profileIcon);
                    break;
                case ("pickGiftIcon.png"):
                    view.FindViewById<ImageView>(UAU.Droid.Resource.Id.menuIcon).SetImageResource(UAU.Droid.Resource.Drawable.pickGiftIcon);
                    break;
                case ("myListIcon.png"):
                    view.FindViewById<ImageView>(UAU.Droid.Resource.Id.menuIcon).SetImageResource(UAU.Droid.Resource.Drawable.myListIcon);
                    break;
            }

            this.Cell.Tapped += delegate
            {
                UnselectOther(x.Index);
                SelectItem(x.Index);
            };

            return view;
        }

        private void UnselectOther(int index)
        {
            for (int i = 0; i < menuList.Count; i++)
            {
                if(i != index)
                {
                    menuList[i].FindViewById<TextView>(UAU.Droid.Resource.Id.menuText).SetTextColor(ColorStateList.ValueOf(Android.Graphics.Color.Argb(125,255,255,255)));
                    menuList[index].FindViewById<ImageView>(UAU.Droid.Resource.Id.menuIcon).SetColorFilter((Android.Graphics.Color.Argb(125, 255, 255, 255)));
                    menuList[i].FindViewById<TextView>(UAU.Droid.Resource.Id.selectLabel).SetBackgroundColor(Android.Graphics.Color.Argb(125,255,255,255));

                }
            }
        }

        private void SelectItem(int index)
        {
            menuList[index].FindViewById<TextView>(UAU.Droid.Resource.Id.menuText).SetTextColor(ColorStateList.ValueOf(Android.Graphics.Color.White));
            menuList[index].FindViewById<ImageView>(UAU.Droid.Resource.Id.menuIcon).SetColorFilter(Android.Graphics.Color.White);
            menuList[index].FindViewById<TextView>(UAU.Droid.Resource.Id.selectLabel).SetBackgroundColor(Android.Graphics.Color.Rgb(59, 89, 152));
        }
    }
}