using System;
using Android.App;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using UAU;
using UAU.Droid;
using Xamarin.Facebook.Login.Widget;
using Xamarin.Facebook.Login;
using System.Collections.Generic;
using Xamarin.Facebook;
using Android.OS;
using CustomRenderer;
using Android.Views;
using System.Threading.Tasks;

[assembly: ExportRenderer(typeof(FacebookLoginButton), typeof(FacebookLoginButtonRendererAndroid))]
namespace CustomRenderer
{
	public class FacebookLoginButtonRendererAndroid : ViewRenderer<Button, LoginButton>, GraphRequest.IGraphJSONObjectCallback
    {
        private static Activity _activity;
        private static bool facebookLogin = false;

        protected override void OnElementChanged(ElementChangedEventArgs<Button> e)
        {
            base.OnElementChanged(e);
            
            _activity = this.Context as Activity;

			var loginButton = new LoginButton (_activity);
			List<string> readPermissions = new List<string> { "public_profile", "user_friends", "email", "user_birthday" };
			loginButton.SetReadPermissions (readPermissions);

			var facebookCallback = new FacebookCallback<LoginResult> {
				HandleSuccess = shareResult => {
					Action<FBLoginResponse> local = App.PostSuccessFacebookAction;
					if (local != null)
					{
						GraphRequest request = GraphRequest.NewMeRequest (AccessToken.CurrentAccessToken, this);

						Bundle parameters = new Bundle();
						parameters.PutString("fields", "id,first_name,last_name,birthday,email,gender,friends{id,name}");
						request.Parameters = parameters;
						request.ExecuteAsync();
					}
				},
				HandleCancel = () => {
					Console.WriteLine ("HelloFacebook: Canceled");
				},
				HandleError = shareError => {
					Console.WriteLine ("HelloFacebook: Error: {0}", shareError);
				}
			};
									
			LoginManager.Instance.RegisterCallback (MainActivity.CallbackManager, facebookCallback);
			base.SetNativeControl (loginButton);
            
            if(facebookLogin)
            {
                LoginManager.Instance.LogOut();
            }
            facebookLogin = true;
        }

		async public void OnCompleted (Org.Json.JSONObject json, GraphResponse graphResponse)
		{
			Console.WriteLine (json);

			FBUserData FBUser = new FBUserData ();
			JsonUtilities<FBUserData>.DeserializeToObject (json.ToString(), out FBUser);

			await FillUserData (FBUser);
		}

		async private Task FillUserData(FBUserData data)
		{
			string uid = data.id;
			string name = data.first_name;
			string surname = data.last_name;
			string birthday = data.birthday;
			string email = data.email;
			string gender = "";
			string token = AccessToken.CurrentAccessToken.Token;

			// FB date format is MM/DD/YYYY, so we need to convert to DD/MM/YYYY
			DateTime dt = Convert.ToDateTime (birthday);
			birthday = string.Format("{0}/{1}/{2}", dt.Month, dt.Day, dt.Year);

			gender = data.gender;

			if (gender == "maschio")
			{
				gender = "m";
			}
			else if (gender == "femmina")
			{
				gender = "f";
			}
			else
			{
				gender = "o";
			}


			User user = new User (uid, name, surname, birthday, email, gender, token, data.friends);

			////TODO: ripensare
			//user.password = "40778171d95db27ada13dc4042c5833053dc82a9644302378a60a0a0190a8c61";
			////

			StaticHttpClient.GetInstance.currentUser = user;
			string response = await StaticHttpClient.GetInstance.RequestFromApp (RequestType.fbRegistration, user);
			FBLoginResponse responseObject = new FBLoginResponse ();
			JsonUtilities<FBLoginResponse>.DeserializeToObject (response, out responseObject);

			App.PostSuccessFacebookAction (responseObject);
		}
    }
}