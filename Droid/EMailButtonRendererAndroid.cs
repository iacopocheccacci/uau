﻿using System;
using Xamarin.Forms;
using CustomRenderer;
using UAU.Droid;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(EMailButton), typeof(EMailButtonRendererAndroid))]
namespace UAU.Droid
{
	public class EMailButtonRendererAndroid : ButtonRenderer
	{
		protected override void OnElementChanged(ElementChangedEventArgs<Button> e)
		{
			base.OnElementChanged (e);

			if (Control != null)
			{
				var button = Control;

				button.Click += delegate {
					HandleEMailClicked ();
				};
			}
		}

		private void HandleEMailClicked()
		{
			var userId = App.CurrentApp.Properties.ContainsKey (Constants.userIdProperty) ? App.CurrentApp.Properties [Constants.userIdProperty] : string.Empty;
			string referralUrl = "mailto:?&body=Iscriviti a SpyGift! " + Constants.serverUrl + "/index.php/user/getReferralLink/" +
								 userId + "/analyticCode";
			Device.OpenUri(new Uri(referralUrl));
		}
	}
}

