using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using CustomRenderer;
using UAU.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Android.Graphics.Drawables;
using Android.Graphics;
using Android.Content.Res;
using UAU;

[assembly: ExportRenderer(typeof(CustomPage), typeof(CustomPageRenderer))]
namespace CustomRenderer
{
    public class CustomPageRenderer : PageRenderer
    {

        protected override void OnElementChanged(ElementChangedEventArgs<Page> e)
        {
            base.OnElementChanged(e);

            this.SetBackgroundColor(Android.Graphics.Color.Rgb(250, 249, 251));

            var actionBar = ((Activity)this.Context).ActionBar;
            SetLayout(actionBar);
            actionBar.SetDisplayShowCustomEnabled(true);
            actionBar.SetIcon(new ColorDrawable(Xamarin.Forms.Color.Transparent.ToAndroid()));

            if (e.NewElement != null)
            {
                if (e.NewElement.Title != null)
                {
                   SetText(e.NewElement.Title);
                }
            }
            
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            if (e.PropertyName == CustomPage.TitleProperty.PropertyName)
            {
                var actionBar = ((Activity)this.Context).ActionBar;
                SetLayout(actionBar);
                SetText(this.Element.Title);
            }
        }

        private void SetText(string t)
        {
            var actionBar = ((Activity)this.Context).ActionBar;
			if (actionBar.CustomView != null)
			{
	            var text = actionBar.CustomView.FindViewById<TextView>(UAU.Droid.Resource.Id.text1);
	            if (text != null)
	            {
	                text.Text = t;
	                text.TextSize = 20;
	                text.SetTextColor(ColorStateList.ValueOf(Android.Graphics.Color.White));
	                Typeface font = Typeface.CreateFromAsset(Forms.Context.Assets, "DaxMedium.ttf");  // font name specified here
	                text.Typeface = font;
	            }
			}
        }

        private void SetLayout(ActionBar actionBar)
        {
            CustomPage page = (CustomPage)this.Element;
            switch(page.PageType)
            {
                case "NoButtons":
                    actionBar.SetCustomView(UAU.Droid.Resource.Layout.Actionbar);
                    break;
                case "OneButton":
                    SetOneButton(actionBar, page);
                    break;
                case "DefaultButton":
                    SetDefaultButton(actionBar);
                    break;
                case "TwoButtons":
                    SetTwoButtons(actionBar, page);
                    break;
                case "DefaultAndCustomButton":
                    SetDefaultAndCustomButton(actionBar, page);
                    break;

				case "IconAndDefaultButton":
					SetIconAndDefaultButton(actionBar, page);
					break;
            }
    //        if (this.Element.Title == "Login")
    //        { 
    //            actionBar.SetCustomView(UAU.Droid.Resource.Layout.Actionbar);
    //        }
    //        else if(this.Element.Title == "Sign Up")
    //        {
    //            actionBar.SetCustomView(UAU.Droid.Resource.Layout.ActionbarBack);
    //            var cancel = actionBar.CustomView.FindViewById<TextView>(UAU.Droid.Resource.Id.cancel);
    //            Typeface font = Typeface.CreateFromAsset(Forms.Context.Assets, "Lato-Regular.ttf"); 
    //            cancel.Typeface = font;
    //            cancel.Click += delegate {
    //                var page = (CustomPage)Element;
    //                page.OnCancelClick();
    //            };
    //        }
    //        else if (this.Element.Title == "Home" || this.Element.Title == "Profile" || this.Element.Title == "" || this.Element.Title == "Pick A Gift")
    //        {
    //            actionBar.SetCustomView(UAU.Droid.Resource.Layout.ActionbarMenu);
    //            if(this.Element.Title == "Home" && MainActivity.galleryImagePath != null && MainActivity.galleryImagePath != String.Empty)
    //            {
    //                var feedPage = (FeedPage)Element;
    //                if (feedPage != null)
    //                {
    //                    await feedPage.OpenPage(MainActivity.galleryImagePath);
    //                    MainActivity.galleryImagePath = null;
    //                }
    //            }
    //        }
    //        else if (this.Element.Title == "List Settings" || this.Element.Title == "Create List" || this.Element.Title == "Add wish" || this.Element.Title == "Browse for the link" || this.Element.Title == "Add list admin" || this.Element.Title == "Create a kitty" || this.Element.Title == "Kitty admin" || this.Element.Title == "Custom Wish" || this.Element.Title == "Edit wish" || this.Element.Title == "Wish Detail")
    //        {
    //            actionBar.SetCustomView(UAU.Droid.Resource.Layout.ActionbarTwoButton);

    //            if (this.Element.Title != "Wish Detail")
    //            {
    //                var button = actionBar.CustomView.FindViewById<TextView>(UAU.Droid.Resource.Id.button);
    //                Typeface font = Typeface.CreateFromAsset(Forms.Context.Assets, "Lato-Regular.ttf");
    //                button.Typeface = font;
    //                button.Click += delegate
    //                {
    //                    var page = (CustomPage)Element;
    //                    page.OnButtonClick();
    //                };


    //                switch (this.Element.Title)
    //                {
    //                    case "List Settings":
    //                        button.Text = "Done";
    //                        break;
    //                    case "Create List":
    //                        button.Text = "Next";
    //                        break;
    //                    case "Add wish":
    //                        button.Text = "Done";
    //                        break;
    //                    case "Browse for the link":
    //                        button.Text = "Done";
    //                        break;
    //                    case "Add list admin":
    //                        button.Text = "Done";
    //                        break;
    //                    case "Create a kitty":
    //                        button.Text = "Done";
    //                        break;
    //                    case "Kitty admin":
    //                        button.Text = "Done";
    //                        break;
    //                    case "Custom Wish":
    //                        button.Text = "Done";
    //                        break;
    //                    case "Edit wish":
    //                        button.Text = "Done";
    //                        break;
    //                }

    //                var cancel = actionBar.CustomView.FindViewById<TextView>(UAU.Droid.Resource.Id.cancel);
    //                cancel.Typeface = font;
    //                cancel.Click += delegate
    //                {
    //                    var page = (CustomPage)Element;
    //                    page.OnCancelClick();
    //                };
    //            }
    //            else
    //            {
    //                actionBar.CustomView.FindViewById<TextView>(UAU.Droid.Resource.Id.cancel).Visibility = ViewStates.Invisible;
    //                actionBar.CustomView.FindViewById<TextView>(UAU.Droid.Resource.Id.button).Visibility = ViewStates.Invisible;
    //            }
    //        }
    //        else if (this.Element.Title == "My Lists" || this.Element.Title == null || this.Element.Title == "Add friends"  || this.Element.Title == "Add Friends" || this.Element.Title == "Search Friends" || this.Element.Title == "Connect with friends" || this.Element.Title == "Invite friends" || this.Element.Title == "Connect with friends" || this.Element.Title == "Friends")
    //        {
    //            actionBar.SetCustomView(UAU.Droid.Resource.Layout.ActionbarButton);
    //            var button = actionBar.CustomView.FindViewById<TextView>(UAU.Droid.Resource.Id.button);
    //            Typeface font = Typeface.CreateFromAsset(Forms.Context.Assets, "Lato-Regular.ttf");
    //            button.Typeface = font;
    //            button.Click += delegate {
    //                var page = (CustomPage)Element;
    //                if (page != null)
    //                {
    //                    page.OnButtonClick();
    //                }
    //            };


				//// TODO: Urgente rifare!!! Così non va bene assolutmante
    //            switch (this.Element.Title)
    //            {
    //                case "My Lists":
    //                    button.Text = "Add List";
    //                    break;
    //                case null:
    //                    button.Text = "Settings";
    //                    break;
    //                case "Add friends":
    //                    button.Text = "Next";
    //                    break;
    //                case "Add Friends":
    //                    button.Text = "Done";
    //                    break;
    //                case "Search Friends":
    //                    button.Text = "Add";
    //                    break;
    //                case "Connect with friends":
    //                    button.Text = "Next";
    //                    break;
    //                case "Invite friends":
    //                    button.Text = String.Empty;
    //                    break;
				//	case "Connect with friends ":
    //                    button.Text = String.Empty;
    //                    break;
    //                case "Friends":
    //                    button.Text = "Add Friends";
    //                    break;
    //            }
    //        }
        }

        private void SetOneButton(ActionBar actionBar, CustomPage page)
        {
            actionBar.SetCustomView(UAU.Droid.Resource.Layout.ActionbarBack);
            var cancel = actionBar.CustomView.FindViewById<TextView>(UAU.Droid.Resource.Id.cancel);
            Typeface font = Typeface.CreateFromAsset(Forms.Context.Assets, "Lato-Regular.ttf");
            
			cancel.Text = page.BackButton;
			cancel.Typeface = font;
            cancel.Click += delegate {
                page.OnCancelClick();
            };
        }

        async private void SetDefaultButton(ActionBar actionBar)
        {
            actionBar.SetCustomView(UAU.Droid.Resource.Layout.ActionbarMenu);

			FeedPage feedPage = this.Element as FeedPage;

			if ((feedPage != null) && MainActivity.galleryImagePath != null && MainActivity.galleryImagePath != String.Empty)
            {
                await feedPage.OpenPage(MainActivity.galleryImagePath);
                MainActivity.galleryImagePath = null;
            }
        }

        private void SetTwoButtons(ActionBar actionBar,CustomPage page)
        {
            actionBar.SetCustomView(UAU.Droid.Resource.Layout.ActionbarTwoButton);

            if (this.Element.Title != "Wish Detail")
            {
                var button = actionBar.CustomView.FindViewById<TextView>(UAU.Droid.Resource.Id.button);
                Typeface font = Typeface.CreateFromAsset(Forms.Context.Assets, "Lato-Regular.ttf");
                button.Typeface = font;
                button.Click += delegate
                {
                    page.OnButtonClick();
                };
                
				button.Text = page.Button;
                var cancel = actionBar.CustomView.FindViewById<TextView>(UAU.Droid.Resource.Id.cancel);
                cancel.Typeface = font;
                cancel.Click += delegate
                {
                    page.OnCancelClick();
                };

				cancel.Text = page.BackButton;
            }
        }

        private void SetDefaultAndCustomButton(ActionBar actionBar, CustomPage page)
        {
            actionBar.SetCustomView(UAU.Droid.Resource.Layout.ActionbarButton);
            var button = actionBar.CustomView.FindViewById<TextView>(UAU.Droid.Resource.Id.button);
            Typeface font = Typeface.CreateFromAsset(Forms.Context.Assets, "Lato-Regular.ttf");
            button.Typeface = font;
            button.Click += delegate {
                if (page != null)
                {
                    page.OnButtonClick();
                }
            };
            button.Text = page.Button;
        }

		private void SetIconAndDefaultButton(ActionBar actionBar, CustomPage page)
		{
			actionBar.SetCustomView(UAU.Droid.Resource.Layout.ActionbarIcon);
		}
     }
 }