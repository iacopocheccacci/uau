﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using CustomRenderer;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Android.Graphics;
using System.Net;
using Android.Content.Res;

[assembly: ExportRenderer(typeof(MyItemViewCell), typeof(MyItemViewCellRendererAndroid))]
namespace CustomRenderer
{
	class MyItemViewCellRendererAndroid : ViewCellRenderer
	{
		protected override Android.Views.View GetCellCore(Xamarin.Forms.Cell item, Android.Views.View convertView, ViewGroup parent, Context context)
		{
			var x = (MyItemViewCell)item;

			var view = convertView;

			if (view == null)
			{
				//                // no view to re-use, create new
				view = (context as Activity).LayoutInflater.Inflate(UAU.Droid.Resource.Layout.MyItemViewCellAndroid, null);
			}

			Typeface latoItalic = Typeface.CreateFromAsset(Forms.Context.Assets, "Lato-Italic.ttf");
			Typeface latoBold = Typeface.CreateFromAsset(Forms.Context.Assets, "Lato-Bold.ttf");

			view.FindViewById<TextView>(UAU.Droid.Resource.Id.itemName).Text = x.Name;
			view.FindViewById<TextView>(UAU.Droid.Resource.Id.itemName).Typeface = latoBold;

			view.FindViewById<TextView>(UAU.Droid.Resource.Id.itemDetail).Text = x.Details;
			view.FindViewById<TextView>(UAU.Droid.Resource.Id.itemDetail).Typeface = latoItalic;

			if (!String.IsNullOrWhiteSpace(x.Image))
			{
				view.FindViewById<ImageView>(UAU.Droid.Resource.Id.itemImage).SetImageBitmap(GetImageFormUrl(x.Image));
			}

			if(x.IsNew)
			{
				view.FindViewById<TextView>(UAU.Droid.Resource.Id.itemName).Visibility = ViewStates.Visible;
			}

			return view;
		}

		private Bitmap GetImageFormUrl(string url, bool isProfilePhoto = false)
		{

			Bitmap imageBitmap = null;

			using (var webClient = new WebClient())
			{
				var imageBytes = webClient.DownloadData(url);
				if (imageBytes != null && imageBytes.Length > 0)
				{
					imageBitmap = BitmapFactory.DecodeByteArray(imageBytes, 0, imageBytes.Length);
					if(isProfilePhoto)
					{
						if (imageBitmap.Width < imageBitmap.Height)
						{
							Bitmap squareBitmap = Bitmap.CreateBitmap(imageBitmap, 0, 0, imageBitmap.Width, imageBitmap.Width);
							return squareBitmap;
						}
						else if (imageBitmap.Width > imageBitmap.Height)
						{
							Bitmap squareBitmap = Bitmap.CreateBitmap(imageBitmap, 0, 0, imageBitmap.Height, imageBitmap.Height);
							return squareBitmap;
						}
					}
				}
			}

			return imageBitmap;
		}

	}
}