using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using CustomRenderer;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Android.Graphics;

[assembly: ExportRenderer(typeof(Picker), typeof(PickerRendererAndroid))]
namespace CustomRenderer
{
    class PickerRendererAndroid : PickerRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Picker> e)
        {
            base.OnElementChanged(e);

            if (Control != null)
            {
                Typeface latoRegular = Typeface.CreateFromAsset(Forms.Context.Assets, "Lato-Regular.ttf");
                Control.Typeface = latoRegular;
                Control.SetTextColor(global::Android.Graphics.Color.Black);
                Control.SetHighlightColor(global::Android.Graphics.Color.Black);
            }
        }
    }
}