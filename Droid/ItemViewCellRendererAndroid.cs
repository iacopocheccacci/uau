using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using CustomRenderer;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Android.Graphics;
using System.Net;
using Android.Content.Res;
using UAU;

[assembly: ExportRenderer(typeof(ItemViewCell), typeof(ItemViewCellRendererAndroid))]
namespace CustomRenderer
{
    class ItemViewCellRendererAndroid : ViewCellRenderer
    {
        protected override Android.Views.View GetCellCore(Xamarin.Forms.Cell item, Android.Views.View convertView, ViewGroup parent, Context context)
        {
            var x = (ItemViewCell)item;

            var view = convertView;

            if (view == null)
            {
                //                // no view to re-use, create new
                view = (context as Activity).LayoutInflater.Inflate(UAU.Droid.Resource.Layout.ItemViewCellAndroid, null);
            }

            Typeface latoItalic = Typeface.CreateFromAsset(Forms.Context.Assets, "Lato-Italic.ttf");
            Typeface latoBold = Typeface.CreateFromAsset(Forms.Context.Assets, "Lato-Bold.ttf");

            view.FindViewById<TextView>(UAU.Droid.Resource.Id.itemName).Text = x.Name;
            view.FindViewById<TextView>(UAU.Droid.Resource.Id.itemName).Typeface = latoBold;

            view.FindViewById<TextView>(UAU.Droid.Resource.Id.itemDetail).Text = x.Details;
            view.FindViewById<TextView>(UAU.Droid.Resource.Id.itemDetail).Typeface = latoItalic;



            if (!String.IsNullOrWhiteSpace(x.Photo))
            {
                ImageView profilePhoto = view.FindViewById<ImageView>(UAU.Droid.Resource.Id.imageCircle);
                if (profilePhoto != null)
                {
                    profilePhoto.SetImageBitmap(GetImageFormUrl(x.Photo, true));
                }
            }

            if (!String.IsNullOrWhiteSpace(x.Image))
            {
                view.FindViewById<ImageView>(UAU.Droid.Resource.Id.itemImage).SetImageBitmap(GetImageFormUrl(x.Image));
            }

            if(x.IsNew)
            {
                view.FindViewById<TextView>(UAU.Droid.Resource.Id.itemName).Visibility = ViewStates.Visible;
            }

            SetItemSattus(x.Reservation, view, x);

            return view;
        }

        private Bitmap GetImageFormUrl(string url, bool isProfilePhoto = false)
        {

            Bitmap imageBitmap = null;

            using (var webClient = new WebClient())
            {
                var imageBytes = webClient.DownloadData(url);
                if (imageBytes != null && imageBytes.Length > 0)
                {
                    imageBitmap = BitmapFactory.DecodeByteArray(imageBytes, 0, imageBytes.Length);
                    if(isProfilePhoto)
                    {
                        if (imageBitmap.Width < imageBitmap.Height)
                        {
                            Bitmap squareBitmap = Bitmap.CreateBitmap(imageBitmap, 0, 0, imageBitmap.Width, imageBitmap.Width);
                            return squareBitmap;
                        }
                        else if (imageBitmap.Width > imageBitmap.Height)
                        {
                            Bitmap squareBitmap = Bitmap.CreateBitmap(imageBitmap, 0, 0, imageBitmap.Height, imageBitmap.Height);
                            return squareBitmap;
                        }
                    }
                }
            }

            return imageBitmap;
        }

        private void SetItemSattus(ReservationType status, Android.Views.View view, ItemViewCell x)
        {
            view.FindViewById<ImageView>(UAU.Droid.Resource.Id.reservedLine).Visibility = ViewStates.Invisible;
            view.FindViewById<ImageView>(UAU.Droid.Resource.Id.reservedByMe).Visibility = ViewStates.Invisible;
            view.FindViewById<TextView>(UAU.Droid.Resource.Id.statusBar).Visibility = ViewStates.Invisible;
            view.FindViewById<ImageView>(UAU.Droid.Resource.Id.reservedByOther).Visibility = ViewStates.Invisible;
            view.FindViewById<ImageView>(UAU.Droid.Resource.Id.CollectJoined).Visibility = ViewStates.Invisible;
            view.FindViewById<ImageView>(UAU.Droid.Resource.Id.collectOpen).Visibility = ViewStates.Invisible;

            if (status == ReservationType.ReservedByMe)
            {
                view.FindViewById<ImageView>(UAU.Droid.Resource.Id.reservedByMe).Visibility = ViewStates.Visible;
				view.FindViewById<TextView>(UAU.Droid.Resource.Id.statusText).Text = Constants.myReservationExpirationLabel + x.DaysNumber + " " + x.DaysLabel;
                view.FindViewById<TextView>(UAU.Droid.Resource.Id.statusText).SetTextColor(ColorStateList.ValueOf(Android.Graphics.Color.Rgb(59, 89, 152)));
				view.FindViewById<TextView>(UAU.Droid.Resource.Id.statusBar).Text = Constants.reservedByMeItemLabel;
                view.FindViewById<TextView>(UAU.Droid.Resource.Id.statusBar).SetTextColor(ColorStateList.ValueOf(Android.Graphics.Color.White));
				view.FindViewById<TextView>(UAU.Droid.Resource.Id.statusBar).SetBackgroundColor(Android.Graphics.Color.Rgb(59, 89, 152));
                view.FindViewById<TextView>(UAU.Droid.Resource.Id.statusBar).Visibility = ViewStates.Visible;

            }

            else if (status == ReservationType.ReservedByOther)
            {
                view.FindViewById<ImageView>(UAU.Droid.Resource.Id.reservedByOther).Visibility = ViewStates.Visible;
				view.FindViewById<TextView>(UAU.Droid.Resource.Id.statusText).Text = Constants.byOtherReservationExpirationLabel + x.DaysNumber + " " + x.DaysLabel;
                view.FindViewById<TextView>(UAU.Droid.Resource.Id.statusText).SetTextColor(ColorStateList.ValueOf(Android.Graphics.Color.Rgb(74,74,74)));
				view.FindViewById<TextView>(UAU.Droid.Resource.Id.statusBar).Text = Constants.reservedByOtherLabel;
                view.FindViewById<TextView>(UAU.Droid.Resource.Id.statusBar).SetTextColor(ColorStateList.ValueOf(Android.Graphics.Color.White));
                view.FindViewById<TextView>(UAU.Droid.Resource.Id.statusBar).SetBackgroundColor(Android.Graphics.Color.Rgb(49, 46, 51));
                view.FindViewById<TextView>(UAU.Droid.Resource.Id.statusBar).Visibility = ViewStates.Visible;
            }

            else if (status == ReservationType.ReservedByMeCollect)
            {
                view.FindViewById<ImageView>(UAU.Droid.Resource.Id.CollectJoined).Visibility = ViewStates.Visible;
				view.FindViewById<TextView>(UAU.Droid.Resource.Id.statusText).Text =Constants.reservedByMeCollectLabel;
				view.FindViewById<TextView>(UAU.Droid.Resource.Id.statusText).SetTextColor(ColorStateList.ValueOf(Android.Graphics.Color.Rgb(59, 89, 152)));
            }

            else if (status == ReservationType.MyCollect)
            {
                view.FindViewById<ImageView>(UAU.Droid.Resource.Id.CollectJoined).Visibility = ViewStates.Visible;
				view.FindViewById<TextView>(UAU.Droid.Resource.Id.statusText).Text = Constants.myCollectExpirationLabel + x.DaysNumber + " " + x.DaysLabel;
				view.FindViewById<TextView>(UAU.Droid.Resource.Id.statusText).SetTextColor(ColorStateList.ValueOf(Android.Graphics.Color.Rgb(59, 89, 152)));
				view.FindViewById<TextView>(UAU.Droid.Resource.Id.statusBar).Text = Constants.myCollectLabel;
                view.FindViewById<TextView>(UAU.Droid.Resource.Id.statusBar).SetTextColor(ColorStateList.ValueOf(Android.Graphics.Color.White));
				view.FindViewById<TextView>(UAU.Droid.Resource.Id.statusBar).SetBackgroundColor(Android.Graphics.Color.Rgb(59, 89, 152));
                view.FindViewById<TextView>(UAU.Droid.Resource.Id.statusBar).Visibility = ViewStates.Visible;

            }

            else if (status == ReservationType.ActiveCollect)
            {
                view.FindViewById<ImageView>(UAU.Droid.Resource.Id.collectOpen).Visibility = ViewStates.Visible;
				view.FindViewById<TextView>(UAU.Droid.Resource.Id.statusText).Text = Constants.activeCollectLabel;
				view.FindViewById<TextView>(UAU.Droid.Resource.Id.statusText).SetTextColor(ColorStateList.ValueOf(Android.Graphics.Color.Rgb(59, 89, 152)));
            }

            else
            {
                view.FindViewById<ImageView>(UAU.Droid.Resource.Id.reservedLine).Visibility = ViewStates.Visible;
				view.FindViewById<TextView>(UAU.Droid.Resource.Id.statusText).Text = Constants.availableItemLabel;
				view.FindViewById<TextView>(UAU.Droid.Resource.Id.statusText).SetTextColor(ColorStateList.ValueOf(Android.Graphics.Color.Rgb(59, 89, 152)));
            }
            
        }
    }
}