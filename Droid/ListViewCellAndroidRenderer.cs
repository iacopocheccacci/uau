using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Forms;
using CustomRenderer;
using UAU.Droid;
using Xamarin.Forms.Platform.Android;
using Android.Graphics;

[assembly: ExportRenderer(typeof(ListViewCell), typeof(ListViewCellAndroidRenderer))]

namespace CustomRenderer
{
    public class ListViewCellAndroidRenderer : ViewCellRenderer
    {
        protected override Android.Views.View GetCellCore(Xamarin.Forms.Cell item, Android.Views.View convertView, ViewGroup parent, Context context)
        {
            var x = (ListViewCell)item;

            var view = convertView;

            if (view == null)
            {
                // no view to re-use, create new
                view = (context as Activity).LayoutInflater.Inflate(UAU.Droid.Resource.Layout.ListViewCellAndroid, null);
            }

            Typeface latoRegular = Typeface.CreateFromAsset(Forms.Context.Assets, "Lato-Regular.ttf");
            Typeface latoBold = Typeface.CreateFromAsset(Forms.Context.Assets, "Lato-Bold.ttf");

            view.FindViewById<TextView>(UAU.Droid.Resource.Id.listname).Text = x.Name;
            view.FindViewById<TextView>(UAU.Droid.Resource.Id.listname).Typeface = latoBold;

            view.FindViewById<TextView>(UAU.Droid.Resource.Id.numwish).Text = x.Wishes;
            view.FindViewById<TextView>(UAU.Droid.Resource.Id.numwish).Typeface = latoRegular;

            view.FindViewById<TextView>(UAU.Droid.Resource.Id.numdays).Text =x.DaysNumber;
            view.FindViewById<TextView>(UAU.Droid.Resource.Id.days).Text = x.DaysLabel;
			view.FindViewById<TextView>(UAU.Droid.Resource.Id.expireIn).Text = x.ExpireInLabel;

            if(!x.Lock)
            {
                view.FindViewById<ImageView>(UAU.Droid.Resource.Id.imageView1).SetImageResource(UAU.Droid.Resource.Drawable.publicListIcon);
            }

            return view;
        }
    }
}