﻿

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Xamarin.Facebook;
using Xamarin.Forms;
using Android.Provider;
using Java.IO;
using Android.Graphics;
using static Android.Provider.MediaStore;
using System.IO;
using Android.Net;
using Android.Database;

namespace UAU.Droid
{
    [Activity(Label = "UAU.Droid", Icon = "@drawable/icon", Theme = "@style/MyTheme", ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation,
        ScreenOrientation = ScreenOrientation.Portrait)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsApplicationActivity
    {
        public static string galleryImagePath;
        public static ICallbackManager CallbackManager = CallbackManagerFactory.Create();

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            global::Xamarin.Forms.Forms.Init(this, bundle);
            FacebookSdk.SdkInitialize(this.ApplicationContext);
            if (Android.OS.Build.VERSION.SdkInt >= Android.OS.BuildVersionCodes.Lollipop)
            {
                Window.ClearFlags(Android.Views.WindowManagerFlags.TranslucentStatus);
                Window.AddFlags(Android.Views.WindowManagerFlags.DrawsSystemBarBackgrounds);
                Window.SetStatusBarColor(Android.Graphics.Color.Rgb(49, 46, 51));
            }
            Intent intent = Intent;
            if (Intent.ActionSend.Equals(intent.Action))
            {
                Uri imageUri = (Uri)intent.Extras.Get(Intent.ExtraStream);
                if (imageUri != null)
                {
                    string path = getRealPathFromURI(imageUri);
                    galleryImagePath = path;
                }
            }

            LoadApplication(new App());
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);
            CallbackManager.OnActivityResult(requestCode, (int)resultCode, data);
        }
        public string getRealPathFromURI(Uri uri)
        {
            ICursor cursor = ContentResolver.Query(uri, null, null, null, null);
            cursor.MoveToFirst();
            int idx = cursor.GetColumnIndex(MediaStore.Images.ImageColumns.Data);
            return cursor.GetString(idx);
        }
    }
}

