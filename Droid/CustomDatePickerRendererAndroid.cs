﻿using System;
using Xamarin.Forms;
using UAU;
using UAU.Droid;
using Xamarin.Forms.Platform.Android;
using System.ComponentModel;
using Android.Content.Res;
using CustomRenderer;
using Android.Graphics;

[assembly: ExportRenderer(typeof(CustomDatePicker), typeof(CustomDatePickerRendererAndroid))]
namespace CustomRenderer
{
	public class CustomDatePickerRendererAndroid : DatePickerRenderer
	{
		protected override void OnElementChanged(ElementChangedEventArgs<DatePicker> e)
		{
			base.OnElementChanged(e);

			if (this.Control == null) return;
            var customDatePicker = e.NewElement as CustomDatePicker;
            Typeface latoRegular = Typeface.CreateFromAsset(Forms.Context.Assets, "Lato-Regular.ttf");
            Control.Typeface = latoRegular;

            if (customDatePicker != null)
			{
				this.SetValue(customDatePicker);

			}
		}

		protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged(sender, e);

			if (this.Control == null) return;

			var customDatePicker = this.Element as CustomDatePicker;

			if (customDatePicker != null)
			{
				switch (e.PropertyName)
				{
				case CustomDatePicker.NullableDatePropertyName:
					this.SetValue(customDatePicker);
					break;
				case CustomDatePicker.NullTextPropertyName:
					this.SetValue(customDatePicker);
					break;
				}
			}
		}

		private void SetValue(CustomDatePicker customDatePicker)
		{
			if (customDatePicker.NullableDate.HasValue)
			{
				//this.Control.Placeholder = customDatePicker.NullText ?? string.Empty;
				this.Control.Text = customDatePicker.NullableDate.Value.ToString(customDatePicker.Format);
                Control.SetTextColor(global::Android.Graphics.Color.Black);
            }
			else
			{
                //this.Control.Placeholder = customDatePicker.NullText ?? "Birthday";
                this.Control.Text = customDatePicker.NullText ?? "Compleanno";
                this.Control.SetTextColor(ColorStateList.ValueOf(Android.Graphics.Color.Gray));
            }
		}
	}
}

