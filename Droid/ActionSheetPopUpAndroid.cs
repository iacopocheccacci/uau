using System;
using Xamarin.Forms;
using UAU;
using UAU.Droid;
using Xamarin.Forms.Platform.Android;
using Android.App;
using Android.Widget;
using CustomRenderer;
using Android.Content;

[assembly: Xamarin.Forms.Dependency(typeof(ActionSheetPopUpAndroid))]
namespace CustomRenderer
{
    class ActionSheetPopUpAndroid : IActionSheetPopUp
    {
        public void WhatsappClick()
        {
            Intent sendIntent = new Intent(Intent.ActionSend);
            var userId = App.CurrentApp.Properties.ContainsKey(Constants.userIdProperty) ? App.CurrentApp.Properties[Constants.userIdProperty] : string.Empty;

			string referralUrl = "Iscriviti a SpyGift! " + Constants.serverUrl + "/index.php/user/getReferralLink/" +
                                 userId + "/analyticCode";
            try
            {
                sendIntent.PutExtra(Intent.ExtraText, referralUrl);
                sendIntent.SetType("text/plain");
                sendIntent.SetPackage("com.whatsapp");
                Forms.Context.StartActivity(sendIntent);
            }
            catch (Exception e)
            {
                AlertDialog.Builder alert = new AlertDialog.Builder(Forms.Context);
                alert.SetTitle(Constants.warningMessage);
                alert.SetMessage("Whatsapp not detected on this device");
                alert.SetPositiveButton(Constants.okMessage, (sendAlert, args) =>
                {
                    Toast.MakeText(Forms.Context, "Confirm", ToastLength.Short).Show();
                });

                Dialog dialog = alert.Create();
                dialog.Show();

                Console.WriteLine(e.Message);
            }
        }

        public void MailClick()
        {
            var userId = App.CurrentApp.Properties.ContainsKey(Constants.userIdProperty) ? App.CurrentApp.Properties[Constants.userIdProperty] : string.Empty;
			string referralUrl = "mailto:?&body=Iscriviti a SpyGift! " + Constants.serverUrl + "/index.php/user/getReferralLink/" +
                                 userId + "/analyticCode";
            Device.OpenUri(new Uri(referralUrl));
        }
    }
}