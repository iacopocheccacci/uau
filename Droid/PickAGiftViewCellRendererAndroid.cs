using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Android.Graphics.Drawables;
using Android.Graphics;
using System.Net;
using CustomRenderer;
using UAU.Droid;
using Android.Content.Res;
using UAU;

[assembly: ExportRenderer(typeof(PickAGiftViewCell), typeof(PickAGiftViewCellRendererAndroid))]
namespace CustomRenderer
{
    class PickAGiftViewCellRendererAndroid : ViewCellRenderer
    {

        protected override Android.Views.View GetCellCore(Xamarin.Forms.Cell item, Android.Views.View convertView, ViewGroup parent, Context context)
        {
            var x = (PickAGiftViewCell)item;

            var view = convertView;

            if (view == null)
            {
                // no view to re-use, create new
                view = (context as Activity).LayoutInflater.Inflate(UAU.Droid.Resource.Layout.PickAGiftViewCellAndroid, null);
            }

            view.FindViewById<TextView>(UAU.Droid.Resource.Id.Text1).Text = x.Name;
            view.FindViewById<TextView>(UAU.Droid.Resource.Id.Text2).Text = x.Details;


            // If a new image is required, display it
            if (!String.IsNullOrWhiteSpace(x.ImageFileName))
            {
                string uri = String.Empty;

                if (!x.ImageFileName.Contains("https://"))
                {
					uri = Constants.serverUrl;
                }

                uri += x.ImageFileName;


                view.FindViewById<ImageView>(UAU.Droid.Resource.Id.imageCircle).SetImageBitmap(GetImageFormUrl(uri));
            }
            else
            {
                view.FindViewById<ImageView>(UAU.Droid.Resource.Id.imageCircle).SetImageResource(UAU.Droid.Resource.Drawable.avatar);
            }



            return view;
        }

        public Bitmap GetImageFormUrl(string url)
        {
            Bitmap imageBitmap = null;

            using (var webClient = new WebClient())
            {
                var imageBytes = webClient.DownloadData(url);
                if (imageBytes != null && imageBytes.Length > 0)
                {
                    //imageBitmap = BitmapFactory.DecodeByteArray(imageBytes, 0, imageBytes.Length);
					imageBitmap = ImageHandler.decodeSampledBitmapFromResource(imageBytes, 100, 100);

                    if (imageBitmap.Width < imageBitmap.Height)
                    {
                        Bitmap squareBitmap = Bitmap.CreateBitmap(imageBitmap, 0, 0, imageBitmap.Width, imageBitmap.Width);
                        return squareBitmap;
                    }
                    else if (imageBitmap.Width > imageBitmap.Height)
                    {
                        Bitmap squareBitmap = Bitmap.CreateBitmap(imageBitmap, 0, 0, imageBitmap.Height, imageBitmap.Height);
                        return squareBitmap;
                    }
                }

                return imageBitmap;
            }
        }
    }
}