﻿using System;
using Android.Graphics;

namespace UAU.Droid
{
	public class ImageHandler
	{
		public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) 
		{
			// Raw height and width of image
			int height = options.OutHeight;
			int width = options.OutWidth;
			int inSampleSize = 1;

			if (height > reqHeight || width > reqWidth) {

				int halfHeight = height / 2;
				int halfWidth = width / 2;

				// Calculate the largest inSampleSize value that is a power of 2 and keeps both
				// height and width larger than the requested height and width.
				while ((halfHeight / inSampleSize) >= reqHeight && (halfWidth / inSampleSize) >= reqWidth) 
				{
					inSampleSize *= 2;
				}
			}

			return inSampleSize;
		}


		public static Bitmap decodeSampledBitmapFromResource(byte[] res, int reqWidth, int reqHeight) 
		{
			// First decode with inJustDecodeBounds=true to check dimensions
			BitmapFactory.Options options = new BitmapFactory.Options();
			options.InJustDecodeBounds = true;
			BitmapFactory.DecodeByteArray(res, 0, res.Length, options);

			// Calculate inSampleSize
			options.InSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

			// Decode bitmap with inSampleSize set
			options.InJustDecodeBounds = false;
			return BitmapFactory.DecodeByteArray(res, 0, res.Length, options);
		}
	}
}

