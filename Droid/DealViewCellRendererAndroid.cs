using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using CustomRenderer;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Android.Graphics;
using System.Net;

[assembly: ExportRenderer(typeof(DealViewCell), typeof(DealViewCellRendererAndroid))]
namespace CustomRenderer
{
    class DealViewCellRendererAndroid : ViewCellRenderer
    {
        protected override Android.Views.View GetCellCore(Xamarin.Forms.Cell item, Android.Views.View convertView, ViewGroup parent, Context context)
        {
            var x = (DealViewCell)item;

            var view = convertView;

            if (view == null)
            {
                //                // no view to re-use, create new
                view = (context as Activity).LayoutInflater.Inflate(UAU.Droid.Resource.Layout.DealViewCellAndroid, null);
            }

            Typeface latoRegular = Typeface.CreateFromAsset(Forms.Context.Assets, "Lato-Regular.ttf");

            view.FindViewById<TextView>(UAU.Droid.Resource.Id.itemName).Text = x.Name;
            view.FindViewById<TextView>(UAU.Droid.Resource.Id.itemName).Typeface = latoRegular;

            view.FindViewById<TextView>(UAU.Droid.Resource.Id.itemPrice).Text = x.Price;
            AdjustsFontSize(view.FindViewById<TextView>(UAU.Droid.Resource.Id.itemPrice));
            view.FindViewById<TextView>(UAU.Droid.Resource.Id.itemPrice).Typeface = latoRegular;

            if (!String.IsNullOrWhiteSpace(x.Image))
            {
                view.FindViewById<ImageView>(UAU.Droid.Resource.Id.itemImage).SetImageBitmap(GetImageFormUrl(x.Image));
            }

            if (!String.IsNullOrWhiteSpace(x.ShopImage) && (x.ShopImage.Contains("http") || x.ShopImage.Contains("https")))
            {
                view.FindViewById<ImageView>(UAU.Droid.Resource.Id.itemShop).SetImageBitmap(GetImageFormUrl(x.ShopImage));
            }
            else
            {
                view.FindViewById<ImageView>(UAU.Droid.Resource.Id.itemShop).SetImageResource(UAU.Droid.Resource.Drawable.AmazonLogo);
            }

            return view;
        }

        private Bitmap GetImageFormUrl(string url)
        {

            Bitmap imageBitmap = null;

            using (var webClient = new WebClient())
            {
                var imageBytes = webClient.DownloadData(url);
                if (imageBytes != null && imageBytes.Length > 0)
                {
                    imageBitmap = BitmapFactory.DecodeByteArray(imageBytes, 0, imageBytes.Length);
                }
            }

            return imageBitmap;
        }

        private void AdjustsFontSize(TextView txt)
        {
            if(txt.Text.Length == 4)
            {
                txt.TextSize = 12;
            }
            else if (txt.Text.Length == 5)
            {
                txt.TextSize = 12;
            }
            else if (txt.Text.Length == 6)
            {
                txt.TextSize = 11;
            }
            else if (txt.Text.Length == 7)
            {
                txt.TextSize = 11;
            }
            else if (txt.Text.Length == 8)
            {
                txt.TextSize = 10;
            }
            else if (txt.Text.Length == 9)
            {
                txt.TextSize = 10;
            }
            else if (txt.Text.Length >= 10)
            {
                txt.TextSize = 9;
            }
        }
    }
}