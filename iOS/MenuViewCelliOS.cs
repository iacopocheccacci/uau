﻿using System;
using Foundation;
using UIKit;

namespace CustomRenderer.iOS
{
	public class MenuViewCelliOS : UITableViewCell
	{
		UILabel headingLabel;
		UIImageView imageView, separatorImageView;
		UIView separatorView;

		public MenuViewCelliOS (NSString cellId) : base (UITableViewCellStyle.Default, cellId)
		{
			SelectionStyle = UITableViewCellSelectionStyle.None;

			imageView = new UIImageView ();

			headingLabel = new UILabel () {
				Font = UIFont.FromName ("Lato-Light", 16f),
				BackgroundColor = UIColor.Clear
			};

			separatorImageView = new UIImageView() 
			{
				Image = new UIImage("lineaSeparazione.png")
			};

			separatorView = new UIView () {
				BackgroundColor = UIColor.FromRGB(59, 89, 152),
				Hidden = true
			};

			ContentView.Add (headingLabel);
			ContentView.Add (imageView);
			ContentView.Add (separatorImageView);
			ContentView.Add (separatorView);
		}

		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();

			headingLabel.Frame = new CoreGraphics.CGRect (70, 31, 120, 20);
			headingLabel.TextAlignment = UITextAlignment.Left;
			headingLabel.TextColor = UIColor.White;
			imageView.Frame = new CoreGraphics.CGRect (30, 23, 30, 33);
			separatorImageView.Frame = new CoreGraphics.CGRect(2, ContentView.Bounds.Height, ContentView.Bounds.Width - 20, 1);
			separatorView.Frame = new CoreGraphics.CGRect(2, ContentView.Bounds.Height, ContentView.Bounds.Width - 20, 3);
		}

		public void UpdateCell (MenuViewCell cell, UIImage image)
		{
			headingLabel.Text = cell.Name;
			imageView.Image = image;

			if (cell.Index == 0)
			{
				SelectCell();
			}
			else
			{
				UnselectCell();
			}
		}

		public void SelectCell ()
		{
			separatorView.Hidden = false;
			headingLabel.Alpha = 1f;
			imageView.Alpha = 1f;
		}

		public void UnselectCell()
		{
			separatorView.Hidden = true;
			headingLabel.Alpha = 0.5f;
			imageView.Alpha = 0.5f;
		}
	}
}

