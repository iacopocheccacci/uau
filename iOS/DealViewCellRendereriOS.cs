﻿using System;
using CustomRenderer;
using CustomRenderer.iOS;
using Foundation;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer (typeof (DealViewCell), typeof (DealViewCellRendereriOS))]
namespace CustomRenderer.iOS
{
	public class DealViewCellRendereriOS : ViewCellRenderer
	{
		static NSString rid = new NSString ("DealViewCell");

		public override UITableViewCell GetCell (Xamarin.Forms.Cell item, UITableViewCell reusableCell, UITableView tv)
		{
			var x = (DealViewCell)item;
			Console.WriteLine (x);

			DealViewCelliOS c = reusableCell as DealViewCelliOS;

			if (c == null) 
			{
				c = new DealViewCelliOS (rid);
			}

			UIImage i = null;
			if (!String.IsNullOrWhiteSpace (x.Image)) 
			{
				string uri = x.Image;

				using (var url = new NSUrl (uri)) 
				{
					using (var data = NSData.FromUrl (url)) 
					{
						if (data != null) 
						{
							i = UIImage.LoadFromData (data);
						}
						else 
						{
							i = UIImage.FromFile ("avatar.png");
						}
					}
				}
			}

			UIImage logo = null;
			if (!String.IsNullOrWhiteSpace (x.Image)) {
				string uri = x.ShopImage;

				using (var url = new NSUrl (uri)) 
				{
					using (var data = NSData.FromUrl (url)) 
					{
						if (data != null) 
						{
							logo = UIImage.LoadFromData (data);
						} 
						else 
						{
							logo = UIImage.FromFile ("AmazonLogo.png");
						}
					}
				}
			}

			c.UpdateCell (x, i, logo);

			return c;
		}
	}
}

