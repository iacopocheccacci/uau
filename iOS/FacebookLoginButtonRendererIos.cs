using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using XamarinNativeFacebook;
using XamarinNativeFacebook.iOS;
using UAU;
using System.Diagnostics;
using Facebook.CoreKit;
using Facebook.LoginKit;
using System.Collections.Generic;
using Foundation;
using System;
using CustomRenderer;
using System.Globalization;

[assembly: ExportRenderer(typeof(FacebookLoginButton), typeof(FacebookLoginButtonRendererIos))]
namespace XamarinNativeFacebook.iOS
{
    public class FacebookLoginButtonRendererIos : ButtonRenderer
    {
		List<string> readPermissions = new List<string> { "public_profile", "user_friends", "email", "user_birthday" };

        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.Button> e)
        {
            base.OnElementChanged(e);

            if (Control != null)
            {
                UIButton button = Control;

                button.TouchUpInside += delegate
                {
                    HandleFacebookLoginClicked();
                };
            }
        }

        private void HandleFacebookLoginClicked()
        {
			Debug.WriteLine ("Clicked!");

			Login ();
        }


		private void Login()
		{
			LoginManager loginManager = new LoginManager ();

			loginManager.LogInWithReadPermissions (readPermissions.ToArray(), this.ViewController, delegate(LoginManagerLoginResult result, NSError error)
				{
					if (error != null)
					{
						//	App.OnFacebookAuthFailed();
					}
					else if (result.IsCancelled)
					{
						//	App.OnFacebookAuthFailed();
					}
					else
					{
						var accessToken = result.Token.TokenString;
						GetUserData(accessToken);
					}
				});
		}

		private void GetUserData(string accessToken)
		{
			NSDictionary parameters = new NSDictionary ("fields", "id,first_name,last_name,birthday,email,gender,friends{id,name}");
			GraphRequest request = new GraphRequest ("me", parameters);

			request.Start (delegate(GraphRequestConnection connection, NSObject connectionResult, NSError connectionError) 
				{
					Debug.WriteLine(connectionResult.ToString());

					NSDictionary dict = (NSDictionary) connectionResult;

					if (dict != null)
					{
						FillUserData(dict, accessToken);
					}
				});
		}

		async private void FillUserData(NSDictionary dict, string accessToken)
		{
			string uid = "";
			string name = "";
			string surname = "";
			string birthday = "";
			string email = "";
			string gender = "";
			string token = "";
			Friends friends = new Friends ();

			if (dict ["id"] != null)
			{
				uid = dict ["id"].ToString ();
				Debug.WriteLine (uid);
			}

			if (dict ["first_name"] != null)
			{
				name = dict ["first_name"].ToString ();
				Debug.WriteLine (name);
			}

			if (dict ["last_name"] != null)
			{
				surname = dict ["last_name"].ToString ();
				Debug.WriteLine (surname);
			}

			if (dict ["birthday"] != null)
			{
				birthday = dict ["birthday"].ToString ();

				// FB date format is MM/DD/YYYY, so we need to convert to DD/MM/YYYY
				DateTime dt = DateTime.ParseExact(birthday, "MM/dd/yyyy", null);
				birthday = dt.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);

				Debug.WriteLine (birthday);
			}

			if (dict ["email"] != null)
			{
				email = dict ["email"].ToString ();
				Debug.WriteLine (email);
			}

			if (dict ["gender"] != null)
			{
				gender = dict ["gender"].ToString ();

				if (gender == "maschio")
				{
					gender = "m";
				}
				else if (gender == "femmina")
				{
					gender = "f";
				}
				else
				{
					gender = "o";
				}
				
				Debug.WriteLine (gender);
			}

			if (dict ["friends"] != null)
			{
				friends.data = new List<FriendData> ();
				NSString dataString = new NSString ("data");
				NSArray array = (NSArray)dict ["friends"].ValueForKey(dataString);

				for (uint i = 0; i < array.Count; i++)
				{
					FriendData fd = new FriendData ();
					NSDictionary dataDict = (NSDictionary)ObjCRuntime.Runtime.GetNSObject (array.ValueAt (i));

					fd.id = dataDict ["id"].ToString ();
					fd.name = dataDict ["name"].ToString ();

					friends.data.Add (fd);

					Debug.WriteLine(dataDict ["id"].ToString() + " " + dataDict["name"].ToString());
				}
			}

			token = accessToken;
			Debug.WriteLine(token);

			User user = new User (uid, name, surname, birthday, email, gender, token, friends);

			////TODO: ripensare
			//user.password = "40778171d95db27ada13dc4042c5833053dc82a9644302378a60a0a0190a8c61";
			////

			StaticHttpClient.GetInstance.currentUser = user;
			string response = await StaticHttpClient.GetInstance.RequestFromApp (RequestType.fbRegistration, user);

			FBLoginResponse responseObject = new FBLoginResponse ();
			JsonUtilities<FBLoginResponse>.DeserializeToObject (response, out responseObject);

			App.PostSuccessFacebookAction (responseObject);
		}
    }
}