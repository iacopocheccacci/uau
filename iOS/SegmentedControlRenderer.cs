﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using UIKit;
using CustomRenderer;
using CustomRenderer.iOS;

[assembly:ExportRenderer(typeof(SegmentedControl), typeof(SegmentedControlRenderer))]
namespace CustomRenderer.iOS
{
	public class SegmentedControlRenderer : ViewRenderer<SegmentedControl, UISegmentedControl>
	{
		public SegmentedControlRenderer ()
		{
			UISegmentedControl.Appearance.TintColor = UIColor.FromRGB (59, 89, 152);
		}

		protected override void OnElementChanged (ElementChangedEventArgs<SegmentedControl> e)
		{
			base.OnElementChanged (e);

			var segmentedControl = new UISegmentedControl ();

			if (e.NewElement != null)
			{
				for (var i = 0; i < e.NewElement.Children.Count; i++)
				{
					segmentedControl.InsertSegment (e.NewElement.Children [i].Text, i, false);
				}

				segmentedControl.SelectedSegment = 0;

				segmentedControl.ValueChanged += (sender, eventArgs) => {
					e.NewElement.SelectedValue = segmentedControl.TitleAt (segmentedControl.SelectedSegment);
				};

				SetNativeControl (segmentedControl);
			}
		}
	}
}