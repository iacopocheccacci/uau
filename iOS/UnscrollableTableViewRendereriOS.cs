﻿using System;
using CustomRenderer;
using CustomRenderer.iOS;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly:ExportRenderer(typeof(UnscrollableTableView), typeof(UnscrollableTableViewRendereriOS))]
namespace CustomRenderer.iOS
{
	public class UnscrollableTableViewRendereriOS : TableViewRenderer
	{
		protected override void OnElementChanged(ElementChangedEventArgs<TableView> e)
		{
			base.OnElementChanged(e);

			if (e.NewElement == null)
				return;

			if (Control != null)
			{
				Control.ScrollEnabled = false;
			}   
		}
	}
}

