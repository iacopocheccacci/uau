﻿using System;
using Xamarin.Forms;
using CustomRenderer;
using CustomRenderer.iOS;
using Xamarin.Forms.Platform.iOS;
using Foundation;
using UIKit;

[assembly: ExportRenderer (typeof(ItemViewCell), typeof(ItemViewCellRendereriOS))]
namespace CustomRenderer.iOS
{
	public class ItemViewCellRendereriOS : ViewCellRenderer
	{
		static NSString rid = new NSString ("ItemViewCell");

		public override UITableViewCell GetCell (Xamarin.Forms.Cell item, UITableViewCell reusableCell, UITableView tv)
		{
			tv.SeparatorStyle = UITableViewCellSeparatorStyle.None;

			var x = (ItemViewCell)item;
			Console.WriteLine (x);

			ItemViewCelliOS c = reusableCell as ItemViewCelliOS;

			if (c == null)
			{
				c = new ItemViewCelliOS (rid);
			}

			UIImage pi = GetImageFromString(x.Photo);
			UIImage i = GetImageFromString(x.Image);

			c.UpdateCell (x, pi, i);

			return c;
		}

		private UIImage GetImageFromString(string imageString)
		{
			UIImage i = null;
			if (!String.IsNullOrWhiteSpace (imageString))
			{
				string uri = imageString;

				using (var url = new NSUrl (uri))
				{
					using (var data = NSData.FromUrl (url))
					{
						if (data != null)
						{
							i = UIImage.LoadFromData (data);
						}
						else
						{
							i = UIImage.FromFile ("avatar.png");
						}
					}
				}
			}

			return i;
		}
	}
}

