﻿using System;
using System.ComponentModel;
using CustomRenderer.iOS;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(ScrollView), typeof(NoBarScrollViewRendereriOS))]
namespace CustomRenderer.iOS
{
	public class NoBarScrollViewRendereriOS : ScrollViewRenderer
	{
		protected override void OnElementChanged(VisualElementChangedEventArgs e)
		{
			base.OnElementChanged(e);

			if (e.OldElement != null || Element == null)
				return;

			if (e.OldElement != null)
				e.OldElement.PropertyChanged -= OnElementPropertyChanged;

			e.NewElement.PropertyChanged += OnElementPropertyChanged;
		}

		protected void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			ShowsHorizontalScrollIndicator = false;
		}
	}
}

