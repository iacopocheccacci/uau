﻿using System;
using System.Collections.Generic;
using System.Linq;

using Foundation;
using UIKit;
using Facebook.CoreKit;

namespace UAU.iOS
{
	[Register ("AppDelegate")]
	public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
	{
		private UIWindow _window;
		private const string FacebookAppId = "571377753028496";
		private const string FacebookAppName = "SpyGift";

		public override bool FinishedLaunching (UIApplication app, NSDictionary options)
		{
			Rg.Plugins.Popup.IOS.Popup.Init(); // Init Popup

			global::Xamarin.Forms.Forms.Init ();
			LoadApplication (new App ());

			_window = new UIWindow(UIScreen.MainScreen.Bounds);

			Settings.AppID = FacebookAppId;
			Settings.DisplayName = FacebookAppName;

			ApplicationDelegate.SharedInstance.FinishedLaunching (app, options);

			return base.FinishedLaunching (app, options);
		}

		public override bool OpenUrl(UIApplication application, NSUrl url, string sourceApplication, NSObject annotation)
		{
//			base.OpenUrl(application, url, sourceApplication, annotation);
			return ApplicationDelegate.SharedInstance.OpenUrl (application, url, sourceApplication, annotation);
		}

		public override void OnActivated(UIApplication application)
		{
//			base.OnActivated(application);
			AppEvents.ActivateApp ();
		}
	}
}

