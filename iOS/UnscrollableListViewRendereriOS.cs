﻿using System;
using CustomRenderer;
using CustomRenderer.iOS;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly:ExportRenderer(typeof(UnscrollableListView), typeof(UnscrollableListViewRendereriOS))]
namespace CustomRenderer.iOS
{
	public class UnscrollableListViewRendereriOS : ListViewRenderer
	{
		protected override void OnElementChanged(ElementChangedEventArgs<ListView> e)
		{
			base.OnElementChanged(e);

			if (e.NewElement == null)
				return;

			if (Control != null)
			{
				Control.ScrollEnabled = false;
			}   
		}
	}
}

