﻿using System;
using UIKit;
using Foundation;

namespace CustomRenderer.iOS
{
	public class FriendsViewCelliOS : UITableViewCell
	{
		UILabel headingLabel, subheadingLabel;
		UIImageView imageView;
		UISwitch switchView;
		FriendsViewCell cell;

		public FriendsViewCelliOS (NSString cellId) : base (UITableViewCellStyle.Default, cellId)
		{
			SelectionStyle = UITableViewCellSelectionStyle.Default;

			ContentView.BackgroundColor = UIColor.FromRGB (255, 255, 255);

			imageView = new UIImageView ();

			headingLabel = new UILabel () {
				Font = UIFont.FromName ("Lato-Bold", 19f),
				BackgroundColor = UIColor.Clear
			};

			subheadingLabel = new UILabel () {
				Font = UIFont.FromName ("Lato-Regular", 16f),
				BackgroundColor = UIColor.Clear
			};

			switchView = new UISwitch ();
//			switchView.UserInteractionEnabled = false;

			ContentView.Add (headingLabel);
			ContentView.Add (subheadingLabel);
			ContentView.Add (imageView);
			ContentView.Add (switchView);

			switchView.ValueChanged += delegate {
				cell.Switch = switchView.On;
			};
		}

		public void UpdateCell (FriendsViewCell cell, UIImage image)
		{
			this.cell = cell;
			headingLabel.Text = cell.Name;
			subheadingLabel.Text = cell.Details;
			imageView.Image = image;
			switchView.On = cell.Switch;
		}

		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();

			headingLabel.Frame = new CoreGraphics.CGRect (80, 20, ContentView.Bounds.Width - 63, 25);
			subheadingLabel.Frame = new CoreGraphics.CGRect (80, 42, ContentView.Bounds.Width - 63, 20);
			imageView.Frame = new CoreGraphics.CGRect (20, ContentView.Bounds.Height/4, ContentView.Bounds.Height/1.7, ContentView.Bounds.Height/1.7);
			switchView.Frame = new CoreGraphics.CGRect (ContentView.Bounds.Width - 80,ContentView.Bounds.Height/3, 20 , 20);

			CreateCircleImage ();
		}

		private void CreateCircleImage()
		{
			double min = Math.Min(imageView.Frame.Width, imageView.Frame.Height);

			imageView.Layer.CornerRadius = (float)(min / 2.0);
			imageView.Layer.MasksToBounds = false;
			imageView.ClipsToBounds = true;
		}
	}
}

