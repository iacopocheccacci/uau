﻿using System;
using Xamarin.Forms;
using CustomRenderer;
using CustomRenderer.iOS;
using Xamarin.Forms.Platform.iOS;
using Foundation;
using UIKit;
using UAU;

[assembly: ExportRenderer (typeof(PickAGiftViewCell), typeof(PickAGiftViewCellRendereriOS))]
namespace CustomRenderer.iOS
{
	public class PickAGiftViewCellRendereriOS : ViewCellRenderer
	{
		static NSString rid = new NSString ("PickAGiftViewCell");

		public override UITableViewCell GetCell (Xamarin.Forms.Cell item, UITableViewCell reusableCell, UITableView tv)
		{
			var x = (PickAGiftViewCell)item;
			Console.WriteLine (x);

			PickAGiftViewCelliOS c = reusableCell as PickAGiftViewCelliOS;

			if (c == null)
			{
				c = new PickAGiftViewCelliOS (rid);
			}

			UIImage i = null;
			if (!String.IsNullOrWhiteSpace (x.ImageFileName))
			{
				string uri = string.Empty;

				if (!x.ImageFileName.Contains ("https://"))
				{
					uri = Constants.serverUrl;
				}

				uri += x.ImageFileName;

				using (var url = new NSUrl (uri))
				{
					using (var data = NSData.FromUrl (url))
					{
						if (data != null)
						{
							i = UIImage.LoadFromData (data);
						}
						else
						{
							i = UIImage.FromFile ("avatar.png");
						}
					}
				}
			}
			else
			{
				i = UIImage.FromFile ("avatar.png");
			}

			c.UpdateCell (x, i);

			return c;
		}
	}
}

