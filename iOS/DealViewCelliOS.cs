﻿using System;
using UIKit;
using Foundation;

namespace CustomRenderer.iOS
{
	public class DealViewCelliOS : UITableViewCell
	{
		UILabel headingLabel;
		UIView imageBGView;
		UIImageView imageView;
		UILabel priceLabel;
		UIView labelBGView;
		UIImageView storeLogoImageView;

		public DealViewCelliOS (NSString cellId) : base (UITableViewCellStyle.Default, cellId)
		{
			SelectionStyle = UITableViewCellSelectionStyle.Default;

			ContentView.BackgroundColor = UIColor.FromRGB (241, 239, 243);

			imageBGView = new UIView () {
				BackgroundColor = UIColor.FromRGB (255, 255, 255)
			};

			imageView = new UIImageView ();

			headingLabel = new UILabel () {
				Font = UIFont.FromName ("Lato-Regular", 13f),
				BackgroundColor = UIColor.Clear
			};

			labelBGView = new UIView() {
				BackgroundColor = UIColor.FromRGB (59, 89, 152)
			};

			labelBGView.Layer.CornerRadius = 10;

			priceLabel = new UILabel () {
				TextColor = UIColor.White,
				TextAlignment = UITextAlignment.Center
			};

			priceLabel.Lines = 1;
			priceLabel.AdjustsFontSizeToFitWidth = true;
			priceLabel.LineBreakMode = UILineBreakMode.Clip;

			storeLogoImageView = new UIImageView ();

			ContentView.Add (headingLabel);
			ContentView.Add (imageBGView);
			ContentView.Add (labelBGView);
			ContentView.Add (priceLabel);
			ContentView.Add (imageView);
			ContentView.Add (storeLogoImageView);
		}

		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();

			headingLabel.Frame = new CoreGraphics.CGRect (100, 16, ContentView.Bounds.Width - 230, 48);
			imageBGView.Frame = new CoreGraphics.CGRect(0, 0, ContentView.Bounds.Height, ContentView.Bounds.Height);
			priceLabel.Frame = new CoreGraphics.CGRect (ContentView.Bounds.Width - 80, 18, 60, 20);
			labelBGView.Frame = new CoreGraphics.CGRect(ContentView.Bounds.Width - 90, 18, 80, 20);
			imageView.Frame = new CoreGraphics.CGRect (0, 0, ContentView.Bounds.Height, ContentView.Bounds.Height);
			storeLogoImageView.Frame = new CoreGraphics.CGRect (ContentView.Bounds.Width - 90, 50, 80, 20);
		}

		public void UpdateCell (DealViewCell cell, UIImage image, UIImage logo)
		{
			headingLabel.Text = cell.Name;
			priceLabel.Text = cell.Price;
			imageView.Image = image;
			imageView.ContentMode = UIViewContentMode.ScaleAspectFit;
			storeLogoImageView.Image = logo;
			storeLogoImageView.ContentMode = UIViewContentMode.ScaleAspectFit;
		}
	}
}

