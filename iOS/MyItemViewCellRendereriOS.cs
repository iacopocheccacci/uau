﻿using System;
using Xamarin.Forms;
using CustomRenderer;
using CustomRenderer.iOS;
using Xamarin.Forms.Platform.iOS;
using Foundation;
using UIKit;

[assembly: ExportRenderer (typeof(MyItemViewCell), typeof(MyItemViewCellRendereriOS))]
namespace CustomRenderer.iOS
{
	public class MyItemViewCellRendereriOS : ViewCellRenderer
	{
		static NSString rid = new NSString ("MyItemViewCell");

		public override UITableViewCell GetCell (Xamarin.Forms.Cell item, UITableViewCell reusableCell, UITableView tv)
		{
			tv.SeparatorStyle = UITableViewCellSeparatorStyle.None;

			var x = (MyItemViewCell)item;
			Console.WriteLine (x);

			MyItemViewCelliOS c = reusableCell as MyItemViewCelliOS;

			if (c == null)
			{
				c = new MyItemViewCelliOS (rid);
			}

			UIImage i = GetImageFromString(x.Image);

			c.UpdateCell (x, i);

			return c;
		}

		private UIImage GetImageFromString(string imageString)
		{
			UIImage i = null;
			if (!String.IsNullOrWhiteSpace (imageString))
			{
				string uri = imageString;

				using (var url = new NSUrl (uri))
				{
					using (var data = NSData.FromUrl (url))
					{
						if (data != null)
						{
							i = UIImage.LoadFromData (data);
						}
						else
						{
							i = UIImage.FromFile ("avatar.png");
						}
					}
				}
			}

			return i;
		}
	}
}

