using System;
using Xamarin.Forms;
using UAU;
using UAU.iOS;
using Xamarin.Forms.Platform.iOS;
using UIKit;
using System.Diagnostics;
using Foundation;
using CustomRenderer;

[assembly: Xamarin.Forms.Dependency(typeof(ActionSheetPopUpIOS))]
namespace CustomRenderer
{
    class ActionSheetPopUpIOS : IActionSheetPopUp
    {
        public void WhatsappClick()
        {
            var userId = App.CurrentApp.Properties.ContainsKey(Constants.userIdProperty) ? App.CurrentApp.Properties[Constants.userIdProperty] : string.Empty;

            try
            {
                // TODO: create a string var and format it in UTF-8
                string referralUrl = "whatsapp://send?text=Iscriviti a SpyGift!\n" +
									 Constants.serverUrl + "/index.php/user/getReferralLink/" +
                                     userId + "/analyticCode/";

                var uri = new Uri(referralUrl);
                var nsUrl = new NSUrl(uri.GetComponents(UriComponents.HttpRequestUrl, UriFormat.UriEscaped));

                UIApplication.SharedApplication.OpenUrl(nsUrl);

            }
            catch (Exception ex)
            {
                var alertView = new UIAlertView("Error", ex.Message.ToString(), null, "OK", null);
                alertView.Show();
            }
        }

        public void MailClick()
        {

            var userId = App.CurrentApp.Properties.ContainsKey(Constants.userIdProperty) ? App.CurrentApp.Properties[Constants.userIdProperty] : string.Empty;

            try
            {
                // TODO: create a string var and format it in UTF-8
                string referralUrl = "mailto:?&body=Iscriviti%20a%20SpyGift!%20" +
									 Constants.serverUrl + "/index.php/user/getReferralLink/" +
                                     userId + "/analyticCode";

                NSUrl request = new NSUrl(referralUrl);
                UIApplication.SharedApplication.OpenUrl(request);

            }
            catch (Exception ex)
            {
                var alertView = new UIAlertView("Error", ex.Message.ToString(), null, "OK", null);
                alertView.Show();
            }
        }
    }
}