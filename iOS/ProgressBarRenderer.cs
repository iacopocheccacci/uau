﻿using System;
using Xamarin.Forms;
using CustomRenderer.iOS;
using Xamarin.Forms.Platform.iOS;
using UIKit;
using CoreGraphics;

[assembly:ExportRenderer(typeof(ProgressBar), typeof(ProgressBarRendereriOS))]
namespace CustomRenderer.iOS
{
	public class ProgressBarRendereriOS : ProgressBarRenderer
	{
		protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.ProgressBar> e)
		{
			base.OnElementChanged(e);

			Control.ProgressTintColor = UIColor.FromRGB(59, 89, 152);
		}

		public override void LayoutSubviews()
		{
			base.LayoutSubviews ();
			CGAffineTransform transform = CGAffineTransform.MakeScale(1f, 12.0f);
			transform.TransformSize (this.Frame.Size);
			this.Transform = transform; 
		}
	}
}

