﻿using System;
using UIKit;
using Foundation;

namespace CustomRenderer.iOS
{
	public class MyItemViewCelliOS : UITableViewCell
	{
		UILabel headingLabel, subheadingLabel;
		UIImageView imageView;
		UILabel isNewLabel;

		UIImageView separatorImageView;

		public MyItemViewCelliOS (NSString cellId) : base (UITableViewCellStyle.Default, cellId)
		{
			SelectionStyle = UITableViewCellSelectionStyle.Default;

			ContentView.BackgroundColor = UIColor.FromRGB (255, 255, 255);

			imageView = new UIImageView ();

			headingLabel = new UILabel () {
				Font = UIFont.FromName ("Lato-Bold", 16f),
				BackgroundColor = UIColor.Clear
			};

			subheadingLabel = new UILabel () {
				Font = UIFont.FromName ("Lato-Regular", 13f),
				BackgroundColor = UIColor.Clear
			};

			isNewLabel = new UILabel () {
				Font = UIFont.FromName ("Lato-Bold", 13f),
				BackgroundColor = UIColor.FromRGB (59, 89, 152),
				TextColor = UIColor.White,
				TextAlignment = UITextAlignment.Center,
				Text = "new!",
				Hidden = true
			};

			var separatorImage = UIImage.FromFile ("feedSeparatore.png");
			separatorImageView = new UIImageView () {
				Image = separatorImage
			};

			ContentView.Add (imageView);
			ContentView.Add (headingLabel);
			ContentView.Add (subheadingLabel);
			ContentView.Add (isNewLabel);
			ContentView.Add (separatorImageView);
		}

		public void UpdateCell (MyItemViewCell cell, UIImage image)
		{
			headingLabel.Text = cell.Name;
			subheadingLabel.Text = cell.Details;
			imageView.Image = image;

			if (cell.IsNew)
			{
				isNewLabel.Hidden = false;
			}
		}

		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();

			headingLabel.Frame = new CoreGraphics.CGRect (20, 10, ContentView.Bounds.Width - 110, 25);
			subheadingLabel.Frame = new CoreGraphics.CGRect (20, 32, ContentView.Bounds.Width - 110, 20);

			imageView.Frame = new CoreGraphics.CGRect (20, 70, ContentView.Bounds.Width - 40, 170);
			imageView.ContentMode = UIViewContentMode.ScaleAspectFill;
			imageView.ClipsToBounds = true;

			isNewLabel.Frame = new CoreGraphics.CGRect (ContentView.Bounds.Width - 100, 80, 80, 20);

			separatorImageView.Frame = new CoreGraphics.CGRect(0, 270, ContentView.Bounds.Width, 12);
		}
	}
}

