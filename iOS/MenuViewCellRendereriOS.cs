﻿using System;
using CustomRenderer;
using CustomRenderer.iOS;
using Foundation;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer (typeof (MenuViewCell), typeof (MenuViewCellRendereriOS))]
namespace CustomRenderer.iOS
{
	public class MenuViewCellRendereriOS : ViewCellRenderer
	{
		static NSString rid = new NSString ("MenuViewCell");

		public override UITableViewCell GetCell (Xamarin.Forms.Cell item, UITableViewCell reusableCell, UITableView tv)
		{
			tv.SeparatorStyle = UITableViewCellSeparatorStyle.None;

			var x = (MenuViewCell)item;
			Console.WriteLine (x);

			MenuViewCelliOS c = reusableCell as MenuViewCelliOS;

			if (c == null) 
			{
				c = new MenuViewCelliOS (rid);
			}

			UIImage i = UIImage.FromFile (x.Image);

			c.UpdateCell (x, i);

			item.Tapped += (sender, args) =>
			{
				foreach(UITableViewCell cell in tv.VisibleCells)
				{
					if (cell == c)
					{
						((MenuViewCelliOS)cell).SelectCell();
					}
					else
					{
						((MenuViewCelliOS)cell).UnselectCell();
					}
				}
			};

			return c;
		}
	}
}

