﻿using System;
using Xamarin.Forms;
using CustomRenderer.iOS;
using Xamarin.Forms.Platform.iOS;
using Foundation;
using UIKit;
using CustomRenderer;
using UAU;

[assembly: ExportRenderer (typeof(FriendsViewCell), typeof(FriendsViewCellRendereriOS))]
namespace CustomRenderer.iOS
{
	public class FriendsViewCellRendereriOS : ViewCellRenderer
	{
		static NSString rid = new NSString ("FriendsViewCell");

		public override UITableViewCell GetCell (Xamarin.Forms.Cell item, UITableViewCell reusableCell, UITableView tv)
		{
			var x = (FriendsViewCell)item;
			Console.WriteLine (x);

			FriendsViewCelliOS c = reusableCell as FriendsViewCelliOS;

			if (c == null)
			{
				c = new FriendsViewCelliOS (rid);
			}

			UIImage i = null;
			if (!String.IsNullOrWhiteSpace (x.ImageFileName))
			{
				string uri = string.Empty;

				if (!x.ImageFileName.Contains ("https://"))
				{
					uri = Constants.serverUrl;
				}

				uri += x.ImageFileName;

				using (var url = new NSUrl (uri))
				{
					using (var data = NSData.FromUrl (url))
					{
						if (data != null)
						{
							i = UIImage.LoadFromData (data);
						}
						else
						{
							i = UIImage.FromFile ("avatar.png");
						}
					}
				}
			}
			else
			{
				i = UIImage.FromFile ("avatar.png");
			}

			c.UpdateCell (x, i);

			return c;
		}
	}
}
