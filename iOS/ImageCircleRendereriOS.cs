﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using System.Diagnostics;
using System.ComponentModel;
using CustomRenderer;
using CustomRenderer.iOS;

[assembly: ExportRenderer(typeof(ImageCircle), typeof(ImageCircleRendereriOS))]
namespace CustomRenderer.iOS
{
	public class ImageCircleRendereriOS : ImageRenderer
	{
		private void CreateCircle()
		{
			try
			{
				double min = Math.Min(Element.Width, Element.Height);
				Control.Layer.CornerRadius = (float)(min / 2.0);
				Control.ContentMode = UIKit.UIViewContentMode.ScaleAspectFill;
			}
			catch(Exception ex)
			{
				Debug.WriteLine ("Unable to create circle image: " + ex);
			}
		}

		protected override void OnElementChanged(ElementChangedEventArgs<Image> e)
		{
			base.OnElementChanged(e);

			if (e.OldElement != null || Element == null)
				return;

			CreateCircle();
		}

		protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged(sender, e);

			if (e.PropertyName == VisualElement.HeightProperty.PropertyName ||
				e.PropertyName == VisualElement.WidthProperty.PropertyName)
			{
				CreateCircle();
			}
		}
	}
}

