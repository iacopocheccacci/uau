﻿using System;
using UIKit;
using Foundation;

namespace CustomRenderer.iOS
{
	public class PickAGiftViewCelliOS : UITableViewCell
	{
		UILabel headingLabel, subheadingLabel;
		UIImageView imageView;

		public PickAGiftViewCelliOS (NSString cellId) : base (UITableViewCellStyle.Default, cellId)
		{
			SelectionStyle = UITableViewCellSelectionStyle.Default;

			ContentView.BackgroundColor = UIColor.FromRGB (255, 255, 255);

			imageView = new UIImageView ();

			headingLabel = new UILabel () {
				Font = UIFont.FromName ("Lato-Bold", 19f),
				BackgroundColor = UIColor.Clear
			};

			subheadingLabel = new UILabel () {
				Font = UIFont.FromName ("Lato-Regular", 16f),
				BackgroundColor = UIColor.Clear
			};

			ContentView.Add (headingLabel);
			ContentView.Add (subheadingLabel);
			ContentView.Add (imageView);
		}

		public void UpdateCell (PickAGiftViewCell cell, UIImage image)
		{
			headingLabel.Text = cell.Name;
			subheadingLabel.Text = cell.Details;
			imageView.Image = image;
		}

		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();

			headingLabel.Frame = new CoreGraphics.CGRect (100, 20, ContentView.Bounds.Width - 63, 25);
			subheadingLabel.Frame = new CoreGraphics.CGRect (100, 42, ContentView.Bounds.Width - 63, 20);
			imageView.Frame = new CoreGraphics.CGRect (30, ContentView.Bounds.Height/4, ContentView.Bounds.Height/1.7, ContentView.Bounds.Height/1.7);

			CreateCircleImage ();
		}

		private void CreateCircleImage()
		{
			double min = Math.Min(imageView.Frame.Width, imageView.Frame.Height);

			imageView.Layer.CornerRadius = (float)(min / 2.0);
			imageView.Layer.MasksToBounds = false;
			imageView.ClipsToBounds = true;
		}
	}
}

