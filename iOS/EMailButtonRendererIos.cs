﻿using System;
using Xamarin.Forms;
using UAU;
using UAU.iOS;
using Xamarin.Forms.Platform.iOS;
using UIKit;
using System.Diagnostics;
using Foundation;
using CustomRenderer;

[assembly: ExportRenderer(typeof(EMailButton), typeof(EMailButtonRendererIos))]
namespace UAU.iOS
{
	public class EMailButtonRendererIos : ButtonRenderer
	{
		protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.Button> e)
		{
			base.OnElementChanged(e);

			if (Control != null)
			{
				UIButton button = Control;

				button.TouchUpInside += delegate
				{
					HandleEMailClicked();
				};
			}
		}

		private void HandleEMailClicked()
		{
			Debug.WriteLine ("Clicked!");

			var userId = App.CurrentApp.Properties.ContainsKey (Constants.userIdProperty) ? App.CurrentApp.Properties [Constants.userIdProperty] : string.Empty;

			try{
				// TODO: create a string var and format it in UTF-8
				string referralUrl = "mailto:?&body=Iscriviti%20a%20SpyGift!%20" + 
									 Constants.serverUrl + "/index.php/user/getReferralLink/" + 
									 userId + 
									 "/analyticCode";

				NSUrl request = new NSUrl(referralUrl);
				UIApplication.SharedApplication.OpenUrl(request);

			} catch (Exception ex) {
				var alertView = new UIAlertView("Error", ex.Message.ToString(), null, "OK", null);
				alertView.Show();
			}
		}
	}
}

