﻿using System;
using Xamarin.Forms;
using CustomRenderer;
using CustomRenderer.iOS;
using Xamarin.Forms.Platform.iOS;
using Foundation;
using UIKit;

[assembly: ExportRenderer (typeof(ListViewCell), typeof(ListViewCellRendereriOS))]
namespace CustomRenderer.iOS
{
	public class ListViewCellRendereriOS : ViewCellRenderer
	{
		static NSString rid = new NSString ("ListViewCell");

		public override UITableViewCell GetCell (Xamarin.Forms.Cell item, UITableViewCell reusableCell, UITableView tv)
		{
			var x = (ListViewCell)item;
			Console.WriteLine (x);

			ListViewCelliOS c = reusableCell as ListViewCelliOS;

			if (c == null)
			{
				c = new ListViewCelliOS (rid);
			}

			UIImage i = null;
			if (x.Lock)
			{
				i = UIImage.FromFile ("reservedListIcon.png");
			}
			else
			{
				i = UIImage.FromFile ("publicListIcon.png");
			}

			c.UpdateCell (x, i);

			return c;
		}
	}
}

