﻿using System;
using Xamarin.Forms;
using UAU;
using XamarinNativeFacebook.iOS;
using Xamarin.Forms.Platform.iOS;
using UIKit;
using System.Diagnostics;
using Facebook.ShareKit;
using CustomRenderer;

[assembly: ExportRenderer(typeof(FacebookShareButton), typeof(FacebookShareButtonRendererIos))]
namespace XamarinNativeFacebook.iOS
{
	public class FacebookShareButtonRendererIos : ButtonRenderer, ISharingDelegate
	{
		protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.Button> e)
		{
			base.OnElementChanged(e);

			if (Control != null)
			{
				UIButton button = Control;

				button.TouchUpInside += delegate
				{
					HandleFacebookShareClicked();
				};
			}
		}

		private void HandleFacebookShareClicked()
		{
			Debug.WriteLine ("Clicked!");

			Share ();
		}

		private void Share()
		{
			var userId = App.CurrentApp.Properties.ContainsKey (Constants.userIdProperty) ? App.CurrentApp.Properties [Constants.userIdProperty] : string.Empty;
			string link = Constants.serverUrl + "/index.php/user/getReferralLink/" +
			              userId +  "/analyticCode";

			var uri = new Uri (string.Format(link));
			
			ShareLinkContent content = new ShareLinkContent ();
			content.SetContentUrl(uri);
			UIWindow window = UIApplication.SharedApplication.KeyWindow;
			UIViewController rootViewController = window.RootViewController;

			ShareDialog.Show (rootViewController, content, this);
		}

		public void DidComplete (ISharing sharer, Foundation.NSDictionary results)
		{
			Debug.WriteLine ("Share completed");
		}

		public void DidFail (ISharing sharer, Foundation.NSError error)
		{
			Debug.WriteLine ("Share failed");
		}

		public void DidCancel (ISharing sharer)
		{
			Debug.WriteLine ("Share canceled");
		}
	}
}


