﻿using System;
using UIKit;
using Foundation;
using UAU;

namespace CustomRenderer.iOS
{
	public class ItemViewCelliOS : UITableViewCell
	{
		UIImageView profileImageView;
		UILabel headingLabel, subheadingLabel;
		UIImageView imageView;
		UILabel isNewLabel;

		UIImage availableImage;
		UIImage myReservationImage;
		UIImage reservedByOtherImage;
		UIImage joinedCollectImage;
		UIImage activeCollectImage;

		UILabel reservationLabel;
		UILabel footLabel;

		UIImageView footImageView;
		UIImageView separatorImageView;

		public ItemViewCelliOS (NSString cellId) : base (UITableViewCellStyle.Default, cellId)
		{
			SelectionStyle = UITableViewCellSelectionStyle.Default;

			ContentView.BackgroundColor = UIColor.FromRGB (255, 255, 255);

			profileImageView = new UIImageView ();

			imageView = new UIImageView ();

			headingLabel = new UILabel () {
				Font = UIFont.FromName ("Lato-Bold", 16f),
				BackgroundColor = UIColor.Clear
			};

			subheadingLabel = new UILabel () {
				Font = UIFont.FromName ("Lato-Regular", 13f),
				BackgroundColor = UIColor.Clear
			};

			isNewLabel = new UILabel () {
				Font = UIFont.FromName ("Lato-Bold", 13f),
				BackgroundColor = UIColor.FromRGB (59, 89, 152),
				TextColor = UIColor.White,
				TextAlignment = UITextAlignment.Center,
				Text = "new!",
				Hidden = true
			};

			footLabel = new UILabel() {
				Font = UIFont.FromName("Lato-Regular", 13f),
				Hidden = true
			};

			footImageView = new UIImageView {
				Hidden = true
			};

			reservationLabel = new UILabel () {
				
				Font = UIFont.FromName ("Lato-Regular", 13),
				TextAlignment = UITextAlignment.Center,
				Hidden = true
			};
		
			availableImage = UIImage.FromFile("reservedLine.png");
			myReservationImage = UIImage.FromFile ("reservedByMe.png");
			reservedByOtherImage = UIImage.FromFile ("reservedGrey.png");
			activeCollectImage = UIImage.FromFile ("collectOpen.png");
			joinedCollectImage = UIImage.FromFile ("CollectJoined.png");

			var separatorImage = UIImage.FromFile ("feedSeparatore.png");
			separatorImageView = new UIImageView () {
				Image = separatorImage
			};

			ContentView.Add (imageView);
			ContentView.Add (headingLabel);
			ContentView.Add (subheadingLabel);
			ContentView.Add (profileImageView);
			ContentView.Add (isNewLabel);
			ContentView.Add (footLabel);
			ContentView.Add (footImageView);
			ContentView.Add (reservationLabel);
			ContentView.Add (separatorImageView);
		}

		public void UpdateCell (ItemViewCell cell, UIImage profileImage, UIImage image)
		{
			headingLabel.Text = cell.Name;
			subheadingLabel.Text = cell.Details;
			profileImageView.Image = profileImage;
			imageView.Image = image;

			if (cell.IsNew)
			{
				isNewLabel.Hidden = false;
			}

			switch (cell.Reservation)
			{
			case ReservationType.Reservable:
				footLabel.Hidden = false;
				footLabel.Text = Constants.availableItemLabel;
				footLabel.TextColor = UIColor.FromRGB(59, 89, 152);
				footImageView.Hidden = false;
				footImageView.Image = availableImage;
				break;

			case ReservationType.ReservedByMe:
				reservationLabel.Hidden = false;
				reservationLabel.Text = Constants.reservedByMeItemLabel;
				reservationLabel.TextColor = UIColor.White;
				reservationLabel.BackgroundColor = UIColor.FromRGB (59, 89, 152);
				footImageView.Hidden = false;
				footImageView.Image = myReservationImage;
				footLabel.Hidden = false;
				footLabel.Text = Constants.myReservationExpirationLabel + cell.DaysNumber + " " + Constants.daysLabel;
				footLabel.TextColor = UIColor.FromRGB(59, 89, 152);
				break;

			case ReservationType.ReservedByMeCollect:
				footImageView.Hidden = false;
				footImageView.Image = joinedCollectImage;
				footLabel.Hidden = false;
				footLabel.Text = Constants.reservedByMeCollectLabel;
				footLabel.TextColor = UIColor.FromRGB(59, 89, 152);
				break;

			case ReservationType.ActiveCollect:
				footImageView.Hidden = false;
				footImageView.Image = activeCollectImage;
				footLabel.Hidden = false;
				footLabel.Text = Constants.activeCollectLabel;
				footLabel.TextColor = UIColor.FromRGB(59, 89, 152);
				break;

			case ReservationType.ReservedByOther:
				reservationLabel.Hidden = false;
				reservationLabel.Text = Constants.reservedByOtherLabel;
				reservationLabel.TextColor = UIColor.White;
				reservationLabel.BackgroundColor = UIColor.FromRGB (49, 46, 51);
				footLabel.Hidden = false;
				footLabel.Text = Constants.byOtherReservationExpirationLabel + cell.DaysNumber + " " + Constants.daysLabel;
				footLabel.TextColor = UIColor.FromRGB(74, 74, 74);
				footImageView.Hidden = false;
				footImageView.Image = reservedByOtherImage;
				break;

			case ReservationType.MyCollect:
				reservationLabel.Hidden = false;
				reservationLabel.Text = Constants.myCollectLabel;
				reservationLabel.TextColor = UIColor.White;
				reservationLabel.BackgroundColor = UIColor.FromRGB (59, 89, 152);
				footLabel.Hidden = false;
				footLabel.Text = Constants.myCollectExpirationLabel + cell.DaysNumber + " " + Constants.daysLabel;
				footLabel.TextColor = UIColor.FromRGB(59, 89, 152);
				footImageView.Hidden = false;
				footImageView.Image = joinedCollectImage;
				break;
			}
		}

		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();

			profileImageView.Frame = new CoreGraphics.CGRect (20, 10, 50, 50);
			CreateCircleImage (profileImageView);

			headingLabel.Frame = new CoreGraphics.CGRect (80, 10, ContentView.Bounds.Width - 110, 25);
			subheadingLabel.Frame = new CoreGraphics.CGRect (80, 32, ContentView.Bounds.Width - 110, 20);

			imageView.Frame = new CoreGraphics.CGRect (20, 70, ContentView.Bounds.Width - 40, 170);
			imageView.ContentMode = UIViewContentMode.ScaleAspectFill;
			imageView.ClipsToBounds = true;

			isNewLabel.Frame = new CoreGraphics.CGRect (ContentView.Bounds.Width - 100, 80, 80, 20);

			footLabel.Frame = new CoreGraphics.CGRect(55, 264, ContentView.Bounds.Width - 100, 16);
			footImageView.Frame = new CoreGraphics.CGRect(20, 252, 32, 32);

			reservationLabel.Frame = new CoreGraphics.CGRect (20, 136, ContentView.Bounds.Width - 40, 40);

			separatorImageView.Frame = new CoreGraphics.CGRect(0, 295, ContentView.Bounds.Width, 12);
		}

		private void CreateCircleImage(UIImageView iv)
		{
			double min = Math.Min(iv.Frame.Width, iv.Frame.Height);

			iv.Layer.CornerRadius = (float)(min / 2.0);
			iv.Layer.MasksToBounds = false;
			iv.ClipsToBounds = true;
		}
	}
}

