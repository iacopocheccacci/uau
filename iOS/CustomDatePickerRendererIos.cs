﻿using System;
using Xamarin.Forms.Platform.iOS;
using Xamarin.Forms;
using System.ComponentModel;
using UAU;
using UAU.iOS;
using CustomRenderer;

[assembly: ExportRenderer(typeof(CustomDatePicker), typeof(CustomDatePickerRendererIos))]
namespace UAU.iOS
{
	public class CustomDatePickerRendererIos : DatePickerRenderer
	{
		protected override void OnElementChanged(ElementChangedEventArgs<DatePicker> e)
		{
			base.OnElementChanged(e);

			if (this.Control == null) return;

			var customDatePicker = e.NewElement as CustomDatePicker;

			if (customDatePicker != null)
			{
				this.SetValue(customDatePicker);
			}
		}

		protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged(sender, e);

			if (this.Control == null) return;

			var customDatePicker = this.Element as CustomDatePicker;

			if (customDatePicker != null)
			{
				switch (e.PropertyName)
				{
				case CustomDatePicker.NullableDatePropertyName:
					this.SetValue(customDatePicker);
					break;
				case CustomDatePicker.NullTextPropertyName:
					this.SetValue(customDatePicker);
					break;
				}
			}
		}

		private void SetValue(CustomDatePicker customDatePicker)
		{
			if (customDatePicker.NullableDate.HasValue)
			{
				this.Control.Placeholder = customDatePicker.NullText ?? string.Empty;
				this.Control.Text = customDatePicker.NullableDate.Value.ToString(customDatePicker.Format);
			}
			else
			{
				this.Control.Placeholder = customDatePicker.NullText ?? "Compleanno";
				this.Control.Text = customDatePicker.NullText ?? string.Empty;
			}
		}
	}
}

