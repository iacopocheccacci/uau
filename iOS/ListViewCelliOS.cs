﻿using System;
using UIKit;
using Foundation;

namespace CustomRenderer.iOS
{
	public class ListViewCelliOS : UITableViewCell
	{
		UILabel headingLabel, subheadingLabel, daysNumberLabel, daysLabel, expireInLabel;
		UIImageView lockImage;

		public ListViewCelliOS (NSString cellId) : base (UITableViewCellStyle.Default, cellId)
		{
			SelectionStyle = UITableViewCellSelectionStyle.Default;

			ContentView.BackgroundColor = UIColor.FromRGB (255, 255, 255);

			lockImage = new UIImageView ();

			headingLabel = new UILabel () {
				Font = UIFont.FromName ("Lato-Bold", 16f),
				BackgroundColor = UIColor.Clear
			};

			subheadingLabel = new UILabel () {
				Font = UIFont.FromName ("Lato-Regular", 13f),
				BackgroundColor = UIColor.Clear
			};

			expireInLabel = new UILabel () {
				Text="Scade tra",
				Font = UIFont.FromName ("Lato-Regular", 10f),
				BackgroundColor = UIColor.Clear
			};

			daysNumberLabel = new UILabel () {
				Font = UIFont.FromName ("Lato-Bold", 22f),
				BackgroundColor = UIColor.Clear
			};

			daysLabel = new UILabel () {
				Font = UIFont.FromName ("Lato-Regular", 10f),
				BackgroundColor = UIColor.Clear
			};
					
			ContentView.Add (headingLabel);
			ContentView.Add (subheadingLabel);
			ContentView.Add (lockImage);
			ContentView.Add (expireInLabel);
			ContentView.Add (daysNumberLabel);
			ContentView.Add (daysLabel);
		}

		public void UpdateCell (ListViewCell cell, UIImage image)
		{
			headingLabel.Text = cell.Name;
			subheadingLabel.Text = cell.Wishes;
			lockImage.Image = image;
			daysNumberLabel.Text = cell.DaysNumber;
			daysLabel.Text = cell.DaysLabel;
			expireInLabel.Text = cell.ExpireInLabel;
		}

		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();

			headingLabel.Frame = new CoreGraphics.CGRect (100, 20, ContentView.Bounds.Width - 63, 25);
			subheadingLabel.Frame = new CoreGraphics.CGRect (100, 42, ContentView.Bounds.Width - 63, 20);
			lockImage.Frame = new CoreGraphics.CGRect (30, ContentView.Bounds.Height/4, ContentView.Bounds.Height/2, ContentView.Bounds.Height/2);
			expireInLabel.Frame = new CoreGraphics.CGRect (ContentView.Bounds.Width - 110, 10, 100 , 20);
			expireInLabel.TextAlignment = UITextAlignment.Center;
			daysNumberLabel.Frame = new CoreGraphics.CGRect (ContentView.Bounds.Width - 100, 30, 80 , 20);
			daysNumberLabel.TextAlignment = UITextAlignment.Center;
			daysLabel.Frame = new CoreGraphics.CGRect (ContentView.Bounds.Width - 80, 49, 40 , 20);
			daysLabel.TextAlignment = UITextAlignment.Center;
		}
	}
}

