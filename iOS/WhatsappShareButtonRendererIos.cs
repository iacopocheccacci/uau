﻿using System;
using Xamarin.Forms;
using UAU;
using UAU.iOS;
using Xamarin.Forms.Platform.iOS;
using UIKit;
using System.Diagnostics;
using Foundation;
using CustomRenderer;

[assembly: ExportRenderer(typeof(WhatsappShareButton), typeof(WhatsappShareButtonRendererIos))]
namespace UAU.iOS
{
	public class WhatsappShareButtonRendererIos : ButtonRenderer
	{
		protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.Button> e)
		{
			base.OnElementChanged(e);

			if (Control != null)
			{
				UIButton button = Control;

				button.TouchUpInside += delegate
				{
					HandleWhatsappShareClicked();
				};
			}
		}

		private void HandleWhatsappShareClicked()
		{
			Debug.WriteLine ("Clicked!");

			var userId = App.CurrentApp.Properties.ContainsKey (Constants.userIdProperty) ? App.CurrentApp.Properties [Constants.userIdProperty] : string.Empty;

			try{
				// TODO: create a string var and format it in UTF-8
				string referralUrl = "whatsapp://send?text=Iscriviti a SpyGift!\n" + 
									 Constants.serverUrl + "/index.php/user/getReferralLink/" + 
									 userId + "/analyticCode/";

				var uri = new Uri(referralUrl);
				var nsUrl = new NSUrl(uri.GetComponents(UriComponents.HttpRequestUrl, UriFormat.UriEscaped));

				UIApplication.SharedApplication.OpenUrl(nsUrl);

			} catch (Exception ex) {
				var alertView = new UIAlertView("Error", ex.Message.ToString(), null, "OK", null);
				alertView.Show();
			}
		}
	}
}

